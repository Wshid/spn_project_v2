/*
 * 예외 처리를 위해 document.getElementById()를 직접적으로 사용하지 않도록 한다.
 */

function append_element(){ // gene의 존재 여부에 따라 update 혹은 insert를 리턴한다. 
	alert("in append_element");
	
	var ret;
	
	//object가 null인 상태에서 value를 하게 되면 에러가 난다. 이를 방지하기 위한 처리가 필요하다.
	var exp_date = pre_list_encode(load_id("exp_date")),
		gene= pre_list_encode(load_id("gene")),
		position =pre_list_encode(load_id("position")),
		snp= pre_list_encode(load_id("snp")),
		type = pre_list_encode(load_id("type")),
		de_novo = pre_list_encode(load_id("de_novo")),
		mutation= pre_list_encode(load_id("mutation")),
		family_test_f = pre_list_encode(load_id("family_test_f")),
		family_test_m = pre_list_encode(load_id("family_test_m")),
		family_test_s = pre_list_encode(load_id("family_test_s")),
		result = pre_list_encode(load_id("result")),
		reported = pre_list_encode(load_id("reported")),
		comment = pre_list_encode(load_id("comment")),
		idate = pre_list_encode(load_id("idate")),
		iid = pre_list_encode(load_id("iid")),
		ss_no = pre_list_encode(load_id("ss_no")),
		chart_no=document.getElementById("chart_no").value;
	
	// 새로운 Gene인지 판별하여 구성한다. 새로운 행을 추가해야 함 
	var gene_list=pre_list(load_id("gene"));
	var cur_gene=load_item_zero("form_gene");
	alert("cur_gene : "+cur_gene);
	alert(gene);
	console.log("cur_gene : "+cur_gene);
	console.log(gene);
	
	if(gene.indexOf(cur_gene)==-1){ // gene list에 없을 경우, 새로운 행을 생성한다.
		alert("NEW GENE")
		
		ret="insert";
		
		exp_date=load_item_zero("exp_date");
		gene=load_item_zero("form_gene");
		position=load_item_zero("form_position");
		snp=load_item_zero("form_snp");
		type=load_item_zero("form_type");
		de_novo=load_item_zero("form_de_novo");
		mutation=load_item_zero("form_mutation");
		family_test_f=load_item_zero("form_family_test_f");
		family_test_m=load_item_zero("form_family_test_m");
		family_test_s=load_item_zero("form_family_test_s");
		result=load_item_zero("form_result");
		reported=load_item_zero("form_reported");
		comment=load_item_zero("form_comment");
		idate=load_item_zero("form_idate");
		iid=load_item_zero("form_iid");
		ss_no=load_item_zero("form_ss_no");
		
	}else{ // gene list의 있을경우, slash를 사용하여 구현한다.
		alert("ALREADY");
		
		ret="update"
		
		exp_date=exp_date+add_element_slash_str("form_exp_date");
		gene=gene+add_element_slash_str("form_gene");
		position=position+add_element_slash_str("form_position");
		snp=snp+add_element_slash_str("form_snp");
		type=type+add_element_slash_str("form_type");
		de_novo=de_novo+add_element_slash_str("form_de_novo");
		mutation=mutation+add_element_slash_str("form_mutation");
		family_test_f=family_test_f+add_element_slash_str("form_family_test_f");
		family_test_m=family_test_m+add_element_slash_str("form_family_test_m");
		family_test_s=family_test_s+add_element_slash_str("form_family_test_s");
		result=result+add_element_slash_str("form_result");
		reported=reported+add_element_slash_str("form_reported");
		comment=comment+add_element_slash_str("form_comment");
		idate=idate+add_element_slash_str("form_idate");
		iid=iid+add_element_slash_str("form_iid");
		ss_no=ss_no+add_element_slash_str("form_ss_no");	
	}
	
	alert(gene);
	
	
	
	save_id("exp_date", exp_date);
	save_id("gene", gene);
	save_id("position", position);
	save_id("snp", snp);
	save_id("type", type);
	save_id("de_novo", de_novo);
	save_id("mutation", mutation);
	save_id("family_test_f", family_test_f);
	save_id("family_test_m", family_test_m);
	save_id("family_test_s",family_test_s);
	save_id("result", result);
	save_id("reported", reported);
	save_id("comment", comment);
	save_id("idate", idate);
	save_id("iid", iid);
	save_id("ss_no", ss_no);
	
	return ret;
	
	
	
}

function pre_list(str){ // seq.js와 동일한 내용 
	console.log("str : "+str);
	if(str==null){
		return "";
	}else{
		var src = str.split('[')[1].split(']')[0]; //단순히 [] 제거
		var splited=src.split(","); // 리스트 형태의 값
		var slashed=splited.map(x => x.split("/").map(y => (y=="=")?"":y.trim()));
		return slashed;
	}

}

function reform_empty(str){ // seq.js와 동일한 내용 
	if(str==null || str==undefined || str=="null") return "";
	else return str;
}

function pre_list_encode(str){
	if(is_empty_null(str)) return "";
	else return src = str.split('[')[1].split(']')[0]; //단순히 [] 제거

}


function add_element_slash_str(src, var_name){ // src+var_name
	var slash_str="";
	var cur=document.getElementsByName(var_name);
	
	if(is_empty_null(src)) slash_str="/";
	
	if(is_empty_null(cur)) return slash_str+"=";
	else return slash_str+load_item_zero(cur);
	//return "/"+reform_empty_equal(document.getElementsByName(var_name).item(0).value);
}


//////////////
function is_empty_null(obj){
	if(!obj || obj.length==0) return true;
	else false;
}

function load_id(var_id){
	var cur=document.getElementById(var_id);
	if(is_empty_null(cur)) return "";
	else return cur.value;
}

function save_id(var_id, new_value){
	var cur=document.getElementById(var_id);
	if(is_empty_null(cur)) return "";
	else return cur.value=new_value;
}

function reform_empty_equal(str){
	if(is_empty_null(str)) return "=";
	else return str;
}

function load_item_zero(str){
	var cur = document.getElementsByName(str);
	if(is_empty_null(cur)) return "";
	else return cur.item(0).value;
}



