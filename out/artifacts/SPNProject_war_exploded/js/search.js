function search_result(search_text, src_data){

	$(function(){
		var $container=$(".search_container");
		//var json_data=$.parseJSON(src_data);
		var json_data=src_data;
		var hits=json_data["hits"];
		//console.log(data);
		//console.log(json_data);
		//console.log(json.length);
		//console.log("hits : " + hits[0]);
		console.log(hits["total"]);

		var block_string="";
		block_string+=
						'<form name="search_link_form" action="">'+
								'<strong> SearchText : '+search_text+'</strong><br />'+
								'<strong> Count : '+hits["total"]+'</strong><hr />'+
								'<input type="hidden" name="cmd" value="" />'+
								'<input type="hidden" name="searchVal" value="" />';

		if(hits["total"]>0){
			$.each(hits["hits"], (index, data) => {
				var result_data=data["_source"];
				var _id=data["_id"]; // 삭제를 위한 해당 id 값을 추출 하도록 
				//console.log(data["_source"]); // javascript 데이터 추출 완
				block_string+=make_block_string(result_data, _id);
			});

		}else{
			console.log("Search Result is not exists");
		}

		block_string+="</form>";
		$container.html($(block_string));

		box_activing();

	});
	console.log("IN SEARCH RESULT");
}


// 결과 데이터 json 배열을 받아 div로 감싸진 block들을 반환하는 함
function make_block_string(result_data, _id){
	//console.log(result_data);
	var chart_no=result_data["chart_no"];
	var menu=result_data["menu"];
	var block_string="<div class=search_block onclick='searchMoveLink(\""+chart_no+"\",\""+set_property_by_menu(menu)+"\")'>";
	block_string+=`<strong> chart no : ${result_data['chart_no']}</strong><br/>`;
	block_string+=`<strong> menu : ${result_data['menu']}</strong><hr />`;
	block_string+='<table class="table table-bordered table-striped table-hover">';
	//block_string+=("id : "+_id+"<br />");
	
	//console.log("SEARCH.js result_data.key");
	//console.log(result_data["chart_no"]);
	var idx=0;
	$.each(result_data, (key, value)=>{ // 한 개의 객체 내부에서 key,value 값 출력
		//console.log(key + " : " + value); // key, value 값 추출 완료
		//console.log(item);
		if(idx>=5) return false; // $.each 구문 탈출 방법 
		
		if(!is_empty_null(value)){
			block_string+=`
				<tr>
					<td><strong>${key}</strong></td>
					<td>${value}</td>
				</tr>
				`;
			//block_string+="<input type='hidden' name='"+key+"' value='"+value+"' />";
			idx++;
		}
		
		
	});
	block_string+="</table></div>";

	return block_string;
}

function box_activing(){
	$(".search_block").each(function(){
		 $(this).on('mouseover', function(){
			 $(this).addClass('box_active');
		 })
		 .on('mouseleave', function(){
				 $(this).removeClass('box_active');
		 });
	 });
}

function set_property_by_menu(menu){
	switch(menu){
		case "info":
			return "infoform";
			break;
		case "lab_sanger":
			return "labsangerform";
			break;
		case "lab_ngs":
			return "labngsform";
			break;
		case "lab_cgh":
			return "labcghform"
			break;
		case "lab_stain":
			return "labstainform";
			break;
		case "sample_dna":
			return "samplednaform";
			break;
		case "sample_tissue":
			return "sampletissueform";
			break;
		case "sample_cell":
			return "samplecellform";
			break;
		case "sample_cfdna":
			return "samplecfdnaform"
			break;
		case "sample_serum":
			return "sampleserumform";
			break;
		case "sample_rna":
			return "samplernaform";
			break;
		default:
			alert("NOT FIND MENU : set_property_by_menu function");
			break;
	}
}
