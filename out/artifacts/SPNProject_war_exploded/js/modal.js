//$("#myModal").modal('show');

function show_modal(page_name, event_name, close_modal_name){ // 해당 모달을 보여주는 함수
	$(function(){
		var open_modal="#modal_"+page_name+"_"+event_name;
		if(close_modal_name===null || close_modal_name===""){
			console.log("#modal_"+page_name+"_"+event_name);
			//$("#modal_"+page_name+"_"+event_name).modal('show'); // 매우 간단한데..
			$(open_modal).modal(); // 매우 간단한데..	
		}else{
			var close_modal_id="#"+close_modal_name;
			$(close_modal_id).modal('toggle');
			setTimeout(function(){
				$(open_modal).modal(); // 1000ms 뒤에 open_modal을 open 한다
				
				setTimeout(function(){
					$(open_modal).modal('toggle');
					$(close_modal_id).modal('toggle');
				},1500);
			},500);
		}
		
		
		var close_modal_id='#'+close_modal_name;
	});
}

function make_modal_yn(page_name, event_name, title, comment, container){
	console.log("make_modal id : modal_"+page_name+"_"+event_name);
	var modalHTML='<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_'+page_name+'_'+event_name+'" id="modal_'+page_name+'_'+event_name+'">'+
						'<div class="modal-dialog" role="document">'+
							'<div class="modal-content">'+
								'<div class="modal-header">'+
									'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
									'<h3 class="modal-title" id="modal_'+page_name+'_'+event_name+'_label">'+title+'</h3>'+
								'</div>'+
								'<div class="modal-body">'+
									'<p>'+comment+'</p>'+
								'</div>'+
								'<div class="modal-footer">'+
								'<button type="button" class="btn btn-primary button_reform" style="float: right;" onclick="return true">Submit</button>'+
					        		'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
					        	'</div>'+
					        '</div>'+
					     '</div>'+
				'</div>';
	$(function(){
		//console.log("container : "+container);
		$(container).append(modalHTML);
		//console.log("occur error");
	});
	
//'</form>';
}

function make_modal(page_name, event_name, title, comment, container){
	console.log("make_modal id : modal_"+page_name+"_"+event_name);
	var modalHTML='<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_'+page_name+'_'+event_name+'" id="modal_'+page_name+'_'+event_name+'">'+
						'<div class="modal-dialog" role="document">'+
							'<div class="modal-content">'+
								'<div class="modal-header">'+
									'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
									'<h3 class="modal-title" id="modal_'+page_name+'_'+event_name+'_label">'+title+'</h3>'+
								'</div>'+
								'<div class="modal-body">'+
									'<p>'+comment+'</p>'+
								'</div>'+
								'<div class="modal-footer">'+
					        		//'<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'+
					        	'</div>'+
					        '</div>'+
					     '</div>'+
				'</div>';
	$(function(){
		//console.log("container : "+container);
		$(container).append(modalHTML);
		//console.log("occur error");
	});
	
//'</form>';
}

function make_modal_form(page_name, event_name, title, comment, container){
	console.log("make_modal id : modal_input_"+page_name+"_"+event_name);
	
	var modalHTML=
		`
			<form class="spn_input_form" method="post" action="SPN?cmd=labsanger" name="sangerinput" id="sangerinput">
				<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_${page_name}_${event_name}" id="modal_${page_name}_${event_name}">
					<div class="modal-dialog" role="document">
						<div class="modal-content" style="width:120%">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h3 class="modal-title" id="modal_${page_name}_${event_name}_label">${title}</h3>
							</div>
							<div class="modal-body-form">
								<p> ${comment} </p>
								<table class="table table-bordered">
									<tr>
										<td><strong>Exp. date<font color="red">*</font></strong></td>								
										<td>
										<input type="date" name="form_exp_date" class="form_var"
											style="border: transparent; background-color: transparent;"
											></td>
										<td><strong>Gene</strong><font color="red">*</font></td>
										<td><input type="text" name="form_gene" class="form_var"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
											></td>
									</tr>
									<tr>
										<td><strong>Position</strong></td>
										<td><input type="text" name="form_position" class="form_var"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
											></td>
										<td><strong>Snp</strong></td>
										<td><input type="text" name="form_snp" class="form_var"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
											></td>
									</tr>
									<tr>
										<td><strong>Type<font color="red">*</font></strong></td>
										<td><select name="form_type" style="color:black">
											<option value="">select
											<option value="Hetero">Hetero
											<option value="Homo">Homo
											<option value="X-linked">X-linked
										</select></td>
										<td><strong>De novo<font color="red">*</font></strong></td>
										<td><input type="radio" name="form_de_novo"  class="form_var" value="Y">Yes&nbsp;
											<input type="radio" name="form_de_novo"  class="form_var" value="N">No</td>
									</tr>
									<tr>
										<td><strong>Mutation</strong></td>
										<td><input type="text" name="form_mutation"  class="form_var"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
											></td>
					
										<td><strong>Family Test</strong></td>
										<td><input type="checkbox" name="form_family_test_f" class="form_var" value="Y">Father&nbsp;
											<input type="checkbox" name="form_family_test_m" class="form_var" value="Y">Mother&nbsp;
											<input type="checkbox" name="form_family_test_s" class="form_var" value="Y">Sibling</td>
									</tr>
									<tr>
										<td><strong>Reported</strong></td>
										<td colspan="4"><input type="text" name="form_reported" class="form_var"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
											></td>
									</tr>
									<tr>
										<td><strong>Result</strong></td>
										<td colspan="4"><input type="text" name="form_result" class="form_var"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
											></td>
									</tr>
									<tr>
										<td><strong>Comment</strong></td>
										<td colspan="4"><input type="text" name="form_comment" class="form_var"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
											></td>
									</tr>
									<input type="hidden" name="form_chart_no" value="${chart_no.value}" />
									<input type="hidden" name="form_iid" value="${cur_iid.value}" />
							</table>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="return checkIt('lab_sanger')">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</form>
		`; // ss_no가 과연 필요할지? -> InsertSanger에서 필요 없는것으로 판단됨
		
		
		
		
	
	$(function(){
		//console.log("container : "+container);
		$(container).append(modalHTML);
		//console.log("occur error");
	});
	
//'</form>';
}

/*
 * target : 닫을 모달 이름
 * message : body 출력 메세지
 * link_modal : 연결할 modal 이름
 */
function modal_success(target, message){
	
}
/*
 * target : 닫을 모달 이름
 * message : 
 */
function modal_fail(target, message){
	
}

function modal_success(close_modal_name){ // Autobio modal_success 코드
    if(close_modal_name===null || close_modal_name===""){ // 같은 null이어도 string으로 받는경우가 있음
        var open_modal='#modal_success';
        $(function(){
            setTimeout(function(){
                $(open_modal).modal('show'); // 1000ms 뒤에 open_modal을 open 한다
            },500);
            
            $('#button_close_refresh').on('click',function(){
                location.reload(true); // 여기서 리로드
                // 여기서 실행시키는 부분은 DELETE나 ADDBLOCK시의 창임
            });
        }); 
    }else{
        var close_modal_id='#'+close_modal_name, // 닫을 Modal
            open_modal='#modal_success' // 새로 띄울 Modal
        $(function(){
           $(close_modal_id).modal('toggle');
            setTimeout(function(){
                $(open_modal).modal('show'); // 1000ms 뒤에 open_modal을 open 한다
            },500);
            
            $('#button_close_refresh').on('click',function(){
                location.reload(); //리로드
            });
        }); 
    }


}