$(function(){
	var $container=$(".stat_container"),
		$info=$(".info_container"),
		$lab=$(".lab_container"),
		$sample=$(".sample_container"),
		$stat_block=$(".stat_block"),
		$menu=$("#menu"),
		$form=$("#stat_link_form");
	var info="", lab="", sample="";
	
	var menu_count=$.parseJSON(document.getElementById("menu_count").value);
	console.log(menu_count);
	
	new numberCounter("info", menu_count["info"]);
	
	new numberCounter("lab_sanger", menu_count["lab_sanger"]);
	new numberCounter("lab_ngs", menu_count["lab_ngs"]);
	new numberCounter("lab_cgh", menu_count["lab_cgh"]);
	new numberCounter("lab_stain", menu_count["lab_stain"]);
	
	new numberCounter("sample_dna", menu_count["sample_dna"]);
	new numberCounter("sample_tissue", menu_count["sample_tissue"]);
	new numberCounter("sample_cell", menu_count["sample_cell"]);
	new numberCounter("sample_cfdna", menu_count["sample_cfdna"]);
	new numberCounter("sample_serum", menu_count["sample_serum"]);
	new numberCounter("sample_rna", menu_count["sample_rna"]);
	
	$stat_block.each(function(){
		$(this).on('click', function(){
			console.log("clicked : "+$(this).find('.menu_body').attr('id'));
			$menu.val($(this).find('.menu_body').attr('id'));
			$form.submit();
			
		});
	});
	
	
	box_activing();
	
	function box_activing(){
		$(".stat_block").each(function(){
			 $(this).on('mouseover', function(){
				 $(this).addClass('box_active');
			 })
			 .on('mouseleave', function(){
					 $(this).removeClass('box_active');
			 });
		 });
	}
	
	
});