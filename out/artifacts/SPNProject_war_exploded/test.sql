
create table mysql.test (
id varchar(20) primary key,
pw varchar(20),
rname varchar(20)
);

create table mysql.testinfo(
rname varchar(20),
spnn int(20),
sex varchar(20),
birth date,
categorize varchar(20),
doctor varchar(20),
site varchar(20),
final_diagnosis varchar(20),
clinical_diagnosis varchar(20),
symptom varchar(20),
remark varchar(20)
)

drop table mysql.test;
drop table mysql.testinfo;
drop table mysql.info;

/* info */
create table mysql.info(
snupn char(11) primary key,
chartno int(8) not null,
rname varchar(20) not null,
sex char(1) not null,
birth date,
doctor char(3),
site varchar(20),
category varchar(8),
final_diagnosis varchar(20),
clinical_diagnosis varchar(20),
symptom varchar(20),
remark varchar(20)
);
desc mysql.info;

select * from mysql.info;
alter table mysql.info add idate timestamp default current_timestamp

insert into mysql.info(snupn,chartno,rname,sex,birth) 
values ('test1111','1111','test1111','F','2015-12-29');

/* member */
create table spn.member(
id varchar(20),
pw varchar(20),
name varchar(20),
position varchar(10),
auth varchar(10),
idate date
);
desc spn.member;

insert into spn.member values ('test_id', 'test_pw', 'test_name', 'test_pos', 'test_auth', now());

insert into mysql.member values ('TestUser', 'test', 'test', 'test', 'test', now());
insert into mysql.member values ('datetest', 'datetest', 'datetest', 'datetest', 'datetest', now());
delete from mysql.member where id='test';

/*컬럼수정*/
alter table mysql.info modify column birth varchar(10);
/*컬럼추가*/
alter table mysql.member add idate timestamp default current_timestamp not null
/*컬럼삭제*/
alter table mysql.info drop column idate

select * from mysql.testinfo;
select * from mysql.member;


show databases;
