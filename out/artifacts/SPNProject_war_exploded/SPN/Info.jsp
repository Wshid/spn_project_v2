
<%@page import="spn.dto.HistoryDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="spn.dao.HistoryDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css">
<link rel="stylesheet" type="text/css" href="./css/file.css">
<link rel="stylesheet" type="text/css" href="./css/modal.css?ver=4">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>

<title>SPN General Info</title>

<script type="text/javascript">
function updateSubmitForm()
{	
	form=document.getElementById('infoinput');
    
   	if(checkIt('info')){
   		form.action='SPN?cmd=infoupdate';
   		//console.log('is in');
   	}
   	else
   		return false;
    form.submit();
}
</script>

</head>
<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9"
				style="min-width: 700px; overflow-x: auto;">
				<h3>General Info</h3>

				<!--  -->
				<%
					HistoryDAO dao=HistoryDAO.getHistoryDAO();
					List<HistoryDTO> his_li=new ArrayList<HistoryDTO>();
					
					String menu="info";
					String searchVal=(String)session.getAttribute("searchVal");
					
					his_li=(List)dao.selectHistory(searchVal, menu);
					System.out.println("History List");
					System.out.println(his_li);
					System.out.println(his_li.size());
					if(his_li.size()!=0){
					%>
				<form name="historyForm" action="SPN?cmd=historyform"
					target="Window" method="post">
					<input type="hidden" name="page_name" value="info" />
					<input class="btn btn-default" style="float: right;" type="button"
						value="History" onClick="popup_post4();"> <input
						type="hidden" value="111" name="hidden">
				</form>
						<%
					}
					
				%>
				<br> <br> <br>
				
				<form method="post" action="SPN?cmd=info" name="infoinput"
					id="infoinput" onSubmit="return checkIt('info')">
					
					<font color="#0191D0" style="float: right;">${iid} &nbsp;|&nbsp; ${idate} </font>
					<table class="table table-bordered table-striped table-hover">
						<tr>
							<td><strong>SNUPN<font color="red">*</font></strong></td>
							<td><input type="text" name="snupn" value="${snupn}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><strong>Chart No<font color="red">*</font></strong></td>
							<!-- <form action="SPN?cmd=confirmchartno" target="Window" method="post"> -->
							<td><input type="text" name="chart_no" value="${chart_no}"
								style="border: transparent; background-color: transparent; width: 50%; height: 100%;">
								<!-- 중복 확인 숨김 -->
								<!-- <input type="button" name="confirm_chart_no" value="중복확인"
								onClick="openConfirmChartNo();"></td> -->
							</td>
							<!-- </form>-->
						</tr>
						<tr>
							<td><strong>Name<font color="red">*</font></strong></td>
							<td><input type="text" name="name" value="${name}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><strong>Sex<font color="red">*</font></strong></td>
							<td><input type="radio" name="sex" value="M">Male&nbsp;
								<input type="radio" name="sex" value="F">Female</td>

						</tr>
						<tr>
							<td><strong>Date of Birth</strong></td>
							<td><input type="text" name="birth" value="${birth}"
								style="border: transparent; background-color: transparent;"></td>
							<td><strong>Doctor</strong></td>

							<td><select name="doctor">
									<option value="">select</option>
									<option value="김기중">김기중</option>
									<option value="채종희">채종희</option>
									<option value="임병찬">임병찬</option>
							</select></td>
						</tr>
						<tr>
							<!-- 
							<td><strong>refered DR.</strong></td>
							<td><input type="text" name="refered_dr"
								value="${refered_dr}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><strong>분류</strong></td>
							<td><select name="category" value="${category}">
									<option value="">select</option>
									<option value="Muscle">Muscle</option>
									<option value="Epilepsy">Epilepsy</option>
									<option value="Others">Others</option>
							</select></td>
							 -->
							<td><strong>Other Site</strong></td>
							<td colspan="4"><input type="text" name="other_site"
								value="${other_site}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<!-- 
						<tr>
							<td><strong>Final Diagnosis</strong></td>
							<td colspan="4"><input type="text" name="final_diagnosis"
								value="${final_diagnosis}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						 -->
						<tr>
							<td><strong>Clinical Diagnosis</strong></td>
							<td colspan="4"><input type="text" name="clinical_diagnosis"
								value="${clinical_diagnosis}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Symptom</strong></td>
							<td colspan="4"><input type="text" name="symptom"
								value="${symptom}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<!-- 
						<tr>
							<td><strong>Remark</strong></td>
							<td colspan="4"><input type="text" name="remark"
								value="${remark}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						 -->
						
						<!--  input hidden 위치 -->
					</table>
					
					<input type="hidden" name="gi_no" value="${gi_no}" />
					<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />

					
					
					
					<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
					<%
						String chart_no=(String)session.getAttribute("searchVal");
						if(chart_no==null || chart_no.equals("")){
					%>
							<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
							<%
						}else{
							%>
							<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="return updateSubmitForm()">Change</button>
										
							<%
						}
					%>
					
					

				</form>

			
			<!--  -->
				<%
				String real_path="";
				String path=request.getAttribute("path").toString();
				System.out.println("path : "+path);
				
				if((path==null) || path.length()==0){
					
				%>
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_info_upload">Image Upload</button>					
				<%
				}else{
					//String path=request.getAttribute("path").toString();
					int split_idx=path.indexOf("db_image"); // db_image로 해주어야 정상 참조 가능
					real_path=path.substring(split_idx, path.length());
					System.out.println(real_path);
				%>
				<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#modal_view_image">View Image</button>
				<%
				}
				%>
				
				
				<form action="SPN?cmd=dbimageprocess" method="post" enctype="multipart/form-data">					
					<input type="hidden" name="img_idx" value="${img_idx}" />
					<input type="hidden" name="menu" value="info"/><br />
					<input type="hidden" id="subtitle" name="subtitle" value="${next_idx}" /><br />					
					<div id="modal_info_upload" class="modal fade" role="dialog">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button btn-primary" class="close" data-dismiss="modal">&times;</button>
					        <h4 class="modal-title">Image Upload</h4>
					      </div>
					      <div class="modal-body">
					         <p>

								<div class="filebox">
									<button class="replace btn-primary">Upload</button>  
									<input type="file" name="file" value="파일 업로드" class="upload"> <!--  name도 file로 해야함 -->
									<!-- <input type="file" name="file"/><br /> -->
								</div>
								
					         </p>
					      </div>
					      <div class="modal-footer">
					      	<button type="submit" class="btn btn-primary button_reform">Submit</button>
					        <button type="button" class="btn btn-default burron_reform" data-dismiss="modal">Close</button>
					      </div>
					    </div>
					
					  </div>
					</div>
				</form>	
				

		
		
		<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_view_image_label" id="modal_view_image">
   			<div class="modal-dialog" role="document">
   				<form class="dbimage_lab_sanger" name="dbimage_info" action="SPN?cmd=dbimagedelete" method="post">
					<input type="hidden" name="delete_file_name" value="${file_name}"/>
					<input type="hidden" name="delete_page_name" value="${page_name}"/>
   						<div class="modal-content">
   							<div class="modal-header">
   								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
   								<h3 class="modal-title" id="modal_'+i+'_sanger_label">${file_name}</h3>
   							</div>
   							<div class="modal-body">
   								<img src="<%= real_path %>" width=100% height=100% />
   								<a id="image_load" class="selectDatabaseAllButton" href="<%= real_path %>" download>Download</a>
   							</div>
   							<div class="modal-footer">
   	                    		<!-- <button type="submit" class="btn btn-primary button_reform">Submit</button> -->
   	                    		<button type="submit" class="btn btn-danger button_reform">Delete</button>
   	                    		<button type="button" class="btn btn-default lab_sanger_modal_close" data-dismiss="modal">Close</button>
   	                    	</div>
   	                    </div>
   	                </form>
   	                 </div>
   	             </div>
			
			
			<!--  -->
					
			</div>
			
			
			<!-- /.col-xs-12 main -->
		</div>
		<!--/.container-->
	</div>

	<div class="modal_container font_shadow"> <!-- modal 관련 내용이 들어갈 부분 -->
	</div>

	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
						<font size=2><p>
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
								| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL 
							</p></font>
					</center>
				</div>
			</div>
		</div>


		<!-- script -->
		
		<jsp:include page="Script.jsp" flush="false" />
		<script src="//code.jquery.com/jquery-latest.min.js"></script> <!--  jqeury last version -->
		<script src="./js/bootstrap.min.js"></script>
		<script src="js/modal.js?ver=6"></script> <!--  modal 관련 자바스크립트 함수 -->
		<script src="js/input_check.js?ver=2"></script>
		<script src="js/modal_script.js?ver=2"></script>
		<script type="text/javascript">
			function initSet() {
				//debugger; // 일단은 bloking을 하기로
				modal_script('info'); // 새로 추가되는 구문
				
				var select1 = document.getElementsByName("doctor").item(0);
				for (var i = 0; i < select1.options.length; i++) {
					if (select1.options[i].value == "${doctor}") {
						select1.selectedIndex = i;
					}
				}
				
				var radioBtn = document.getElementsByName("sex");
				for (var i = 0; i < radioBtn.length; i++) {
					if (radioBtn.item(i).value == "${sex}") {
						radioBtn.item(i).checked = true;
					}
				}
				
				if("${NotFound}"=="1"){ // 못찾았다면, 
					show_modal("info", "not_found");
				}
				
				console.log("Duplicate Chart : ${DuplicateChart}");
				if("${DuplicateChart}"=="1"){
					show_modal("info", "duplicate_chart");
				}

			}
		</script>
		
		<!-- 입력에 따른 modal 객체 생성 -->
		<!-- 
		<script language="JavaScript">
			//$(function(){
				make_modal("info","snupn","SNUPN", "SNUPN을 확인해주세요", ".modal_container");
				//console.log("already modal");
				make_modal("info","name","NAME","name을 확인해주세요", ".modal_container");
				make_modal("info","chart_no","CHART_NO","chart_no를 확인해주세요", ".modal_container");
				make_modal("info","sex","GENDER","성별을 확인해주세요", ".modal_container");
			//});
		</script>
 -->
		<!--chart no 중복 여부 판단-->
		<script>
			function openConfirmChartNo() {
				//chart id 입력했는지 검사
				if (!document.infoinput.chart_no.value) {
					alert("중복확인에러 : Chart No를 입력하세요");
					return;
				} else {

					window
							.open(
									"",
									"Window",
									"toolbar=no, location=no, status=no, menubar=no, scrollbar=no, resizable=no, width=310, height=180");
					document.confirmchartno.submit();
				}
			}
		</script>

		<script language="javascript">
			function popup_post1() {

				window
						.open("", "Window",
								"width=450, height=600, menubar=no,status=yes,scrollbars=no");
				document.patientlistForm.submit();

			}
		</script>
</body>

</html>
