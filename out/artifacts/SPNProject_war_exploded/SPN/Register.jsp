<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<title>Insert title here</title>
</head>
<body>

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="container" class="container">

		<div class="modal-dialog">
			<h3>
				<b><font color="#446E9B">User Add</font></b>
			</h3>
			<a href="SPN?cmd=memberlist" class="btn btn-link"
				style="float: right;">User List</a>

			<div class="well bs-component">

				<form class="form-horizontal" method="post"
					action="SPN?cmd=register" name="register" id="register"
					onSubmit="return checkIt()">
					<fieldset>
						<legend></legend>
						<div class="form-group">
							<label for="id" class="col-lg-2 control-label">Email<font
								color="red">*</font></label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="id"
									placeholder="Input Email	ex) test@snuh.org">
							</div>
						</div>
						<div class="form-group">
							<label for="pw" class="col-lg-2 control-label">Password<font
								color="red">*</font></label>
							<div class="col-lg-10">
								<input type="password" class="form-control" name="pw" id="pw"
									placeholder="Input Password">
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-lg-2 control-label">이름<font
								color="red">*</font></label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="name" id="name"
									placeholder="Input Name">
							</div>
						</div>
						<div class="form-group">
							<label for="affiliation" class="col-lg-2 control-label">소속</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="affiliation"
									id="affiliation" placeholder="Input Affiliation	ex) 소아신경과">
							</div>
						</div>
						<div class="form-group">
							<label for="phone" class="col-lg-2 control-label">연락처<font
								color="red">*</font></label>
							<div class="col-lg-10">
								<input type="text" class="form-control" name="phone" id="phone"
									placeholder="Input Phone Number">
							</div>
						</div>

						<div class="form-group">
							<label class="col-lg-2 control-label">권한<font color="red">*</font></label>
							<div class="col-lg-10">
								<div class="radio">
									<label> <input type="radio" name="permission"
										id="permission1" value="Admin" checked=""> Admin
									</label>
								</div>
								<div class="radio">
									<label> <input type="radio" name="permission"
										id="permission2" value="Fellow"> Fellow
									</label>
								</div>
								<div class="radio">
									<label> <input type="radio" name="permission"
										id="permission3" value="Lab"> Lab
									</label>
								</div>
								<div class="radio">
									<label> <input type="radio" name="permission"
										id="permission4" value="Nurse"> Nurse
									</label>
								</div>
							</div>
						</div>

						<!-- <div class="form-group">
								<label for="textArea" class="col-lg-2 control-label">Textarea</label>
								<div class="col-lg-10">
									<textarea class="form-control" rows="3" id="textArea"></textarea>
									<span class="help-block">A longer block of help text
										that breaks onto a new line and may extend beyond one line.</span>
								</div>
							</div>
							<div class="form-group">
								<label for="select" class="col-lg-2 control-label">Selects</label>
								<div class="col-lg-10">
									<select class="form-control" id="select">
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select> <br> <select multiple="" class="form-control">
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>
							</div> -->

						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="reset" class="btn btn-default"
									style="float: right;">Cancel</button>
								<button type="submit" class="btn btn-primary"
									style="float: right;">Submit</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>

		<div id="footer">
			<div class="row">
				<div class="col-lg-12">
					<div class="page-header">
						<br> <br>
						<center>
							<font size=2><p>
									서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
									| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
									HOSPITAL
								</p></font>
						</center>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!-- /.container -->

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />

	<!--입력값 체크-->
	<script language="JavaScript">
		function checkIt() {
			if (!document.register.id.value) {
				alert("Email을 입력하세요.");
				return false;
			}
			if (!document.register.pw.value) {
				alert("Password을 입력하세요.");
				return false;
			}
			if (!document.register.name.value) {
				alert("이름을 입력하세요.");
				return false;
			}
			if (!document.register.phone.value) {
				alert("연락처를 입력하세요.");
				return false;
			}

		}
	</script>




</body>
</html>