<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<title>SPN Urine</title>

<script type="text/javascript">
function updateSubmitForm()
{	
	form=document.getElementById('urineinput');
    form.action='SPN?cmd=sampleurineupdate';
    form.submit();
}
</script>

</head>
<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- info sub -->
			<jsp:include page="InfoSub.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9 table_right">
				<h3>Urine</h3>

				<!-- <button type="submit" class="btn btn-default" style="float: right;">History</button> -->
				<form name="historyForm" action="SPN?cmd=historyform"
					target="Window" method="post">
					<input type="hidden" name="page_name" value="sample_urine" />
					<input class="btn btn-default" style="float: right;" type="button"
						value="History" onClick="popup_post4();"> <input
						type="hidden" value="111" name="hidden">
				</form>			
				<br> <br> <br>

				<form method="post" action="SPN?cmd=sampleurine" name="urineinput" id="urineinput">
				<font color="#0191D0" style="float: right;">${iid} &nbsp;|&nbsp; ${idate} </font><br>
					<table class="table table-bordered table-striped table-hover"
						style="width: 35%; float: right;">
						<tr>
							<td><strong>Request Date</strong></td>
							<td><input type="date" name="req_date" value="${req_date}"
								style="border: transparent; background-color: transparent;"></td>
						</tr>
						<tr>
							<td><strong>Distribution Type</strong></td>
							<td><select name="req_distribution">
									<option value="">select
									<option value="Urine Cell">Urine Cell
									<option value="UiPSC">UiPSC
							</select></td>
						</tr>
						<tr>
							<td><strong>Order Name</strong></td>
							<td><input type="text" name="req_order_name"
								value="${req_order_name}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Number of Vial</strong></td>
							<td><input type="text" name="req_vial" value="${req_vial}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Comment</strong></td>
							<td><input type="text" name="req_comment"
								value="${req_comment}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
					</table>

					<table class="table table-bordered table-striped table-hover"
						style="width: 32%; float: right;">
						<tr>
							<td><strong>UiPSC</strong></td>
							<td><input type="checkbox" name="uipsc" value="UiPSC"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Collection Date</strong></td>
							<td><input type="date" name="uipsc_col_date"
								value="${uipsc_col_date}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Index No</strong></td>
							<td><input type="text" name="uipsc_index"
								value="${uipsc_index}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Storage</strong></td>
							<td><input type="text" name="uipsc_storage"
								value="${uipsc_storage}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Location</strong></td>
							<td><input type="text" name="uipsc_location"
								value="${uipsc_location}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Comment</strong></td>
							<td><input type="text" name="uipsc_comment"
								value="${uipsc_comment}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
					</table>

					<table class="table table-bordered table-striped table-hover"
						style="width: 32%; float: right;">
						<tr>
							<td><strong>Urine Cell</strong></td>
							<td><input type="checkbox" name="urine" value="Urine Cell"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Collection Date</strong></td>
							<td><input type="date" name="urine_col_date"
								value="${urine_col_date}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Index No</strong></td>
							<td><input type="text" name="urine_index"
								value="${urine_index}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Volume(ml)</strong></td>
							<td><input type="text" name="urine_volume"
								value="${urine_volume}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Storage</strong></td>
							<td><input type="text" name="urine_storage"
								value="${urine_storage}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Location</strong></td>
							<td><input type="text" name="urine_location"
								value="${urine_location}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Comment</strong></td>
							<td><input type="text" name="urine_comment"
								value="${urine_comment}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
					</table>
					<input type="hidden" name="chart_no" value="${chart_no}" />
					<input type="hidden" name="uri_no" value="${uri_no}" />
					<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
					<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
					<!-- <button type="submit" class="btn btn-primary" style="float: right;" onclick="updateSubmitForm()">Change</button> -->
					<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button> -->
					<%
						System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
						String check=(String)request.getAttribute("uri_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.
						//String searchSangerNumber=(String)session.getAttribute("iid"); // null
						/* Enumeration으로 Session 체크, searchval, pw, id 밖에 없었음
						Enumeration<String> attr=session.getAttributeNames();

						while(attr.hasMoreElements()){
							System.out.println(attr.nextElement());
						}*/
						
						//System.out.println("SS_NO in SEss : "+searchSangerNumber);
						if(check==null || check.equals("")){
						//if(ss_no==0){
					%>
							<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
							<%
						}else{
							%>
							<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm()">Change</button>
										
							<%
						}
					%>
					
					
				</form>
			</div>
			<!-- /.col-xs-12 main -->
		</div>
		<!--/.container-->
	</div>

	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
						<font size=2><p>
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
								| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL 
							</p></font>
					</center>
				</div>
			</div>
		</div>
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />

	<script language="JavaScript">
		function initSet() {
			var select1 = document.getElementsByName("req_distribution").item(0);
			for (var i = 0; i < select1.options.length; i++) {
				if (select1.options[i].value == "${req_distribution}") {
					select1.selectedIndex = i;
				}
			}

			var checkBox1 = document.getElementsByName("urine");
			if ("${urine}" == "Urine Cell") {
				checkBox1.item(0).checked = true;
			}

			var checkBox2 = document.getElementsByName("uipsc");
			if ("${uipsc}" == "UiPSC") {
				checkBox2.item(0).checked = true;
			}

		}
	</script>

</body>
</html>