<%@page import="java.util.ArrayList"%>
<%@page import="spn.dto.HistoryDTO"%>
<%@page import="java.util.List"%>
<%@page import="spn.dao.HistoryDAO"%>
<%@page import="java.util.Enumeration"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/new_set.css?ver=3">
<link rel="stylesheet" type="text/css" href="./css/file.css?ver=33534">
<link rel="stylesheet" type="text/css" href="./css/modal.css?ver=2">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<title>SPN Sanger Sequencing</title>

<script type="text/javascript">
/*
	function updateSubmitForm()
	{
		form=document.getElementById('sangerinput');
	    //form.action='SPN?cmd=labsangerupdate';
	    if(checkIt('lab_sanger')) form.action='SPN?cmd=labsangerupdate';
	    else return false;

	    form.submit();
	}
	*/
	function updateSubmitForm() // modal 관련 구문으로 대체됨 
	{
		//form=document.getElementById('dnainput');
		//var forms=document.getElementsByName('sangerinput');
		var form=document.sangerinput;
	    //for(i=0 ; i<forms.length;i++){
	   
	    	if(checkIt('lab_sanger')){ // update 전환은 하지 않는다.
	    		console.log("LabSnager.jsp, updateSubmitform(), checkIt inner");
	   		//if(append_element=="update") form.action='SPN?cmd=labsangerupdate';
	   	}
	   	else
	   		return false;
	  
	    	
	    form.submit();
	}
	    
	function deleteSubmitForm(form_idx){ // delete 버턴에 따라 실제값이 있는지 체크한 후, 값을 넘긴다.
		
		var forms=document.getElementsByName('sangerinput_ori');
		var form=forms[form_idx];
		form.action='SPN?cmd=labsangerdelete';
		//form.iid.value
		form.iid.value=document.getElementById("cur_iid").value; // 현재 사용자를 history에서 사용하기 위해 사용 
		if(!form.exp_date.value) return false;
		else form.submit();
	}
</script>

</head>
<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- info sub -->
			<jsp:include page="InfoSub.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9 table_right">
			<div class="main-header-area">
				<h3>Sanger Sequencing</h3>



				
				<%
					HistoryDAO dao=HistoryDAO.getHistoryDAO();
					List<HistoryDTO> his_li=new ArrayList<HistoryDTO>();
					
					String menu="lab_sanger";
					String searchVal=(String)session.getAttribute("searchVal");
					
					his_li=(List)dao.selectHistory(searchVal, menu);
					System.out.println("History List");
					System.out.println(his_li);
					System.out.println(his_li.size());
					if(his_li.size()!=0){
					%>
						<form name="historyForm" action="SPN?cmd=historyform"
					target="Window" method="post">
					<input type="hidden" name="page_name" value="lab_sanger" />
					<input class="btn btn-default" style="float: right;" type="button"
						value="History" onClick="popup_post4();"> <input
						type="hidden" value="111" name="hidden">
				</form>
						<%
					}
					
				%>
				<br /><br /><br />

				<!--  js에서 사용할 변수를 초기화 한다. -->
				<input type="hidden" id="exp_date" value="${exp_date}" />
				<input type="hidden" id="gene" value="${gene}" />
				<input type="hidden" id="position" value="${position}" />
				<input type="hidden" id="snp" value="${snp}" />
				<input type="hidden" id="type" value="${type}" />
				<input type="hidden" id="de_novo" value="${de_novo}" />
				<input type="hidden" id="mutation" value="${mutation}" />
				<input type="hidden" id="family_test_f" value="${family_test_F}" />
				<input type="hidden" id="family_test_m" value="${family_test_M}" />
				<input type="hidden" id="family_test_s" value="${family_test_S}" />
				<input type="hidden" id="result" value="${result}" />
				<input type="hidden" id="reported" value="${reported}" />
				<input type="hidden" id="comment" value="${comment}" />
				<input type="hidden" id="idate" value="${idate}" />
				<input type="hidden" id="iid" value="${iid}" />
				<input type="hidden" id="ss_no" value="${ss_no}" />
				<input type="hidden" id="chart_no" value="${chart_no}"  />
				<input type="hidden" id="iid" name="iid" value="${iid}" />
				<input type="hidden" id="cur_iid" name="cur_iid" value="<%=session.getAttribute("id")%>" />

				<!--  spn_input_form insert 부분 -->
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal_lab_sanger_spn_input_form">
				 	Insert New Data
				</button>
			
			</div>
								
				<div class="seq_container"> <!-- js로 추가되는 부분 -->

				</div>

				<!--  탭으로 추가될` 부분 -->
				<!-- 
				<ul class="nav nav-tabs">
				  <li class="active"><a data-toggle="tab" href="#gene">GENE 1</a></li>
				  <li><a data-toggle="tab" href="#gene2">GENE 2</a></li>
				  <li><a data-toggle="tab" href="#gene3">GENE 3</a></li>
				</ul>

				<div class="tab-content">
				  <div id="gene1" class="tab-pane fade in active">
				  <div id="gene2" class="tab-pane fade"></div>
				  <div id="gene3" class="tab-pane fade"></div>
				</div>
				 -->

				<!-- enctype을 활용하여 이미지 업로드 기능을 가능하게 하도록 한다. -->




			<div class="picture_grid_container"> <!-- 인풋되는 사진을 기준으로 그리드 형태로 보여준다. --><!--  해당 사진을 클릭하였을 때 확대하여 보여질 수 있도록 한다. -->
				<h3> Pictures</h3>
				<button id="${page_name}_button_modal" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal_${page_name}_input">Image Upload</button>
<!--
				<div class="row">
					<div class="col-sm-3">
					A
					</div>
					<div class="col-sm-3">
					B
					</div>
					<div class="col-sm-3">
					C
					</div>
				</div>
	  -->
	  			<hr> <!--  선긋기 -->
				<input type="hidden" class="db_image" name="db_image" value="${db_image}" />
				<input type="hidden" class="names" name="names" value="${names}" />
				<input type="hidden" class="doctors" name="doctors" value="${doctors}" />
				<input type="hidden" class="page_name" name="page_name" value="${page_name}" />



				<form action="SPN?cmd=dbimageprocess" method="post" enctype="multipart/form-data">
					<input type="hidden" name="img_idx" value="${img_idx}" />
					<input type="hidden" name="menu" value="lab_sanger"/><br />
					<input type="hidden" id="subtitle" name="subtitle" value="${next_idx}" /><br />
					<div id="modal_${page_name}_input" class="modal fade" role="dialog">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button btn-primary" class="close" data-dismiss="modal">&times;</button>
					        <h4 class="modal-title">Image Upload</h4>
					      </div>
					      <div class="modal-body">
					         <p>

								<div class="filebox">
									<button class="replace btn-primary">Upload</button>
									<input type="file" name="file" value="파일 업로드" class="upload"> <!--  name도 file로 해야함 -->
									<!-- <input type="file" name="file"/><br /> -->
								</div>

					         </p>
					      </div>
					      <div class="modal-footer">
					      	<button type="submit" class="btn btn-primary button_reform">Submit</button>
					        <button type="button" class="btn btn-default burron_reform" data-dismiss="modal">Close</button>
					      </div>
					    </div>

					  </div>
					</div>
				</form>

			</div>

			</div>
			<!-- /.col-xs-12 main -->


		</div>
		<!--/.container-->
	</div>

	<!-- modal 관련 내용이 들어갈 부분 -->
	<div class="modal_container font_shadow"> </div>

	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
						<font size=2><p>
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
								| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL
							</p></font>
					</center>
				</div>
			</div>
		</div>
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />
	<script src="//code.jquery.com/jquery-latest.min.js"></script> <!--  jqeury last version -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/prev_str.js?ver=1"></script>
	<script src="js/modal.js?ver=12"></script> <!--  modal 관련 자바스크립트 함수 -->
	<script src="js/input_check.js?ver=9"></script>
	<script src="js/modal_script.js?ver=8"></script>
	<script src="js/picture_grid.js?ver=725"></script>
	<script src="js/seq.js?ver=129"></script>
	<script src="js/seq_encode.js?ver=4"></script>

	<!--입력값 체크-->
	<script language="JavaScript">

	</script>

	<script language="JavaScript">
		function initSet() {
			var menu="lab_sanger";
			modal_script(menu);
			check_chart(menu);
/*
			var select1 = document.getElementsByName("type").item(0);
			for (var i = 0; i < select1.options.length; i++) {
				if (select1.options[i].value == "${type[0]}") {
					select1.selectedIndex = i;
				}
			}

			var radioBtn = document.getElementsByName("de_novo");
			for (var i = 0; i < radioBtn.length; i++) {
				if (radioBtn.item(i).value == "${de_novo[0]}") {
					radioBtn.item(i).checked = true;
				}
			}

			var checkBox1 = document.getElementsByName("family_test_f");
			if ("${family_test_F[0]}" == "Y") {
				checkBox1.item(0).checked = true;
			}

			var checkBox2 = document.getElementsByName("family_test_m");
			if ("${family_test_M[0]}" == "Y") {
				checkBox2.item(0).checked = true;
			}

			var checkBox3 = document.getElementsByName("family_test_s");
			if ("${family_test_S[0]}" == "Y") {
				checkBox3.item(0).checked = true;
			}
*/

		}
	</script>
</body>
</html>
