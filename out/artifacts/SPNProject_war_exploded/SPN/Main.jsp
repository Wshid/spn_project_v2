<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html><!-- PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> --> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/columns_s4.css?ver=2">
<link rel="stylesheet" type="text/css" href="css/carousel.css?ver=3">
<link rel="stylesheet" type="text/css" href="css/new_set.css?ver=4">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<title>SPN Main</title>
</head>
<body>

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
		      <!-- Indicators -->
		      <ol class="carousel-indicators">
		        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		        <li data-target="#myCarousel" data-slide-to="1"></li>
		        <li data-target="#myCarousel" data-slide-to="2"></li> 
		      </ol>
		      <div class="carousel-inner" role="listbox">
		        <div class="item active">
		          <img class="first-slide" src="images/bg_login_2.jpg" alt="First slide">
		          <div class="container">
		            <div class="carousel-caption">
		              <h1></h1>
		              <p class="carouselBody"></p>
		              <p><a class="btn btn-lg btn-primary" href="login.php" role="button">Login</a></p>
		            </div>
		          </div>
		        </div>
		        <div class="item">
		          <img class="second-slide" src="images/bg_login_2.jpg" alt="Second slide">
		          <div class="container">
		            <div class="carousel-caption">
		              <h1></h1>
		              <p class="carouselBody"></p>
		              <p><a class="btn btn-lg btn-primary" href="block.php" role="button">Go to Blocks</a></p>
		            </div>
		          </div>
		        </div>
		        <div class="item">
		          <img class="third-slide" src="images/bg_login_2.jpg" alt="Third slide">
		          <div class="container">
		            <div class="carousel-caption">
		              <h1></h1>
		              <p class="carouselBody"></p>
		              <p><a class="btn btn-lg btn-primary" href="chart.php" role="button">Move to Chart</a></p>
		            </div>
		          </div>
		        </div>
		      </div>
		      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
		        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		        <span class="sr-only">Previous</span>
		      </a>
		      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
		        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		        <span class="sr-only">Next</span>
		      </a>
		</div><!-- /.carousel -->
		
		
		<div class="container columns_s4">
			<form id="main_link_form" name="main_link_form" action="SPN?cmd=searchform" method="POST">
				<input type="hidden" name="prev_page" value="main" />
				<input type="hidden" id="menu" name="menu"/> <!--  id : javascript, name : java -->
				<h3> Common</h3>
				<p>차트의 기본 정보 및 데이터 통계자료를 확인할 수 있습니다.</p>
				<hr />
			      <div class="row" id="block_direction">
			        <div class="col-sm-4 main_block">
			        	  <div class="menu_body" id="info"></div>
			            <img class="img-circle" src="images/bg_login.jpg" alt="Generic placeholder image" width="140" height="140">
			            <h2><!--menu[i][0]--></h2>
			            <p class="marketingBody"><!-- jquery --></p>
			            <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
			          <!-- </a>  -->
			        </div><!-- /.col-sm-4 -->
			        <div class="col-sm-4">
			          <a href="SPN?cmd=statisticform" style="color:black; text-decoration:none;"> <!-- Statistic만 a태그를 사용한다.  -->
			            <img class="img-circle" src="images/bg_login.jpg" alt="Generic placeholder image" width="140" height="140">
			            <h2><!--menu[i][0]--></h2>
			            <p class="marketingBody"><!-- jquery --></p>
			            <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
			            </a>
			        </div><!-- /.col-sm-4 -->
			      </div><!-- /.row -->
			      
			    <h3> Laboratory Test</h3>
			    <p> Laboratory에 대한 데이터를 확인할 수 있습니다.</p>
				<hr />
			      
			      <div class="row" id="block_direction">
			        <div class="col-sm-4 main_block">
			          <div class="menu_body" id="lab_sanger"></div>
			            <img class="img-circle" src="images/bg_login.jpg" alt="Generic placeholder image" width="140" height="140">
			            <h2><!--menu[i][0]--></h2>
			            <p class="marketingBody"><!-- jquery --></p>
			            <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
			        </div><!-- /.col-sm-4 -->
			        <div class="col-sm-4 main_block">
			          <div class="menu_body" id="lab_cgh"></div>
			            <img class="img-circle" src="images/bg_login.jpg" alt="Generic placeholder image" width="140" height="140">
			            <h2></h2>
			            <p class="marketingBody"></p>
			            <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
			        </div><!-- /.col-sm-4 -->
			        <div class="col-sm-4 main_block">
			          <div class="menu_body" id="lab_ngs"></div>
			            <img class="img-circle" src="images/bg_login.jpg" alt="Generic placeholder image" width="140" height="140">
			            <h2></h2>
			            <p class="marketingBody"></p>
			            <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
			        </div><!-- /.col-sm-4 -->
			     	</div>
			     	<div class="row" id="block_direction">
			        <div class="col-sm-4 main_block">
			          <div class="menu_body" id="lab_stain"></div>
			            <img class="img-circle" src="images/bg_login.jpg" alt="Generic placeholder image" width="140" height="140">
			            <h2></h2>
			            <p class="marketingBody"></p>
			            <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
			        </div><!-- /.col-sm-4 -->
			      </div><!-- /.row -->
			      
			      <h3> Sample Bank</h3>
			      <p>Sample에 관련한 6가지 항목을 확인할 수 있습니다.</p>
					<hr />
			      
			      <div class="row" id="block_direction">
			        <div class="col-sm-4 main_block">
			          <div class="menu_body" id="sample_dna"></div>
			            <img class="img-circle" src="images/bg_login.jpg" alt="Generic placeholder image" width="140" height="140">
			            <h2><!--menu[i][0]--></h2>
			            <p class="marketingBody"><!-- jquery --></p>
			            <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
			        </div><!-- /.col-sm-4 -->
			        <div class="col-sm-4 main_block">
			          <div class="menu_body" id="sample_tissue"></div>
			            <img class="img-circle" src="images/bg_login.jpg" alt="Generic placeholder image" width="140" height="140">
			            <h2></h2>
			            <p class="marketingBody"></p>
			            <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
			        </div><!-- /.col-sm-4 -->
			        <div class="col-sm-4 main_block">
			          <div class="menu_body" id="sample_cell"></div>
			            <img class="img-circle" src="images/bg_login.jpg" alt="Generic placeholder image" width="140" height="140">
			            <h2></h2>
			            <p class="marketingBody"></p>
			            <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
			        </div><!-- /.col-sm-4 -->
			        </div>
			     	<div class="row" id="block_direction">
			        <div class="col-sm-4 main_block">
			        <div class="menu_body" id="sample_cfdna"></div>
			            <img class="img-circle" src="images/bg_login.jpg" alt="Generic placeholder image" width="140" height="140">
			            <h2></h2>
			            <p class="marketingBody"></p>
			            <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
			        </div><!-- /.col-sm-4 -->
			        <div class="col-sm-4 main_block">
			          <div class="menu_body" id="sample_serum"></div>
			            <img class="img-circle" src="images/bg_login.jpg" alt="Generic placeholder image" width="140" height="140">
			            <h2></h2>
			            <p class="marketingBody"></p>
			            <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
			        </div><!-- /.col-sm-4 -->
			        <div class="col-sm-4 main_block">
			          <div class="menu_body" id="sample_rna"></div>
			            <img class="img-circle" src="images/bg_login.jpg" alt="Generic placeholder image" width="140" height="140">
			            <h2></h2>
			            <p class="marketingBody"></p>
			            <!--<p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>-->
			        </div><!-- /.col-sm-4 -->
			      </div><!-- /.row -->
		      </form>
	    </div>
	</div>


	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
					<p> <!--  p 태그와 font 태그의 위치 수정 -->
						<font size=2>
							
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
								| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL 
							</font>
							</p>
					</center>
				</div>
			</div>
		</div>
	</div>


	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />
	<script src="js/prev_str.js?ver=3"></script>
	<script src="js/carousel.js?ver=5"></script>
	<script src="js/main.js?ver=3"></script>

</body>
</html>