<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css">
<link rel="stylesheet" type="text/css" href="./css/modal.css?">
<link rel="stylesheet" type="text/css" href="./css/stat.css?ver=3">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<title>SPN Statistic</title>

</head>

<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />


			<!-- main area -->
			<div class="col-xs-12 col-sm-9 table_right">
				<h3>SPN Statistic</h3>
				
				<input type="hidden" id="menu_count" value='${menu_count}' />
				<div class="stat_container"> <!--  stat의 main_container -->
					<form id="stat_link_form" name="stat_link_form" action="SPN?cmd=searchform" method="POST">
						<input type="hidden" name="prev_page" value="statistic" />
						<input type="hidden" id="menu" name="menu"/> <!--  id : javascript, name : java -->
						<div class="info_container"> </div>
							<h3>Info</h3>
							<p>입력된 사용자의 차트 수를 표시합니다. </p>
							<hr />
							<div class="row">
								<div class="col-sm-4 stat_block"">
									<div class="menu_title">Info</div>
									<div class="menu_body" id="info"></div>
								</div>
							</div>
						<div class="lab_container"> </div>
							<h3>Laboratory Test</h3>
							<hr />
							<div class="row">
								<div class="col-sm-3 stat_block">
									<div class="menu_title">Sanger Sequencing</div>
									<div class="menu_body" id="lab_sanger"></div>
								</div>
								<div class="col-sm-3 stat_block">
									<div class="menu_title">Next Generation Sequencing</div>
									<div class="menu_body" id="lab_ngs"></div>
								</div>
								<div class="col-sm-3 stat_block">
									<div class="menu_title">CGH array</div>
									<div class="menu_body" id="lab_cgh"></div>
								</div>
								<div class="col-sm-3 stat_block">
									<div class="menu_title">Stain Muscle & Skin</div>
									<div class="menu_body" id="lab_stain"></div>
								</div>
							</div>
						<div class="sample_container">
							<h3>Sample Bank</h3>
							<hr />
							<div class="row">
								<div class="col-sm-4 stat_block">
									<div class="menu_title">DNA</div>
									<div class="menu_body" id="sample_dna"></div>
								</div>
								<div class="col-sm-4 stat_block">
									<div class="menu_title">Tissue</div>
									<div class="menu_body" id="sample_tissue"></div>
								</div>
								<div class="col-sm-4 stat_block">
									<div class="menu_title">Cell</div>
									<div class="menu_body" id="sample_cell"></div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-sm-4 stat_block">
									<div class="menu_title">Cell Free DNA</div>
									<div class="menu_body" id="sample_cfdna"></div>
								</div>
								<div class="col-sm-4 stat_block">
									<div class="menu_title">Serum & CSF</div>
									<div class="menu_body" id="sample_serum"></div>
								</div>
								<div class="col-sm-4 stat_block">
									<div class="menu_title">RNA</div>
									<div class="menu_body" id="sample_rna"></div>
								</div>
							</div>
							
						</div>
					</form>
				</div>
			</div>
		</div>
		

		
		<div class="modal_container font_shadow"> </div>
		
		<div id="footer">
			<div class="row">
				<div class="col-lg-12">
					<div class="page-header">
						<br> <br>
						<center>
							<font size=2><p>
									서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
									| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
									HOSPITAL
								</p></font>
						</center>
					</div>
				</div>
			</div>
		</div>

		<!-- script -->
		<jsp:include page="Script.jsp" flush="false" />
    	<script src="//code.jquery.com/jquery-latest.min.js"></script> <!--  jqeury last version -->
	    <script src="./js/bootstrap.min.js"></script>
	    <script src="js/prev_str.js?ver=1"></script>
	    <script src="js/modal.js?ver=4"></script> <!--  modal 관련 자바스크립트 함수 -->
		<script src="js/input_check.js?ver=1"></script>
		<script src="js/modal_script.js?ver=1"></script>
		<script src="js/fiddle.js?ver=3"></script>
		<script src="js/stat.js?ver=4"></script>

		<script language="JavaScript">
			function initSet() {
				console.log("initset");

			}
		</script>

</body>
</html>
