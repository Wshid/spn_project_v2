<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page import="spn.dao.MemberDAO"%>
<%@ page import="spn.dto.MemberDTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<title>User List</title>
</head>
<body>

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="contatiner">
			<div style="padding-right: 10%; padding-left: 10%;">

				<h3>
					<b><font color="#446E9B">User list</font></b>
				</h3>
				<%
					List<MemberDTO> list = (List<MemberDTO>) request.getAttribute("list");
				%>


				<a href="SPN?cmd=registerform" class="btn btn-link"
					style="float: right;">Back to User Add</a>


				<form method="post" action="SPN?cmd=memberdelete" name="memberlist"
					id="memberlist">
					<table class="table table-bordered table-striped table-hover">
						<tr>
							<td style="width: 4%;"><strong>#</strong></td>
							<td style="width: 20%;"><strong>Email</strong>
							<td style="width: 10%;"><strong>이름</strong></td>
							<td style="width: 15%;"><strong>소속</strong></td>
							<td style="width: 15%;"><strong>연락처</strong></td>
							<td style="width: 7%;"><strong>권한</strong></td>
							<td style="width: 19%;"><strong>생성일자</strong></td>
							<td style="width: 10%;"><strong></strong></td>
						</tr>

						<%
							for (int i = 0; i < list.size(); i++) {
								MemberDTO m = list.get(i);
						%>

						<tr>
							<td><%=i + 1%></td>
							<td><%=m.getId()%></td>
							<td><%=m.getName()%></td>
							<td><%=m.getAffiliation()%></td>
							<td><%=m.getPhone()%></td>
							<td><%=m.getPermission()%>
							<td><%=m.getRegdate()%>
							<td><button type="submit" class="btn btn-default"
									name="mb_no" id="mb_no" value="<%=m.getMb_no()%>">
									Delete</button></td>
							<%-- <td><a href="Matrix?cmd=listdetailform" class="btn btn-link" name="study_no" value="<%=l.getStudy_no()%>">Select</a></td> --%>

							<%
								}
							%>
						
					</table>
				</form>
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
						<font size=2><p>
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
								| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL
							</p></font>
					</center>
				</div>
			</div>
		</div>
	</div>

		<!-- script -->
		<jsp:include page="Script.jsp" flush="false" />
</body>
</html>