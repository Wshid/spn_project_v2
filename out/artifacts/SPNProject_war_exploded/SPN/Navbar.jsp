<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html><!-- PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> --> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css?ver=1">	


</head>
<body>
<!-- navbar -->
<nav class="navbar navbar-default">
<div class="container">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed"
			data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>

		<a href="SPN?cmd=mainform"><img src="./images/logo.jpg"></a>
	</div>

	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav">
		<!-- 
			<form class="navbar-form navbar-left" role="search"
				action="SPN?cmd=searchinfo" method="POST"> -->
				
			<form class="navbar-form navbar-left" role="search"
				action="SPN?cmd=searchform" method="POST">
				<div class="form-group">
					<input type="text" class="form-control"
						placeholder="Search" name="searchVal"
						value="">
						<!-- value="session.getAttribute("searchVal")">  -->

				</div>
				<button type="submit" class="btn btn-primary">Search</button>
				<a type="button" class="btn btn-success" href="SPN?cmd=infoform">ADD Chart</a>
				
				
			</form>

			<!-- Patient List pop up -->
			<!-- <form class="navbar-form navbar-left" name="patientlistForm"
				action="SPN?cmd=patientlistform" target="Window" method="post">
				<input class="btn btn-transparent" style="float: right;"
					type="button" value="Patient List" onClick="popup_post1();">
				<input type="hidden" value="111" name="hidden">
			</form> -->
<!--  Order List 제거 
			<li class="dropdown"><a href="#" class="dropdown-toggle"
				data-toggle="dropdown" role="button" aria-expanded="false">Order
					<span class="caret"></span>
			</a>
				<ul class="dropdown-menu" role="menu">
				<li>
					<form class="navbar-form navbar-left" name="orderlistForm"
						action="SPN?cmd=orderlistform" target="Window" method="post">
						<input class="btn btn-transparent" style="float: right;"
							type="button" value="Order List" onClick="popup_post2();">
						<input type="hidden" value="111" name="hidden">
					</form></li>
					<li>
					<form class="navbar-form navbar-left" name="orderaddForm"
						action="SPN?cmd=orderaddform" target="Window" method="post">
						<input class="btn btn-transparent" style="float: right;"
							type="button" value="Order List Add" onClick="popup_post3();">
						<input type="hidden" value="111" name="hidden">
					</form></li>
				</ul></li>
-->
		</ul>
		<ul class="nav navbar-nav ">
		<form class="navbar-form navbar-left" action="SPN?cmd=infoform" method="POST">
					<input type="hidden" name="clear" value="true" />
					<button type="submit" class="btn btn-info">CLEAR Chart</button>
		</form>
		</ul>

		<ul class="nav navbar-nav navbar-right">
			<%
				String perm=session.getAttribute("perm").toString();
				System.out.println(perm);
				if(perm.equals("Admin")){
			%>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown" role="button" aria-expanded="false"><%=session.getAttribute("id")%><span
						class="caret"></span></a>
					<ul class="dropdown-menu" role="menu"> <!--  Admin만 접근할 수 있도록 변경해야함 -->
						<li><a href="SPN?cmd=registerform">User Add</a></li>
						<li><a href="SPN?cmd=memberlist">User List</a></li>
					</ul>
				</li>
			<%
				}else{
					System.out.println("Not Admin");
				}
			%>
			
			<li style="white-space: nowrap;"><a href="SPN?cmd=logout"
				method="post"><strong>Logout</strong></a></li>
		</ul>
	</div>
</nav>
<!-- <script src="../js/bootstrap.min.js"></script>-->
</body>
</html>