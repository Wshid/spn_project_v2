<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<title>SPN CSF</title>

<!--navbar-->
<jsp:include page="Navbar.jsp" flush="false" />

</head>
<body>
	<!--main page-->
	<div class="page-container">
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- info -->
			<jsp:include page="InfoSub.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9">
				<h3>CSF</h3>
				<form>
					<button type="submit" class="btn btn-default" style="float: right;">History</button>
					<br> <br> <br>

					<table class="table table-bordered table-striped table-hover">
						<tr>
							<td><strong>Collection Date</strong></td>
							<td><input type="date"
								style="border: transparent; background-color: transparent;"></td>
						</tr>
						<tr>
							<td><strong>Distribution Date</strong></td>
							<td><input type="date"
								style="border: transparent; background-color: transparent;"></td>
						</tr>
						<tr>
							<td><strong>CSF No</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Storage</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Serum No</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Storage</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Aquaporin 4 Ab</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Autoimmune Encephalitis</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>place of experiment</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Comment</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Location</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Comment</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
					</table>

					<button type="reset" class="btn btn-default" style="float: right;">Cancel</button>
					<button type="submit" class="btn btn-primary" style="float: right;">Change</button>
					<button type="submit" class="btn btn-primary" style="float: right;">Submit</button>

				</form>
			</div>
			<!-- /.col-xs-12 main -->
		</div>
		<!--/.row-->
	</div>
	<!--/.container-->
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />


</body>
</html>