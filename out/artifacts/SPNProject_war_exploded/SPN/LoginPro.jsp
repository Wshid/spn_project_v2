<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.SQLException"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<%
		//id passwd가져오기
		request.setCharacterEncoding("utf-8");
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");

		ResultSet rs = null;
		Connection con = null;

		PreparedStatement pstmt = null;
		String sql = "";
		try {
			//1단계 드라이버로더
			Class.forName("com.mysql.jdbc.Driver");
			//2단계 디비연결
			con = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/mysql", "root",
					//"tjdnfeoquddnjs1!");
					"admin");
			//3단계 id에 해당하는 passwd가져오기
			sql = "select pw from member where id=?";
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, id);
			//4단계 실행 => rs
			rs = pstmt.executeQuery();
			//5단계 데이터가 있으면 아이디있음
			if (rs.next()) {
				//id있음
				//   rs.getString(1);
				String dbPw = rs.getString("pw");
				if (pw.equals(dbPw)) {
					//비밀번호비교 맞으면 세션값생성 "id"
					//    이동 main.jsp
					session.setAttribute("id", id);
					session.setAttribute("pw", pw);
	%>
	<script type="text/javascript">
		alert("세션값 생성");
		location.href = "SPN?cmd=mainform";
		//location.href = "Main.jsp";
	</script>
	<%
		} else {
	%>
	<script type="text/javascript">
		alert("비밀번호 틀려써여");
		history.back();
	</script>
	<%
		}
			} else {
	%>
	<script type="text/javascript">
		alert("id가 존재하지 않아여");
		history.back();
	</script>
	<%
		}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			//rs!=null : 기억장소가 확보되어 있다는 뜻
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException ex) {
				}
			if (pstmt != null)
				try {
					pstmt.close();
				} catch (SQLException ex) {
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException ex) {
				}
		}
	%>


</body>
</html>