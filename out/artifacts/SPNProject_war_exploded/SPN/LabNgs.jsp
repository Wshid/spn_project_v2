<%@page import="java.util.ArrayList"%>
<%@page import="spn.dto.HistoryDTO"%>
<%@page import="java.util.List"%>
<%@page import="spn.dao.HistoryDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css">
     <link rel="stylesheet" type="text/css" href="./css/modal.css?">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<title>SPN Next Generation Sequencing</title>

<script type="text/javascript">
	function updateSubmitForm() {
		form = document.getElementById("ngsinput");
		//form.action = "SPN?cmd=labngsupdate";
		if(checkIt('lab_ngs')) form.action='SPN?cmd=labngsupdate';
	   	else return false;
		form.submit();
	}
</script>

</head>

<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- info sub-->
			<jsp:include page="InfoSub.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9 table_right">
				<h3>Next Generation Sequencing</h3>

				<!-- <button type="submit" class="btn btn-default" style="float: right;">History</button>  -->
				
				<%
					HistoryDAO dao=HistoryDAO.getHistoryDAO();
					List<HistoryDTO> his_li=new ArrayList<HistoryDTO>();
					
					String menu="lab_ngs";
					String searchVal=(String)session.getAttribute("searchVal");
					
					his_li=(List)dao.selectHistory(searchVal, menu);
					System.out.println("History List");
					System.out.println(his_li);
					System.out.println(his_li.size());
					if(his_li.size()!=0){
					%>
						<form name="historyForm" action="SPN?cmd=historyform"
					target="Window" method="post">
					<input type="hidden" name="page_name" value="lab_ngs" />
					<input class="btn btn-default" style="float: right;" type="button"
						value="History" onClick="popup_post4();"> <input
						type="hidden" value="111" name="hidden">
				</form>
						<%
					}
					
				%>
				<br> <br> <br>

				<form class="spn_input_form" method="post" action="SPN?cmd=labngs" name="ngsinput"
					id="ngsinput" onSubmit="return checkIt('lab_ngs')">
					<font style="float: right" color="#0191D0">${iid} &nbsp;|&nbsp; ${idate} </font>
					<table class="table table-bordered table-striped table-bover">
						<!--  style="width: 80%;"> -->
						<tr>
							<td><strong>Experiment<font color="red">*</font></strong></td>
							<td><select name="experiment">
									<option value="">select
									<option value="WGS"> WGS
									<option value="WES_Singleton"> WES Singleton
									<option value="WES_Trio"> WES Trio
									<option value="Targeted NGS">Targeted NGS
									<option value="RNA Seq">RNA Seq
							</select></td>
						</tr>
						<tr>
							<td><strong>Exp. date<font color="red">*</font></strong></td>
							<td><input type="date" name="exp_date" value="${exp_date}"
								style="border: transparent; background-color: transparent;"></td>
						</tr>
						<tr>
							<td><strong>Label No</strong>
							<td><input type="text" name="case_no" value="${case_no}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						<tr>
							<td><strong>Request Site</strong></td>
							<td><input type="text" name="exp_place" value="${exp_place}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Subject</strong></td>
							<td><input type="checkbox" name="sample_yn_f" value="Y">Father
								&nbsp; <input type="checkbox" name="sample_yn_m" value="Y">Mother
								&nbsp; <input type="checkbox" name="sample_yn_s" value="Y">Sibling	
								&nbsp; <input type="checkbox" name="sample_yn_o" value="Y">Other
								&nbsp; <input type="text" name="sample_yn_o_detail"  value="${sample_yn_o_detail}"
								style="width: auto"></td>
						</tr>
						<tr>
							<td><strong>Result</strong>
							<td><input type="radio" name="result" value="Pending">Pending
								<input type="text" name="result_pending_detail"
								value="${result_pending_detail}" style="width: 70%"><br>
								<input type="radio" name="result" value="Final">Final
							</td>
						</tr>
						<tr>
							<td><strong>Comment</strong></td>
							<td><input type="text" name="comment" value="${comment}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<input type="hidden" name="chart_no" value="${chart_no}" />
						<input type="hidden" name="ngs_no" value="${ngs_no}" />
						<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
					</table>

					<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
					<!-- <button type="submit" class="btn btn-primary" style="float: right;"  
						onclick="updateSubmitForm()">Change</button> -->
					<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button>  -->
					<%
						//System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
						String check_ns=(String)request.getAttribute("ngs_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.

						if(check_ns==null || check_ns.equals("")){
						//if(ss_no==0){
					%>
							<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
							<%
						}else{
							%>
							<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm()">Change</button>
										
							<%
						}
					%>		

				</form>
				
				
			</div>
			<!-- /.col-xs-12 main -->
		</div>
		
		<div class="modal_container font_shadow"> </div>
		
		<div id="footer">
			<div class="row">
				<div class="col-lg-12">
					<div class="page-header">
						<br> <br>
						<center>
							<font size=2><p>
									서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
									| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
									HOSPITAL
								</p></font>
						</center>
					</div>
				</div>
			</div>
		</div>

		<!-- script -->
		<jsp:include page="Script.jsp" flush="false" />
    	<script src="//code.jquery.com/jquery-latest.min.js"></script> <!--  jqeury last version -->
	    <script src="./js/bootstrap.min.js"></script>
	    <script src="js/prev_str.js?ver=1"></script>
	    <script src="js/modal.js?ver=4"></script> <!--  modal 관련 자바스크립트 함수 -->
		<script src="js/input_check.js?ver=1"></script>
		<script src="js/modal_script.js?ver=1"></script>

		<script language="JavaScript">
			function initSet() {
				var menu="lab_ngs";
				modal_script(menu);
				check_chart(menu);
				var select1 = document.getElementsByName("experiment").item(0);
				for (var i = 0; i < select1.options.length; i++) {
					if (select1.options[i].value == "${experiment}") {
						select1.selectedIndex = i;
					}
				}
				var checkBox1 = document.getElementsByName("sample_yn_f");
				if ("${sample_yn_f}" == "Y") {
					checkBox1.item(0).checked = true;
				}
				var checkBox2 = document.getElementsByName("sample_yn_m");
				if ("${sample_yn_m}" == "Y") {
					checkBox2.item(0).checked = true;
				}
				var checkBox3 = document.getElementsByName("sample_yn_s");
				if ("${sample_yn_s}" == "Y") {
					checkBox3.item(0).checked = true;
				}
				var checkBox4 = document.getElementsByName("sample_yn_o");
				if ("${sample_yn_o}" == "Y") {
					checkBox4.item(0).checked = true;
				}

				var radioBtn = document.getElementsByName("result");
				for (var i = 0; i < radioBtn.length; i++) {
					if (radioBtn.item(i).value == "${result}") {
						radioBtn.item(i).checked = true;
					}
				}

			}
		</script>

</body>
</html>
