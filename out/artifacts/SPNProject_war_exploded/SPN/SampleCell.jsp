<%@page import="java.util.ArrayList"%>
<%@page import="spn.dto.HistoryDTO"%>
<%@page import="java.util.List"%>
<%@page import="spn.dao.HistoryDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css">
<link rel="stylesheet" type="text/css" href="./css/modal.css?">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<title>SPN cell</title>

<script type="text/javascript">
function updateSubmitForm()
{	
	form=document.getElementById('cellinput');
    form.action='SPN?cmd=samplecellupdate';
	if(checkIt('sample_cell')) form.action='SPN?cmd=samplecellupdate';
   	else return false;
    //alert(form.action);
    form.submit();
}
</script>

</head>
<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- info sub -->
			<jsp:include page="InfoSub.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9 table_right">
				<h3>Cell</h3>

				<!-- <button type="submit" class="btn btn-default" style="float: right;">History</button> -->
				
				
				<%
					HistoryDAO dao=HistoryDAO.getHistoryDAO();
					List<HistoryDTO> his_li=new ArrayList<HistoryDTO>();
					
					String menu="sample_cell";
					String searchVal=(String)session.getAttribute("searchVal");
					
					his_li=(List)dao.selectHistory(searchVal, menu);
					System.out.println("History List");
					System.out.println(his_li);
					System.out.println(his_li.size());
					if(his_li.size()!=0){
					%>
						<form name="historyForm" action="SPN?cmd=historyform"
					target="Window" method="post">
					<input type="hidden" name="page_name" value="sample_cell" />
					<input class="btn btn-default" style="float: right;" type="button"
						value="History" onClick="popup_post4();"> <input
						type="hidden" value="111" name="hidden">
				</form>
						<%
					}
					
				%>
				<br> <br> <br>

				<form method="post" action="SPN?cmd=samplecell" name="cellinput" id="cellinput" onSubmit="return checkIt('sample_cell')">
				<font color="#0191D0" style="float: right;">${iid} &nbsp;|&nbsp; ${idate} </font><br>
					<table class="table table-bordered table-striped table-hover">
						<!-- style="width: auto; float: right;"> -->
						
						<tr>
							<td rowspan=2 ><strong>Refered Site</strong></td>
							<td><strong>Date</strong></td>
							<td><input type="date" name="req_date" value="${req_date}"
								style="border: transparent; background-color: transparent;"></td>
						</tr>
						
						<tr>
							<td><strong>Location</strong></td>
							<td><input type="text" name="req_location" value="${req_location}"
								style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
						</tr>
						
						<tr>
							<td><strong>Order Name</strong></td>
							<td colspan=2><input type="text" name="req_order_name" value="${req_order_name}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						
						<tr>
							<td><strong>Number of Vial</strong></td>
							<td colspan=2><input type="text" name="req_vial" value="${req_vial}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Comment</strong></td>
							<td colspan=2><input type="text" name="req_comment"
								value="${req_comment}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
					</table>

					<table class="table table-bordered table-striped table-hover">
						<!-- style="width: 50%">  -->
						
						<tr>
							<td><strong>Collection Date</strong></td>
							<td><input type="date" name="col_date" value="${col_date}"
								style="border: transparent; background-color: transparent;"></td>
						</tr>
						
						<tr>
							<td><strong>Sample Type</strong></td>
							<td><!-- <input type="radio" name="bio_type" value="Muscle">Muscle<br>
								<input type="radio" name="bio_type" value="Skin">Skin<br> -->
								<input type="checkbox" name="cell_type_myoblast" value="Y">&nbsp;Myoblast<br>
								<input type="checkbox" name="cell_type_fibroblast" value="Y">&nbsp;Fibroblast<br>
								<input type="checkbox" name="cell_type_u_epithelial" value="Y">&nbsp;U-epithelial<br>
								<input type="checkbox" name="cell_type_u_ipsc" value="Y">&nbsp;U-ipsc<br>
								<input type="checkbox" name="cell_type_other" value="Y">&nbsp;Other&nbsp;
								<input type="text" name="cell_type_other_detail"
								value="${cell_type_other_detail}" style="width: 60%"></td>
						</tr>
						<tr>
							<td><strong>Index No</strong></td>
							<td><input type="text" name="cell_index"
								value="${cell_index}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						 
						<tr>
							<td><strong>Phase</strong></td>
							<td><input type="text" name="cell_phase" value="${cell_phase}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						
						<tr>
							<td><strong>Storage Location</strong></td>
							<td><input type="text" name="cell_storage"
								value="${cell_storage}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<!-- 
						<tr>
							<td><strong>Location</strong></td>
							<td><input type="text" name="bio_location_2"
								value="${bio_location_2}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						 -->
						<tr>
							<td><strong>Comment</strong></td>
							<td><input type="text" name="cell_comment"
								value="${cell_comment}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
					</table>
					<input type="hidden" name="chart_no" value="${chart_no}" />
					<input type="hidden" name="ce_no" value="${ce_no}" />
					<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
					<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
					<!-- <button type="submit" class="btn btn-primary" style="float: right;" onclick="updateSubmitForm()">Change</button> -->
					<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button> -->
					<%
						System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
						String check=(String)request.getAttribute("ce_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.
						//String searchSangerNumber=(String)session.getAttribute("iid"); // null
						/* Enumeration으로 Session 체크, searchval, pw, id 밖에 없었음
						Enumeration<String> attr=session.getAttributeNames();

						while(attr.hasMoreElements()){
							System.out.println(attr.nextElement());
						}*/
						System.out.println("check : "+check);
						
						//System.out.println("SS_NO in SEss : "+searchSangerNumber);
						if(check==null || check.equals("")){
						//if(ss_no==0){
					%>
							<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
							<%
						}else{
							%>
							<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm()">Change</button>
										
							<%
						}
					%>						
				</form>
			</div>
			<!-- /.col-xs-12 main -->
		</div>
		<!--/.container-->
	</div>
	
	<div class="modal_container font_shadow"> </div>

	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
						<font size=2><p>
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
								| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL 
							</p></font>
					</center>
				</div>
			</div>
		</div>
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />
	<script src="//code.jquery.com/jquery-latest.min.js"></script> <!--  jqeury last version -->
    <script src="./js/bootstrap.min.js"></script>
    <script src="js/prev_str.js?ver=1"></script>
    <script src="js/modal.js?ver=4"></script> <!--  modal 관련 자바스크립트 함수 -->
    <script src="js/input_check.js"></script>
    <script src="js/modal_script.js"></script>
	

	<script language="JavaScript">
		function initSet() {
			var menu="sample_cell";
			modal_script(menu);
			check_chart(menu);
			
			var checkBox1 = document.getElementsByName("cell_type_myoblast");
			if ("${cell_type_myoblast}" == "Y") {
				checkBox1.item(0).checked = true;
			}
			var checkBox2 = document.getElementsByName("cell_type_fibroblast");
			if ("${cell_type_fibroblast}" == "Y") {
				checkBox2.item(0).checked = true;
			}
			var checkBox3 = document.getElementsByName("cell_type_u_epithelial");
			if ("${cell_type_u_epithelial}" == "Y") {
				checkBox3.item(0).checked = true;
			}
			var checkBox4 = document.getElementsByName("cell_type_u_ipsc");
			if ("${cell_type_u_ipsc}" == "Y") {
				checkBox4.item(0).checked = true;
			}
			var checkBox5 = document.getElementsByName("cell_type_other");
			if ("${cell_type_other}" == "Y") {
				checkBox5.item(0).checked = true;
			}

		}
	</script>
</body>
</html>