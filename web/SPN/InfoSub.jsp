<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<title>Insert title here</title>
</head>
<body>
	<div class="col-xs-12 col-sm-9"
		style="min-width: 100px; overflow-x: auto;">
		
		<table class="table table-bordered table-striped table-hover">
			<tr>
				<td style="width: 20%"><strong>Name</strong></td>
				<td style="width: 30%">${name}</td>
				<td style="width: 20%"><strong>SNUPN</strong></td>
				<td style="width: 30%">${snupn}</td>

			</tr>
			<tr>
				<td><strong>Sex</strong></td>
				<td>${sex}</td>
				<td><strong>Date of Birth</strong></td>
				<td>${birth}</td>
			</tr>
			<tr>
				<td><strong>Symptom</strong></td>
				<td>${symptom}</td>

				<td><strong>Doctor</strong></td>
				<td>${doctor}</td>

			</tr>
			<tr>
				<td><strong>Other Site</strong></td>
				<td colspan="4">${other_site}</td>
			</tr>
			<tr>
				<td><strong>Clinical Diagnosis</strong></td>
				<td colspan="4">${clinical_diagnosis}</td>
			</tr>
		</table>
	</div>

</body>
</html>