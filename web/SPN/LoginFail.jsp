<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html><!-- PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> --> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<title>SPN Login</title>
</head>
<body>

	<!--login modal-->
	<div id="loginModal" class="modal show" tabindex="-1" role="dialog"
		aria-hidden="true">

		<div style="padding-top: 250px;"></div>

		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h1 class="text-center">Login</h1>
				</div>

				<div class="modal-body">
					<form class="form col-md-12 center-block" method="post"
						action="SPN?cmd=login">
						<div class="form-group">
							<input type="text" id="id" name="id"
								class="form-control input-lg" placeholder="Input ID">
						</div>

						<div class="form-group">
							<input type="password" id="pw" name="pw"
								class="form-control input-lg" placeholder="Input Password">
						</div>
						
						<font size="3" color="red">
						아이디 또는 비밀번호를 다시 확인하세요.<br><br>
						</font>
						
						<div class="form-group">
							<button class="btn btn-primary btn-lg btn-block">Login</button>
						</div>
					</form>
				</div>

				<div class="modal-footer"></div>
			</div>
		</div>
	</div>



	<!-- <script src="./js/bootstrap.min.js"></script> -->

</body>
</html>