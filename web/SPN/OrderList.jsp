<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html><!-- PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> --> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<title>OrderList</title>
</head>
<body>
	<div class="wrapper style4 special container 60%"
		style="padding-top: 10px;">
		<h3>Order List</h3>
		<table class="table">

			
			<tr>
				<td bgcolor="#F5F5F5">OrderDate</td>
				<td bgcolor="#F5F5F5">Name</td>
				<td bgcolor="#F5F5F5">SPNN</td>
				<td bgcolor="#F5F5F5">Sanger Sequencing</td>
				<td bgcolor="#F5F5F5">Whole Genome Sequencing</td>
				<td bgcolor="#F5F5F5">Whole Exome Sequencing</td>
				<td bgcolor="#F5F5F5">Targeted NGS</td>
				<td bgcolor="#F5F5F5">CGH array</td>
				<td bgcolor="#F5F5F5">Muscle Culture</td>
				<td bgcolor="#F5F5F5">Muscle Stain</td>
				<td bgcolor="#F5F5F5">Western</td>
				<td bgcolor="#F5F5F5">Urine Culture</td>
				<td bgcolor="#F5F5F5">Remark</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
</body>
</html>