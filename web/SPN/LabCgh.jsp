<%@page import="java.util.ArrayList"%>
<%@page import="spn.dto.HistoryDTO"%>
<%@page import="java.util.List"%>
<%@page import="spn.dao.HistoryDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css">
<link rel="stylesheet" type="text/css" href="./css/file.css">
<link rel="stylesheet" type="text/css" href="./css/modal.css?">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<title>SPN CGH array</title>

<script type="text/javascript">
	function updateSubmitForm()
	{	
		form=document.getElementById('cghinput');
		if(checkIt('lab_cgh')) form.action='SPN?cmd=labcghupdate';
	   	else return false;
	    form.submit();
	}
</script>

</head>
<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- info sub -->
			<jsp:include page="InfoSub.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9 table_right">
				<h3>CGH array</h3>

				<!-- <button type="submit" class="btn btn-default" style="float: right;">History</button> -->
				
				
				<%
					HistoryDAO dao=HistoryDAO.getHistoryDAO();
					List<HistoryDTO> his_li=new ArrayList<HistoryDTO>();
					
					String menu="lab_cgh";
					String searchVal=(String)session.getAttribute("searchVal");
					
					his_li=(List)dao.selectHistory(searchVal, menu);
					System.out.println("History List");
					System.out.println(his_li);
					System.out.println(his_li.size());
					if(his_li.size()!=0){
					%>
						<form name="historyForm" action="SPN?cmd=historyform"
					target="Window" method="post">
					<input type="hidden" name="page_name" value="lab_cgh" />
					<input class="btn btn-default" style="float: right;" type="button"
						value="History" onClick="popup_post4();"> <input
						type="hidden" value="111" name="hidden">
				</form>
						<%
					}
					
				%>
				<br> <br> <br>

				<form class="spn_input_form" method="post" action="SPN?cmd=labcgh" name="cghinput"
					id="cghinput" onSubmit="return checkIt('lab_cgh')">
					<font color="#0191D0" style="float: right;">${iid} &nbsp;|&nbsp; ${idate} </font>
					<table class="table table-bordered table-striped table-hover">
						<tr>
							<td><strong>Exp. date<font color="red">*</font></strong></td>
							<td><input type="date" name="exp_date" value="${exp_date}"
								style="border: transparent; background-color: transparent;"></td>
							<td><strong>Chip No<font color="red">*</font></strong></td>
							<td><input type="text" name="chip_no" value="${chip_no}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>

						</tr>
						<tr>
							<td><strong>Karyotyping</strong></td>
							<td colspan="4"><!-- <input type="text" name="chromosome_study"
								value="${chromosome_study}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td> -->
								<strong>Y</strong><input type="radio" name="chromosome_study_yn" value="Y" style="margin-left:10px;">
								<strong>N</strong><input type="radio" name="chromosome_study_yn" value="N" style="margin-left:10px;">
							</td>
						</tr>
						<tr>
							<td><strong>FISH Confirmation</strong></td>
							<td colspan="4"><!-- <input type="text" name="fish_confirmation"
								value="${fish_confirmation}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td> -->
								<strong>Y</strong><input type="radio" name="fish_confirmation_yn" value="Y" style="margin-left:10px;">
								<strong>N</strong><input type="radio" name="fish_confirmation_yn" value="N" style="margin-left:10px;">
							</td>
						</tr>
						<tr>
							<td><strong>QPCR</strong></td>
							 <td colspan="4"><!-- <input type="text" name="fish_confirmation"
								value="${fish_confirmation}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td> -->
								<strong>Y</strong><input type="radio" name="qpcr_yn" value="Y" style="margin-left:10px;">
								<strong>N</strong><input type="radio" name="qpcr_yn" value="N" style="margin-left:10px;">
							</td>
						</tr>
						
						<tr>
							<td><strong>Platform</strong>
							<td colspan="4"><input type="radio" name="platform" value="agilent_60k">Agilent-60k<br/>
								<input type="radio" name="platform" value="agilent_180k">Agilent-180k<br/>
							<input type="radio" name="platform" value="agilent_244k">Agilent-244k<br/>
								<input type="radio" name="platform" value="agilent_other">Other
								&nbsp; <input type="text" name="platform_other_detail"
								value="${platform_other_detail}" style="width: auto%">
							</td>
						</tr>
						
						
						<tr>
							<td><strong>Parents test</strong></td>
							<td colspan="4">
							<input type="checkbox" name="parents_test_f" value="Y">Father
								&nbsp; <input type="checkbox" name="parents_test_m" value="Y">Mother</td>
							<!-- <input type="text" name="parents_test"
								value="${parents_test}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td> -->
						</tr>
						<tr>
							<td><strong>CNV type</strong></td>
							<td colspan="4">
								<!--  select 태그 다시 집어넣기 -->
								<select name="cnv_type">
								
									<option value="">select
									<option value="Non">Non
									<option value="Dup">Dup
									<option value="Del">Del
									<option value="Dup&Del">Dup&Del
									<option value="Mutiple Dup">Multiple Dup
									<option value="Mutiple Del">Multiple Del
								</select>
								
							</td>
						</tr>
						<tr>
							<td rowspan="3"><strong>Pathogenic result</strong></td>
							<td><strong>Location</strong></td>
							<td colspan="4"><input type="text"
								name="pathogenic_result_location"
								value="${pathogenic_result_location}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Size</strong></td>
							<td colspan="4"><input type="text"
								name="pathogenic_result_size" value="${pathogenic_result_size}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>

						<tr>
							<td><strong>Result</strong></td>
							<td colspan="4"><input type="text"
								name="pathogenic_result_url" value="${pathogenic_result_url}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>

						<tr>
							<td rowspan="3"><strong>Non Pathogenic result</strong></td>
							<td><strong>Location</strong></td>
							<td colspan="4"><input type="text"
								name="non_pathogenic_result_location"
								value="${non_pathogenic_result_location}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Size</strong></td>
							<td colspan="4"><input type="text"
								name="non_pathogenic_result_size"
								value="${non_pathogenic_result_size}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Result</strong></td>
							<td colspan="4"><input type="text"
								name="non_pathogenic_result_url" value="${non_pathogenic_result_url}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						
						<tr>
							<td><strong>Comment</strong></td>
							<td colspan="4"><input type="text" name="comment"
								value="${comment}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<input type="hidden" name="chart_no" value="${chart_no}" />
						<input type="hidden" name="cgh_no" value="${cgh_no}" />
						<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
					</table>

					<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
					<!-- <button type="submit" class="btn btn-primary" style="float: right;" onclick="updateSubmitForm()">Change</button> -->
					<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button> -->

					<%
						//System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
						String check_cgh=(String)request.getAttribute("cgh_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.
						//String searchSangerNumber=(String)session.getAttribute("iid"); // null
						/* Enumeration으로 Session 체크, searchval, pw, id 밖에 없었음
						Enumeration<String> attr=session.getAttributeNames();

						while(attr.hasMoreElements()){
							System.out.println(attr.nextElement());
						}*/
						
						//System.out.println("SS_NO in SEss : "+searchSangerNumber);
						if(check_cgh==null || check_cgh.equals("")){
						//if(ss_no==0){
					%>
							<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
							<%
						}else{
							%>
							<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm()">Change</button>
										
							<%
						}
					%>					
				</form>

			<div class="picture_grid_container"> <!-- 인풋되는 사진을 기준으로 그리드 형태로 보여준다. --><!--  해당 사진을 클릭하였을 때 확대하여 보여질 수 있도록 한다. -->
				<h3> Pictures</h3>
				<button id="${page_name}_button_modal" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal_${page_name}_input">Image Upload</button>

	  			<hr> <!--  선긋기 -->
				<input type="hidden" class="db_image" name="db_image" value="${db_image}" />
				<input type="hidden" class="names" name="names" value="${names}" />
				<input type="hidden" class="doctors" name="doctors" value="${doctors}" />	
				<input type="hidden" class="page_name" name="page_name" value="${page_name}" />				

				
				
				<form action="SPN?cmd=dbimageprocess" method="post" enctype="multipart/form-data">					
					<input type="hidden" name="img_idx" value="${img_idx}" />
					<input type="hidden" name="menu" value="lab_cgh"/><br />
					<input type="hidden" id="subtitle" name="subtitle" value="${next_idx}" /><br />					
					<div id="modal_${page_name}_input" class="modal fade" role="dialog">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button btn-primary" class="close" data-dismiss="modal">&times;</button>
					        <h4 class="modal-title">Image Upload</h4>
					      </div>
					      <div class="modal-body">
					         <p>

								<div class="filebox">
									<button class="replace btn-primary">Upload</button>  
									<input type="file" name="file" value="파일 업로드" class="upload"> <!--  name도 file로 해야함 -->
									<!-- <input type="file" name="file"/><br /> -->
								</div>
								
					         </p>
					      </div>
					      <div class="modal-footer">
					      	<button type="submit" class="btn btn-primary button_reform">Submit</button>
					        <button type="button" class="btn btn-default burron_reform" data-dismiss="modal">Close</button>
					      </div>
					    </div>
					
					  </div>
					</div>
				</form>				
				
			</div>

			</div>
			<!-- /.col-xs-12 main -->
		</div>
		<!--/.container-->
	</div>

	<div class="modal_container font_shadow">
	</div>

	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
						<font size=2><p>
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
								| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL 
							</p></font>
					</center>
				</div>
			</div>
		</div>
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />
    <script src="//code.jquery.com/jquery-latest.min.js"></script> <!--  jqeury last version -->
    <script src="./js/bootstrap.min.js"></script>
    <script src="js/prev_str.js?ver=1"></script>
    <script src="js/modal.js?ver=4"></script> <!--  modal 관련 자바스크립트 함수 -->
    <script src="js/input_check.js"></script>
    <script src="js/modal_script.js?ver=2"></script>
	<script src="./js/picture_grid.js"></script>

	<script language="JavaScript">

		function initSet() {
			var menu="lab_cgh";
			modal_script(menu);
			check_chart(menu);

			var select1 = document.getElementsByName("cnv_type").item(0);
			for (var i = 0; i < select1.options.length; i++) {
				if (select1.options[i].value == "${cnv_type}") {
					select1.selectedIndex = i;
				}
			}

			var checkBox1 = document.getElementsByName("chromosome_study_yn");
				if ("${chromosome_study_yn}" == "Y") {
					checkBox1.item(0).checked = true;
				}
				var checkBox2 = document.getElementsByName("fish_confirmation_yn");
				if ("${fish_confirmation_yn}" == "Y") {
					checkBox2.item(0).checked = true;
				}
				var checkBox3 = document.getElementsByName("qpcr_yn");
				if ("${qpcr_yn}" == "Y") {
					checkBox3.item(0).checked = true;
				}
				var checkBox4 = document.getElementsByName("parents_test_f");
				if ("${parents_test_f}" == "Y") {
					checkBox4.item(0).checked = true;
				}
				var checkBox5 = document.getElementsByName("parents_test_m");
				if ("${parents_test_m}" == "Y") {
					checkBox5.item(0).checked = true;
				}

			var radioBtn = document.getElementsByName("platform");
			for (var i = 0; i < radioBtn.length; i++) {
				if (radioBtn.item(i).value == "${platform}") {
					radioBtn.item(i).checked = true;
				}
			}

			var radioBtn2 = document.getElementsByName("chromosome_study_yn");
			for (var i = 0; i < radioBtn2.length; i++) {
				if (radioBtn2.item(i).value == "${chromosome_study_yn}") {
					radioBtn2.item(i).checked = true;
				}
			}

			var radioBtn3 = document.getElementsByName("fish_confirmation_yn");
			for (var i = 0; i < radioBtn3.length; i++) {
				if (radioBtn3.item(i).value == "${fish_confirmation_yn}") {
					radioBtn3.item(i).checked = true;
				}
			}

			var radioBtn4 = document.getElementsByName("qpcr_yn");
			for (var i = 0; i < radioBtn4.length; i++) {
				if (radioBtn4.item(i).value == "${qpcr_yn}") {
					radioBtn4.item(i).checked = true;
				}
			}
		}
	</script>

</body>
</html>