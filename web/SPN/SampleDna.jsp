<%@page import="spn.dao.HistoryDAO"%>
<%@page import="spn.dto.HistoryDTO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css">
<link rel="stylesheet" type="text/css" href="./css/modal.css">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>

<title>SPN DNA</title>

<script type="text/javascript">
function updateSubmitForm(form_idx)
{	
	//form=document.getElementById('dnainput');
	var forms=document.getElementsByName('dnainput');
	var form=forms[form_idx];
    //for(i=0 ; i<forms.length;i++){
   	if(checkIt('sample_dna')) form.action='SPN?cmd=samplednaupdate';
   	else return false;
    //}
    //alert("change Action");
    //alert(forms[0].action);
    form.submit();
}
</script>

</head>

<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- info sub-->
			<jsp:include page="InfoSub.jsp" flush="false" />

			<!-- main area -->
			<!-- <div class="col-xs-12 col-sm-9 table_right"> -->
			<div class="col-xs-12 col-sm-9 table_right">
				<h3>DNA</h3>

				<!-- <button type="submit" class="btn btn-default" style="float: right;">History</button> -->
				
				
				<%
					HistoryDAO dao=HistoryDAO.getHistoryDAO();
					List<HistoryDTO> his_li=new ArrayList<HistoryDTO>();
					
					String menu="sample_dna";
					String searchVal=(String)session.getAttribute("searchVal");
					
					his_li=(List)dao.selectHistory(searchVal, menu);
					System.out.println("History List");
					System.out.println(his_li);
					System.out.println(his_li.size());
					if(his_li.size()!=0){
					%>
						<form name="historyForm" action="SPN?cmd=historyform" target="Window" method="post">
							<input type="hidden" name="page_name" value="sample_dna" />
							<input class="btn btn-default" style="float: right;" type="button" value="History" onClick="popup_post4();">
							<input type="hidden" value="111" name="hidden">
						</form>
					
						<%
					}
					
				%>
				
				<br> <br> <br>
				
				<!--  탭으로 추가될 부분 -->
				<ul class="nav nav-tabs">
				  <li class="active"><a data-toggle="tab" href="#dna1">DNA 1</a></li>
				  <li><a data-toggle="tab" href="#dna2">DNA 2</a></li>
				  <li><a data-toggle="tab" href="#dna3">DNA 3</a></li>
				</ul>
				
				<div class="tab-content">
				  <div id="dna1" class="tab-pane fade in active">
				  <br>
					<form method="post" action="SPN?cmd=sampledna" name="dnainput" id="dnainput" onSubmit="return checkIt('sample_dna')">
					<%
						System.out.println("dna_no : "+request.getAttribute("dna_no"));
					%>
						<font color="#0191D0" style="float: right;">${iid[0]} &nbsp;|&nbsp; ${idate[0]} </font><br>
							<table class="table table-bordered table-striped table-hover">
								<!-- style="width: auto; float: right;"> -->
								<tr>
									<td rowspan=2 ><strong>Refered Site</strong></td>
									<td><strong>Date</strong></td>
									<td><input type="date" name="req_date" value="${req_date[0]}"
										style="border: transparent; background-color: transparent;"></td>
								</tr>
								<tr>
									<td><strong>Location</strong></td>
									<td><input type="text" name="req_location" value="${req_location[0]}"
										style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
								</tr>
								<!-- <tr>
									<td><strong>Distribution Type</strong></td>
									<td colspan=2><input type="radio" name="req_distribution"
										value="External">External &nbsp; <input type="radio"
										name="req_distribution" value="Internal">Internal</td>
								</tr> -->
								<tr>
									<td><strong>Order Name</strong></td>
									<td colspan=2><input type="text" name="req_order_name" value="${req_order_name[0]}"
										style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								</tr>
								<!--
								<tr>
									<td><strong>Sample Type</strong></td>
									<td colspan=2><input class="req_type_0" type="radio" name="req_type" value="Muscle">Muscle<br>
										<input class="req_type_0" type="radio" name="req_type" value="Skin">Skin<br>
										<input class="req_type_0" type="radio" name="req_type" value="Myoblast">Myoblast<br>
										<input class="req_type_0" type="radio" name="req_type" value="Fibroblast">Fibroblast<br>
										<input class="req_type_0" type="radio" name="req_type" value="Other">Other&nbsp;
										<input type="text" name="req_type_other_detail"
										value="${req_type_other_detail[0]}" style="width: 60%"></td>
								</tr> -->
								<tr>
									<td><strong>Number of Vial</strong></td>
									<td colspan=2><input type="text" name="req_vial" value="${req_vial[0]}"
										style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								</tr>
								<tr>
									<td><strong>Comment</strong></td>
									<td colspan=2><input type="text" name="req_comment"
										value="${req_comment[0]}"
										style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								</tr>
							</table>
							
							<br>
						
							<table class="table table-bordered table-striped table-hover"
								style="width: 100%; float: right;">
								<!-- 
								<tr>
									<td><strong>Distribution Type</strong></td>
									<td><input type="radio" name="req_distribution"
										value="External">External &nbsp; <input type="radio"
										name="req_distribution" value="Internal">Internal</td>
								</tr> -->
								<!-- 
								<tr>
									<td><strong>Order Name</strong></td>
									
									<td><input type="text" name="req_order_name" value="${req_order_name}"
										style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									<td><select name="req_order_name">
											<option value="">select</option>
											<option value="김기중">김기중</option>
											<option value="채종희">채종희</option>
											<option value="임병찬">임병찬</option>
									</select></td>
								</tr>
								 -->
								 
								<tr>
									<td><strong>Collection Date</strong></td>
									<td><input type="date" name="col_date" value="${col_date[0]}"
										style="border: transparent; background-color: transparent;"></td>
								</tr>
								<tr>
									<td><strong>Subject</strong>
									<td><input class="col_who_0" type="radio" name="col_who" value="Patient">Patient&nbsp;
											<input type="text" name="col_who_patient_detail" value="${col_who_patient_detail[0]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
										<input class="col_who_0" type="radio" name="col_who" value="Father">Father&nbsp;
											<input type="text" name="col_who_father_detail" value="${col_who_father_detail[0]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
										<input class="col_who_0" type="radio" name="col_who" value="Mother">Mother&nbsp;
											<input type="text" name="col_who_mother_detail"value="${col_who_mother_detail[0]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
										<input class="col_who_0" type="radio" name="col_who" value="Sibling">Sibling&nbsp;
											<input type="text" name="col_who_sibling_detail" value="${col_who_sibling_detail[0]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
										<input class="col_who_0" type="radio" name="col_who" value="Other">Other&nbsp;
											<input type="text" name="col_who_other_detail" value="${col_who_other_detail[0]}" placeholder="Input Name" style="border: transparent; background-color: transparent;" style="width: 60%;">
								</tr>
								<tr>
									<td><strong>Sample Type</strong></td>
									<td><input class="col_type_0" type="radio" name="col_type"
										value="Peripheral Blood">Peripheral Blood<br> <input
										type="radio" name="col_type" value="Muscle">Muscle<br>
										<input class="col_type_0" type="radio" name="col_type" value="AFCell">AF
										cell<br> <input type="radio" class="col_type_0" name="col_type" value="Other">Other&nbsp;
										<input type="text" name="col_type_other_detail" value="${col_type_other_detail[0]}"
										style="width: 60%"></td>
								</tr>
								<tr>
									<td><strong>Index No</strong></td>
									<td><input type="text" name="col_index" value="${col_index[0]}"
										style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								</tr>
								<tr>
									<td><strong>Agreement</strong></td>
									<td><input type="text" name="col_agreement" value="${col_agreement[0]}"
										style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								</tr>
								<tr>
									<td><strong>Performed By</strong></td>
									<!-- <td><input type="text" name="col_performed_by" value="${col_performed_by[0]}"
										style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td> -->
									<td><input class="col_performed_by_0" type="radio" name="col_performed_by" value="Y">Yes&nbsp;
										<input class="col_performed_by_0" type="radio" name="col_performed_by" value="N">No</td>
								</tr>								
								<tr>
									<td><strong>Concentration</strong></td>
									<td><input type="text" name="col_concentration" value="${col_concentration[0]}"
										style="border: transparent; background-color: transparent; width: 70%;"><font color="#0191D0">ng/ul</font></td>
								</tr>
								<tr>
									<td><strong>260/280</strong></td>
									<td><input type="text" name="col_260_280" value="${col_260_280[0]}"
										style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								</tr>
								<tr>
									<td><strong>260/230</strong></td>
									<td><input type="text" name="col_260_230" value="${col_260_230[0]}"
										style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								</tr>
								<tr>
									<td><strong>Location</strong></td>
									<td><input type="text" name="col_location" value="${col_location[0]}"
										style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								</tr>
								<tr>
									<td><strong>Comment</strong></td>
									<td><input type="text" name="col_comment" value="${col_comment[0]}"
										style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								</tr>
							</table>
							<input type="hidden" name="chart_no" value="${chart_no}" />
							<input type="hidden" name="dna_no" value="${dna_no[0]}" />
							<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
							<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
							<!-- <button type="submit" class="btn btn-primary" style="float: right;" onclick="updateSubmitForm()">Change</button> -->
							<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button> -->
		
							<%
								System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
								ArrayList<String> check_list=(ArrayList<String>)request.getAttribute("dna_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.
								
								//System.out.println(check_list);
								if(check_list.size()<1){
								
								/*	
								}
								String check=check_list.get(0);
								System.out.println("OUT");
								//String check=null;
								if(check==null || check.equals("")){*/
									
								//if(ss_no==0){
							%>
									<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
									<%
								}else{
									%>
									<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm(0)">Change</button>
												
									<%
								}
							%>		
						</form>
				  	</div>
					<div id="dna2" class="tab-pane fade"> <!-- DNA2 -->
					  <br>
						<form method="post" action="SPN?cmd=sampledna" name="dnainput" id="dnainput" onSubmit="return checkIt('sample_dna')">
						<%
							System.out.println("dna_no : "+request.getAttribute("dna_no"));
						%>
							<font color="#0191D0" style="float: right;">${iid[1]} &nbsp;|&nbsp; ${idate[1]} </font><br>
								<table class="table table-bordered table-striped table-hover">
									<!-- style="width: auto; float: right;"> -->
									<tr>
										<td rowspan=2 ><strong>Refered Site</strong></td>
										<td><strong>Date</strong></td>
										<td><input type="date" name="req_date" value="${req_date[1]}"
											style="border: transparent; background-color: transparent;"></td>
									</tr>
									<tr>
										<td><strong>Location</strong></td>
										<td><input type="text" name="req_location" value="${req_location[1]}"
											style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
									</tr>
									<!-- <tr>
										<td><strong>Distribution Type</strong></td>
										<td colspan=2><input type="radio" name="req_distribution"
											value="External">External &nbsp; <input type="radio"
											name="req_distribution" value="Internal">Internal</td>
									</tr> -->
									 
									<tr>
										<td><strong>Order Name</strong></td>
										<td colspan=2><input type="text" name="req_order_name" value="${req_order_name[1]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr> 
									<!-- 
									<tr>
										<td><strong>Sample Type</strong></td>
										<td colspan=2><input class="req_type_1" type="radio" name="req_type" value="Muscle">Muscle<br>
											<input class="req_type_1" type="radio" name="req_type" value="Skin">Skin<br>
											<input class="req_type_1" type="radio" name="req_type" value="Myoblast">Myoblast<br>
											<input class="req_type_1" type="radio" name="req_type" value="Fibroblast">Fibroblast<br>
											<input class="req_type_1" type="radio" name="req_type" value="Other">Other&nbsp;
											<input type="text" name="req_type_other_detail"
											value="${req_type_other_detail[1]}" style="width: 60%"></td>
									</tr> -->
									<tr>
										<td><strong>Number of Vial</strong></td>
										<td colspan=2><input type="text" name="req_vial" value="${req_vial[1]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									<tr>
										<td><strong>Comment</strong></td>
										<td colspan=2><input type="text" name="req_comment"
											value="${req_comment[1]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
								</table>
								
								<br>
							
								<table class="table table-bordered table-striped table-hover"
									style="width: 100%; float: right;">
									<!-- 
									<tr>
										<td><strong>Distribution Type</strong></td>
										<td><input type="radio" name="req_distribution"
											value="External">External &nbsp; <input type="radio"
											name="req_distribution" value="Internal">Internal</td>
									</tr> -->
									<!-- 
									<tr>
										<td><strong>Order Name</strong></td> -->
										<!-- 
										<td><input type="text" name="req_order_name" value="${req_order_name}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td> -->
										<!--  
										<td><select name="req_order_name">
												<option value="">select</option>
												<option value="김기중">김기중</option>
												<option value="채종희">채종희</option>
												<option value="임병찬">임병찬</option>
										</select></td>
										
									</tr>-->
									<tr>
										<td><strong>Collection Date</strong></td>
										<td><input type="date" name="col_date" value="${col_date[1]}"
											style="border: transparent; background-color: transparent;"></td>
									</tr>
									<tr>
										<td><strong>Subject</strong>
										<td><input class="col_who_1" type="radio" name="col_who" value="Patient">Patient&nbsp;
												<input type="text" name="col_who_patient_detail" value="${col_who_patient_detail[1]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
											<input class="col_who_1" type="radio" name="col_who" value="Father">Father&nbsp;
												<input type="text" name="col_who_father_detail" value="${col_who_father_detail[1]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
											<input class="col_who_1" type="radio" name="col_who" value="Mother">Mother&nbsp;
												<input type="text" name="col_who_mother_detail"value="${col_who_mother_detail[1]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
											<input class="col_who_1" type="radio" name="col_who" value="Sibling">Sibling&nbsp;
												<input type="text" name="col_who_sibling_detail" value="${col_who_sibling_detail[1]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
											<input class="col_who_1" type="radio" name="col_who" value="Other">Other&nbsp;
												<input type="text" name="col_who_other_detail" value="${col_who_other_detail[1]}" placeholder="Input Name" style="border: transparent; background-color: transparent;" style="width: 60%;">
									</tr>
									<tr>
										<td><strong>Sample Type</strong></td>
										<td><input class="col_type_1" type="radio" name="col_type"
											value="Peripheral Blood">Peripheral Blood<br> <input
											type="radio" name="col_type" value="Muscle">Muscle<br>
											<input class="col_type_1" type="radio" name="col_type" value="AFCell">AF
											cell<br> <input type="radio" class="col_type_1" name="col_type" value="Other">Other&nbsp;
											<input type="text" name="col_type_other_detail" value="${col_type_other_detail[1]}"
											style="width: 60%"></td>
									</tr>
									<tr>
										<td><strong>Index No</strong></td>
										<td><input type="text" name="col_index" value="${col_index[1]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									<tr>
										<td><strong>Agreement</strong></td>
										<td><input type="text" name="col_agreement" value="${col_agreement[1]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									<tr>
										<td><strong>Performed By</strong></td>
										<!-- <td><input type="text" name="col_performed_by" value="${col_performed_by[0]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td> -->
										<td><input class="col_performed_by_1" type="radio" name="col_performed_by" value="Y">Yes&nbsp;
											<input class="col_performed_by_1" type="radio" name="col_performed_by" value="N">No</td>
									</tr>		
									<tr>
										<td><strong>Concentration</strong></td>
										<td><input type="text" name="col_concentration" value="${col_concentration[1]}"
											style="border: transparent; background-color: transparent; width: 70%;"><font color="#0191D0">ng/ul</font></td>
									</tr>
									<tr>
										<td><strong>260/280</strong></td>
										<td><input type="text" name="col_260_280" value="${col_260_280[1]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									<tr>
										<td><strong>260/230</strong></td>
										<td><input type="text" name="col_260_230" value="${col_260_230[1]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									<tr>
										<td><strong>Location</strong></td>
										<td><input type="text" name="col_location" value="${col_location[1]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									<tr>
										<td><strong>Comment</strong></td>
										<td><input type="text" name="col_comment" value="${col_comment[1]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
								</table>
								<input type="hidden" name="chart_no" value="${chart_no}" />
								<input type="hidden" name="dna_no" value="${dna_no[1]}" />
								<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
								<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
								<!-- <button type="submit" class="btn btn-primary" style="float: right;" onclick="updateSubmitForm()">Change</button> -->
								<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button> -->
			
								<%
									System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
									//ArrayList<String> check_list=(ArrayList<String>)request.getAttribute("dna_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.
									
									//System.out.println(check_list);
									//String check1=check_list.get(1);
									
									//System.out.println(check_list);
									if(check_list.size()<2){
									
									/*	
									}
									String check=check_list.get(0);
									System.out.println("OUT");
									//String check=null;
									if(check==null || check.equals("")){*/
										
									//if(ss_no==0){
								%>
										<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
										<%
									}else{
										%>
										<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm(1)">Change</button>
													
										<%
									}
								%>		
							</form>
					</div>
					<div id="dna3" class="tab-pane fade"> <!-- DNA3 -->
<br>
						<form method="post" action="SPN?cmd=sampledna" name="dnainput" id="dnainput" onSubmit="return checkIt('sample_dna')">
						<%
							System.out.println("dna_no : "+request.getAttribute("dna_no"));
						%>
							<font color="#0191D0" style="float: right;">${iid[2]} &nbsp;|&nbsp; ${idate[2]} </font><br>
								<table class="table table-bordered table-striped table-hover">
									<!-- style="width: auto; float: right;"> -->
									<tr>
										<td rowspan=2 ><strong>Refered Site</strong></td>
										<td><strong>Date</strong></td>
										<td><input type="date" name="req_date" value="${req_date[2]}"
											style="border: transparent; background-color: transparent;"></td>
									</tr>
									<tr>
										<td><strong>Location</strong></td>
										<td><input type="text" name="req_location" value="${req_location[2]}"
											style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
									</tr>
									<!-- <tr>
										<td><strong>Distribution Type</strong></td>
										<td colspan=2><input type="radio" name="req_distribution"
											value="External">External &nbsp; <input type="radio"
											name="req_distribution" value="Internal">Internal</td>
									</tr> -->
									
									<tr>
										<td><strong>Order Name</strong></td>
										<td colspan=2><input type="text" name="req_order_name" value="${req_order_name[2]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									
									<!-- 
									<tr>
										<td><strong>Sample Type</strong></td>
										<td colspan=2><input class="req_type_2" type="radio" name="req_type" value="Muscle">Muscle<br>
											<input class="req_type_2" type="radio" name="req_type" value="Skin">Skin<br>
											<input class="req_type_2" type="radio" name="req_type" value="Myoblast">Myoblast<br>
											<input class="req_type_2" type="radio" name="req_type" value="Fibroblast">Fibroblast<br>
											<input class="req_type_2" type="radio" name="req_type" value="Other">Other&nbsp;
											<input type="text" name="req_type_other_detail"
											value="${req_type_other_detail[2]}" style="width: 60%"></td>
									</tr>
									 -->
									<tr>
										<td><strong>Number of Vial</strong></td>
										<td colspan=2><input type="text" name="req_vial" value="${req_vial[2]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									<tr>
										<td><strong>Comment</strong></td>
										<td colspan=2><input type="text" name="req_comment"
											value="${req_comment[2]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
								</table>
								
								<br>
							
								<table class="table table-bordered table-striped table-hover"
									style="width: 100%; float: right;">
									<!-- 
									<tr>
										<td><strong>Distribution Type</strong></td>
										<td><input type="radio" name="req_distribution"
											value="External">External &nbsp; <input type="radio"
											name="req_distribution" value="Internal">Internal</td>
									</tr> -->
									<!--
									<tr>
										<td><strong>Order Name</strong></td>
										 
										<td><input type="text" name="req_order_name" value="${req_order_name}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
										<td><select name="req_order_name">
												<option value="">select</option>
												<option value="김기중">김기중</option>
												<option value="채종희">채종희</option>
												<option value="임병찬">임병찬</option>
										</select></td>
									</tr>
									  -->
									<tr>
										<td><strong>Collection Date</strong></td>
										<td><input type="date" name="col_date" value="${col_date[2]}"
											style="border: transparent; background-color: transparent;"></td>
									</tr>
									<tr>
										<td><strong>Subject</strong>
										<td><input class="col_who_2" type="radio" name="col_who" value="Patient">Patient&nbsp;
												<input type="text" name="col_who_patient_detail" value="${col_who_patient_detail[2]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
											<input class="col_who_2" type="radio" name="col_who" value="Father">Father&nbsp;
												<input type="text" name="col_who_father_detail" value="${col_who_father_detail[2]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
											<input class="col_who_2" type="radio" name="col_who" value="Mother">Mother&nbsp;
												<input type="text" name="col_who_mother_detail"value="${col_who_mother_detail[2]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
											<input class="col_who_2" type="radio" name="col_who" value="Sibling">Sibling&nbsp;
												<input type="text" name="col_who_sibling_detail" value="${col_who_sibling_detail[2]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
											<input class="col_who_2" type="radio" name="col_who" value="Other">Other&nbsp;
												<input type="text" name="col_who_other_detail" value="${col_who_other_detail[2]}" placeholder="Input Name" style="border: transparent; background-color: transparent;" style="width: 60%;">
									</tr>
									<tr>
										<td><strong>Sample Type</strong></td>
										<td><input class="col_type_2" type="radio" name="col_type"
											value="Peripheral Blood">Peripheral Blood<br> <input
											type="radio" name="col_type" value="Muscle">Muscle<br>
											<input class="col_type_2" type="radio" name="col_type" value="AFCell">AF
											cell<br> <input type="radio" class="col_type_2" name="col_type" value="Other">Other&nbsp;
											<input type="text" name="col_type_other_detail" value="${col_type_other_detail[2]}"
											style="width: 60%"></td>
									</tr>
									<tr>
										<td><strong>Index No</strong></td>
										<td><input type="text" name="col_index" value="${col_index[2]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									<tr>
										<td><strong>Agreement</strong></td>
										<td><input type="text" name="col_agreement" value="${col_agreement[2]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									<tr>
										<td><strong>Performed By</strong></td>
										<!-- <td><input type="text" name="col_performed_by" value="${col_performed_by[0]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td> -->
										<td><input class="col_performed_by_2" type="radio" name="col_performed_by" value="Y">Yes&nbsp;
											<input class="col_performed_by_2" type="radio" name="col_performed_by" value="N">No</td>
									</tr>	
									<tr>
										<td><strong>Concentration</strong></td>
										<td><input type="text" name="col_concentration" value="${col_concentration[2]}"
											style="border: transparent; background-color: transparent; width: 70%;"><font color="#0191D0">ng/ul</font></td>
									</tr>
									<tr>
										<td><strong>260/280</strong></td>
										<td><input type="text" name="col_260_280" value="${col_260_280[2]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									<tr>
										<td><strong>260/230</strong></td>
										<td><input type="text" name="col_260_230" value="${col_260_230[2]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									<tr>
										<td><strong>Location</strong></td>
										<td><input type="text" name="col_location" value="${col_location[2]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
									<tr>
										<td><strong>Comment</strong></td>
										<td><input type="text" name="col_comment" value="${col_comment[2]}"
											style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
									</tr>
								</table>
								<input type="hidden" name="chart_no" value="${chart_no}" />
								<input type="hidden" name="dna_no" value="${dna_no[2]}" />
								<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
								<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
								<!-- <button type="submit" class="btn btn-primary" style="float: right;" onclick="updateSubmitForm()">Change</button> -->
								<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button> -->
			
								<%
									System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
									//ArrayList<String> check_list=(ArrayList<String>)request.getAttribute("dna_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.
									
									//System.out.println(check_list);
									//String check2=check_list.get(2);
									
									//System.out.println(check_list);
									if(check_list.size()<3){
									
									/*	
									}
									String check=check_list.get(0);
									System.out.println("OUT");
									//String check=null;
									if(check==null || check.equals("")){*/
										
									//if(ss_no==0){
								%>
										<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
										<%
									}else{
										%>
										<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm(2)">Change</button>
													
										<%
									}
								%>		
							</form>
					</div>
				</div>
					
					
			</div>
			<!-- /.col-xs-12 main -->
		</div>
		<!--/.container-->
	</div>
	
	<div class="modal_container font_shadow"> </div>

	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
						<font size=2><p>
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
								| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL 
							</p></font>
					</center>
				</div>
			</div>
		</div>
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />
	<script src="//code.jquery.com/jquery-latest.min.js"></script> <!--  jqeury last version -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/prev_str.js?ver=1"></script>
    <script src="js/modal.js?ver=4"></script> <!--  modal 관련 자바스크립트 함수 -->
    <script src="js/input_check.js"></script>
    <script src="js/modal_script.js"></script>
	

	<script language="JavaScript">
		function initSet() {
			var menu="sample_dna";
			modal_script(menu);
			check_chart(menu);
			/*
			var radioBtn1 = document.getElementsByClassName("col_who_0");
			for (var i = 0; i < radioBtn1.length; i++) {
				if (radioBtn1.item(i).value == "${col_who[0]}") {
					radioBtn1.item(i).checked = true;
				}
			}*/
			
			//var radioBtn2 = document.getElementsByName("col_type");
			var radioBtn2 = document.getElementsByClassName("col_type_0");
			for (var i = 0; i < radioBtn2.length; i++) {
				if (radioBtn2.item(i).value == "${col_type[0]}") {
					radioBtn2.item(i).checked = true;
				}
			}
			
			/*
			//var radioBtn3 = document.getElementsByName("req_who");
			var radioBtn3 = document.getElementsByClassName("req_who_0");
			for (var i = 0; i < radioBtn3.length; i++) {
				if (radioBtn3.item(i).value == "${req_who[0]}") {
					radioBtn3.item(i).checked = true;
				}
			}
			
			//var radioBtn4 = document.getElementsByName("req_distribution");
			var radioBtn4 = document.getElementsByClassName("req_distribution_0");
			for (var i = 0; i < radioBtn4.length; i++) {
				if (radioBtn4.item(i).value == "${req_distribution[0]}") {
					radioBtn4.item(i).checked = true;
				}
			}
			*/
			
			//var radioBtn5 = document.getElementsByName("req_type");
			/*
			var radioBtn5 = document.getElementsByClassName("req_type_0");
			for (var i = 0; i < radioBtn5.length; i++) {
				if (radioBtn5.item(i).value == "${req_type[0]}") {
					radioBtn5.item(i).checked = true;
				}
			}*/
			
			var radioBtn11 = document.getElementsByClassName("col_who_0");
			for (var i = 0; i < radioBtn11.length; i++) {
				if (radioBtn11.item(i).value == "${col_who[0]}") {
					radioBtn11.item(i).checked = true;
				}
			}
			
			var radioBtn12 = document.getElementsByClassName("col_who_1");
			for (var i = 0; i < radioBtn12.length; i++) {
				if (radioBtn12.item(i).value == "${col_who[1]}") {
					radioBtn12.item(i).checked = true;
				}
			}
			
			var radioBtn13 = document.getElementsByClassName("col_who_2");
			for (var i = 0; i < radioBtn13.length; i++) {
				if (radioBtn13.item(i).value == "${col_who[2]}") {
					radioBtn13.item(i).checked = true;
				}
			}
			
			//var radioBtn2 = document.getElementsByName("col_type");
			var radioBtn21 = document.getElementsByClassName("col_type_0");
			for (var i = 0; i < radioBtn21.length; i++) {
				if (radioBtn21.item(i).value == "${col_type[0]}") {
					radioBtn21.item(i).checked = true;
				}
			}
			
			var radioBtn22 = document.getElementsByClassName("col_type_1");
			for (var i = 0; i < radioBtn22.length; i++) {
				if (radioBtn22.item(i).value == "${col_type[1]}") {
					radioBtn22.item(i).checked = true;
				}
			}
			
			var radioBtn23 = document.getElementsByClassName("col_type_2");
			for (var i = 0; i < radioBtn23.length; i++) {
				if (radioBtn23.item(i).value == "${col_type[2]}") {
					radioBtn23.item(i).checked = true;
				}
			}
			
			var radioBtn31 = document.getElementsByClassName("col_performed_by_0");
			for (var i = 0; i < radioBtn31.length; i++) {
				if (radioBtn31.item(i).value == "${col_performed_by[0]}") {
					radioBtn31.item(i).checked = true;
				}
			}
			
			var radioBtn32 = document.getElementsByClassName("col_performed_by_1");
			for (var i = 0; i < radioBtn32.length; i++) {
				if (radioBtn32.item(i).value == "${col_performed_by[1]}") {
					radioBtn32.item(i).checked = true;
				}
			}
			
			var radioBtn33 = document.getElementsByClassName("col_performed_by_2");
			for (var i = 0; i < radioBtn33.length; i++) {
				if (radioBtn33.item(i).value == "${col_performed_by[2]}") {
					radioBtn33.item(i).checked = true;
				}
			}
			
			/*
			//var radioBtn3 = document.getElementsByName("req_who");
			var radioBtn3 = document.getElementsByClassName("req_who_0");
			for (var i = 0; i < radioBtn3.length; i++) {
				if (radioBtn3.item(i).value == "${req_who[0]}") {
					radioBtn3.item(i).checked = true;
				}
			}
			
			//var radioBtn4 = document.getElementsByName("req_distribution");
			var radioBtn4 = document.getElementsByClassName("req_distribution_0");
			for (var i = 0; i < radioBtn4.length; i++) {
				if (radioBtn4.item(i).value == "${req_distribution[0]}") {
					radioBtn4.item(i).checked = true;
				}
			}
			*/
			
			//var radioBtn5 = document.getElementsByName("req_type");
			/*
			var radioBtn15 = document.getElementsByClassName("req_type_1");
			for (var i = 0; i < radioBtn15.length; i++) {
				if (radioBtn5.item(i).value == "${req_type[0]}") {
					radioBtn5.item(i).checked = true;
				}
			}*/

		}
	</script>

</body>
</html>