<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html><!-- PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> --> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- sidebar -->
	<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="mainsidebar"
		role="navigation">
		<ul class="nav">
			<!-- <li class="active"><a href="SPN?cmd=overviewform"><strong>Overview</strong></a></li> -->
			<li class="active"><a href="SPN?cmd=infoform"><strong>General
						Info</strong></a></li>
			<li><a href="SPN?cmd=statisticform"><strong>Statistic</strong></a></li>
			<!-- <hr>  -->
			<!-- <li><a href="#"><strong>Family History</strong></a></li> -->

			<li class="nav-header"><a href="#" data-toggle="collapse"
				data-target="#Menu1" aria-expanded="false" class="collapsed"><strong>Laboratory
						<!-- Test</strong><span class="caret"></i></a> -->
						Test</strong><span class="caret"></span></a> <!-- span태그 닫는 부분을 추가 -->
				<ul class="nav nav-stacked collapse" id="Menu1"
					aria-expanded="false" style="height: 0px;">
					<li class="active"><a href="SPN?cmd=labsangerform">
							&nbsp;&nbsp;&nbsp;&nbsp;Sanger<br>&nbsp;&nbsp;&nbsp;&nbsp;Sequencing
					</a></li>
					<li class="active"><a href="SPN?cmd=labngsform">
							&nbsp;&nbsp;&nbsp;&nbsp;Next Generation<br>&nbsp;&nbsp;&nbsp;&nbsp;Sequencing
					</a></li>
					<li class="active"><a href="SPN?cmd=labcghform">
							&nbsp;&nbsp;&nbsp;&nbsp;CGH array</a></li>
					<li class="active"><a href="SPN?cmd=labstainform">
							&nbsp;&nbsp;&nbsp;&nbsp;Stain Muscle & Skin</a></li>
				</ul></li>

			<li class="nav-header"><a href="#" data-toggle="collapse"
				data-target="#Menu2" aria-expanded="false" class="collapsed"><strong>Sample
						Bank</strong><span class="caret"></i></a>
				<ul class="nav nav-stacked collapse" id="Menu2"
					aria-expanded="false" style="height: 0px;">
					<li class="active"><a href="SPN?cmd=samplednaform">
							&nbsp;&nbsp;&nbsp;&nbsp;DNA</a></li>
					<li class="active"><a href="SPN?cmd=sampletissueform">
							&nbsp;&nbsp;&nbsp;&nbsp;Tissue</a></li>
					<li class="active"><a href="SPN?cmd=samplecellform">
							&nbsp;&nbsp;&nbsp;&nbsp;Cell</a></li>		
					<li class="active"><a href="SPN?cmd=samplecfdnaform">
							&nbsp;&nbsp;&nbsp;&nbsp;Cell Free DNA</a></li>
					<li class="active"><a href="SPN?cmd=sampleserumform">
							&nbsp;&nbsp;&nbsp;&nbsp;Serum & CSF</a></li>
					<!-- 
					<li class="active"><a href="SPN?cmd=sampleurineform">
							&nbsp;&nbsp;&nbsp;&nbsp;Urine</a></li>
					-->

					<li class="active"><a href="SPN?cmd=samplernaform">
							&nbsp;&nbsp;&nbsp;&nbsp;RNA</a></li>
				</ul></li>
			<li>&nbsp;</li>
			<!-- <li><a href="#"><strong>Export</strong></a></li> -->
		</ul>
	</div>
</body>
</html>