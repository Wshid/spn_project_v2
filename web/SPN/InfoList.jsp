<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">

<title>SPN General Info</title>

<!--navbar-->
<jsp:include page="Navbar.jsp" flush="false" />

</head>
<body>

	<!--main page-->
	<div class="page-container">
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9"
				style="min-width: 700px; overflow-x: auto;">
				<h3>General Info</h3>

				<form name="historyForm" action="SPN?cmd=historyform"
					target="Window" method="post">
					<input class="btn btn-default" style="float: right;" type="button"
						value="History" onClick="popup_post4();"> <input
						type="hidden" value="111" name="hidden">
				</form>
				<br> <br> <br>

				<form method="post" action="spn?cmd=info">
					<table class="table table-bordered table-striped table-hover">
						<tr>
							<td><strong>SNUPN</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><strong>Chart No</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>

						</tr>
						<tr>
							<td><strong>Name</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><strong>Sex</strong></td>
							<td><input type="radio" name="GENDER" value="M">Male
								<input type="radio" name="GENDER" value="F">Female</td>

						</tr>
						<tr>
							<td><strong>Date of Birth</strong></td>
							<td><input type="date"
								style="border: transparent; background-color: transparent;"></td>
							<td><strong>Doctor</strong></td>
							<td><select>
									<option value="select">선택
									<option value="Doctor1">Doctor1
									<option value="Doctor2">Doctor2
									<option value="Docter3">Docter3
							</select></td>
						</tr>
						<tr>
							<td><strong>refered DR.</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><strong>분류</strong></td>
							<td><select>
									<option value="select">선택
									<option value="Muscle">Muscle
									<option value="Epilepsy">Epilepsy
									<option value="Others">Others
							</select></td>
						</tr>
						<tr>
							<td><strong>Final Diagnosis</strong></td>
							<td colspan="4"><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Clinical Diagnosis</strong></td>
							<td colspan="4"><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Symptom</strong></td>
							<td colspan="4"><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Remark</strong></td>
							<td colspan="4"><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
					</table>

					<button type="reset" class="btn btn-default" style="float: right;">Cancel</button>
					<button type="reset" class="btn btn-primary" style="float: right;">Change</button>
					<button type="submit" class="btn btn-primary" style="float: right;">Submit</button>



				</form>
			</div>
			<!-- /.col-xs-12 main -->
		</div>
		<!--/.row-->
	</div>
	<!--/.container-->
	</div>
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />

</body>
</html>
