<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html> <!--  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/modal.css?ver=13">
<link rel="stylesheet" type="text/css" href="./css/new_set.css?ver=12">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/xeicon@2.3.3/xeicon.min.css"> <!-- xeicon 테스트 -->
<link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/moonspam/NanumSquare/master/nanumsquare.css">
<title>SPN Login</title>
</head>
<body>

	<!-- navbar -->
	<nav class="navbar navbar-default navbar_reform">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<!-- <a class="navbar-brand" href="SPN?cmd=mainform"><strong>SNUH</strong></a> -->
			<a href="SPN?cmd=mainform"><img src="./images/logo.jpg"></a>
		</div>
	</div>
	</nav>

	
	<!--login modal-->
	<div id="loginModal" class="modal show" tabindex="-1" role="dialog"
		aria-hidden="true">

		<div style="padding-top: 300px;"></div>
		<div class="login_title" style="color:white; text-align:center; text-shadow: 0 3px 3px rgba(0, 0, 0, .6);">
			<span style="font-size:50pt; text-weight:bold;"> SPN Project</span>
			<p>서울대학교 소아신경과 차트 관리 어플리케이션</p>
		</div>
		
		<div class="modal-dialog">

			<div class="well bs-component login_modal_component">

 				<form class="form-horizontal" method="post" action="SPN?cmd=login">
<!-- 					<form class="form-horizontal" method="post" action="LoginPro.jsp">-->
					<fieldset>
						<!-- <h4>  --> <!--  legend 태그 사용시 field 태그와 붙어야함 -->
						
							<span style="font-size:20pt; color:white; font-weight:bold; margin-top:0px; margin-bottom:30px">Login</span>
						<!--  </h4> -->
						<hr />
						<div class="form-group">
							<label for="id" class="col-lg-2 control-label">Email</label>
							<div class="col-lg-10">
								<input type="text" class="form-control" id="id" name="id"
									placeholder="Input Email">
							</div>
						</div>
						<div class="form-group">
							<label for="pw" class="col-lg-2 control-label">Password</label>
							<div class="col-lg-10">
								<input type="password" class="form-control" id="pw" name="pw"
									placeholder="Input Password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-10 col-lg-offset-2">
								<button type="submit" class="btn btn-primary btn-lg btn-block">Login</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>

		<!--login modal-->
		<!-- 	<div id="loginModal" class="modal show" tabindex="-1" role="dialog"
		aria-hidden="true">

		<div style="padding-top: 250px;"></div>

		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h1 class="text-center">Login</h1>
				</div>

				<div class="modal-body">
					<form class="form col-md-12 center-block" method="post"
						action="SPN?cmd=login">
						<div class="form-group">
							<input type="text" id="id" name="id"
								class="form-control input-lg" placeholder="Input ID">
						</div>

						<div class="form-group">
							<input type="password" id="pw" name="pw"
								class="form-control input-lg" placeholder="Input Password">
						</div>
						<div class="form-group">
							<button class="btn btn-primary btn-lg btn-block">Login</button>
						</div>
					</form>
				</div>

				<div class="modal-footer"></div>
			</div>
		</div>
	</div>  -->
	</div>


		<!-- <script src="./js/bootstrap.min.js"></script> -->
</body>
</html>