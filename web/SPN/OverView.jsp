<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html><!-- PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> --> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css?ver=1">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>

<title>SPN OverView</title>

<script type="text/javascript">
	function updateSubmitForm() {
		form = document.getElementById('infoinput');
		form.action = 'SPN?cmd=infoupdate';
		form.submit();
	}
</script>

</head>
<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9" style="overflow: auto">
				<h3>Over View</h3>
				<form method="post" action="SPN?cmd=overview" name="overviewinput"
					id="overviewinput">
					<br> <br> 
					
					<font color="#0191D0" style="float: right;">${iid} &nbsp;|&nbsp; ${idate}</font><br>
					<table class="table table-bordered table-striped table-hover table_reform">
					<!-- 	style="width: 50%; float: right;"> -->
						<tr>
							<td colspan="2"><strong>Lab Test</strong></td>
							<td><strong>Order</strong></td>
							<td><strong>OnGoing</strong></td>
							<td><strong>Done</strong></td>
						</tr>
						<tr>
							<td colspan="2">Sanger Sequencing</td>
							<td><input type="checkbox" name="sanger_order" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="sanger_ongoing" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="sanger_done" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						<tr>
							<td rowspan="3">Next<br>generation<br>Sequencing
							</td>
							<td>WGS</td>
							<td><input type="checkbox" name="ngs_wgs_order" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="ngs_wgs_ongoing" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="ngs_wgs_done" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>WES</td>
							<td><input type="checkbox" name="ngs_wes_order" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="ngs_wes_ongoing" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="ngs_wes_done" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Targeted NGS</td>
							<td><input type="checkbox" name="ngs_targeted_order"
								value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="ngs_targeted_ongoing"
								value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="ngs_targeted_done"
								value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td colspan="2">CGH array</td>
							<td><input type="checkbox" name="cgh_order" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="cgh_ongoing" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="cgh_done" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td colspan="2">Muscle Stain</td>
							<td><input type="checkbox" name="muscle_stain_order"
								value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="muscle_stain_ongoing"
								value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="muscle_stain_done"
								value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>

						<tr>
							<td colspan="2">Western</td>
							<td><input type="checkbox" name="western_order" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="western_ongoing" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="western_done" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td colspan="2">Other</td>
							<td><input type="checkbox" name="other_order" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="other_ongoing" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td><input type="checkbox" name="other_done" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>

					</table>

					<table class="table table-bordered table-striped table-hover table_reform">
						<!-- style="width: 50%; float: right;"> -->
						<tr>
							<td colspan="2"><strong>Sample Bank</strong></td>
							<td><strong>Y/N</strong></td>
						</tr>
						<tr>
							<td rowspan="3">DNA</td>
							<td>Genomic DNA</td>
							<td><input type="checkbox" name="dna_genomic" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Cell-Free DNA</td>
							<td><input type="checkbox" name="dna_cellfree" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Mitochondria DNA</td>
							<td><input type="checkbox" name="dna_mitochondria" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td rowspan="4">Tissue</td>
							<td>Muscle</td>
							<td><input type="checkbox" name="tissue_muscle" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Myoblast</td>
							<td><input type="checkbox" name="tissue_myoblast" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Skin</td>
							<td><input type="checkbox" name="tissue_skin" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Fibroblast</td>
							<td><input type="checkbox" name="tissue_fibroblast"
								value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td colspan="2">Cell-Free DNA</td>
							<td><input type="checkbox" name="cell_free_dna" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td colspan="2">Serum</td>
							<td><input type="checkbox" name="serum" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td colspan="2">CSF</td>
							<td><input type="checkbox" name="csf" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td colspan="2">Urine Cell</td>
							<td><input type="checkbox" name="urine_cell" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td colspan="2">UiPSC</td>
							<td><input type="checkbox" name="uipsc" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td colspan="2">RNA</td>
							<td><input type="checkbox" name="rna" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td colspan="2">Informed Consent</td>
							<td><input type="checkbox" name="informed_consent" value="Y"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>

					</table>
					<input type="hidden" name="chart_no" value="${chart_no}" />
					<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
					<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
					<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm()">Change</button>
					<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>

				</form>
			</div>
			<!-- /.col-xs-12 main -->
		</div>
		<!--/.container-->
	</div>

	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
					<p>
						<font size=2>
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000 |
								EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL
							</font></p>
					</center>
				</div>
			</div>
		</div>
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />

	<!-- <script language="JavaScript">-->
	<script type="text/javascript">
		function initSet() {
			var checkBox1 = document.getElementsByName("dna_genomic");
			if ("${dna_genomic}" == "Y") {
				checkBox1.item(0).checked = true;
			}

			var checkBox2 = document.getElementsByName("dna_cellfree");
			if ("${dna_cellfree}" == "Y") {
				checkBox2.item(0).checked = true;
			}

			var checkBox3 = document.getElementsByName("dna_mitochondria");
			if ("${dna_mitochondria}" == "Y") {
				checkBox3.item(0).checked = true;
			}

			var checkBox4 = document.getElementsByName("tissue_muscle");
			if ("${tissue_muscle}" == "Y") {
				checkBox4.item(0).checked = true;
			}

			var checkBox5 = document.getElementsByName("tissue_myoblast");
			if ("${tissue_myoblast}" == "Y") {
				checkBox5.item(0).checked = true;
			}

			var checkBox6 = document.getElementsByName("tissue_skin");
			if ("${tissue_skin}" == "Y") {
				checkBox6.item(0).checked = true;
			}

			var checkBox7 = document.getElementsByName("tissue_fibroblast");
			if ("${tissue_fibroblast}" == "Y") {
				checkBox7.item(0).checked = true;
			}

			var checkBox8 = document.getElementsByName("cell_free_dna");
			if ("${cell_free_dna}" == "Y") {
				checkBox8.item(0).checked = true;
			}

			var checkBox9 = document.getElementsByName("serum");
			if ("${serum}" == "Y") {
				checkBox9.item(0).checked = true;
			}

			var checkBox10 = document.getElementsByName("csf");
			if ("${csf}" == "Y") {
				checkBox10.item(0).checked = true;
			}

			var checkBox11 = document.getElementsByName("urine_cell");
			if ("${urine_cell}" == "Y") {
				checkBox11.item(0).checked = true;
			}

			var checkBox12 = document.getElementsByName("uipsc");
			if ("${uipsc}" == "Y") {
				checkBox12.item(0).checked = true;
			}

			var checkBox13 = document.getElementsByName("rna");
			if ("${rna}" == "Y") {
				checkBox13.item(0).checked = true;
			}

			var checkBox14 = document.getElementsByName("informed_consent");
			if ("${informed_consent}" == "Y") {
				checkBox14.item(0).checked = true;
			}

			var checkBox15 = document.getElementsByName("sanger_order");
			if ("${sanger_order}" == "Y") {
				checkBox15.item(0).checked = true;
			}

			var checkBox16 = document.getElementsByName("sanger_ongoing");
			if ("${sanger_ongoing}" == "Y") {
				checkBox16.item(0).checked = true;
			}

			var checkBox17 = document.getElementsByName("sanger_done");
			if ("${sanger_done}" == "Y") {
				checkBox17.item(0).checked = true;
			}

			var checkBox18 = document.getElementsByName("ngs_wgs_order");
			if ("${ngs_wgs_order}" == "Y") {
				checkBox18.item(0).checked = true;
			}

			var checkBox19 = document.getElementsByName("ngs_wgs_ongoing");
			if ("${ngs_wgs_ongoing}" == "Y") {
				checkBox19.item(0).checked = true;
			}

			var checkBox20 = document.getElementsByName("ngs_wgs_done");
			if ("${ngs_wgs_done}" == "Y") {
				checkBox20.item(0).checked = true;
			}

			var checkBox21 = document.getElementsByName("ngs_wes_order");
			if ("${ngs_wes_order}" == "Y") {
				checkBox21.item(0).checked = true;
			}

			var checkBox22 = document.getElementsByName("ngs_wes_ongoing");
			if ("${ngs_wes_ongoing}" == "Y") {
				checkBox22.item(0).checked = true;
			}

			var checkBox23 = document.getElementsByName("ngs_wes_done");
			if ("${ngs_wes_done}" == "Y") {
				checkBox23.item(0).checked = true;
			}

			var checkBox24 = document.getElementsByName("ngs_targeted_order");
			if ("${ngs_targeted_order}" == "Y") {
				checkBox24.item(0).checked = true;
			}

			var checkBox25 = document.getElementsByName("ngs_targeted_ongoing");
			if ("${ngs_targeted_ongoing}" == "Y") {
				checkBox25.item(0).checked = true;
			}

			var checkBox26 = document.getElementsByName("ngs_targeted_done");
			if ("${ngs_targeted_done}" == "Y") {
				checkBox26.item(0).checked = true;
			}

			var checkBox27 = document.getElementsByName("cgh_order");
			if ("${cgh_order}" == "Y") {
				checkBox27.item(0).checked = true;
			}

			var checkBox28 = document.getElementsByName("cgh_ongoing");
			if ("${cgh_ongoing}" == "Y") {
				checkBox28.item(0).checked = true;
			}

			var checkBox29 = document.getElementsByName("cgh_done");
			if ("${cgh_done}" == "Y") {
				checkBox29.item(0).checked = true;
			}

			var checkBox30 = document.getElementsByName("muscle_stain_order");
			if ("${muscle_stain_order}" == "Y") {
				checkBox30.item(0).checked = true;
			}

			var checkBox31 = document.getElementsByName("muscle_stain_ongoing");
			if ("${muscle_stain_ongoing}" == "Y") {
				checkBox31.item(0).checked = true;
			}

			var checkBox32 = document.getElementsByName("muscle_stain_done");
			if ("${muscle_stain_done}" == "Y") {
				checkBox32.item(0).checked = true;
			}

			var checkBox33 = document.getElementsByName("western_order");
			if ("${western_order}" == "Y") {
				checkBox33.item(0).checked = true;
			}

			var checkBox34 = document.getElementsByName("western_ongoing");
			if ("${western_ongoing}" == "Y") {
				checkBox34.item(0).checked = true;
			}

			var checkBox35 = document.getElementsByName("western_done");
			if ("${western_done}" == "Y") {
				checkBox35.item(0).checked = true;
			}

			var checkBox36 = document.getElementsByName("other_order");
			if ("${other_order}" == "Y") {
				checkBox36.item(0).checked = true;
			}

			var checkBox37 = document.getElementsByName("other_ongoing");
			if ("${other_ongoing}" == "Y") {
				checkBox37.item(0).checked = true;
			}

			var checkBox38 = document.getElementsByName("other_done");
			if ("${other_done}" == "Y") {
				checkBox38.item(0).checked = true;
			}

		}
	</script>



</body>
</html>