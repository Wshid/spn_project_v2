<%@page import="java.util.ArrayList"%>
<%@page import="spn.dto.HistoryDTO"%>
<%@page import="java.util.List"%>
<%@page import="spn.dao.HistoryDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css">
<link rel="stylesheet" type="text/css" href="./css/modal.css?">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<title>SPN Serum & CSF</title>

<script type="text/javascript">
function updateSubmitForm()
{	
	form=document.getElementById('seruminput');
    if(checkIt('sample_serum')) form.action='SPN?cmd=sampleserumupdate';
    else return false;
    form.submit();
}
</script>

</head>
<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- info sub -->
			<jsp:include page="InfoSub.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9 table_right">
				<h3>Serum & CSF</h3>

				<!-- <button type="submit" class="btn btn-default" style="float: right;">History</button> -->
					
				<%
					HistoryDAO dao=HistoryDAO.getHistoryDAO();
					List<HistoryDTO> his_li=new ArrayList<HistoryDTO>();
					
					String menu="sample_serum";
					String searchVal=(String)session.getAttribute("searchVal");
					
					his_li=(List)dao.selectHistory(searchVal, menu);
					System.out.println("History List");
					System.out.println(his_li);
					System.out.println(his_li.size());
					if(his_li.size()!=0){
					%>
						<form name="historyForm" action="SPN?cmd=historyform"
					target="Window" method="post">
					<input type="hidden" name="page_name" value="sample_serum" />
					<input class="btn btn-default" style="float: right;" type="button"
						value="History" onClick="popup_post4();"> <input
						type="hidden" value="111" name="hidden">
				</form>	
					
						<%
					}
					
				%>	
				
				<br> <br> <br>

				<form class="spn_input_form" method="post" action="SPN?cmd=sampleserum" name="seruminput" id="seruminput" onSumbit="return checkIt('sample_serum')">
					<font color="#0191D0" style="float: right;">${iid} &nbsp;|&nbsp; ${idate} </font><br>
					<table class="table table-bordered table-striped table-hover">
							<!-- style="width: auto; float: right;"> -->
							
							<tr>
								<td rowspan=2 ><strong>Refered Site</strong></td>
								<td><strong>Date</strong></td>
								<td><input type="date" name="req_date" value="${req_date}"
									style="border: transparent; background-color: transparent;"></td>
							</tr>
							
							<tr>
								<td><strong>Location</strong></td>
								<td><input type="text" name="req_location" value="${req_location}"
									style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
							</tr>
							
							<tr>
								<td><strong>Order Name</strong></td>
								<td colspan=2><input type="text" name="req_order_name" value="${req_order_name}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							
							<tr>
								<td><strong>Number of Vial</strong></td>
								<td colspan=2><input type="text" name="req_vial" value="${req_vial}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>Comment</strong></td>
								<td colspan=2><input type="text" name="req_comment"
									value="${req_comment}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
						</table>
						<table class="table table-bordered table-striped table-hover">
							<!--  style="width: 65%;">-->
							<tr>
								<td><strong>Experiment</strong></td>
								<td><input type="checkbox" name="experiment_serum" value="Y">&nbsp; Serum
									&nbsp;&nbsp;&nbsp; <input type="checkbox" name="experiment_csf" value="Y">&nbsp;CSF</td>
							</tr>
							<tr>
								<td><strong>Collection Date</strong></td>
								<td><input type="date" name="col_date" value="${col_date}"
									style="border: transparent; background-color: transparent;"></td>
							</tr>
							<tr>
								<td><strong>Distribution Date</strong></td>
								<td><input type="date" name="dis_date" value="${dis_date}"
									style="border: transparent; background-color: transparent;"></td>
							</tr>
							<!-- 
							<tr>
								<td><strong>Index No</strong></td>
								<td><input type="text" name="col_index" value="${col_index}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							 -->
							<tr>
								<td><strong>Serum/CSF No</strong></td>
								<td><input type="text" name="no" value="${no}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>Storage Location</strong></td>
								<td><input type="text" name="storage" value="${storage}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							
							<tr>
								<td><strong>Order Name</strong></td>
								<td>
									<input type="checkbox" name="aquaporin_4_ab" value="Y">&nbsp;Aquaporin 4 Ab<br />
									<!-- style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td> -->
									<input type="checkbox" name="autoimmune_encephalitis" value="Y">&nbsp;Autoimmune Encephalitis<br />
									<input type="checkbox" name="order_other" value="Y">&nbsp;Other
										<input type="text" name="order_other_detail" value="${order_other_detail}" style="width: 60%"></td>
								</td>
									<!-- style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td> -->
							</tr>
							<!-- 
							<tr>
								<td><strong>Aquaporin 4 Ab</strong></td>
								<td><input type="checkbox" name="aquaporin_4_ab" value="Y">
									<!-- style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>Autoimmune Encephalitis</strong></td>
								<td><input type="checkbox" name="autoimmune_encephalitis"
									value="Y"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							 -->
							<!-- 
							<tr>
								<td><strong>place of experiment</strong></td>
								<td><input type="text" name="experiment_place"
									value="${experiment_place}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							 -->
							<!-- 
							<tr>
								<td><strong>Location</strong></td>
								<td><input type="text" name="location" value="${location}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							 -->
													<tr>
								<td><strong>Comment</strong></td>
								<td><input type="text" name="comment_1" value="${comment_1}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<!-- 
							<tr>
								<td><strong>Comment</strong></td>
								<td><input type="text" name="comment_2" value="${comment_2}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							 -->
						</table>
						<input type="hidden" name="chart_no" value="${chart_no}" />
						<input type="hidden" name="sc_no" value="${sc_no}" />
						<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
						<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
						
						<!-- <button type="submit" class="btn btn-primary" style="float: right;" onclick="updateSubmitForm()">Change</button> -->
						<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button> -->
						<%
							System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
							String check=(String)request.getAttribute("sc_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.
							//String searchSangerNumber=(String)session.getAttribute("iid"); // null
							/* Enumeration으로 Session 체크, searchval, pw, id 밖에 없었음
							Enumeration<String> attr=session.getAttributeNames();
	
							while(attr.hasMoreElements()){
								System.out.println(attr.nextElement());
							}*/
							
							//System.out.println("SS_NO in SEss : "+searchSangerNumber);
							if(check==null || check.equals("")){
							//if(ss_no==0){
						%>
								<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
								<%
							}else{
								%>
								<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm()">Change</button>
											
								<%
							}
						%>							
						
				</form>
			</div>
			<!-- /.col-xs-12 main -->
		</div>
		<!--/.container-->
	</div>
	
	<div class="modal_container font_shadow"> </div>

	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
						<font size=2><p>
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
								| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL 
							</p></font>
					</center>
				</div>
			</div>
		</div>
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />
	<script src="//code.jquery.com/jquery-latest.min.js"></script> <!--  jqeury last version -->
    <script src="./js/bootstrap.min.js"></script>
    <script src="js/prev_str.js?ver=1"></script>
    <script src="js/modal.js?ver=4"></script> <!--  modal 관련 자바스크립트 함수 -->
    <script src="js/input_check.js?ver=3"></script>
    <script src="js/modal_script.js?ver=3"></script>
	

	<script language="JavaScript">
		function initSet() {
			
			var menu="sample_serum";
			modal_script(menu);
			check_chart(menu);
			/*
			var radioBtn = document.getElementsByName("experiment");
			for (var i = 0; i < radioBtn.length; i++) {
				if (radioBtn.item(i).value == "${experiment}") {
					radioBtn.item(i).checked = true;
				}
			}
			*/
			
			var checkBox1 = document.getElementsByName("aquaporin_4_ab");
			if ("${aquaporin_4_ab}" == "Y") {
				checkBox1.item(0).checked = true;
			}

			var checkBox2 = document.getElementsByName("autoimmune_encephalitis");
			if ("${autoimmune_encephalitis}" == "Y") {
				checkBox2.item(0).checked = true;
			}
			
			var checkBox3 = document.getElementsByName("order_other");
			if ("${order_other}" == "Y") {
				checkBox3.item(0).checked = true;
			}
			
			var checkBox4 = document.getElementsByName("experiment_serum");
			if ("${experiment_serum}" == "Y") {
				checkBox4.item(0).checked = true;
			}
			
			var checkBox5 = document.getElementsByName("experiment_csf");
			if ("${experiment_csf}" == "Y") {
				checkBox5.item(0).checked = true;
			}

		}
	</script>

</body>
</html>