<%@page import="java.util.ArrayList"%>
<%@page import="spn.dto.HistoryDTO"%>
<%@page import="java.util.List"%>
<%@page import="spn.dao.HistoryDAO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css">
<link rel="stylesheet" type="text/css" href="./css/file.css?ver=33534">
<link rel="stylesheet" type="text/css" href="./css/modal.css?">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<title>SPN Stain Muscle & Skin</title>

<script type="text/javascript">
function updateSubmitForm()
{	
	form=document.getElementById('staininput');
    //form.action='SPN?cmd=labstainupdate';
    if(checkIt('lab_stain')) form.action='SPN?cmd=labstainupdate';
    else return false;
    form.submit();
}
</script>

</head>
<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- info sub-->
			<jsp:include page="InfoSub.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9 table_right">
				<h3>Stain Muscle & Skin</h3>

				<!-- <button type="submit" class="btn btn-default" style="float: right;">History</button> -->
				
				
				<%
					HistoryDAO dao=HistoryDAO.getHistoryDAO();
					List<HistoryDTO> his_li=new ArrayList<HistoryDTO>();
					
					String menu="lab_stain";
					String searchVal=(String)session.getAttribute("searchVal");
					
					his_li=(List)dao.selectHistory(searchVal, menu);
					System.out.println("History List");
					System.out.println(his_li);
					System.out.println(his_li.size());
					if(his_li.size()!=0){
					%>
						<form name="historyForm" action="SPN?cmd=historyform" target="Window" method="post">
							<input type="hidden" name="page_name" value="lab_stain" />
							<input class="btn btn-default" style="float: right;" type="button" value="History"  onClick="popup_post4();"/> 
							<input type="hidden" value="111" name="hidden" />
						</form>
						<%
					}
					
				%>
				
				<br> <br> <br>

				<form class="spn_input_form" method="post" action="SPN?cmd=labstain" name="staininput"
					id="staininput" onSubmit="return checkIt('lab_stain')">
					<font color="#0191D0" style="float: right;">${iid} &nbsp;|&nbsp; ${idate} </font>
					<table class="table table-bordered table-striped table-hover">
						<tr>
							<td><strong>Experiment<font color="red">*</font></strong></td>
							<td><input type="radio" name="experiment" value="muscle">Muscle
								&nbsp; <input type="radio" name="experiment" value="skin">Skin</td>
							<td><strong>M-Index No<font color="red">*</font></strong></td>
							<td><input type="text" name="m_index_no" value="${m_index_no}"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Discription</strong></td>
							<td colspan="4"><input type="text" name="discription" value="${discription}"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Pathology Diagnosis</strong></td>
							<td colspan="4"><input type="text"
								name="pathology_diagnosis" value="${pathology_diagnosis}"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Result</strong></td>
							<td colspan="4"><input type="text" name="result" value="${result}"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Comment</strong></td>
							<td colspan="4"><input type="text" name="comment" value="${comment}"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
					</table>

					<!--  right 관련된 부분이라, 인덱스가 19부터임 -->
					<table class="table table-bordered table-striped table-hover"
						style="width: 35%; float: right;">
						<tr>
							<td><strong>Exp. date</strong></td>
							<td colspan="6"><input type="date" name="exp_date_2" value="${exp_date_2}"
								style="border: transparent; background-color: transparent;"></td>
						</tr>
						<tr>
							<td><strong>Antibody</strong></td>
							<td><strong>Westorn Blot</strong></td>
						</tr>
						<tr>
							<td>Dys 1,2,3</td>
							<td><input type="checkbox" name="antibody_19" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td>Dysferlin</td>
							<td><input type="checkbox" name="antibody_20" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td>α-DG</td>
							<td><input type="checkbox" name="antibody_21" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td>Utrophine</td>
							<td><input type="checkbox" name="antibody_22" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td>Merosin</td>
							<td><input type="checkbox" name="antibody_23" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td>Calpain</td>
							<td><input type="checkbox" name="antibody_24" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td>‘α,β,γ,δ-SG</td>
							<td><input type="checkbox" name="antibody_25" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
					</table>

					<table class="table table-bordered table-striped table-hover"
						style="width: 60%">
						<tr>
							<td><strong>Exp. date</strong></td>
							<td colspan="6"><input type="date" name="exp_date_1" value="${exp_date_1}"
								style="border: transparent; background-color: transparent;"></td>
						</tr>
						<tr>
							<td><strong>Antibody</strong></td>
							<td><strong>IHC</strong></td>
							<td><strong>Antibody</strong></td>
							<td><strong>IHC</strong></td>
							<td><strong>Antibody</strong></td>
							<td><strong>Routine Stain</strong></td>
						</tr>
						<tr>
							<td>Dys 1,2,3</td>
							<td><input type="checkbox" name="antibody_1" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
							<td>Collagen6</td>
							<td><input type="checkbox" name="antibody_2" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
							<td>H&E</td>
							<td><input type="checkbox" name="antibody_3" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td>Dysferlin</td>
							<td><input type="checkbox" name="antibody_4" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
							<td>Desmin</td>
							<td><input type="checkbox" name="antibody_5" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
							<td>Gomori</td>
							<td><input type="checkbox" name="antibody_6" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td>α-DG</td>
							<td><input type="checkbox" name="antibody_7" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
							<td>Emerin</td>
							<td><input type="checkbox" name="antibody_8" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
							<td>SDH</td>
							<td><input type="checkbox" name="antibody_9" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td>Utrophine</td>
							<td><input type="checkbox" name="antibody_10" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
							<td>Calpain3</td>
							<td><input type="checkbox" name="antibody_11" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
							<td>COX</td>
							<td><input type="checkbox" name="antibody_12" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td>Merosin</td>
							<td><input type="checkbox" name="antibody_13" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
							<td>Laminin alpha2</td>
							<td><input type="checkbox" name="antibody_14" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
							<td>NADH</td>
							<td><input type="checkbox" name="antibody_15" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
						<tr>
							<td>Calpain</td>
							<td><input type="checkbox" name="antibody_16" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
							<td>Myosin</td>
							<td><input type="checkbox" name="antibody_17" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
							<td>α,β,γ,δ-SG</td>
							<td><input type="checkbox" name="antibody_18" value="Y"
								style="border: transparent; background-color: transparent; width: 100%;"></td>
						</tr>
					</table>
					<input type="hidden" name="chart_no" value="${chart_no}" />
					<input type="hidden" name="sms_no" value="${sms_no}" />
					<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
					<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
					<!-- <button type="submit" class="btn btn-primary" style="float: right;" onclick="updateSubmitForm()">Change</button> -->
					<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button> -->
					
					<%
						//System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
						String check_sms=(String)request.getAttribute("sms_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.

						if(check_sms==null || check_sms.equals("")){
						//if(ss_no==0){
					%>
							<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
							<%
						}else{
							%>
							<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm()">Change</button>
										
							<%
						}
					%>
				</form>
				
				<div class="picture_grid_container"> <!-- 인풋되는 사진을 기준으로 그리드 형태로 보여준다. --><!--  해당 사진을 클릭하였을 때 확대하여 보여질 수 있도록 한다. -->
					<h3> Pictures</h3>
					<button id="${page_name}_button_modal" type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal_${page_name}_input">Image Upload</button>
		  			<hr> <!--  선긋기 -->
					<input type="hidden" class="db_image" name="db_image" value="${db_image}" />
					<input type="hidden" class="names" name="names" value="${names}" />
					<input type="hidden" class="doctors" name="doctors" value="${doctors}" />	
					<input type="hidden" class="page_name" name="page_name" value="${page_name}" />				
	
					
					
					<form action="SPN?cmd=dbimageprocess" method="post" enctype="multipart/form-data">					
						<input type="hidden" name="img_idx" value="${img_idx}" />
						<input type="hidden" name="menu" value="lab_stain"/><br /> <!--  value 변경해주기 -->
						<input type="hidden" id="subtitle" name="subtitle" value="${next_idx}" /><br />					
						<div id="modal_${page_name}_input" class="modal fade" role="dialog">
						  <div class="modal-dialog">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button btn-primary" class="close" data-dismiss="modal">&times;</button>
						        <h4 class="modal-title">Image Upload</h4>
						      </div>
						      <div class="modal-body">
						         <p>
	
									<div class="filebox">
										<button class="replace btn-primary">Upload</button>  
										<input type="file" name="file" value="파일 업로드" class="upload"> <!--  name도 file로 해야함 -->
										<!-- <input type="file" name="file"/><br /> -->
									</div>
									
						         </p>
						      </div>
						      <div class="modal-footer">
						      	<button type="submit" class="btn btn-primary button_reform">Submit</button>
						        <button type="button" class="btn btn-default burron_reform" data-dismiss="modal">Close</button>
						      </div>
						    </div>
						
						  </div>
						</div>
					</form>				
					
				</div> <!--  picture grid container -->

			</div>
			<!-- /.col-xs-12 main -->
		</div>
		<!--/.container-->
	</div>

	<div class="modal_container font_shadow"> </div>

	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
						<font size=2><p>
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
								| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL 
							</p></font>
					</center>
				</div>
			</div>
		</div>
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />
	<script src="//code.jquery.com/jquery-latest.min.js"></script> <!--  jqeury last version -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/prev_str.js"></script>
    <script src="js/modal.js?ver=4"></script> <!--  modal 관련 자바스크립트 함수 -->
    <script src="js/input_check.js?ver=2"></script>
    <script src="js/modal_script.js?ver=2"></script>
	
	<script src="./js/picture_grid.js?ver=74224"></script>

	<script language="JavaScript">
		function initSet() {
			var menu="lab_stain";
			modal_script(menu);
			check_chart(menu);
			
			var radioBtn = document.getElementsByName("experiment");
			for (var i = 0; i < radioBtn.length; i++) {
				if (radioBtn.item(i).value == "${experiment}") {
					radioBtn.item(i).checked = true;
				}
			}
			
			var checkBox1 = document.getElementsByName("antibody_1");
			if ("${antibody_1}" == "Y") {
				checkBox1.item(0).checked = true;
			}
			
			var checkBox2 = document.getElementsByName("antibody_2");
			if ("${antibody_2}" == "Y") {
				checkBox2.item(0).checked = true;
			}
			
			var checkBox3 = document.getElementsByName("antibody_3");
			if ("${antibody_3}" == "Y") {
				checkBox3.item(0).checked = true;
			}
			
			var checkBox4 = document.getElementsByName("antibody_4");
			if ("${antibody_4}" == "Y") {
				checkBox4.item(0).checked = true;
			}
			
			var checkBox5 = document.getElementsByName("antibody_5");
			if ("${antibody_5}" == "Y") {
				checkBox5.item(0).checked = true;
			}
			
			var checkBox6 = document.getElementsByName("antibody_6");
			if ("${antibody_6}" == "Y") {
				checkBox6.item(0).checked = true;
			}
			
			var checkBox7 = document.getElementsByName("antibody_7");
			if ("${antibody_7}" == "Y") {
				checkBox7.item(0).checked = true;
			}
			
			var checkBox8 = document.getElementsByName("antibody_8");
			if ("${antibody_8}" == "Y") {
				checkBox8.item(0).checked = true;
			}
			
			var checkBox9 = document.getElementsByName("antibody_9");
			if ("${antibody_9}" == "Y") {
				checkBox9.item(0).checked = true;
			}
			
			var checkBox10 = document.getElementsByName("antibody_10");
			if ("${antibody_10}" == "Y") {
				checkBox10.item(0).checked = true;
			}
			
			var checkBox11 = document.getElementsByName("antibody_11");
			if ("${antibody_11}" == "Y") {
				checkBox11.item(0).checked = true;
			}
			
			var checkBox12 = document.getElementsByName("antibody_12");
			if ("${antibody_12}" == "Y") {
				checkBox12.item(0).checked = true;
			}
			
			var checkBox13 = document.getElementsByName("antibody_13");
			if ("${antibody_13}" == "Y") {
				checkBox13.item(0).checked = true;
			}
			
			var checkBox14 = document.getElementsByName("antibody_14");
			if ("${antibody_14}" == "Y") {
				checkBox14.item(0).checked = true;
			}
			
			var checkBox15 = document.getElementsByName("antibody_15");
			if ("${antibody_15}" == "Y") {
				checkBox15.item(0).checked = true;
			}
			
			var checkBox16 = document.getElementsByName("antibody_16");
			if ("${antibody_16}" == "Y") {
				checkBox16.item(0).checked = true;
			}
			
			var checkBox17 = document.getElementsByName("antibody_17");
			if ("${antibody_17}" == "Y") {
				checkBox17.item(0).checked = true;
			}
			
			var checkBox18 = document.getElementsByName("antibody_18");
			if ("${antibody_18}" == "Y") {
				checkBox18.item(0).checked = true;
			}
			
			var checkBox19 = document.getElementsByName("antibody_19");
			if ("${antibody_19}" == "Y") {
				checkBox19.item(0).checked = true;
			}
			
			var checkBox20 = document.getElementsByName("antibody_20");
			if ("${antibody_20}" == "Y") {
				checkBox20.item(0).checked = true;
			}
			
			var checkBox21 = document.getElementsByName("antibody_21");
			if ("${antibody_21}" == "Y") {
				checkBox21.item(0).checked = true;
			}
			
			var checkBox22 = document.getElementsByName("antibody_22");
			if ("${antibody_22}" == "Y") {
				checkBox22.item(0).checked = true;
			}
			
			var checkBox23 = document.getElementsByName("antibody_23");
			if ("${antibody_23}" == "Y") {
				checkBox23.item(0).checked = true;
			}
			
			var checkBox24 = document.getElementsByName("antibody_24");
			if ("${antibody_24}" == "Y") {
				checkBox24.item(0).checked = true;
			}
			
			var checkBox25 = document.getElementsByName("antibody_25");
			if ("${antibody_25}" == "Y") {
				checkBox25.item(0).checked = true;
			}
			
			
			
		}
	</script>

</body>
</html>