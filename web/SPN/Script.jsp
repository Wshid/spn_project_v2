<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

	<!-- script -->
	<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="./js/bootstrap.min.js"></script>
	<!--  <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script> -->
	<!--  <script src="../assets/js/custom.js"></script> -->

<!-- patient list pop-up -->
	<script language="javascript">
		function popup_post1() {

			window
					.open("", "Window",
							"width=450, height=600, menubar=no,status=yes,scrollbars=no");
			document.patientlistForm.submit();

		}
	</script>

	<!-- order list pop-up -->
	<script language="javascript">
		function popup_post2() {

			window
					.open("", "Window",
							"width=1000, height=300, menubar=no,status=yes,scrollbars=no");
			document.orderlistForm.submit();

		}
	</script>

	<!-- order list add pop-up -->
	<!-- 	<script language="javascript">
		function popup_post3() {

			window
					.open("", "Window",
							"width=1000, height=300, menubar=no,status=yes,scrollbars=no");
			document.orderlistaddform.submit();

		}
	</script> -->


	<!-- history pop-up -->
	<script language="javascript">
		function popup_post4() {

			window
					.open("", "Window",
							"width=1000, height=300, menubar=no,status=yes,scrollbars=no");
			document.historyForm.submit();

		}
	</script>



</body>
</html>