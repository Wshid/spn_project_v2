<%@page import="spn.dto.HistoryDTO"%>
<%@page import="java.util.List"%>
<%@page import="spn.dao.HistoryDAO"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css">
<link rel="stylesheet" type="text/css" href="./css/modal.css?">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<title>SPN Cell Free DNA</title>

<script type="text/javascript">
function updateSubmitForm(form_idx)
{	
	var forms=document.getElementsByName('cfdnainput');
	var form=forms[form_idx];
	//form.action='SPN?cmd=samplecfdnaupdate';
	if(checkIt('sample_cfdna')) form.action='SPN?cmd=samplecfdnaupdate';
   	else return false;
    /*for(i=0 ; i<forms.length;i++){
    	forms[i].action='SPN?cmd=samplecfdnaupdate';	
    }*/
    //alert(form);
    form.submit();
}

</script>


</head>
<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- info sub -->
			<jsp:include page="InfoSub.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9 table_right">
				<h3>Cell Free DNA</h3>

				<!-- <button type="submit" class="btn btn-default" style="float: right;">History</button> -->
				
				<%
					HistoryDAO dao=HistoryDAO.getHistoryDAO();
					List<HistoryDTO> his_li=new ArrayList<HistoryDTO>();
					
					String menu="sample_cfdna";
					String searchVal=(String)session.getAttribute("searchVal");
					
					his_li=(List)dao.selectHistory(searchVal, menu);
					System.out.println("History List");
					System.out.println(his_li);
					System.out.println(his_li.size());
					if(his_li.size()!=0){
					%>
						<form name="historyForm" action="SPN?cmd=historyform"
					target="Window" method="post">
					<input type="hidden" name="page_name" value="sample_cfdna" />
					<input class="btn btn-default" style="float: right;" type="button"
						value="History" onClick="popup_post4();"> <input
						type="hidden" value="111" name="hidden">
				</form>	
						<%
					}
					
				%>			
				<br> <br> <br>

				<ul class="nav nav-tabs">
					<li class="active"><a data-toggle="tab" href="#cfdna1">CFDNA 1</a></li>
					<li><a data-toggle="tab" href="#cfdna2">CFDNA 2</a></li>
					<li><a data-toggle="tab" href="#cfdna3">CFDNA 3</a></li>
					<li><a data-toggle="tab" href="#cfdna4">CFDNA 4</a></li>
				</ul>
				
				<div class="tab-content">
				  <div id="cfdna1" class="tab-pane fade in active">
				  <br>
				  <form method="post" action="SPN?cmd=samplecfdna" name="cfdnainput" id="cfdnainput">
					<font color="#0191D0" style="float: right;">${iid[0]} &nbsp;|&nbsp; ${idate[0]} </font><br>
					<table class="table table-bordered table-striped table-hover">
						<!-- style="width: 50%; float: right;"> -->
						  
						<tr>
							<td rowspan=2 ><strong>Refered Site</strong></td>
							<td><strong>Date</strong></td>
							<td><input type="date" name="req_date" value="${req_date[0]}"
								style="border: transparent; background-color: transparent;"></td>
						</tr>
						<tr>
							<td><strong>Location</strong></td>
							<td><input type="text" name="req_location" value="${req_location[0]}"
								style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
						</tr>
						
						<tr>
							<td><strong>Subject</strong>
							<td colspan=2><input class="col_who_0" type="radio" name="col_who" value="Patient">Patient&nbsp;
									<input type="text" name="col_who_patient_detail" value="${col_who_patient_detail[0]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_0" type="radio" name="col_who" value="Father">Father&nbsp;
									<input type="text" name="col_who_father_detail" value="${col_who_father_detail[0]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_0" type="radio" name="col_who" value="Mother">Mother&nbsp;
									<input type="text" name="col_who_mother_detail"value="${col_who_mother_detail[0]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_0" type="radio" name="col_who" value="Sibling">Sibling&nbsp;
									<input type="text" name="col_who_sibling_detail" value="${col_who_sibling_detail[0]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_0" type="radio" name="col_who" value="Other">Other&nbsp;
									<input type="text" name="col_who_other_detail" value="${col_who_other_detail[0]}" placeholder="Input Name" style="border: transparent; background-color: transparent;" style="width: 60%;">
						</tr>
						<!-- 
						<tr>
							<td><strong>Distribution Type</strong></td>
							<td colspan=2><input type="radio" name="req_distribution"
								value="External">External &nbsp; <input type="radio"
								name="req_distribution" value="Internal">Internal</td>
						</tr>
						 -->
						<tr>
							<td><strong>Order Name</strong></td>
							<td colspan=2><input type="text" name="req_order_name" value="${req_order_name[0]}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Number of Vial</strong></td>
							<td colspan=2><input type="text" name="req_vial" value="${req_vial[0]}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Comment</strong></td>
							<td colspan=2><input type="text" name="req_comment" value="${req_comment[0]}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						</table>
	
						<table class="table table-bordered table-striped table-hover">
							<!-- style="width: 50%"> -->
							
							<tr>
								<td rowspan=2 ><strong>Collection Site</strong></td>
								<td><strong>Date</strong></td>
								<td><input type="date" name="col_date" value="${col_date[0]}"
									style="border: transparent; background-color: transparent;"></td>
							</tr>
							<tr>
								<td><strong>Location</strong></td>
								<td><input type="text" name="col_location" value="${col_location[0]}"
									style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
							</tr>
							<!-- 
							<tr>
								<td rowspan=2 ><strong>Collection Site</strong></td>
								<td><strong>Date</strong></td>
								<td colspan="3"><input type="date" name="col_date" value="${col_date}"
									style="border: transparent; background-color: transparent;"></td>
							</tr>
							 -->
							 <!-- 
							<tr>
								<td><strong>Who</strong>
								<td colspan="3"><input type="radio" name="col_who"
									value="Patient">Patient<br> <input type="radio"
									name="col_who" value="Father">Father<br> <input
									type="radio" name="col_who" value="Mother">Mother<br>
									<input type="radio" name="col_who" value="Sibling">Sibling<br>
									<input type="radio" name="col_who" value="Other">Other&nbsp;
									<input type="text" name="col_who_other_detail" value="${col_who_other_detail[0]}"
									style="width: 60%">
							</tr>
							 -->
							<tr>
								<td><strong>Index No</strong></td>
								<td colspan="3"><input type="text" name="col_index" value="${col_index[0]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>WK</strong></td>
								<td colspan="3"><input type="text" name="col_wk" value="${col_wk[0]}"
									style="border: transparent; background-color: transparent; width: 80%;">&nbsp;<font color="#0191D0">Wk</font></td>
							</tr>
							<tr>
								<td><strong>Plasma Volume</strong></td>
								<td colspan="3"><input type="text" name="col_plasma_volume" value="${col_plasma_volume[0]}"
									style="border: transparent; background-color: transparent; width: 80%;">&nbsp;<font color="#0191D0">ml</font></td>
							</tr>
							<!-- 
							<tr>
								<td><strong>Cf No</strong></td>
								<td colspan="3"><input type="text" name="col_cf_no" value="${col_cf_no}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							 -->
							<tr>
								<td><strong>Concentration</strong></td>
								<td><input type="text" name="col_concentration_1" value="${col_concentration_1[0]}"
									style="border: transparent; background-color: transparent; width: 60%;">&nbsp;<font color="#0191D0">ng/ul</font></td>
								<td><input type="text" name="col_concentration_2" value="${col_concentration_2[0]}"
									style="border: transparent; background-color: transparent; width: 60%;">&nbsp;<font color="#0191D0">ng/ul</font></td>
							</tr>
							<tr>
								<td><strong>260/280</strong></td>
								<td><input type="text" name="col_260_280_1" value="${col_260_280_1[0]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								<td><input type="text" name="col_260_280_2" value="${col_260_280_2[0]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>260/230</strong></td>
								<td><input type="text" name="col_260_230_1" value="${col_260_230_1[0]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								<td><input type="text" name="col_260_230_2" value="${col_260_230_2[0]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>Bioanalyzer Size</strong></td>
								<td><input type="text" name="col_bioanalyzer_size_1" value="${col_bioanalyzer_size_1[0]}"
									style="border: transparent; background-color: transparent; width: 60%">&nbsp;<font color="#0191D0">bp</font></td>
								<td><input type="text" name="col_bioanalyzer_size_2" value="${col_bioanalyzer_size_2[0]}"
									style="border: transparent; background-color: transparent; width: 60%">&nbsp;<font color="#0191D0">bp</font></td>
							</tr>
							<tr>
								<td><strong>Bioanalzer Concentation</strong></td>
								<td><input type="text" name="col_bioanalyzer_concentation_1" value="${col_bioanalyzer_concentation_1[0]}"
									style="border: transparent; background-color: transparent; width: 60%">&nbsp;<font color="#0191D0">pg/ul</font></td>
								<td><input type="text" name="col_bioanalyzer_concentation_2" value="${col_bioanalyzer_concentation_2[0]}"
									style="border: transparent; background-color: transparent; width: 60%">&nbsp;<font color="#0191D0">pg/ul</font></td>
							</tr>
							<tr>
								<td><strong>Comment</strong></td>
								<td colspan="3"><input type="text" name="col_comment" value="${col_comment[0]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
						</table>
						<input type="hidden" name="chart_no" value="${chart_no}" />
						<input type="hidden" name="cfd_no" value="${cfd_no[0]}" />
						<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
						<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
						<!-- <button type="submit" class="btn btn-primary" style="float: right;" onclick="updateSubmitForm(0)">Change</button> -->
						<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button> -->
						
						<%
							System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
							ArrayList<String> check_list=(ArrayList<String>)request.getAttribute("cfd_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.
							//System.out.println("cfdna size : "+check_list.size());
							System.out.println(check_list);
							if(check_list.size()<1){
							
						%>
								<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
								<%
							}else{
								%>
								<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm(0)">Change</button>
											
								<%
							}
						%>			
					</form>
				  </div>
				  <div id="cfdna2" class="tab-pane fade">
				  <br>
				  <form method="post" action="SPN?cmd=samplecfdna" name="cfdnainput" id="cfdnainput">
					<font color="#0191D0" style="float: right;">${iid[1]} &nbsp;|&nbsp; ${idate[1]} </font><br>
					<table class="table table-bordered table-striped table-hover">
						<!-- style="width: 50%; float: right;"> -->
						  
						<tr>
							<td rowspan=2 ><strong>Refered Site</strong></td>
							<td><strong>Date</strong></td>
							<td><input type="date" name="req_date" value="${req_date[1]}"
								style="border: transparent; background-color: transparent;"></td>
						</tr>
						<tr>
							<td><strong>Location</strong></td>
							<td><input type="text" name="req_location" value="${req_location[1]}"
								style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
						</tr>
						
						<tr>
							<td><strong>Subject</strong>
							<td colspan=2><input class="col_who_1" type="radio" name="col_who" value="Patient">Patient&nbsp;
									<input type="text" name="col_who_patient_detail" value="${col_who_patient_detail[1]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_1" type="radio" name="col_who" value="Father">Father&nbsp;
									<input type="text" name="col_who_father_detail" value="${col_who_father_detail[1]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_1" type="radio" name="col_who" value="Mother">Mother&nbsp;
									<input type="text" name="col_who_mother_detail"value="${col_who_mother_detail[1]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_1" type="radio" name="col_who" value="Sibling">Sibling&nbsp;
									<input type="text" name="col_who_sibling_detail" value="${col_who_sibling_detail[1]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_1" type="radio" name="col_who" value="Other">Other&nbsp;
									<input type="text" name="col_who_other_detail" value="${col_who_other_detail[1]}" placeholder="Input Name" style="border: transparent; background-color: transparent;" style="width: 60%;">
						</tr>
						<!-- 
						<tr>
							<td><strong>Distribution Type</strong></td>
							<td colspan=2><input type="radio" name="req_distribution"
								value="External">External &nbsp; <input type="radio"
								name="req_distribution" value="Internal">Internal</td>
						</tr>
						 -->
						<tr>
							<td><strong>Order Name</strong></td>
							<td colspan=2><input type="text" name="req_order_name" value="${req_order_name[1]}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Number of Vial</strong></td>
							<td colspan=2><input type="text" name="req_vial" value="${req_vial[1]}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Comment</strong></td>
							<td colspan=2><input type="text" name="req_comment" value="${req_comment[1]}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						</table>
	
						<table class="table table-bordered table-striped table-hover">
							<!-- style="width: 50%"> -->
							
							<tr>
								<td rowspan=2 ><strong>Collection Site</strong></td>
								<td><strong>Date</strong></td>
								<td><input type="date" name="col_date" value="${col_date[1]}"
									style="border: transparent; background-color: transparent;"></td>
							</tr>
							<tr>
								<td><strong>Location</strong></td>
								<td><input type="text" name="col_location" value="${col_location[1]}"
									style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
							</tr>
							<!-- 
							<tr>
								<td rowspan=2 ><strong>Collection Site</strong></td>
								<td><strong>Date</strong></td>
								<td colspan="3"><input type="date" name="col_date" value="${col_date}"
									style="border: transparent; background-color: transparent;"></td>
							</tr>
							 -->
							 <!-- 
							<tr>
								<td><strong>Who</strong>
								<td colspan="3"><input type="radio" name="col_who"
									value="Patient">Patient<br> <input type="radio"
									name="col_who" value="Father">Father<br> <input
									type="radio" name="col_who" value="Mother">Mother<br>
									<input type="radio" name="col_who" value="Sibling">Sibling<br>
									<input type="radio" name="col_who" value="Other">Other&nbsp;
									<input type="text" name="col_who_other_detail" value="${col_who_other_detail[1]}"
									style="width: 60%">
							</tr>
							 -->
							<tr>
								<td><strong>Index No</strong></td>
								<td colspan="3"><input type="text" name="col_index" value="${col_index[1]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>WK</strong></td>
								<td colspan="3"><input type="text" name="col_wk" value="${col_wk[1]}"
									style="border: transparent; background-color: transparent; width: 80%;">&nbsp;<font color="#0191D0">Wk</font></td>
							</tr>
							<tr>
								<td><strong>Plasma Volume</strong></td>
								<td colspan="3"><input type="text" name="col_plasma_volume" value="${col_plasma_volume[1]}"
									style="border: transparent; background-color: transparent; width: 80%;">&nbsp;<font color="#0191D0">ml</font></td>
							</tr>
							<!-- 
							<tr>
								<td><strong>Cf No</strong></td>
								<td colspan="3"><input type="text" name="col_cf_no" value="${col_cf_no}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							 -->
							<tr>
								<td><strong>Concentration</strong></td>
								<td><input type="text" name="col_concentration_1" value="${col_concentration_1[1]}"
									style="border: transparent; background-color: transparent; width: 60%;">&nbsp;<font color="#0191D0">ng/ul</font></td>
								<td><input type="text" name="col_concentration_2" value="${col_concentration_2[1]}"
									style="border: transparent; background-color: transparent; width: 60%;">&nbsp;<font color="#0191D0">ng/ul</font></td>
							</tr>
							<tr>
								<td><strong>260/280</strong></td>
								<td><input type="text" name="col_260_280_1" value="${col_260_280_1[1]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								<td><input type="text" name="col_260_280_2" value="${col_260_280_2[1]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>260/230</strong></td>
								<td><input type="text" name="col_260_230_1" value="${col_260_230_1[1]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								<td><input type="text" name="col_260_230_2" value="${col_260_230_2[1]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>Bioanalyzer Size</strong></td>
								<td><input type="text" name="col_bioanalyzer_size_1" value="${col_bioanalyzer_size_1[1]}"
									style="border: transparent; background-color: transparent; width: 60%">&nbsp;<font color="#0191D0">bp</font></td>
								<td><input type="text" name="col_bioanalyzer_size_2" value="${col_bioanalyzer_size_2[1]}"
									style="border: transparent; background-color: transparent; width: 60%">&nbsp;<font color="#0191D0">bp</font></td>
							</tr>
							<tr>
								<td><strong>Bioanalzer Concentation</strong></td>
								<td><input type="text" name="col_bioanalyzer_concentation_1" value="${col_bioanalyzer_concentation_1[1]}"
									style="border: transparent; background-color: transparent; width: 60%">&nbsp;<font color="#0191D0">pg/ul</font></td>
								<td><input type="text" name="col_bioanalyzer_concentation_2" value="${col_bioanalyzer_concentation_2[1]}"
									style="border: transparent; background-color: transparent; width: 60%">&nbsp;<font color="#0191D0">pg/ul</font></td>
							</tr>
							<tr>
								<td><strong>Comment</strong></td>
								<td colspan="3"><input type="text" name="col_comment" value="${col_comment[1]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
						</table>
						<input type="hidden" name="chart_no" value="${chart_no}" />
						<input type="hidden" name="cfd_no" value="${cfd_no[1]}" />
						<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
						<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
						<!-- <button type="submit" class="btn btn-primary" style="float: right;" onclick="updateSubmitForm(0)">Change</button> -->
						<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button> -->
						
						<%
							System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
							//ArrayList<String> check_list=(ArrayList<String>)request.getAttribute("cfd_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.
							//System.out.println("cfdna size : "+check_list.size());
							System.out.println(check_list);
							if(check_list.size()<2){
							
						%>
								<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
								<%
							}else{
								%>
								<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm(1)">Change</button>
											
								<%
							}
						%>			
					</form>
				  </div>
				  <div id="cfdna3" class="tab-pane fade">
				  <br>
				  <form method="post" action="SPN?cmd=samplecfdna" name="cfdnainput" id="cfdnainput">
					<font color="#0191D0" style="float: right;">${iid[2]} &nbsp;|&nbsp; ${idate[2]} </font><br>
					<table class="table table-bordered table-striped table-hover">
						<!-- style="width: 50%; float: right;"> -->
						  
						<tr>
							<td rowspan=2 ><strong>Refered Site</strong></td>
							<td><strong>Date</strong></td>
							<td><input type="date" name="req_date" value="${req_date[2]}"
								style="border: transparent; background-color: transparent;"></td>
						</tr>
						<tr>
							<td><strong>Location</strong></td>
							<td><input type="text" name="req_location" value="${req_location[2]}"
								style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
						</tr>
						
						<tr>
							<td><strong>Subject</strong>
							<td colspan=2><input class="col_who_2" type="radio" name="col_who" value="Patient">Patient&nbsp;
									<input type="text" name="col_who_patient_detail" value="${col_who_patient_detail[2]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_2" type="radio" name="col_who" value="Father">Father&nbsp;
									<input type="text" name="col_who_father_detail" value="${col_who_father_detail[2]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_2" type="radio" name="col_who" value="Mother">Mother&nbsp;
									<input type="text" name="col_who_mother_detail"value="${col_who_mother_detail[2]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_2" type="radio" name="col_who" value="Sibling">Sibling&nbsp;
									<input type="text" name="col_who_sibling_detail" value="${col_who_sibling_detail[2]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_2" type="radio" name="col_who" value="Other">Other&nbsp;
									<input type="text" name="col_who_other_detail" value="${col_who_other_detail[2]}" placeholder="Input Name" style="border: transparent; background-color: transparent;" style="width: 60%;">
						</tr>
						<!-- 
						<tr>
							<td><strong>Distribution Type</strong></td>
							<td colspan=2><input type="radio" name="req_distribution"
								value="External">External &nbsp; <input type="radio"
								name="req_distribution" value="Internal">Internal</td>
						</tr>
						 -->
						<tr>
							<td><strong>Order Name</strong></td>
							<td colspan=2><input type="text" name="req_order_name" value="${req_order_name[2]}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Number of Vial</strong></td>
							<td colspan=2><input type="text" name="req_vial" value="${req_vial[2]}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Comment</strong></td>
							<td colspan=2><input type="text" name="req_comment" value="${req_comment[2]}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						</table>
	
						<table class="table table-bordered table-striped table-hover">
							<!-- style="width: 50%"> -->
							
							<tr>
								<td rowspan=2 ><strong>Collection Site</strong></td>
								<td><strong>Date</strong></td>
								<td><input type="date" name="col_date" value="${col_date[2]}"
									style="border: transparent; background-color: transparent;"></td>
							</tr>
							<tr>
								<td><strong>Location</strong></td>
								<td><input type="text" name="col_location" value="${col_location[2]}"
									style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
							</tr>
							<!-- 
							<tr>
								<td rowspan=2 ><strong>Collection Site</strong></td>
								<td><strong>Date</strong></td>
								<td colspan="3"><input type="date" name="col_date" value="${col_date}"
									style="border: transparent; background-color: transparent;"></td>
							</tr>
							 -->
							 <!-- 
							<tr>
								<td><strong>Who</strong>
								<td colspan="3"><input type="radio" name="col_who"
									value="Patient">Patient<br> <input type="radio"
									name="col_who" value="Father">Father<br> <input
									type="radio" name="col_who" value="Mother">Mother<br>
									<input type="radio" name="col_who" value="Sibling">Sibling<br>
									<input type="radio" name="col_who" value="Other">Other&nbsp;
									<input type="text" name="col_who_other_detail" value="${col_who_other_detail[2]}"
									style="width: 60%">
							</tr>
							 -->
							<tr>
								<td><strong>Index No</strong></td>
								<td colspan="3"><input type="text" name="col_index" value="${col_index[2]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>WK</strong></td>
								<td colspan="3"><input type="text" name="col_wk" value="${col_wk[2]}"
									style="border: transparent; background-color: transparent; width: 80%;">&nbsp;<font color="#0191D0">Wk</font></td>
							</tr>
							<tr>
								<td><strong>Plasma Volume</strong></td>
								<td colspan="3"><input type="text" name="col_plasma_volume" value="${col_plasma_volume[2]}"
									style="border: transparent; background-color: transparent; width: 80%;">&nbsp;<font color="#0191D0">ml</font></td>
							</tr>
							<!-- 
							<tr>
								<td><strong>Cf No</strong></td>
								<td colspan="3"><input type="text" name="col_cf_no" value="${col_cf_no}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							 -->
							<tr>
								<td><strong>Concentration</strong></td>
								<td><input type="text" name="col_concentration_2" value="${col_concentration_2[2]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								<td><input type="text" name="col_concentration_2" value="${col_concentration_2[2]}"
									style="border: transparent; background-color: transparent; width: 60%;">&nbsp;<font color="#0191D0">ng/ul</font></td>
							</tr>
							<tr>
								<td><strong>260/280</strong></td>
								<td><input type="text" name="col_260_280_2" value="${col_260_280_2[2]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								<td><input type="text" name="col_260_280_2" value="${col_260_280_2[2]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>260/230</strong></td>
								<td><input type="text" name="col_260_230_2" value="${col_260_230_2[2]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								<td><input type="text" name="col_260_230_2" value="${col_260_230_2[2]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>Bioanalyzer Size</strong></td>
								<td><input type="text" name="col_bioanalyzer_size_2" value="${col_bioanalyzer_size_2[2]}"
									style="border: transparent; background-color: transparent; width: 100%;"></td>
								<td><input type="text" name="col_bioanalyzer_size_2" value="${col_bioanalyzer_size_2[2]}"
									style="border: transparent; background-color: transparent; width: 60%">&nbsp;<font color="#0191D0">bp</font></td>
							</tr>
							<tr>
								<td><strong>Bioanalzer Concentation</strong></td>
								<td><input type="text" name="col_bioanalyzer_concentation_2" value="${col_bioanalyzer_concentation_2[2]}"
									style="border: transparent; background-color: transparent; width: 100%;"></td>
								<td><input type="text" name="col_bioanalyzer_concentation_2" value="${col_bioanalyzer_concentation_2[2]}"
									style="border: transparent; background-color: transparent; width: 60%">&nbsp;<font color="#0191D0">pg/ul</font></td>
							</tr>
							<tr>
								<td><strong>Comment</strong></td>
								<td colspan="3"><input type="text" name="col_comment" value="${col_comment[2]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
						</table>
						<input type="hidden" name="chart_no" value="${chart_no}" />
						<input type="hidden" name="cfd_no" value="${cfd_no[2]}" />
						<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
						<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
						<!-- <button type="submit" class="btn btn-primary" style="float: right;" onclick="updateSubmitForm(0)">Change</button> -->
						<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button> -->
						
						<%
							System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
							//ArrayList<String> check_list=(ArrayList<String>)request.getAttribute("cfd_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.
							//System.out.println("cfdna size : "+check_list.size());
							System.out.println(check_list);
							if(check_list.size()<3){
							
						%>
								<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
								<%
							}else{
								%>
								<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm(2)">Change</button>
											
								<%
							}
						%>			
					</form>
				  </div>
				  <div id="cfdna4" class="tab-pane fade">
				  <br>
				  <form method="post" action="SPN?cmd=samplecfdna" name="cfdnainput" id="cfdnainput">
					<font color="#0191D0" style="float: right;">${iid[3]} &nbsp;|&nbsp; ${idate[3]} </font><br>
					<table class="table table-bordered table-striped table-hover">
						<!-- style="width: 50%; float: right;"> -->
						  
						<tr>
							<td rowspan=2 ><strong>Refered Site</strong></td>
							<td><strong>Date</strong></td>
							<td><input type="date" name="req_date" value="${req_date[3]}"
								style="border: transparent; background-color: transparent;"></td>
						</tr>
						<tr>
							<td><strong>Location</strong></td>
							<td><input type="text" name="req_location" value="${req_location[3]}"
								style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
						</tr>
						
						<tr>
							<td><strong>Subject</strong>
							<td colspan=2><input class="col_who_3" type="radio" name="col_who" value="Patient">Patient&nbsp;
									<input type="text" name="col_who_patient_detail" value="${col_who_patient_detail[3]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_3" type="radio" name="col_who" value="Father">Father&nbsp;
									<input type="text" name="col_who_father_detail" value="${col_who_father_detail[3]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_3" type="radio" name="col_who" value="Mother">Mother&nbsp;
									<input type="text" name="col_who_mother_detail"value="${col_who_mother_detail[3]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_3" type="radio" name="col_who" value="Sibling">Sibling&nbsp;
									<input type="text" name="col_who_sibling_detail" value="${col_who_sibling_detail[3]}" placeholder="Input Name" style="border: transparent; background-color: transparent;"><br>
								<input class="col_who_3" type="radio" name="col_who" value="Other">Other&nbsp;
									<input type="text" name="col_who_other_detail" value="${col_who_other_detail[3]}" placeholder="Input Name" style="border: transparent; background-color: transparent;" style="width: 60%;">
						</tr>
						<!-- 
						<tr>
							<td><strong>Distribution Type</strong></td>
							<td colspan=2><input type="radio" name="req_distribution"
								value="External">External &nbsp; <input type="radio"
								name="req_distribution" value="Internal">Internal</td>
						</tr>
						 -->
						<tr>
							<td><strong>Order Name</strong></td>
							<td colspan=2><input type="text" name="req_order_name" value="${req_order_name[3]}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Number of Vial</strong></td>
							<td colspan=2><input type="text" name="req_vial" value="${req_vial[3]}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Comment</strong></td>
							<td colspan=2><input type="text" name="req_comment" value="${req_comment[3]}"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						</table>
	
						<table class="table table-bordered table-striped table-hover">
							<!-- style="width: 50%"> -->
							
							<tr>
								<td rowspan=2 ><strong>Collection Site</strong></td>
								<td><strong>Date</strong></td>
								<td><input type="date" name="col_date" value="${col_date[3]}"
									style="border: transparent; background-color: transparent;"></td>
							</tr>
							<tr>
								<td><strong>Location</strong></td>
								<td><input type="text" name="col_location" value="${col_location[3]}"
									style="border: transparent; background-color: transparent; width: 100%; height:100%;"></td>
							</tr>
							<!-- 
							<tr>
								<td rowspan=2 ><strong>Collection Site</strong></td>
								<td><strong>Date</strong></td>
								<td colspan="3"><input type="date" name="col_date" value="${col_date}"
									style="border: transparent; background-color: transparent;"></td>
							</tr>
							 -->
							 <!-- 
							<tr>
								<td><strong>Who</strong>
								<td colspan="3"><input type="radio" name="col_who"
									value="Patient">Patient<br> <input type="radio"
									name="col_who" value="Father">Father<br> <input
									type="radio" name="col_who" value="Mother">Mother<br>
									<input type="radio" name="col_who" value="Sibling">Sibling<br>
									<input type="radio" name="col_who" value="Other">Other&nbsp;
									<input type="text" name="col_who_other_detail" value="${col_who_other_detail[3]}"
									style="width: 60%">
							</tr>
							 -->
							<tr>
								<td><strong>Index No</strong></td>
								<td colspan="3"><input type="text" name="col_index" value="${col_index[3]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>WK</strong></td>
								<td colspan="3"><input type="text" name="col_wk" value="${col_wk[3]}"
									style="border: transparent; background-color: transparent; width: 80%;">&nbsp;<font color="#0191D0">Wk</font></td>
							</tr>
							<tr>
								<td><strong>Plasma Volume</strong></td>
								<td colspan="3"><input type="text" name="col_plasma_volume" value="${col_plasma_volume[3]}"
									style="border: transparent; background-color: transparent; width: 80%;">&nbsp;<font color="#0191D0">ml</font></td>
							</tr>
							<!-- 
							<tr>
								<td><strong>Cf No</strong></td>
								<td colspan="3"><input type="text" name="col_cf_no" value="${col_cf_no}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							 -->
							<tr>
								<td><strong>Concentration</strong></td>
								<td><input type="text" name="col_concentration_3" value="${col_concentration_3[3]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								<td><input type="text" name="col_concentration_3" value="${col_concentration_3[3]}"
									style="border: transparent; background-color: transparent; width: 60%;">&nbsp;<font color="#0191D0">ng/ul</font></td>
							</tr>
							<tr>
								<td><strong>260/280</strong></td>
								<td><input type="text" name="col_360_380_3" value="${col_360_380_3[3]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								<td><input type="text" name="col_360_380_3" value="${col_360_380_3[3]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>260/230</strong></td>
								<td><input type="text" name="col_360_330_3" value="${col_360_330_3[3]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
								<td><input type="text" name="col_360_330_3" value="${col_360_330_3[3]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
							<tr>
								<td><strong>Bioanalyzer Size</strong></td>
								<td><input type="text" name="col_bioanalyzer_size_3" value="${col_bioanalyzer_size_3[3]}"
									style="border: transparent; background-color: transparent; width: 100%;"></td>
								<td><input type="text" name="col_bioanalyzer_size_3" value="${col_bioanalyzer_size_3[3]}"
									style="border: transparent; background-color: transparent; width: 60%">&nbsp;<font color="#0191D0">bp</font></td>
							</tr>
							<tr>
								<td><strong>Bioanalzer Concentation</strong></td>
								<td><input type="text" name="col_bioanalyzer_concentation_3" value="${col_bioanalyzer_concentation_3[3]}"
									style="border: transparent; background-color: transparent; width: 100%;"></td>
								<td><input type="text" name="col_bioanalyzer_concentation_3" value="${col_bioanalyzer_concentation_3[3]}"
									style="border: transparent; background-color: transparent; width: 60%">&nbsp;<font color="#0191D0">pg/ul</font></td>
							</tr>
							<tr>
								<td><strong>Comment</strong></td>
								<td colspan="3"><input type="text" name="col_comment" value="${col_comment[3]}"
									style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							</tr>
						</table>
						<input type="hidden" name="chart_no" value="${chart_no}" />
						<input type="hidden" name="cfd_no" value="${cfd_no[3]}" />
						<input type="hidden" name="iid" value="<%=session.getAttribute("id")%>" />
						<button type="reset" class="btn btn-default button_reform" style="float: right;">Cancel</button>
						<!-- <button type="submit" class="btn btn-primary" style="float: right;" onclick="updateSubmitForm(0)">Change</button> -->
						<!-- <button type="submit" class="btn btn-primary" style="float: right;">Submit</button> -->
						
						<%
							System.out.println(request.getAttribute("his_li")); // session과 request는 엄연히 다름
							//ArrayList<String> check_list=(ArrayList<String>)request.getAttribute("cfd_no"); // 기존에 이미 있는 정보가 있는지 판단, submit/change를 분기한다.
							//System.out.println("cfdna size : "+check_list.size());
							System.out.println(check_list);
							if(check_list.size()<4){
							
						%>
								<button type="submit" class="btn btn-primary button_reform" style="float: right;">Submit</button>
								<%
							}else{
								%>
								<button type="submit" class="btn btn-primary button_reform" style="float: right;" onclick="updateSubmitForm(3)">Change</button>
											
								<%
							}
						%>			
					</form>
				  </div>
				 </div>
				
				
			</div>
			<!-- /.col-xs-12 main -->
		</div>
		<!--/.container-->
	</div>

	<div class="modal_container font_shadow"> </div>

	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
						<font size=2><p>
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
								| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL 
							</p></font>
					</center>
				</div>
			</div>
		</div>
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />
	<script src="//code.jquery.com/jquery-latest.min.js"></script> <!--  jqeury last version -->
    <script src="./js/bootstrap.min.js"></script>
    <script src="js/prev_str.js?ver=1"></script>
    <script src="js/modal.js?ver=4"></script> <!--  modal 관련 자바스크립트 함수 -->
    <script src="js/input_check.js"></script>
    <script src="js/modal_script.js"></script>
	

	<script language="JavaScript">
		function initSet() {
			
			var menu="sample_cfdna";
			modal_script(menu);
			check_chart(menu);
		/*
			var radioBtn1 = document.getElementsByName("col_who");
			for (var i = 0; i < radioBtn1.length; i++) {
				if (radioBtn1.item(i).value == "${col_who}") {
					radioBtn1.item(i).checked = true;
				}
			}
		*/
			/* col_who_0부터 _3까지 지정이 될 것은 확실한데 */
			for(var i=0;i<4;i++){
				var radio_temp=document.getElementsByClassName("col_who_"+i.toString()); // col_who_0
				console.log()
				for(var j=0;j<radio_temp.length;j++){
					var col_string="col_who["+j.toString()+"]"; 
					switch(i){
						case 0:
							if(radio_temp.item(j).value=="${col_who[0]}"){
								radio_temp.item(j).checked=true;
							}
							break;
						case 1:
							if(radio_temp.item(j).value=="${col_who[1]}"){
								radio_temp.item(j).checked=true;
							}
							break;
						case 2:
							if(radio_temp.item(j).value=="${col_who[2]}"){
								radio_temp.item(j).checked=true;
							}
							break;
						case 3:
							if(radio_temp.item(j).value=="${col_who[3]}"){
								radio_temp.item(j).checked=true;
							}
							break;
						default:
							console.log("SampleCfDNA Script ERROR");
							break;
					}
				}
			}
		/*`
			var radioBtn11 = document.getElementsByClassName("col_who_0");
			for (var i = 0; i < radioBtn11.length; i++) {
				if (radioBtn11.item(i).value == "${col_who[0]}") {
					radioBtn11.item(i).checked = true;
				}
			}
			*/
			
			var radioBtn2 = document.getElementsByName("req_who");
			for (var i = 0; i < radioBtn2.length; i++) {
				if (radioBtn2.item(i).value == "${req_who}") {
					radioBtn2.item(i).checked = true;
				}
			}
			
			var radioBtn3 = document.getElementsByName("req_distribution");
			for (var i = 0; i < radioBtn3.length; i++) {
				if (radioBtn3.item(i).value == "${req_distribution}") {
					radioBtn3.item(i).checked = true;
				}
			}

		}
	</script>

</body>
</html>