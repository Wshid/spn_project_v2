<%@page import="spn.dto.HistoryDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html><!-- PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> --> 
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<title>History</title>
</head>
<body>
<div class="wrapper style4 special container 60%" style="padding-top: 10px;">
<h3>History</h3>
<table class="table table-bordered table-striped table-hover">
<tr>
<td bgcolor="#F5F5F5">Index</td>
<td bgcolor="#F5F5F5">User ID</td>
<td bgcolor="#F5F5F5">User Name</td>
<td bgcolor="#F5F5F5">Chart_No</td>
<td bgcolor="#F5F5F5">SNUHN</td>
<td bgcolor="#F5F5F5">Patient Name</td>
<td bgcolor="#F5F5F5">Sex</td>
<td bgcolor="#F5F5F5">Modified Date</td>
</tr>
<%			
	System.out.println("AA : "+request.getAttribute("his_li"));

	List<HistoryDTO> li = new ArrayList<HistoryDTO>();
	li=(List) request.getAttribute("his_li");//new ArrayList<HistoryDTO>();
					
	if(li.size()!=0){
		for(int i=0;i<li.size();i++){
			out.print("<tr>");
			out.print("<td>"+li.get(i).getHis_idx()+"</td>");
			out.print("<td>"+li.get(i).getIid()+"</td>");
			out.print("<td>"+li.get(i).getName()+"</td>");
			out.print("<td>"+li.get(i).getChart_no()+"</td>");
			out.print("<td>"+li.get(i).getSnupn()+"</td>");
			out.print("<td>"+li.get(i).getPa_name()+"</td>");
			out.print("<td>"+li.get(i).getPa_sex()+"</td>");
			out.print("<td>"+li.get(i).getIdate()+"</td>");
			out.print("</tr>");
		}
	}
%>
<!-- 
<tr>
<td></td>
<td>b</td>
<td>c</td>
<td>c</td>
<td>a</td>
<td>d</td>
<td>c</td>
<td>c</td>
</tr>
<tr>
<td>a</td>
<td>d</td>
<td>c</td>
<td>c</td>
<td>a</td>
<td>d</td>
<td>c</td>
<td>c</td>
</tr>
<tr>
<td>a</td>
<td>d</td>
<td>c</td>
<td>c</td>
<td>a</td>
<td>d</td>
<td>c</td>
<td>c</td>
</tr>
<tr>
<td>a</td>
<td>d</td>
<td>c</td>
<td>c</td>
<td>a</td>
<td>d</td>
<td>c</td>
<td>c</td>
</tr>
 -->
</table>
</div>
</body>
</html>