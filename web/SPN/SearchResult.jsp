<%@page import="java.util.Enumeration"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="./css/new_set.css">
<link rel="stylesheet" type="text/css" href="./css/file.css?ver=33534">
<link rel="stylesheet" type="text/css" href="./css/modal.css?">
<link rel="stylesheet" type="text/css" href="./css/search.css?ver=56">
<link rel="shortcut icon" type='image/x-icon' href='./favicon.ico'>
<title>SPN Sanger Sequencing</title>

<script type="text/javascript">
	function updateSubmitForm()
	{	
		form=document.getElementById('sangerinput');
	    //form.action='SPN?cmd=labsangerupdate';
	    if(checkIt('lab_sanger')) form.action='SPN?cmd=labsangerupdate';
	    else return false;
	    
	    form.submit();
	}
	
	function searchMoveLink(chart_no, menu){ // 특정 블록을 클릭하게 되면, 이동한다.
		var search_form=document.search_link_form;
	    console.log(search_form); 
	    
	    search_form.searchVal.value=chart_no; // chart_no로 바꿈 
	    search_form.cmd.value=menu;
	    console.log("searchVal value : "+search_form.searchVal.value);
	    //console.log("infoform prev, searchMoveLink");
	    //search_form.action='SPN?cmd=infoform';
	    console.log('form action : '+search_form.action);
	    
	    search_form.submit();

	}
</script>

</head>
<body onload="initSet()">

	<div id="header">
		<!--navbar-->
		<jsp:include page="Navbar.jsp" flush="false" />
	</div>

	<div id="main">
		<!-- <div class="page-container"> -->
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- info sub -->
			<!--<jsp:include page="InfoSub.jsp" flush="false" /> -->

			<!-- main area -->
			<div class="col-xs-12 col-sm-9">
				<h3>Search Result</h3>
				<!--  JSON 데이터를 받아처리해야함
					일정 갯수별로 쪼개서 보여주는 것도 좋을 것 같고
					해당 데이터의 인덱스 만큼 전환하여 정렬하는 것도..
			 	-->
				<div class="search_container"></div>					 
				
					 
			</div>
			<!-- /.col-xs-12 main -->
			
			
		</div>
		<!--/.container-->
	</div>

	<!-- modal 관련 내용이 들어갈 부분 -->
	<div class="modal_container font_shadow"> </div>

	<div id="footer">
		<div class="row">
			<div class="col-lg-12">
				<div class="page-header">
					<br> <br>
					<center>
						<font size=2><p>
								서울대학교병원 소아신경과 | 서울특별시 종로구 대학로 101(연건동 28번지) | TEL 02-2072-0000
								| EMAIL aaaaa@snuh.org<br> © SEOUL NATIONAL UNIVERSITY
								HOSPITAL 
							</p></font>
					</center>
				</div>
			</div>
		</div>
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />
	<script src="//code.jquery.com/jquery-latest.min.js"></script> <!--  jqeury last version -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/prev_str.js"></script>
	<script src="js/modal.js?ver=4"></script> <!--  modal 관련 자바스크립트 함수 -->
	<script src="js/input_check.js?ver=4"></script>
	<script src="js/modal_script.js?ver=3"></script>
	<script src="js/search.js?ver=28"></script>
	
	<script language="JavaScript">
		var result_data=search_result(`${searchText}`, ${searchResult} );
		
		console.log(result_data);
	</script>

	<!--입력값 체크-->
	<script language="JavaScript">

	</script>

	<script language="JavaScript">
		function initSet() {
			//modal_script("lab_sanger");	
			
			

		}
	</script>
</body>
</html>