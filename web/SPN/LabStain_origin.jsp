<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="./css/bootstrap.min.css">
<title>SPN Stain Muscle</title>

<!--navbar-->
<jsp:include page="Navbar.jsp" flush="false" />

</head>
<body>
	<!--main page-->
	<div class="page-container">
		<div class="container">

			<!-- sidebar -->
			<jsp:include page="Sidebar.jsp" flush="false" />

			<!-- info -->
			<jsp:include page="InfoSub.jsp" flush="false" />

			<!-- main area -->
			<div class="col-xs-12 col-sm-9">
				<h3>Stain Muscle</h3>
				<form>
					<button type="submit" class="btn btn-default" style="float: right;">History</button>
					<br> <br> <br>

					<table class="table table-bordered table-striped table-hover">
						<tr>
							<td><strong>Exp. date</strong></td>
							<td><input type="date"
								style="border: transparent; background-color: transparent;"></td>
							<td><strong>M-Index No</strong></td>
							<td><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Discription</strong></td>
							<td colspan="4"><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td><strong>Pathology Diagnosis</strong></td>
							<td colspan="4"><input type="text"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
					</table>

					<table class="table table-bordered table-striped table-hover"
						style="width: 30%; float: right;">
						<tr>
							<td><strong>Antibody</strong></td>
							<td><strong>Routine Stain</strong></td>
						</tr>
						<tr>
							<td>H&E</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Gomori</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>SDH</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>COX</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>NADH</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
					</table>

					<table class="table table-bordered table-striped table-hover"
						style="width: 30%; float: right;">
						<tr>
							<td><strong>Antibody</strong></td>
							<td><strong>Westorn Blot</strong></td>
						</tr>
						<tr>
							<td>Dys 1,2,3</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Dysferlin</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>α-DG</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Utrophine</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Merosin</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Calpain</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>‘α,β,γ,δ-SG</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
					</table>

					<table class="table table-bordered table-striped table-hover"
						style="width: 40%">
						<tr>
							<td><strong>Antibody</strong></td>
							<td><strong>IHC</strong></td>
							<td><strong>Antibody</strong></td>
							<td><strong>IHC</strong></td>
						</tr>
						<tr>
							<td>Dys 1,2,3</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td>Collagen6</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Dysferlin</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td>Desmin</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>α-DG</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td>Emerin</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Utrophine</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td>Calpain3</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Merosin</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td>Laminin alpha2</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>Calpain</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td>Myosin</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
						</tr>
						<tr>
							<td>α,β,γ,δ-SG</td>
							<td><input type="checkbox"
								style="border: transparent; background-color: transparent; width: 100%; height: 100%;"></td>
							<td></td>
							<td></td>
						</tr>
					</table>

					<button type="reset" class="btn btn-default" style="float: right;">Cancel</button>
					<button type="submit" class="btn btn-primary" style="float: right;">Change</button>
					<button type="submit" class="btn btn-primary" style="float: right;">Submit</button>

				</form>
			</div>
			<!-- /.col-xs-12 main -->
		</div>
		<!--/.row-->
	</div>
	<!--/.container-->
	</div>

	<!-- script -->
	<jsp:include page="Script.jsp" flush="false" />


</body>
</html>