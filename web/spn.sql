/* member */
create table spn.member(
mb_no int auto_increment not null primary key,
id varchar(20),
pw varchar(20),
name varchar(20),
affiliation varchar(30),
phone varchar(20),
permission varchar(10),
regdate date
);

/*desc spn.member;*/

insert into spn.member(id, pw, name, affiliation, phone, permission, regdate) values ('spnadmin@snu.ac.kr', 'admin', 'Admin', 'snuh', '010-0000-0000', 'Admin', now());

create table spn.history(
his_idx int auto_increment not null primary key,
iid varchar(20) not null,
name varchar(20) not null,
chart_no varchar(30) not null,
snupn char(11) not null,
pa_name varchar(20) not null,
pa_sex char(2) not null,
page_name varchar(20) not null,
idate datetime not null
);

create table spn.db_image(
img_idx int auto_increment not null primary key,
chart_no varchar(30) not null,
image_name varchar(50) not null,
subtitle varchar(30), 
full_path varchar(1000) not null,
cdate datetime not null,
idate datetime not null,
doctor varchar(10) not null,
page_name varchar(100) not null
);

/* SPN INFO */
create table spn.info(
gi_no int auto_increment not null primary key,
snupn char(11) not null,
chart_no varchar(30) not null,
name varchar(50) not null,
sex char(2),
birth varchar(20),
doctor char(5),
/*refered_dr char(5),*/
other_site varchar(30),
category varchar(8),
clinical_diagnosis varchar(5000),
/*final_diagnosis varchar(5000),*/
symptom varchar(5000),
/*remark varchar(5000),*/
iid varchar(30),
idate timestamp not null
); /* general info table ���� �Ϸ� */
/*desc spn.info;
drop table spn.info;
select * from spn.info;
alter table spn.info change final_diganosis final_diagnosis varchar(5000);
select * from spn.info where snupn=11111111111;

alter table spn.info change other_site refered_dr varchar(30);*/

/* OVERVIEW */
create table spn.overview(
chart_no varchar(30) not null,
DNA_GENOMIC char(1),
DNA_CELLFREE char(1),
DNA_MITOCHONDRIA char(1),
TISSUE_MUSCLE char(1),
TISSUE_MYOBLAST char(1),
TISSUE_SKIN char(1),
TISSUE_FIBROBLAST char(1),
CELL_FREE_DNA char(1),
SERUM char(1),
CSF char(1),
URINE_CELL char(1),
UIPSC char(1),
RNA char(1),
INFORMED_CONSENT char(1),
SANGER_ORDER char(1),
SANGER_ONGOING char(1),
SANGER_DONE char(1),
NGS_WGS_ORDER char(1),
NGS_WGS_ONGOING char(1),
NGS_WGS_DONE char(1),
NGS_WES_ORDER char(1),
NGS_WES_ONGOING char(1),
NGS_WES_DONE char(1),
NGS_TARGETED_ORDER char(1),
NGS_TARGETED_ONGOING char(1),
NGS_TARGETED_DONE char(1),
CGH_ORDER char(1),
CGH_ONGOING char(1),
CGH_DONE char(1),
MUSCLE_STAIN_ORDER char(1),
MUSCLE_STAIN_ONGOING char(1),
MUSCLE_STAIN_DONE char(1),
WESTERN_ORDER char(1),
WESTERN_ONGOING char(1),
WESTERN_DONE char(1),
OTHER_ORDER char(1),
OTHER_ONGOING char(1),
OTHER_DONE char(1),
iid varchar(30),
idate timestamp not null
);


/* LAB =============================================================== */

create table spn.lab_sanger(
ss_no int auto_increment not null primary key,
chart_no text(30) not null,
exp_date text(1000),
gene text(2000),
position text(2000),
snp text(2000),
type text(2000),
de_novo text(1000),
mutation text(2000),
family_test_F text(1000),
family_test_M text(1000),
family_test_S text(1000),
result text(2000),
reported text(2000),
judge text(1000),
comment text(2000),
iid text(2000),
idate timestamp not null
);


create table spn.lab_ngs(
ngs_no int auto_increment not null primary key,
chart_no varchar(30) not null,
experiment varchar(20) not null,
exp_date varchar(20) not null,
case_no varchar(10),
exp_place varchar(20),
sample_yn_f char(1),
sample_yn_m char(1),
sample_yn_s char(1),
sample_yn_o char(1),
sample_yn_o_detail varchar(5000),
result varchar(10),
result_pending_detail varchar(5000),
comment varchar(5000),
iid varchar(30),
idate timestamp not null
);


create table spn.lab_cgh(
cgh_no int auto_increment not null primary key,
chart_no varchar(30) not null,
exp_date varchar(20),
chip_no varchar(15),
chromosome_study_yn char(1),
fish_confirmation_yn char(1),
qpcr_yn char(1),
platform varchar(20),
platform_other_detail varchar(5000),
parents_test_f char(1),
parents_test_m char(1),
cnv_type varchar(15),
pathogenic_result_location varchar(50),
pathogenic_result_size varchar(10),
pathogenic_result_url varchar(5000),
non_pathogenic_result_location varchar(50),
non_pathogenic_result_size varchar(10),
non_pathogenic_result_url varchar(5000),
comment varchar(5000),
iid varchar(30),
idate timestamp not null
);

create table spn.lab_stain(
sms_no int auto_increment not null primary key,
chart_no varchar(30) not null,
experiment varchar(10) not null,
m_index_no varchar(10) not null,
discription varchar(5000),
pathology_diagnosis varchar(5000),
result varchar(5000),
comment varchar(5000),
exp_date_1 varchar(20),
antibody_1 char(1),
antibody_2 char(1),
antibody_3 char(1),
antibody_4 char(1),
antibody_5 char(1),
antibody_6 char(1),
antibody_7 char(1),
antibody_8 char(1),
antibody_9 char(1),
antibody_10 char(1),
antibody_11 char(1),
antibody_12 char(1),
antibody_13 char(1),
antibody_14 char(1),
antibody_15 char(1),
antibody_16 char(1),
antibody_17 char(1),
antibody_18 char(1),
exp_date_2 varchar(20),
antibody_19 char(1),
antibody_20 char(1),
antibody_21 char(1),
antibody_22 char(1),
antibody_23 char(1),
antibody_24 char(1),
antibody_25 char(1),
iid varchar(30),
idate timestamp not null
);


/* Simple BAnk ==================================================== */

create table spn.sample_dna(
dna_no int auto_increment not null primary key,
chart_no varchar(30) not null,
col_date varchar(25), /* collection date */
col_who varchar(10),
col_who_patient_detail varchar(30),
col_who_father_detail varchar(30),
col_who_mother_detail varchar(30),
col_who_sibling_detail varchar(30),
col_who_other_detail varchar(30),
col_type varchar(20),
col_type_other_detail varchar(30),
col_index varchar(9),
col_agreement char(1),
col_performed_by varchar(30),
col_concentration varchar(20),
col_260_280 varchar(20),
col_260_230 varchar(20),
col_location varchar(15),
col_comment varchar(5000),
req_date varchar(20),
req_location varchar(50),
req_who varchar(10),
req_who_other_detail varchar(10),
req_distribution varchar(8),
req_order_name varchar(10),
req_type varchar(20),
req_type_other_detail varchar(30),
req_vial varchar(1),
req_comment varchar(5000),
iid varchar(30),
idate timestamp not null
);

/* SAMPLE_TISSUE */
create table spn.sample_tissue(
tis_no int auto_increment not null primary key,
chart_no varchar(30) not null,
bio_date varchar(20),
/*bio_type varchar(10),*/
bio_type_myoblast char(1),
bio_type_fibroblast char(1),
bio_type_u_epithelial char(1),
bio_type_u_ipsc char(1),
bio_type_other char(1),
bio_type_other_detail varchar(30),
bio_index_1 varchar(10),
/*bio_location_1 varchar(15),
bio_cell varchar(10),
bio_index_2 varchar(10),
bio_phase varchar(2),*/
bio_storage varchar(50),
/*bio_location_2 varchar(15),*/
bio_comment varchar(5000),
req_date varchar(20),
req_location varchar(50),
/*req_distribution varchar(8),*/
req_order_name varchar(10),
/*req_type varchar(10),
req_type_other_detail varchar(30),*/
req_vial varchar(1),
req_comment varchar(5000),
iid varchar(30),
idate timestamp not null
);

/* SAMPLE_CELL*/
create table spn.sample_cell(
ce_no int auto_increment not null primary key,
chart_no varchar(30) not null,
col_date varchar(20),
/*bio_type varchar(10),*/
cell_type_myoblast char(1),
cell_type_fibroblast char(1),
cell_type_u_epithelial char(1),
cell_type_u_ipsc char(1),
cell_type_other char(1),
cell_type_other_detail varchar(30),
cell_index varchar(10),
cell_storage varchar(50),
cell_phase varchar(100),
cell_comment varchar(5000),
req_date varchar(20),
req_location varchar(50),
req_order_name varchar(10),
req_vial varchar(1),
req_comment varchar(5000),
iid varchar(30),
idate timestamp not null
);

/* SAMPLE_CEll FREE DNA */
create table spn.sample_cfdna(
cfd_no int auto_increment not null primary key,
chart_no varchar(30) not null,
col_date varchar(20),
col_location varchar(20),
col_who varchar(10),
col_who_patient_detail varchar(30),
col_who_father_detail varchar(30),
col_who_mother_detail varchar(30),
col_who_sibling_detail varchar(30),
col_who_other_detail varchar(30),
col_index varchar(9),
col_wk varchar(2),
col_plasma_volume varchar(2),
col_cf_no varchar(20),
col_concentration_1 varchar(2),
col_concentration_2 varchar(2),
col_260_280_1 varchar(2),
col_260_280_2 varchar(2),
col_260_230_1 varchar(2),
col_260_230_2 varchar(2),
COL_BIOANALYZER_SIZE_1 varchar(3),
COL_BIOANALYZER_SIZE_2 varchar(3),
COL_BIOANALYZER_CONCENTATION_1 varchar(6),
COL_BIOANALYZER_CONCENTATION_2 varchar(6),
COL_COMMENT varchar(5000),
REQ_DATE varchar(20),
REQ_LOCATION varchar(50),
REQ_WHO varchar(10),
REQ_WHO_OTHER_DETAIL varchar(30),
REQ_DISTRIBUTION varchar(8),
REQ_ORDER_NAME varchar(10),
REQ_VIAL varchar(1),
REQ_COMMENT varchar(5000),
iid varchar(30),
idate timestamp not null
);

/* SAMPLE_SERUM & CSF */
create table spn.sample_serum(
sc_no int auto_increment not null primary key,
chart_no varchar(30) not null,
/*EXPERIMENT varchar(5),*/
EXPERIMENT_SERUM char(1),
EXPERIMENT_CSF char(1),
COL_DATE varchar(20),
DIS_DATE varchar(20),
COL_INDEX varchar(9),
NO varchar(10),
STORAGE varchar(1),
AQUAPORIN_4_AB char(1),
AUTOIMMUNE_ENCEPHALITIS char(1),
ORDER_OTHER char(1),
ORDER_OTHER_DETAIL varchar(50),
EXPERIMENT_PLACE char(10),
COMMENT_1 varchar(5000),
LOCATION varchar(15),
COMMENT_2 varchar(5000),
req_date varchar(20),
req_location varchar(50),
req_order_name varchar(10),
req_vial varchar(1),
req_comment varchar(5000),
iid varchar(30),
idate timestamp not null
);

/* SAMPLE_URINE */
create table spn.sample_urine(
uri_no int auto_increment not null primary key,
chart_no varchar(30) not null,
URINE varchar(10),
URINE_COL_DATE varchar(20),
URINE_INDEX varchar(5),
URINE_VOLUME varchar(3),
URINE_STORAGE varchar(15),
URINE_LOCATION varchar(20),
URINE_COMMENT varchar(5000),
UIPSC varchar(5),
UIPSC_COL_DATE varchar(20),
UIPSC_INDEX varchar(10),
UIPSC_STORAGE varchar(15),
UIPSC_LOCATION varchar(20),
UIPSC_COMMENT varchar(5000),
REQ_DATE varchar(10),
REQ_DISTRIBUTION varchar(10),
REQ_ORDER_NAME varchar(10),
REQ_VIAL varchar(10),
REQ_COMMENT varchar(5000),
iid varchar(30),
idate timestamp not null
);

/* SAMPLE_RNA */
create table spn.sample_rna(
rna_no int auto_increment not null primary key,
chart_no varchar(30) not null,
COL_DATE varchar(20),
COL_TYPE varchar(20),
COL_TYPE_OTHER_DETAIL varchar(30),
COL_INDEX varchar(9),
COL_CONCENTRATION varchar(5),
COL_260_280 varchar(3),
COL_260_230 varchar(3),
COL_LOCATION varchar(15),
COL_COMMENT varchar(5000),
REQ_DATE varchar(20),
REQ_LOCATION varchar(50),
REQ_DISTRIBUTION varchar(20),
REQ_DISTRIBUTION_OTHER_DETAIL varchar(30),
REQ_ORDER_NAME varchar(10),
REQ_VIAL varchar(1),
REQ_COMMENT varchar(5000),
iid varchar(30),
idate timestamp not null
);