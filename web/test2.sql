show databases;
show tables;
--testdb 사용

ov_no int auto_increment not null primary key,
----------OVERVIEW
create table testdb.overview(
SNUPN char(11) not null,
DNA_GENOMIC char(1),
DNA_CELLFREE char(1),
DNA_MITOCHONDRIA char(1),
TISSUE_MUSCLE char(1),
TISSUE_MYOBLAST char(1),
TISSUE_SKIN char(1),
TISSUE_FIBROBLAST char(1),
CELL_FREE_DNA char(1),
SERUM char(1),
CSF char(1),
URINE_CELL char(1),
UIPSC char(1),
RNA char(1),
INFORMED_CONSENT char(1),
SANGER_ORDER char(1),
SANGER_ONGOING char(1),
SANGER_DONE char(1),
NGS_WGS_ORDER char(1),
NGS_WGS_ONGOING char(1),
NGS_WGS_DONE char(1),
NGS_WES_ORDER char(1),
NGS_WES_ONGOING char(1),
NGS_WES_DONE char(1),
NGS_TARGETED_ORDER char(1),
NGS_TARGETED_ONGOING char(1),
NGS_TARGETED_DONE char(1),
CGH_ORDER char(1),
CGH_ONGOING char(1),
CGH_DONE char(1),
MUSCLE_STAIN_ORDER char(1),
MUSCLE_STAIN_ONGOING char(1),
MUSCLE_STAIN_DONE char(1),
WESTERN_ORDER char(1),
WESTERN_ONGOING char(1),
WESTERN_DONE char(1),
OTHER_ORDER char(1),
OTHER_ONGOING char(1),
OTHER_DONE char(1),
idate timestamp not null
)
drop table testdb.overview;
select * from testdb.overview where snupn="11111111111";
select * from testdb.overview;
delete from testdb.overview where snupn="11111111111";

----------MEMBER
create table testdb.member(
mb_no int auto_increment not null primary key,
id varchar(30),
pw varchar(30),
name varchar(30),
affiliation varchar(30),
phone varchar(15),
permission varchar(6),
regdate timestamp not null
)
desc testdb.member;
drop table testdb.member;
insert into testdb.member(id, pw, name, affiliation, phone,permission,regdate) 
values ('test', 'test', 'test', 'test', 'test', 'test', now());
select * from testdb.member;
select * from testdb.member order by mb_no desc;

----------INFO
create table testdb.info(
gi_no int auto_increment not null primary key,
snupn char(11) not null,
chart_no varchar(30) not null,
name varchar(50) not null,
sex char(2),
birth varchar(20),
doctor char(3),
other_site varchar(30),
category varchar(8),
clinical_diagnosis varchar(5000),
final_diagnosis varchar(5000),
symptom varchar(5000),
remark varchar(5000),
idate timestamp not null
)
desc testdb.info;
drop table testdb.info;
select * from testdb.info;
alter table testdb.info change final_diganosis final_diagnosis varchar(5000);
select * from testdb.info where snupn=11111111111;

alter table testdb.info change other_site refered_dr varchar(30);

----------LAB_SANGER
create table testdb.lab_sanger(
ss_no int auto_increment not null primary key,
snupn char(11) not null,
exp_date varchar(20),
gene varchar(15),
position varchar(15),
snp varchar(50),
type varchar(10),
de_novo char(1),
mutation varchar(5000),
family_test_F char(1),
family_test_M char(1),
family_test_S char(1),
reported varchar(50),
comment varchar(5000),
idate timestamp not null
)
desc testdb.lab_sanger;
drop table testdb.lab_sanger;
select * from testdb.lab_sanger;
select * from testdb.lab_sanger where snupn=11111111111;
--auto_increment를 뺀 나머지 컬럼만 insert! 주의!
insert into testdb.lab_sanger(snupn, exp_date, gene, position, snp, type, de_novo, mutation, 
family_test_F, family_test_M, family_test_S,reported, comment, idate)
values('12345678913','exp_date','gene','posigion','snp','type','Y','mutation','Y','Y','Y','reported','comment',now());


----------LAB_NGS
create table testdb.lab_ngs(
ngs_no int auto_increment not null primary key,
snupn char(11) not null,
experiment varchar(12) not null,
exp_date varchar(20) not null,
case_no varchar(10),
exp_place varchar(20),
sample_yn_f char(1),
sample_yn_m char(1),
sample_yn_s char(1),
comment varchar(5000),
idate timestamp not null
)
select * from testdb.lab_ngs;
update testdb.lab_ngs set case_no='updateㅠㅠ' where ngs_no='5';
select * from testdb.lab_ngs; where snupn=11111111111;
insert into testdb.lab_ngs(snupn,experiment,exp_date,case_no,exp_place,sample_yn_f,sample_yn_m,sample_yn_s,comment,idate) 
values('11111111111','experiment2','exp_date2','case_no2','exp_place2','Y','Y','Y','comment2',now());

select max(ngs_no),snupn,experiment,exp_Date,case_no,exp_place,sample_yn_f,sample_yn_m,
sample_yn_s,comment, idate from testdb.lab_ngs where snupn=11111111111;

select max(ngs_no),snupn,experiment,exp_Date,case_no,exp_place,sample_yn_f,sample_yn_m,
sample_yn_s,comment, idate from testdb.lab_ngs where snupn=11111111111 group by snupn ;

select * from testdb.lab_ngs where snupn=1111111111 and ngs_no is max;
----------LAB_CGH
create table testdb.lab_cgh(
cgh_no int auto_increment not null primary key,
snupn char(11) not null,
exp_date varchar(20),
chip_no varchar(15),
chromosome_study varchar(15),
fish_confirmation varchar(15),
parents_test varchar(15),
cnv_type varchar(15),
pathogenic_result_location varchar(50),
pathogenic_result_size varchar(10),
non_pathogenic_result_location varchar(50),
non_pathogenic_result_size varchar(10),
comment varchar(5000),
idate timestamp not null
)
select * from testdb.lab_cgh where snupn=11111111111;
select * from testdb.lab_cgh;

----------LAB_STAIN
create table testdb.lab_stain(
sms_no int auto_increment not null primary key,
snupn char(11) not null,
experiment varchar(10) not null,
m_index_no varchar(10) not null,
discription varchar(5000),
pathology_diagnosis varchar(5000),
comment varchar(5000),
exp_date_1 varchar(20),
antibody_1 char(1),
antibody_2 char(1),
antibody_3 char(1),
antibody_4 char(1),
antibody_5 char(1),
antibody_6 char(1),
antibody_7 char(1),
antibody_8 char(1),
antibody_9 char(1),
antibody_10 char(1),
antibody_11 char(1),
antibody_12 char(1),
antibody_13 char(1),
antibody_14 char(1),
antibody_15 char(1),
antibody_16 char(1),
antibody_17 char(1),
antibody_18 char(1),
exp_date_2 varchar(20),
antibody_19 char(1),
antibody_20 char(1),
antibody_21 char(1),
antibody_22 char(1),
antibody_23 char(1),
antibody_24 char(1),
antibody_25 char(1),
idate timestamp not null
)
desc testdb.lab_stain;
drop table testdb.lab_stain;
select * from testdb.lab_stain where snupn=11111111111;
select * from testdb.lab_stain;

----------SAMPLE_DNA
create table testdb.sample_dna(
dna_no int auto_increment not null primary key,
snupn char(11) not null,
col_date varchar(25),
col_who varchar(10),
col_who_other_detail varchar(30),
col_type varchar(20),
col_type_other_detail varchar(30),
col_index varchar(9),
col_concentration varchar(20),
col_260_280 varchar(20),
col_260_230 varchar(20),
col_location varchar(15),
col_comment varchar(5000),
req_date varchar(20),
req_who varchar(10),
req_who_other_detail varchar(10),
req_distribution varchar(8),
req_order_name varchar(10),
req_type varchar(20),
req_type_other_detail varchar(30),
req_vial varchar(1),
req_comment varchar(5000),
idate timestamp not null
)
drop table testdb.sample_dna;
select * from testdb.sample_dna where snupn='11111111111';
select * from testdb.sample_dna;

----------SAMPLE_TISSUE
create table testdb.sample_tissue(
tis_no int auto_increment not null primary key,
snupn char(11) not null,
bio_date varchar(20),
bio_type varchar(10),
bio_type_other_detail varchar(30),
bio_index_1 varchar(10),
bio_location_1 varchar(15),
bio_cell varchar(10),
bio_index_2 varchar(10),
bio_phase varchar(2),
bio_storage varchar(1),
bio_location_2 varchar(15),
bio_comment varchar(5000),
req_date varchar(20),
req_distribution varchar(8),
req_order_name varchar(10),
req_type varchar(10),
req_type_other_detail varchar(30),
req_vial varchar(1),
req_comment varchar(5000),
idate timestamp not null
)
drop table testdb.sample_tis;
select * from testdb.sample_tissue;
select * from testdb.sample_tissue where snupn='11111111111';
delete from testdb.sample_tissue where snupn='11111111111';


----------SAMPLE_CEll FREE DNA
create table testdb.sample_CFDNA(
cfd_no int auto_increment not null primary key,
snupn char(11) not null,
col_date varchar(20),
col_who varchar(10),
col_who_other_detail varchar(30),
col_index varchar(9),
col_wk varchar(2),
col_plasma_volume varchar(2),
col_cf_no varchar(20),
col_concentration_1 varchar(2),
col_concentration_2 varchar(2),
col_260_280_1 varchar(2),
col_260_280_2 varchar(2),
col_260_230_1 varchar(2),
col_260_230_2 varchar(2),
COL_BIOANALYZER_SIZE_1 varchar(3),
COL_BIOANALYZER_SIZE_2 varchar(3),
COL_BIOANALYZER_CONCENTATION_1 varchar(6),
COL_BIOANALYZER_CONCENTATION_2 varchar(6),
COL_COMMENT varchar(5000),
REQ_DATE varchar(20),
REQ_WHO varchar(10),
REQ_WHO_OTHER_DETAIL varchar(30),
REQ_DISTRIBUTION varchar(8),
REQ_ORDER_NAME varchar(10),
REQ_VIAL varchar(1),
REQ_COMMENT varchar(5000),
idate timestamp not null
)
drop table testdb.sample_cfdna;
select * from testdb.sample_cfdna where snupn='11111111111';
delete from testdb.sample_cfdna where snupn='11111111111';
select * from testdb.sample_cfdna;

----------SAMPLE_SERUM & CSF
create table testdb.sample_serum(
sc_no int auto_increment not null primary key,
snupn char(11) not null,
EXPERIMENT varchar(5),
COL_DATE varchar(20),
DIS_DATE varchar(20),
COL_INDEX varchar(9),
NO varchar(10),
STORAGE varchar(1),
AQUAPORIN_4_AB varchar(1),
AUTOIMMUNE_ENCEPHALITIS char(1),
EXPERIMENT_PLACE char(10),
COMMENT_1 varchar(5000),
LOCATION varchar(15),
COMMENT_2 varchar(5000),
idate timestamp not null
)
drop table testdb.sample_serum;
select * from testdb.sample_serum;

----------SAMPLE_URINE
create table testdb.sample_urine(
uri_no int auto_increment not null primary key,
snupn char(11) not null,
URINE varchar(10),
URINE_COL_DATE varchar(20),
URINE_INDEX varchar(5),
URINE_VOLUME varchar(3),
URINE_STORAGE varchar(15),
URINE_LOCATION varchar(20),
URINE_COMMENT varchar(5000),
UIPSC varchar(5),
UIPSC_COL_DATE varchar(20),
UIPSC_INDEX varchar(10),
UIPSC_STORAGE varchar(15),
UIPSC_LOCATION varchar(20),
UIPSC_COMMENT varchar(5000),
REQ_DATE varchar(10),
REQ_DISTRIBUTION varchar(10),
REQ_ORDER_NAME varchar(10),
REQ_VIAL varchar(10),
REQ_COMMENT varchar(5000),
idate timestamp not null
)
drop table testdb.sample_urine;
delete from testdb.sample_urine where snupn='11111111111';
select * from testdb.sample_urine where snupn='11111111111';
select * from testdb.sample_urine;

----------SAMPLE_RNA
create table testdb.sample_rna(
rna_no int auto_increment not null primary key,
snupn char(11) not null,
COL_DATE varchar(20),
COL_TYPE varchar(20),
COL_TYPE_OTHER_DETAIL varchar(30),
COL_INDEX varchar(9),
COL_CONCENTRATION varchar(5),
COL_260_280 varchar(3),
COL_260_230 varchar(3),
COL_LOCATION varchar(15),
REQ_DATE varchar(20),
REQ_DISTRIBUTION varchar(20),
REQ_DISTRIBUTION_OTHER_DETAIL varchar(30),
REQ_ORDER_NAME varchar(10),
REQ_VIAL varchar(1),
idate timestamp not null
)
delete from testdb.sample_rna where snupn='11111111111';
select * from testdb.sample_rna;

