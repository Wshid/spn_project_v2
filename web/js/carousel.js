$(function(){
    
    var box=$('.row .col-sm-4'),
        duration=1500,
        $window=$(window);    
    
    var carousel=[["Innovative Platform", "하나의 웹사이트에서 Laboratory, Sample에 관련한 차트 데이터를 모두 확인할 수 있습니다."],
                ["Search", "검색 엔진을 사용하여 보다 빠르게 정보에 접근하여 조회 및 수정이 가능합니다."],
                ["Statistic", "각 항목별 데이터의 수를 확인할 수 있으며, 해당 내용 조회가 가능합니다."]
                ];
    
    var menu=[['Info', '#', '모든 차트에 대한 기본정보를 확인할 수 있습니다. SNUPN, Chart Number, 이름, 성별, 담당자 등을 포함하고 있습니다.'],
    		['Statistic', 'SPN?cmd=statisticform', '각 항목별 차트 통계 정보를 확인할 수 있습니다. Infomation부터 Sample RNA까지 각 항목별 차트의 수 확인 및 내용을 포함하고 있습니다.'],
        ['Lab Sanger', '#', 'Lab Sanger Sequencing를 확인할 수 있습니다. Gene, Position, Mutation등의 항목을 포함합니다.'],
        ['Lab Cgh', 'word.php', 'Lab Cgh를 확인할 수 있습니다. Karyotyping, FISH Confirmation, Pathogenic result을 포함합니다.'],
        ['Lab Ngs', '#', 'Lab Ngs를 확인할 수 있습니다. WGS, RNA seq, Label No등을 포함합니다.'],
        ['Lab Stain', '#', 'Lab Stain을 확인할 수 있습니다. Pathology Diagnosis 및 각종 Antibody를 포함합니다.'],
        ['Sample DNA', '#', 'Sample DNA를 확인할 수 있습니다. Sample Type, Concentration, 260/280등을 포함합니다.'],
        ['Sample Tissue', '#', 'Sample Tissue를 확인할 수 있습니다. Biopsy Date, Sample Type 등을 포함합니다.'],
        ['Sample Cell', '#', 'Sample Cell을 확인할 수 있습니다. Sample Type, Phase 등을 포합합니다.'],
        ['Sample CFDNA', '#', 'Sample CFDNA를 확인할 수 있습니다. WK, Plasma Volume, Bioanalyzer Size등을 기입할 수 있습니다.'],
        ['Sample Serum & CSF', '#', 'Sample Serum & CSF를 확인할 수 있습니다. Serum/CSF No, Aquaporin 4 Ab 등을 포함합니다.'],
        ['Sample RNA', '#', 'Sample RNA를 확인할 수 있습니다. Collection Site, Concentration을 포함합니다.']
    		];

    $('.carousel-caption').each(function(i, item){
        var titles=$(this).find('h1');
        var bodies=$(this).find('.carouselBody');
        titles.text(carousel[i][0]);
        bodies.text(carousel[i][1]);
    });     
    
    /* blocks-caption */
    $('.columns_s4 .col-sm-4').each(function(i, item){
        var titles=$(this).find('h2'),
            links=$(this).find('a'),
            bodies=$(this).find('.marketingBody');
        titles.text(menu[i][0]);
        links.attr('href', menu[i][1]);
        bodies.text(menu[i][2]);
    });
    
    /* shadow box */    
    box.on('mouseover', function(){
      
      $(this).addClass('box_active');
    })
    .on('mouseleave', function(){
        $(this).removeClass('box_active');
    });

    

});