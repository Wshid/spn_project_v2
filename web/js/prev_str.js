function pre_list(str){
	//console.log("str : "+str);
	if(str==null){
		return "";
	}else{
		var src = str.split('[')[1].split(']')[0]; //단순히 [] 제거
		var splited=src.split(","); // 리스트 형태의 값
		var slashed=splited.map(x => x.split("/").map(y => (y=="=")?"":y.trim()));
		return slashed;
	}

}

function reform_empty(str){
	if(str==null || str==undefined || str=="null") return "";
	else return str;
}

function norm_checked(str, norm){
	if(str==norm) return "checked='checked'";
}

function norm_selected(str, norm){
	if(str==norm) return "selected";
	else return "";
}

function is_empty_null(obj){
	if(!obj || obj.length==0) return true;
	else return false;
}

function is_empty_list(obj){
	if(obj.length<=1 && is_empty_null(obj[0])) return true;
	else return false;
}

function load_id(var_id){
	var cur=document.getElementById(var_id);
	if(is_empty_null(cur)) return "";
	else return cur.value;
}

function save_id(var_id, new_value){
	var cur=document.getElementById(var_id);
	if(is_empty_null(cur)) return "";
	else return cur.value=new_value;
}

function reform_empty_equal(str){
	if(is_empty_null(str)) return "=";
	else return str;
}

function load_item_zero(str){
	var cur = document.getElementsByName(str);
	if(is_empty_null(cur)) return "";
	else return cur.item(0).value;
}