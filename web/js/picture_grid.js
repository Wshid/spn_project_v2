$(function(){
	
	var $grid=$(".picture_grid_container");
	//$grid.html($(elements)); // elements는 html 코드를 의미한다.
	var elements="";
	var plusHTML=""; // 만들어낼 HTML 코드가 담겨 있을 배열
		// 현재는 json 형태로 저장된 파일을 읽어 로드하는 경우였지만,
		// 이번에는 Mysql에 참조하여 직접 얻어올 수 있도록 해야할 것 같다
	
	var $db_image=$(".db_image");
	var $names=$(".names");
	var $doctors=$(".doctors");
	var $subtitle=$("#subtitle");
	var page_name=$(".page_name").val();
	
	var raw_paths=$db_image.val(); /* 현재 길이는 글자수만 나옴 */
	var local_paths=raw_paths.substring(1,raw_paths.length-1).trim().split(", "); // 배열 -> 문자열이기 때문에 []라는 불순물이 들어가게 됨 콤마 뒤에 띄어쓰기를 해야 제대로 잘라짐
	var names=$names.val().substring(1,$names.val().length-1).trim().split(", "); // 배열 -> 문자열이기 때문에 []라는 불순물이 들어가게 됨
	var doctors=$doctors.val().substring(1,$doctors.val().length-1).trim().split(", "); // 배열 -> 문자열이기 때문에 []라는 불순물이 들어가게 됨
	
	//var web_
	 
	//var res=paths.split(","); // path별로 나눔
	
	//$subtitle.val(local_paths.length+1); // 구현방식 변경
	//console.log($subtitle.val());
	var paths=new Array();
	
	console.log(local_paths);
	plusHTML=""; // 최종적으로 $에 적용시킬 HTML 구문
	$.each(local_paths, function(i, item){
		//paths[i]=local_paths[i].indexOf('SPNProject'); // 되게 인위적이기 때문에 SPN Project 글자의 위치를 찾은 뒤의 그 전의 /를 찾아 없애버리는게 나을듯
		var split_idx=local_paths[i].indexOf('SPNProject')-1; // slash를 하나 집어넣어야 정상참조 가능함
		console.log(local_paths[i].substring(split_idx, local_paths[i].length));
		paths[i]=local_paths[i].substring(split_idx, local_paths[i].length);
	});
	
	console.log(paths);
	if(!is_empty_list(paths)){
		console.log("NOT EMPTY LIST");
		$.each(paths, function(i, item){
			var modalHTML=""; // modal 생성부에 해당하는 html
			var innerHTML=""; // modal 선언부에 해당하는 html
			
			if(i%3==0){ //0,1,2가 한 조가 되어야함
				innerHTML+='<div class="row">';
			}
			innerHTML+=
				/*'<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>'+*/
				'<div class="col-sm-4 proj-div block" data-toggle="modal" data-target="#modal_'+i+'_'+page_name+'">'+
					'<p>'+
						'<h5>'+names[i]+'</h5>'+'<br />'+
						'<strong>'+doctors[i]+'</strong>'+
					'<\p>';
			
			// i 수만큼 html 태그 생성을 해야함
				//console.log(i);
				//console.log("path "+i+"\t"+paths[i]);
			innerHTML+='</div>';
			
			if(i%3==2){
				innerHTML+="</div>";
			}
			plusHTML+=innerHTML;
			
			// modal 관련 객체 선언부
			
			/* sanger, ngs 공통으로 사용하므로, menu관련 변수를 가져와야만 한다. */
			//'<form class="dbimage_lab_sanger" name="dbimage_lab_sanger_'+i+'" action="SPN?cmd=dbimageprocess" method="post" enctype="multipart/form-data">'+
			modalHTML+='<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal_'+i+'_'+page_name+'_label" id="modal_'+i+'_'+page_name+'">'+
	        					'<div class="modal-dialog" role="document">'+
	        				'<form class="dbimage_lab_sanger" name="dbimage_lab_sanger_'+i+'" action="SPN?cmd=dbimagedelete" method="post">'+
	    						'<input type="hidden" name="delete_file_name" value="'+names[i]+'"/>'+
	    						'<input type="hidden" name="delete_page_name" value="'+page_name+'"/>'+
	        						'<div class="modal-content">'+
	        							'<div class="modal-header">'+
	        								'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
	        								'<h3 class="modal-title" id="modal_'+i+'_sanger_label">'+names[i]+'</h3>'+
	        							'</div>'+
	        							'<div class="modal-body">'+
	        								'<img src="'+paths[i]+'"width=100% height=100% />'+
	        								'<a id="image_load" class="selectDatabaseAllButton" href="'+paths[i]+'" download>Download</a>' +
	        							'</div>'+
	        							'<div class="modal-footer">'+
	        	                    		//'<button type="submit" class="btn btn-primary button_reform">Submit</button>'+
	        	                    		'<button type="submit" class="btn btn-danger button_reform">Delete</button>'+
	        	                    		'<button type="button" class="btn btn-default lab_sanger_modal_close" data-dismiss="modal">Close</button>'+
	        	                    	'</div>'+
	        	                    '</div>'+
	        	                '</form>'+
	        	                 '</div>'+
	        	             '</div>';
	        	        //'</form>';
			plusHTML+=modalHTML;
			
		});
		
	}
	
	
	
	$grid.append($(plusHTML));
	
	function make_modal_string(idx, title, path){
		var innerHTML="";
		var title="";
		
		
	}
	
	/* bootstrap 2버전에서나 사용되는 방법 
	function open_modal(modal_name){
		$(modal_name).modal('show');
	}*/
	
	

	
	$(".col-sm-4").each(function(){
		$(this).on('mouseover', function(){
			$(this).addClass('box_active');
		})
		.on('mouseleave', function(){
			$(this).removeClass('box_active');
		});
	});
	
	$(".filebox").each(function(){
		$(this).on('mouseover', function(){
			$(this).addClass('box_active');
		})
		.on('mouseleave', function(){
			$(this).removeClass('box_active');
		});
	});
	
});