function modal_script(menu){
	switch(menu){
		case "info":
			make_modal(menu,"snupn","SNUPN", "SNUPN을 확인해주세요", ".modal_container");
			//console.log("already modal");
			make_modal(menu,"name","NAME","name을 확인해주세요", ".modal_container");
			make_modal(menu,"chart_no","CHART_NO","chart_no를 확인해주세요", ".modal_container");
			make_modal(menu,"sex","GENDER","성별을 확인해주세요", ".modal_container");
			make_modal(menu,"not_found","Chart Number", "해당 Chart Number가 없습니다.", ".modal_container");
			make_modal(menu,"duplicate_chart","Already Exist Chart", "이미 해당 Chart Number가 있습니다.", ".modal_container");
			break;
		case "lab_sanger":
			make_modal(menu,"exp_date","Expire Date", "Expire Date를 확인해주세요", ".modal_container");
			make_modal(menu,"gene","Gene","Gene을 확인해주세요", ".modal_container");
			make_modal(menu,"type","Type","Type을 확인해주세요", ".modal_container");
			make_modal(menu,"de_novo","DE_NOVO","De Novo를 확인해주세요", ".modal_container");
			make_modal_form(menu, "spn_input_form", "Sanger Sequencing", 'Sanger Sequence 정보를 입력해주세요', ".modal_container");
			break;
		case "lab_ngs":
			make_modal(menu,"experiment","Experiment", "Experiment를 확인해주세요", ".modal_container");
			make_modal(menu,"exp_date","Expire Date", "Expire Date를 확인해주세요", ".modal_container");
			break;
		case "lab_cgh":
			make_modal(menu,"exp_date","Expire Date", "Expire Date를 확인해주세요", ".modal_container");
			make_modal(menu,"chip_no","Chip Number","Chip Number을 확인해주세요", ".modal_container");
			break;
		case "lab_stain":
			make_modal(menu,"experiment","Experiment", "Experiment를 확인해주세요", ".modal_container");
			make_modal(menu,"m_index","M Index", "M Index를 확인해주세요", ".modal_container");
			break;
		case "sample_dna":
			break;
		case "sample_tissue":
			break;
		case "sample_cell":
			break;
		case "sample_cfdna":
			break;
		case "sample_serum":
			break;
		case "sample_rna":
			break;
		default:
			alert("NOT FIND MENU : checkIt function");
			break;
	}
	if(menu!=="info"){
		console.log("modal_script in");
		make_modal("spn", "chart_no", "Chart Number", "Chart를 검색하거나 새로 만들어주세요", ".modal_container");
	}
	// 추후 나중에 구현하기로 
	//make_modal("spn", "delete", "DELETE", "삭제하시겠습니까?", ".modal_container");
	//make_modal("spn", "update", "UPDATE", "수정하시겠습니까?", ".modal_container");
}