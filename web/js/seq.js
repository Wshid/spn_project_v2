/* lab_sanger의 탭 부분을 담당하는 javascript
 * javascript의 경우 JSP의 변수를 참조할 수 없다는 단점 존재
 * JQuery파일로 변경하여, 해당 변수들을 ajax로 로드하도록 한다.
 */
$(function(){
	var $container=$('.seq_container');

	// 각 변수별 리스트로 받아올 것 같음. 해당 내용 파싱 어떻게 할지 고민하기
	var exp_date = pre_list($('#exp_date').val()),
				gene= pre_list($('#gene').val()),
				position =pre_list($('#position').val()), // slash 구분 인자로 사용
				snp= pre_list($('#snp').val()),
				type = pre_list($('#type').val()),
				de_novo = pre_list($('#de_novo').val()),
				mutation= pre_list($('#mutation').val()),
				family_test_f = pre_list($('#family_test_f').val()),
				family_test_m = pre_list($('#family_test_m').val()),
				family_test_s = pre_list($('#family_test_s').val()),
				result = pre_list($('#result').val()),
				reported = pre_list($('#reported').val()),
				comment = pre_list($('#comment').val()),
				idate = pre_list($('#idate').val()),
				iid = pre_list($('#iid').val()),
				ss_no = pre_list($('#ss_no').val()),
				chart_no=$('#chart_no').val();

			//console.log(exp_date, gene, position);
	var mainHTML="";
	const TABS=gene.length;
	// 일단은 하드코딩으로 하고 추후 수정하기로
	/*
	var tab_nav=
			`
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#gene">GENE 1</a></li>
				<li><a data-toggle="tab" href="#gene2">GENE 2</a></li>
				<li><a data-toggle="tab" href="#gene3">GENE 3</a></li>
			</ul>
		`;
		*/
	var $tab_nav=$(`<ul class="nav nav-tabs">`);
	var $tab_div=$(`<div class="tab-content"></div>`); // JQuery객체로 받아낸다. //추후 append를 이용하여 코드를 추가할 예정

	////console.log($d_form);

	for(tab_idx=0;tab_idx<TABS;tab_idx++){ // 탭별로 작업을 실시한다. // TABS는 상수로 취급한다.
		//console.log("tab iterator");
		//console.log("position length : "+position.length);
		//var tab_exp_date=exp_date[j];

		if(tab_idx==0){
			var $tab_nav_pane=$(`<li class="active"><a data-toggle="tab" href="#data_1">DATA 1</a></li>`);
			var $tab_div_pane=$(`<div id="data_1" class="tab-pane fade in active"></div>`);
		}else{
			var $tab_nav_pane=$(`<li><a data-toggle="tab" href="#data_${tab_idx+1}">DATA ${tab_idx+1}</a></li>`);
			var $tab_div_pane=$(`<div id="data_${tab_idx+1}" class="tab-pane fade"></div>`);
		}

		// tab마다 form을 한개씩 보유한다.
		var $d_form=
		$(`
			<form class="spn_input_form" method="post" action="SPN?cmd=labsanger" name="sangerinput_ori" id="sangerinput_ori">
		`);
		$d_form.append(
			$(`
			<br> <br> <br>
			<font color="#0191D0" style="float: right;">${reform_empty(iid[tab_idx][0])} &nbsp;|&nbsp; ${idate[tab_idx][0]} </font><br>
			`)
		);

		//console.log($d_form);

		for(j=0;j<position[tab_idx].length;j++){ // 여러 라인을 구성할 수 있는 기준 지표가 된다.
			// 여기서부터 테이블 구성 구문이 들어가야 함
			var $table=$(`<table class="table table-bordered table-striped table-hover">`);
			//template string을 사용하여 간단하게 표기 => es6 문법
			// position / snp / type / de_novo / mutation / report / judge
			var $exp_date_gene=$(`
			<tr>
				<td><strong>Exp. date<font color="red">*</font></strong></td>
				<td><input type="date" name="exp_date"
					style="border: transparent; background-color: transparent;"
					value="${exp_date[tab_idx][0]}" disabled></td>
				<td><strong>Gene</strong></td>
				<td><input type="text" name="gene"
					style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
					value="${gene[tab_idx][0]}" disabled></td>
			</tr>
			`);

			var $position_snp=$(`
				<tr>

					<td><strong>Position</strong></td>
					<td><input type="text" name="position"
						style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
						value="${position[tab_idx][j]}" disabled></td>
					<td><strong>Snp</strong></td>
					<td><input type="text" name="snp"
						style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
						value="${snp[tab_idx][j]}" disabled></td>
				</tr>
			`);
			// checkbox형태를 제거하기로
			/*
			<td><select name="type">
					<option value="">select
					<option value="Hetero">Hetero
					<option value="Homo">Homo
					<option value="X-linked">X-linked
			</select></td>
			*/
			/*
			var checked_de_novo="";
			var checked_de_novo_n="";
			
			if(de_novo[tab_idx][j]=="Y"){ // 옵션단추 체크 구문, 추후 테스트 해볼 것
				checked_de_novo="checked='checked'";
			}else if(de_novo[tab_idx][j]=="N"){
				checked_de_novo_n="checked='checked'";
			}
			*/
			
			// norm_checked 함수를 사용하여 해결 
			/*
			var select1 = document.getElementsByName("type").item(0);
			for (var i = 0; i < select1.options.length; i++) {
				if (select1.options[i].value == "${type[tab_idx][j]}") {
					select1.selectedIndex = i;
				}
			}
*/			
			
			var $type_de_novo=$(`
				<tr>
					<td><strong>Type<font color="red">*</font></strong></td>
					<td><select name="type" disabled>
							<option value="">select
							<option value="Hetero" ${norm_selected(type[tab_idx][j], "Hetero")}>Hetero
							<option value="Homo" ${norm_selected(type[tab_idx][j], "Homo")}>Homo
							<option value="X-linked" ${norm_selected(type[tab_idx][j], "X-linked")}>X-linked
					</select></td>
					<td><strong>De novo<font color="red">*</font></strong></td>
					<td><input type="radio" name="de_novo" value="Y" ${norm_checked(de_novo[tab_idx][j], "Y")} disabled>Yes&nbsp;
						<input type="radio" name="de_novo" value="N" ${norm_checked(de_novo[tab_idx][j], "N")} disabled>No</td>
				</tr>
				`); 

			
			var $mutation_family=$(`
				<tr>
					<td><strong>Mutation</strong></td>
					<td><input type="text" name="mutation"
						style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
						value="${reform_empty(mutation[tab_idx][j])}" disabled></td>

					<td><strong>Family Test</strong></td>
					<td><input type="checkbox" name="family_test_f" value="Y" ${norm_checked(family_test_f[tab_idx][j], "Y")} disabled>Father&nbsp;
						<input type="checkbox" name="family_test_m" value="Y" ${norm_checked(family_test_m[tab_idx][j], "Y")} disabled>Mother&nbsp;
						<input type="checkbox" name="family_test_s" value="Y" ${norm_checked(family_test_s[tab_idx][j], "Y")} disabled>Sibling</td>
				</tr>
				`);

			var $reported=$(`
				<tr>
					<td><strong>Reported</strong></td>
					<td colspan="4"><input type="text" name="reported"
						style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
						value="${reform_empty(reported[tab_idx][j])}" disabled></td>
				</tr>
				`);

			var $result=$(`
				<tr>
					<td><strong>Result</strong></td>
					<td colspan="4"><input type="text" name="result"
						style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
						value="${reform_empty(result[tab_idx][j])}" disabled></td>
				</tr>
				`);

			var $comment=$(`
				<tr>
					<td><strong>Comment</strong></td>
					<td colspan="4"><input type="text" name="comment"
						style="border: transparent; background-color: transparent; width: 100%; height: 100%;"
						value="${reform_empty(comment[tab_idx][j])}" disabled></td>
				</tr>
				`);
			var $hidden_info=$(`
				<input type="hidden" name="chart_no" value="${chart_no}" />
				<input type="hidden" name="iid" />
				<input type="hidden" name="ss_no" value="${reform_empty(ss_no[tab_idx][j])}" />
				`); // 여기 부분도 수정해야함

			
			var $delete_button=$(`
					<button type="submit" class="btn btn-danger" style="float: right; margin-bottom:40px;" onclick="return deleteSubmitForm(${tab_idx})">DELETE</button>
			`);

			$table.append($exp_date_gene);
			$table.append($position_snp);
			$table.append($type_de_novo);
			$table.append($mutation_family);
			$table.append($reported);
			$table.append($result);
			$table.append($comment);
			$table.append($hidden_info);
			//console.log(tab_idx, j);
			//console.log(reform_empty(comment[tab_idx][j]));
			//console.log(reform_empty(result[tab_idx][j]));
			//console.log("$TABLE-------");
			//console.log($table);
			$d_form.append($table);
			$d_form.append($delete_button);
			//console.log("$d_form------");
			//console.log($d_form);
		}
		$tab_nav.append($tab_nav_pane);
		//console.log("tab_nav------");
		//console.log($tab_nav);
		//$tab_div_pane.html($d_form);
		$tab_div_pane.append($d_form);
		//console.log("$tab_div_pane------");
		//console.log($tab_div_pane);

		$tab_div.append($tab_div_pane); // object형태로 리턴되려고 
		//console.log("$tab_div------");
		//console.log($tab_div);
	}
	//console.log("======tab_nav");
	//console.log($tab_nav);
	//console.log("======tab_div");
	//console.log($tab_div);

	$container.append($tab_nav);
	$container.append($tab_div);
	//console.log("$container");
	//console.log($container);

	/* 변수에서 해당 내용을 관리할때 */

	// 특정 문자열을 /를 기준으로 분리, 리스트 형태로 리턴한다.
	// ex) "[a,b,c/d/e,f/=/g]" => [[a],[b],[c,d,e], [f,"",g]] 형태


});
