function check_chart(menu){
	if(menu!=="info"){
		var re=/\d{8}-\d|\d{8}/g;
		var chart=document.getElementsByName('chart_no');
		var chart_value=chart[0].value;
		
		if(is_empty_list(chart)){ // 리스트가 비어있는지 판단 
			show_modal("spn", "chart_no");
			console.log('chart_no modal show');
			
			setTimeout(function() { // 비어있으면 에러를 띄운다. 
				location.href="SPN?cmd=infoform";
			}, 3000);
		}else{
			if(!re.test(chart_value)){ // chart no 접속이 아닐경우 
				show_modal("spn", "chart_no");
				console.log('chart_no modal show');
				
				setTimeout(function() {
					location.href="SPN?cmd=infoform";
				}, 3000);
			}else{
				console.log("pass chart_value");
			}
		}
	}
}

function checkIt(menu){
	switch(menu){
	case "info":
		console.log(!document.infoinput.snupn.value);
		if (!document.infoinput.snupn.value) {
			//alert("SNUPN을 입력하세요.");
			show_modal(menu, "snupn");
			console.log('snupn modal show');
			return false;
		}
		if (!document.infoinput.name.value) {
			show_modal(menu, "name");
			//alert("Name을 입력하세요.");
			return false;
		}
		if (!document.infoinput.chart_no.value) {
			show_modal(menu, "chart_no");
			//alert("Chart No를 입력하세요.");
			return false;
		}
		if (!document.infoinput.sex.value) {
			show_modal(menu, "sex");
			//alert("성별을 입력하세요.");
			return false;
		}
		break;
	case "lab_sanger":
		var input_modal_id="modal_lab_sanger_spn_input_form";
		
		if (!document.sangerinput.form_exp_date.value) {
			//alert("Exp. date를 입력하세요.");
			
			show_modal(menu, "exp_date", input_modal_id);
			console.log('exp_date modal show');
			return false;
		}
		if (!document.sangerinput.form_gene.value) {
			//alert("Exp. date를 입력하세요.");
			
			show_modal(menu, "gene", input_modal_id);
			console.log('gene modal show');
			return false;
		}
		if (!document.sangerinput.form_type.value) {
			//alert("Type을 입력하세요.");
			show_modal(menu, "type", input_modal_id);
			console.log('type modal show');
			return false;
		}
		if (!document.sangerinput.form_de_novo.value) {
			show_modal(menu, "de_novo", input_modal_id);
			console.log('de_novo modal show');
			//alert("De novo를 입력하세요.");
			return false;
		}
		console.log("input_check.js running lab_sanger");
		break;
	case "lab_ngs":
		if (!document.ngsinput.experiment.value) {
			show_modal(menu, "experiment");
			console.log('Experiment modal show');
			//alert("Experiment를 입력하세요.");
			return false;
		}
		if (!document.ngsinput.exp_date.value) {
			show_modal(menu, "exp_date");
			console.log('exp_date modal show');
			//alert("Exp. date를 입력하세요.");
			return false;
		}
		break;
	case "lab_cgh":
		if (!document.cghinput.exp_date.value) {
			show_modal(menu, "exp_date");
			console.log('type modal show');
			//alert("Exp. date를 입력하세요.");
			return false;
		}
		if (!document.cghinput.chip_no.value) {
			show_modal(menu, "chip_no");
			console.log('chip_no modal show');
			//alert("Chip no를 입력하세요.");
			return false;
		}
		break;
	case "lab_stain":
		if (!document.staininput.experiment.value) {
			show_modal(menu, "experiment");
			console.log('Experiment modal show');
			//alert("Experiment를 입력하세요.");
			return false;
		}
		if (!document.staininput.m_index_no.value) {
			show_modal(menu, "m_index");
			console.log('M-Index modal show');
			//alert("M-Index No를 입력하세요.");
			return false;
		}
		break;
	case "sample_dna":
		break;
	case "sample_tissue":
		break;
	case "sample_cell":
		break;
	case "sample_cfdna":
		break;
	case "sample_serum":
		break;
	case "sample_rna":
		break;
	default:
		alert("NOT FIND MENU : checkIt function");
		break;
	}
	return true;
}