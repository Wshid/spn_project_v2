package spn.dto;

public class SampleCellDTO {
	private String ce_no;
	private String chart_no;
	private String col_date;
	private String cell_type_myoblast;
	private String cell_type_fibroblast;
	private String cell_type_u_epithelial;
	private String cell_type_u_ipsc;
	private String cell_type_other;
	private String cell_type_other_detail;
	private String cell_index;
	private String cell_storage;
	private String cell_phase;
	private String cell_comment;
	private String req_date;
	private String req_location;
	private String req_order_name;
	private String req_vial;
	private String req_comment;
	private String idate;
	private String iid;
	
	public String getCe_no() {
		return ce_no;
	}
	public void setCe_no(String ce_no) {
		this.ce_no = ce_no;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getCol_date() {
		return col_date;
	}
	public void setCol_date(String col_date) {
		this.col_date = col_date;
	}
	public String getCell_type_myoblast() {
		return cell_type_myoblast;
	}
	public void setCell_type_myoblast(String cell_type_myoblast) {
		this.cell_type_myoblast = cell_type_myoblast;
	}
	public String getCell_type_fibroblast() {
		return cell_type_fibroblast;
	}
	public void setCell_type_fibroblast(String cell_type_fibroblast) {
		this.cell_type_fibroblast = cell_type_fibroblast;
	}
	public String getCell_type_u_epithelial() {
		return cell_type_u_epithelial;
	}
	public void setCell_type_u_epithelial(String cell_type_u_epithelial) {
		this.cell_type_u_epithelial = cell_type_u_epithelial;
	}
	public String getCell_type_u_ipsc() {
		return cell_type_u_ipsc;
	}
	public void setCell_type_u_ipsc(String cell_type_u_ipsc) {
		this.cell_type_u_ipsc = cell_type_u_ipsc;
	}
	public String getCell_type_other() {
		return cell_type_other;
	}
	public void setCell_type_other(String cell_type_other) {
		this.cell_type_other = cell_type_other;
	}
	public String getCell_type_other_detail() {
		return cell_type_other_detail;
	}
	public void setCell_type_other_detail(String cell_type_other_detail) {
		this.cell_type_other_detail = cell_type_other_detail;
	}
	public String getCell_index() {
		return cell_index;
	}
	public void setCell_index(String cell_index) {
		this.cell_index = cell_index;
	}
	public String getCell_storage() {
		return cell_storage;
	}
	public void setCell_storage(String cell_storage) {
		this.cell_storage = cell_storage;
	}
	public String getCell_phase() {
		return cell_phase;
	}
	public void setCell_phase(String cell_phase) {
		this.cell_phase = cell_phase;
	}
	public String getCell_comment() {
		return cell_comment;
	}
	public void setCell_comment(String cell_comment) {
		this.cell_comment = cell_comment;
	}
	public String getReq_date() {
		return req_date;
	}
	public void setReq_date(String req_date) {
		this.req_date = req_date;
	}
	public String getReq_location() {
		return req_location;
	}
	public void setReq_location(String req_location) {
		this.req_location = req_location;
	}
	public String getReq_order_name() {
		return req_order_name;
	}
	public void setReq_order_name(String req_order_name) {
		this.req_order_name = req_order_name;
	}
	public String getReq_vial() {
		return req_vial;
	}
	public void setReq_vial(String req_vial) {
		this.req_vial = req_vial;
	}
	public String getReq_comment() {
		return req_comment;
	}
	public void setReq_comment(String req_comment) {
		this.req_comment = req_comment;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	
	
	

}
