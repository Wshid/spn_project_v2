package spn.dto;

public class LabSangerDTO {
	private String ss_no;
	private String chart_no;
	private String exp_date;
	private String gene;
	private String position;
	private String snp;
	private String type;
	private String de_novo;
	private String mutation;
	private String family_test_f;
	private String family_test_m;
	private String family_test_s;
	private String result;
	private String reported;
	private String comment;
	private String iid;
	private String idate;
	
	
	
	public String getSs_no() {
		return ss_no;
	}
	public void setSs_no(String ss_no) {
		this.ss_no = ss_no;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getExp_date() {
		return exp_date;
	}
	public void setExp_date(String exp_date) {
		this.exp_date = exp_date;
	}
	public String getGene() {
		return gene;
	}
	public void setGene(String gene) {
		this.gene = gene;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getSnp() {
		return snp;
	}
	public void setSnp(String snp) {
		this.snp = snp;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDe_novo() {
		return de_novo;
	}
	public void setDe_novo(String de_novo) {
		this.de_novo = de_novo;
	}
	public String getMutation() {
		return mutation;
	}
	public void setMutation(String mutation) {
		this.mutation = mutation;
	}
	public String getFamily_test_f() {
		return family_test_f;
	}
	public void setFamily_test_f(String family_test_f) {
		this.family_test_f = family_test_f;
	}
	public String getFamily_test_m() {
		return family_test_m;
	}
	public void setFamily_test_m(String family_test_m) {
		this.family_test_m = family_test_m;
	}
	public String getFamily_test_s() {
		return family_test_s;
	}
	public void setFamily_test_s(String family_test_s) {
		this.family_test_s = family_test_s;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getReported() {
		return reported;
	}
	public void setReported(String reported) {
		this.reported = reported;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}

	
	

}
