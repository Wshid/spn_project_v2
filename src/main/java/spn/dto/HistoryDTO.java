package spn.dto;

public class HistoryDTO {
	private String his_idx;
	private String iid;
	private String name;
	private String chart_no;
	private String snupn;
	private String pa_name;
	private String page_name;
	private String idate;
	 // patient name
	
	public String getSnupn() {
		return snupn;
	}
	public void setSnupn(String snupn) {
		this.snupn = snupn;
	}
	private String pa_sex;
	
	public String getHis_idx() {
		return his_idx;
	}
	public void setHis_idx(String his_idx) {
		this.his_idx = his_idx;
	}
	public String getIid() {
		return iid;
	}
	public String getPa_name() {
		return pa_name;
	}
	public void setPa_name(String pa_name) {
		this.pa_name = pa_name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPa_sex() {
		return pa_sex;
	}
	public void setPa_sex(String pa_sex) {
		this.pa_sex = pa_sex;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getPage_name() {
		return page_name;
	}
	public void setPage_name(String page_name) {
		this.page_name = page_name;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	
	
	

}
