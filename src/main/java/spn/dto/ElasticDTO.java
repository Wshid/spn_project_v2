package spn.dto;

import org.json.simple.JSONObject;

public class ElasticDTO {
	private String size; // GET
	private String searchText; // String // SearchText => makeQueryString => QueryText
	private String requestJsonString; // JSON
	private String responseJsonString; // return
	//private String query_json; // Json String
	private String url; // Elastic index, Related URL
	private String menu;
	private String _id;
	private String count;
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getSearchText() {
		return searchText;
	}
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}
	public String getRequestJsonString() {
		return requestJsonString;
	}
	public void setRequestJsonString(String requestJsonString) {
		this.requestJsonString = requestJsonString;
	}
	public String getResponseJsonString() {
		return responseJsonString;
	}
	public void setResponseJsonString(String responseJsonString) {
		this.responseJsonString = responseJsonString;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}	
	public String getMenu() {
		return menu;
	}
	public void setMenu(String menu) {
		this.menu = menu;
	}
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	
	
}
