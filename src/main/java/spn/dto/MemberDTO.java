package spn.dto;

import java.sql.Date;

public class MemberDTO {
	private String mb_no;
	private String id;
	private String pw;
	private String name;
	private String affiliation;
	private String phone;
	private String permission;
	private String regdate;
	public String getMb_no() {
		return mb_no;
	}
	public void setMb_no(String mb_no) {
		this.mb_no = mb_no;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAffiliation() {
		return affiliation;
	}
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPermission() {
		return permission;
	}
	public void setPermission(String permission) {
		this.permission = permission;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}

	/* get/set �ڵ� �ϼ� : Shift + Alt + s */
	
}
