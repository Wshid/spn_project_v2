package spn.dto;

public class SampleCfdnaDTO {
	private String cfd_no;
	private String chart_no;
	private String col_date;
	private String col_location;
	private String col_who;
	private String col_who_patient_detail;
	private String col_who_father_detail;
	private String col_who_mother_detail;
	private String col_who_sibling_detail;
	private String col_who_other_detail;
	private String col_index;
	private String col_wk;
	private String col_plasma_volume;
	private String col_cf_no;
	private String col_concentration_1;
	private String col_concentration_2;
	private String col_260_280_1;
	private String col_260_280_2;
	private String col_260_230_1;
	private String col_260_230_2;
	private String col_bioanalyzer_size_1;
	private String col_bioanalyzer_size_2;
	private String col_bioanalyzer_concentation_1;
	private String col_bioanalyzer_concentation_2;
	private String col_comment;
	private String req_date;
	private String req_location;
	private String req_who;
	private String req_who_other_detail;
	private String req_distribution;
	private String req_order_name;
	private String req_vial;
	private String req_comment;
	private String idate;
	private String iid;

	public String getCfd_no() {
		return cfd_no;
	}
	public void setCfd_no(String cfd_no) {
		this.cfd_no = cfd_no;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getCol_date() {
		return col_date;
	}
	public void setCol_date(String col_date) {
		this.col_date = col_date;
	}
	public String getCol_location() {
		return col_location;
	}
	public void setCol_location(String col_location) {
		this.col_location = col_location;
	}
	public String getCol_who() {
		return col_who;
	}
	public void setCol_who(String col_who) {
		this.col_who = col_who;
	}
	public String getCol_who_patient_detail() {
		return col_who_patient_detail;
	}
	public void setCol_who_patient_detail(String col_who_patient_detail) {
		this.col_who_patient_detail = col_who_patient_detail;
	}
	public String getCol_who_father_detail() {
		return col_who_father_detail;
	}
	public void setCol_who_father_detail(String col_who_father_detail) {
		this.col_who_father_detail = col_who_father_detail;
	}
	public String getCol_who_mother_detail() {
		return col_who_mother_detail;
	}
	public void setCol_who_mother_detail(String col_who_mother_detail) {
		this.col_who_mother_detail = col_who_mother_detail;
	}
	public String getCol_who_sibling_detail() {
		return col_who_sibling_detail;
	}
	public void setCol_who_sibling_detail(String col_who_sibling_detail) {
		this.col_who_sibling_detail = col_who_sibling_detail;
	}
	public String getCol_who_other_detail() {
		return col_who_other_detail;
	}
	public void setCol_who_other_detail(String col_who_other_detail) {
		this.col_who_other_detail = col_who_other_detail;
	}
	public String getCol_index() {
		return col_index;
	}
	public void setCol_index(String col_index) {
		this.col_index = col_index;
	}
	public String getCol_wk() {
		return col_wk;
	}
	public void setCol_wk(String col_wk) {
		this.col_wk = col_wk;
	}
	public String getCol_plasma_volume() {
		return col_plasma_volume;
	}
	public void setCol_plasma_volume(String col_plasma_volume) {
		this.col_plasma_volume = col_plasma_volume;
	}
	public String getCol_cf_no() {
		return col_cf_no;
	}
	public void setCol_cf_no(String col_cf_no) {
		this.col_cf_no = col_cf_no;
	}
	public String getCol_concentration_1() {
		return col_concentration_1;
	}
	public void setCol_concentration_1(String col_concentration_1) {
		this.col_concentration_1 = col_concentration_1;
	}
	public String getCol_concentration_2() {
		return col_concentration_2;
	}
	public void setCol_concentration_2(String col_concentration_2) {
		this.col_concentration_2 = col_concentration_2;
	}
	public String getCol_260_280_1() {
		return col_260_280_1;
	}
	public void setCol_260_280_1(String col_260_280_1) {
		this.col_260_280_1 = col_260_280_1;
	}
	public String getCol_260_280_2() {
		return col_260_280_2;
	}
	public void setCol_260_280_2(String col_260_280_2) {
		this.col_260_280_2 = col_260_280_2;
	}
	public String getCol_260_230_1() {
		return col_260_230_1;
	}
	public void setCol_260_230_1(String col_260_230_1) {
		this.col_260_230_1 = col_260_230_1;
	}
	public String getCol_260_230_2() {
		return col_260_230_2;
	}
	public void setCol_260_230_2(String col_260_230_2) {
		this.col_260_230_2 = col_260_230_2;
	}
	public String getCol_bioanalyzer_size_1() {
		return col_bioanalyzer_size_1;
	}
	public void setCol_bioanalyzer_size_1(String col_bioanalyzer_size_1) {
		this.col_bioanalyzer_size_1 = col_bioanalyzer_size_1;
	}
	public String getCol_bioanalyzer_size_2() {
		return col_bioanalyzer_size_2;
	}
	public void setCol_bioanalyzer_size_2(String col_bioanalyzer_size_2) {
		this.col_bioanalyzer_size_2 = col_bioanalyzer_size_2;
	}
	public String getCol_bioanalyzer_concentation_1() {
		return col_bioanalyzer_concentation_1;
	}
	public void setCol_bioanalyzer_concentation_1(
			String col_bioanalyzer_concentation_1) {
		this.col_bioanalyzer_concentation_1 = col_bioanalyzer_concentation_1;
	}
	public String getCol_bioanalyzer_concentation_2() {
		return col_bioanalyzer_concentation_2;
	}
	public void setCol_bioanalyzer_concentation_2(
			String col_bioanalyzer_concentation_2) {
		this.col_bioanalyzer_concentation_2 = col_bioanalyzer_concentation_2;
	}
	public String getCol_comment() {
		return col_comment;
	}
	public void setCol_comment(String col_comment) {
		this.col_comment = col_comment;
	}
	public String getReq_date() {
		return req_date;
	}
	public void setReq_date(String req_date) {
		this.req_date = req_date;
	}
	public String getReq_location() {
		return req_location;
	}
	public void setReq_location(String req_location) {
		this.req_location = req_location;
	}
	public String getReq_who() {
		return req_who;
	}
	public void setReq_who(String req_who) {
		this.req_who = req_who;
	}
	public String getReq_who_other_detail() {
		return req_who_other_detail;
	}
	public void setReq_who_other_detail(String req_who_other_detail) {
		this.req_who_other_detail = req_who_other_detail;
	}
	public String getReq_distribution() {
		return req_distribution;
	}
	public void setReq_distribution(String req_distribution) {
		this.req_distribution = req_distribution;
	}
	public String getReq_order_name() {
		return req_order_name;
	}
	public void setReq_order_name(String req_order_name) {
		this.req_order_name = req_order_name;
	}
	public String getReq_vial() {
		return req_vial;
	}
	public void setReq_vial(String req_vial) {
		this.req_vial = req_vial;
	}
	public String getReq_comment() {
		return req_comment;
	}
	public void setReq_comment(String req_comment) {
		this.req_comment = req_comment;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	
	

}
