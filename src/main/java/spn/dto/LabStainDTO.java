package spn.dto;

public class LabStainDTO {
	private String sms_no;
	private String chart_no;
	private String experiment;
	private String m_index_no;
	private String discription;
	private String pathology_diagnosis;
	private String result;
	private String comment;
	private String exp_date_1;
	private String antibody_1;
	private String antibody_2;
	private String antibody_3;
	private String antibody_4;
	private String antibody_5;
	private String antibody_6;
	private String antibody_7;
	private String antibody_8;
	private String antibody_9;
	private String antibody_10;
	private String antibody_11;
	private String antibody_12;
	private String antibody_13;
	private String antibody_14;
	private String antibody_15;
	private String antibody_16;
	private String antibody_17;
	private String antibody_18;
	private String exp_date_2;
	private String antibody_19;
	private String antibody_20;
	private String antibody_21;
	private String antibody_22;
	private String antibody_23;
	private String antibody_24;
	private String antibody_25;
	private String idate;
	private String iid;
	
	
	public String getSms_no() {
		return sms_no;
	}
	public void setSms_no(String sms_no) {
		this.sms_no = sms_no;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getExperiment() {
		return experiment;
	}
	public void setExperiment(String experiment) {
		this.experiment = experiment;
	}
	public String getM_index_no() {
		return m_index_no;
	}
	public void setM_index_no(String m_index_no) {
		this.m_index_no = m_index_no;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public String getPathology_diagnosis() {
		return pathology_diagnosis;
	}
	public void setPathology_diagnosis(String pathology_diagnosis) {
		this.pathology_diagnosis = pathology_diagnosis;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getExp_date_1() {
		return exp_date_1;
	}
	public void setExp_date_1(String exp_date_1) {
		this.exp_date_1 = exp_date_1;
	}
	public String getAntibody_1() {
		return antibody_1;
	}
	public void setAntibody_1(String antibody_1) {
		this.antibody_1 = antibody_1;
	}
	public String getAntibody_2() {
		return antibody_2;
	}
	public void setAntibody_2(String antibody_2) {
		this.antibody_2 = antibody_2;
	}
	public String getAntibody_3() {
		return antibody_3;
	}
	public void setAntibody_3(String antibody_3) {
		this.antibody_3 = antibody_3;
	}
	public String getAntibody_4() {
		return antibody_4;
	}
	public void setAntibody_4(String antibody_4) {
		this.antibody_4 = antibody_4;
	}
	public String getAntibody_5() {
		return antibody_5;
	}
	public void setAntibody_5(String antibody_5) {
		this.antibody_5 = antibody_5;
	}
	public String getAntibody_6() {
		return antibody_6;
	}
	public void setAntibody_6(String antibody_6) {
		this.antibody_6 = antibody_6;
	}
	public String getAntibody_7() {
		return antibody_7;
	}
	public void setAntibody_7(String antibody_7) {
		this.antibody_7 = antibody_7;
	}
	public String getAntibody_8() {
		return antibody_8;
	}
	public void setAntibody_8(String antibody_8) {
		this.antibody_8 = antibody_8;
	}
	public String getAntibody_9() {
		return antibody_9;
	}
	public void setAntibody_9(String antibody_9) {
		this.antibody_9 = antibody_9;
	}
	public String getAntibody_10() {
		return antibody_10;
	}
	public void setAntibody_10(String antibody_10) {
		this.antibody_10 = antibody_10;
	}
	public String getAntibody_11() {
		return antibody_11;
	}
	public void setAntibody_11(String antibody_11) {
		this.antibody_11 = antibody_11;
	}
	public String getAntibody_12() {
		return antibody_12;
	}
	public void setAntibody_12(String antibody_12) {
		this.antibody_12 = antibody_12;
	}
	public String getAntibody_13() {
		return antibody_13;
	}
	public void setAntibody_13(String antibody_13) {
		this.antibody_13 = antibody_13;
	}
	public String getAntibody_14() {
		return antibody_14;
	}
	public void setAntibody_14(String antibody_14) {
		this.antibody_14 = antibody_14;
	}
	public String getAntibody_15() {
		return antibody_15;
	}
	public void setAntibody_15(String antibody_15) {
		this.antibody_15 = antibody_15;
	}
	public String getAntibody_16() {
		return antibody_16;
	}
	public void setAntibody_16(String antibody_16) {
		this.antibody_16 = antibody_16;
	}
	public String getAntibody_17() {
		return antibody_17;
	}
	public void setAntibody_17(String antibody_17) {
		this.antibody_17 = antibody_17;
	}
	public String getAntibody_18() {
		return antibody_18;
	}
	public void setAntibody_18(String antibody_18) {
		this.antibody_18 = antibody_18;
	}
	public String getExp_date_2() {
		return exp_date_2;
	}
	public void setExp_date_2(String exp_date_2) {
		this.exp_date_2 = exp_date_2;
	}
	public String getAntibody_19() {
		return antibody_19;
	}
	public void setAntibody_19(String antibody_19) {
		this.antibody_19 = antibody_19;
	}
	public String getAntibody_20() {
		return antibody_20;
	}
	public void setAntibody_20(String antibody_20) {
		this.antibody_20 = antibody_20;
	}
	public String getAntibody_21() {
		return antibody_21;
	}
	public void setAntibody_21(String antibody_21) {
		this.antibody_21 = antibody_21;
	}
	public String getAntibody_22() {
		return antibody_22;
	}
	public void setAntibody_22(String antibody_22) {
		this.antibody_22 = antibody_22;
	}
	public String getAntibody_23() {
		return antibody_23;
	}
	public void setAntibody_23(String antibody_23) {
		this.antibody_23 = antibody_23;
	}
	public String getAntibody_24() {
		return antibody_24;
	}
	public void setAntibody_24(String antibody_24) {
		this.antibody_24 = antibody_24;
	}
	public String getAntibody_25() {
		return antibody_25;
	}
	public void setAntibody_25(String antibody_25) {
		this.antibody_25 = antibody_25;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	
	
}
