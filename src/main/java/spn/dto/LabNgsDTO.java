package spn.dto;

public class LabNgsDTO {
	private String ngs_no;
	private String chart_no;
	private String exp_date;
	private String experiment;
	private String case_no;
	private String exp_place;
	private String sample_yn_f;
	private String sample_yn_m;
	private String sample_yn_s;
	private String sample_yn_o;
	private String sample_yn_o_detail;
	private String result;
	private String result_pending_detail;
	private String comment;
	private String idate;
	private String iid;
	
	public String getSample_yn_o_detail() {
		return sample_yn_o_detail;
	}
	public void setSample_yn_o_detail(String sample_yn_o_detail) {
		this.sample_yn_o_detail = sample_yn_o_detail;
	}
	public String getSample_yn_o() {
		return sample_yn_o;
	}
	public void setSample_yn_o(String sample_yn_o) {
		this.sample_yn_o = sample_yn_o;
	}
	
	public String getNgs_no() {
		return ngs_no;
	}
	public void setNgs_no(String ngs_no) {
		this.ngs_no = ngs_no;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getExp_date() {
		return exp_date;
	}
	public void setExp_date(String exp_date) {
		this.exp_date = exp_date;
	}
	public String getExperiment() {
		return experiment;
	}
	public void setExperiment(String experiment) {
		this.experiment = experiment;
	}
	public String getCase_no() {
		return case_no;
	}
	public void setCase_no(String case_no) {
		this.case_no = case_no;
	}
	public String getExp_place() {
		return exp_place;
	}
	public void setExp_place(String exp_place) {
		this.exp_place = exp_place;
	}
	public String getSample_yn_f() {
		return sample_yn_f;
	}
	public void setSample_yn_f(String sample_yn_f) {
		this.sample_yn_f = sample_yn_f;
	}
	public String getSample_yn_m() {
		return sample_yn_m;
	}
	public void setSample_yn_m(String sample_yn_m) {
		this.sample_yn_m = sample_yn_m;
	}
	public String getSample_yn_s() {
		return sample_yn_s;
	}
	public void setSample_yn_s(String sample_yn_s) {
		this.sample_yn_s = sample_yn_s;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getResult_pending_detail() {
		return result_pending_detail;
	}
	public void setResult_pending_detail(String result_pending_detail) {
		this.result_pending_detail = result_pending_detail;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	
	
	

}
