package spn.dto;

public class SampleTissueDTO {
	private String tis_no;
	private String chart_no;
	private String bio_date;
	//private String bio_type;
	private String bio_type_myoblast;
	private String bio_type_fibroblast;
	private String bio_type_u_epithelial;
	private String bio_type_u_ipsc;
	private String bio_type_other;
	private String bio_type_other_detail;
	private String bio_index_1;
	//private String bio_location_1;
	//private String bio_cell;
	//private String bio_index_2;
	//private String bio_phase;
	private String bio_storage;
	//private String bio_location_2;
	private String bio_comment;
	private String req_date;
	private String req_location;
	//private String req_distribution;
	private String req_order_name;
	//private String req_type;
	//private String req_type_other_detail;
	private String req_vial;
	private String req_comment;
	private String idate;
	private String iid;
	
		
	public String getTis_no() {
		return tis_no;
	}
	public void setTis_no(String tis_no) {
		this.tis_no = tis_no;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getBio_date() {
		return bio_date;
	}
	public void setBio_date(String bio_date) {
		this.bio_date = bio_date;
	}
	public String getBio_type_myoblast() {
		return bio_type_myoblast;
	}
	public void setBio_type_myoblast(String bio_type_myoblast) {
		this.bio_type_myoblast = bio_type_myoblast;
	}
	public String getBio_type_fibroblast() {
		return bio_type_fibroblast;
	}
	public void setBio_type_fibroblast(String bio_type_fibroblast) {
		this.bio_type_fibroblast = bio_type_fibroblast;
	}
	public String getBio_type_u_epithelial() {
		return bio_type_u_epithelial;
	}
	public void setBio_type_u_epithelial(String bio_type_u_epithelial) {
		this.bio_type_u_epithelial = bio_type_u_epithelial;
	}
	public String getBio_type_u_ipsc() {
		return bio_type_u_ipsc;
	}
	public void setBio_type_u_ipsc(String bio_type_u_ipsc) {
		this.bio_type_u_ipsc = bio_type_u_ipsc;
	}
	public String getBio_type_other() {
		return bio_type_other;
	}
	public void setBio_type_other(String bio_type_other) {
		this.bio_type_other = bio_type_other;
	}
	/*
	public String getBio_type() {
		return bio_type;
	}
	public void setBio_type(String bio_type) {
		this.bio_type = bio_type;
	}*/
	public String getBio_type_other_detail() {
		return bio_type_other_detail;
	}
	public void setBio_type_other_detail(String bio_type_other_detail) {
		this.bio_type_other_detail = bio_type_other_detail;
	}
	public String getBio_index_1() {
		return bio_index_1;
	}
	public void setBio_index_1(String bio_index_1) {
		this.bio_index_1 = bio_index_1;
	}/*
	public String getBio_location_1() {
		return bio_location_1;
	}
	public void setBio_location_1(String bio_location_1) {
		this.bio_location_1 = bio_location_1;
	}
	public String getBio_cell() {
		return bio_cell;
	}
	public void setBio_cell(String bio_cell) {
		this.bio_cell = bio_cell;
	}
	public String getBio_index_2() {
		return bio_index_2;
	}
	public void setBio_index_2(String bio_index_2) {
		this.bio_index_2 = bio_index_2;
	}
	public String getBio_phase() {
		return bio_phase;
	}
	public void setBio_phase(String bio_phase) {
		this.bio_phase = bio_phase;
	}*/
	public String getBio_storage() {
		return bio_storage;
	}
	public void setBio_storage(String bio_storage) {
		this.bio_storage = bio_storage;
	}
	/*
	public String getBio_location_2() {
		return bio_location_2;
	}
	public void setBio_location_2(String bio_location_2) {
		this.bio_location_2 = bio_location_2;
	}
	*/
	public String getBio_comment() {
		return bio_comment;
	}
	public void setBio_comment(String bio_comment) {
		this.bio_comment = bio_comment;
	}
	public String getReq_date() {
		return req_date;
	}
	public void setReq_date(String req_date) {
		this.req_date = req_date;
	}
	public String getReq_location() {
		return req_location;
	}
	public void setReq_location(String req_location) {
		this.req_location = req_location;
	}
	/*
	public String getReq_distribution() {
		return req_distribution;
	}
	public void setReq_distribution(String req_distribution) {
		this.req_distribution = req_distribution;
	}
	*/
	public String getReq_order_name() {
		return req_order_name;
	}
	public void setReq_order_name(String req_order_name) {
		this.req_order_name = req_order_name;
	}
	/*
	public String getReq_type() {
		return req_type;
	}
	public void setReq_type(String req_type) {
		this.req_type = req_type;
	}
	public String getReq_type_other_detail() {
		return req_type_other_detail;
	}
	public void setReq_type_other_detail(String req_type_other_detail) {
		this.req_type_other_detail = req_type_other_detail;
	}*/
	public String getReq_vial() {
		return req_vial;
	}
	public void setReq_vial(String req_vial) {
		this.req_vial = req_vial;
	}
	public String getReq_comment() {
		return req_comment;
	}
	public void setReq_comment(String req_comment) {
		this.req_comment = req_comment;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	

}
