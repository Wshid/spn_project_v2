package spn.dto;

public class LabCghDTO {
	private String cgh_no;
	private String chart_no;
	private String exp_date;
	private String chip_no;
	private String chromosome_study_yn;
	private String fish_confirmation_yn;
	private String qpcr_yn;
	private String platform;
	private String platform_other_detail;
	private String parents_test_f;
	private String parents_test_m;
	private String cnv_type;
	private String pathogenic_result_location;
	private String pathogenic_result_size;
	private String pathogenic_result_url;
	private String non_pathogenic_result_location;
	private String non_pathogenic_result_size;
	private String non_pathogenic_result_url;
	private String comment;
	private String idate;
	private String iid;
	
	
	public String getCgh_no() {
		return cgh_no;
	}
	public void setCgh_no(String cgh_no) {
		this.cgh_no = cgh_no;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getExp_date() {
		return exp_date;
	}
	public void setExp_date(String exp_data) {
		this.exp_date = exp_data;
	}
	public String getChip_no() {
		return chip_no;
	}
	public void setChip_no(String chip_no) {
		this.chip_no = chip_no;
	}
	public String getChromosome_study_yn() {
		return chromosome_study_yn;
	}
	public void setChromosome_study_yn(String chromosome_study) {
		this.chromosome_study_yn = chromosome_study;
	}
	public String getFish_confirmation_yn() {
		return fish_confirmation_yn;
	}
	public void setFish_confirmation_yn(String fish_confirmation) {
		this.fish_confirmation_yn = fish_confirmation;
	}
	public String getQpcr_yn() {
		return qpcr_yn;
	}
	public void setQpcr_yn(String qpcr_yn) {
		this.qpcr_yn = qpcr_yn;
	}
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
	public String getPlatform_other_detail() {
		return platform_other_detail;
	}
	public void setPlatform_other_detail(String platform_other_detail) {
		this.platform_other_detail = platform_other_detail;
	}
	public String getParents_test_f() {
		return parents_test_f;
	}
	public void setParents_test_f(String parents_test_f) {
		this.parents_test_f = parents_test_f;
	}
	public String getParents_test_m() {
		return parents_test_m;
	}
	public void setParents_test_m(String parents_test_m) {
		this.parents_test_m = parents_test_m;
	}
	public String getCnv_type() {
		return cnv_type;
	}
	public void setCnv_type(String cnv_type) {
		this.cnv_type = cnv_type;
	}
	public String getPathogenic_result_location() {
		return pathogenic_result_location;
	}
	public void setPathogenic_result_location(String pathogenic_result_location) {
		this.pathogenic_result_location = pathogenic_result_location;
	}
	public String getPathogenic_result_size() {
		return pathogenic_result_size;
	}
	public void setPathogenic_result_size(String pathogenic_result_size) {
		this.pathogenic_result_size = pathogenic_result_size;
	}
	public String getPathogenic_result_url() {
		return pathogenic_result_url;
	}
	public void setPathogenic_result_url(String pathogenic_result_url) {
		this.pathogenic_result_url = pathogenic_result_url;
	}
	public String getNon_pathogenic_result_location() {
		return non_pathogenic_result_location;
	}
	public void setNon_pathogenic_result_location(String non_pathogenic_result_location) {
		this.non_pathogenic_result_location = non_pathogenic_result_location;
	}
	public String getNon_pathogenic_result_size() {
		return non_pathogenic_result_size;
	}
	public void setNon_pathogenic_result_size(String non_pathogenic_result_size) {
		this.non_pathogenic_result_size = non_pathogenic_result_size;
	}
	public String getNon_pathogenic_result_url() {
		return non_pathogenic_result_url;
	}
	public void setNon_pathogenic_result_url(String non_pathogenic_result_url) {
		this.non_pathogenic_result_url = non_pathogenic_result_url;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	
	

}
