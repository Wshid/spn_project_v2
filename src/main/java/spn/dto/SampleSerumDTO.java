package spn.dto;

public class SampleSerumDTO {
	private String sc_no;
	private String chart_no;
	//private String experiment;
	private String experiment_serum;
	private String experiment_csf;
	private String col_date;
	private String dis_date;
	private String col_index;
	private String no;
	private String storage;
	private String aquaporin_4_ab;
	private String autoimmune_encephalitis;
	private String order_other;
	private String order_other_detail;
	private String experiment_place;
	private String comment_1;
	private String location;
	private String comment_2;
	private String req_date;
	private String req_location;
	private String req_order_name;
	private String req_vial;
	private String req_comment;
	private String idate;
	private String iid;
	
	public String getSc_no() {
		return sc_no;
	}
	public void setSc_no(String sc_no) {
		this.sc_no = sc_no;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	/*
	public String getExperiment() {
		return experiment;
	}
	public void setExperiment(String experiment) {
		this.experiment = experiment;
	}
	*/
	public String getExperiment_serum() {
		return experiment_serum;
	}
	public void setExperiment_serum(String experiment_serum) {
		this.experiment_serum = experiment_serum;
	}
	public String getExperiment_csf() {
		return experiment_csf;
	}
	public void setExperiment_csf(String experiment_csf) {
		this.experiment_csf = experiment_csf;
	}
	public String getCol_date() {
		return col_date;
	}
	public void setCol_date(String col_date) {
		this.col_date = col_date;
	}
	public String getDis_date() {
		return dis_date;
	}
	public void setDis_date(String dis_date) {
		this.dis_date = dis_date;
	}
	public String getCol_index() {
		return col_index;
	}
	public void setCol_index(String col_index) {
		this.col_index = col_index;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	public String getAquaporin_4_ab() {
		return aquaporin_4_ab;
	}
	public void setAquaporin_4_ab(String aquaporin_4_ab) {
		this.aquaporin_4_ab = aquaporin_4_ab;
	}
	public String getAutoimmune_encephalitis() {
		return autoimmune_encephalitis;
	}
	public void setAutoimmune_encephalitis(String autoimmune_encephalitis) {
		this.autoimmune_encephalitis = autoimmune_encephalitis;
	}
	public String getOrder_other() {
		return order_other;
	}
	public void setOrder_other(String order_other) {
		this.order_other = order_other;
	}
	public String getOrder_other_detail() {
		return order_other_detail;
	}
	public void setOrder_other_detail(String order_other_detail) {
		this.order_other_detail = order_other_detail;
	}
	public String getExperiment_place() {
		return experiment_place;
	}
	public void setExperiment_place(String experiment_place) {
		this.experiment_place = experiment_place;
	}
	public String getComment_1() {
		return comment_1;
	}
	public void setComment_1(String comment_1) {
		this.comment_1 = comment_1;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getComment_2() {
		return comment_2;
	}
	public void setComment_2(String comment_2) {
		this.comment_2 = comment_2;
	}
	public String getReq_date() {
		return req_date;
	}
	public void setReq_date(String req_date) {
		this.req_date = req_date;
	}
	public String getReq_location() {
		return req_location;
	}
	public void setReq_location(String req_location) {
		this.req_location = req_location;
	}
	public String getReq_order_name() {
		return req_order_name;
	}
	public void setReq_order_name(String req_order_name) {
		this.req_order_name = req_order_name;
	}
	public String getReq_vial() {
		return req_vial;
	}
	public void setReq_vial(String req_vial) {
		this.req_vial = req_vial;
	}
	public String getReq_comment() {
		return req_comment;
	}
	public void setReq_comment(String req_comment) {
		this.req_comment = req_comment;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	
	

}
