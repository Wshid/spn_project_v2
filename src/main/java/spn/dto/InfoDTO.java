package spn.dto;

public class InfoDTO {
	private String gi_no;
	private String name; // 필드에서는 접근제어자를 private으로 사용한다. 객체지향 특성중의 정보은닉 특성때문에
	private String snupn;
	private String sex;
	private String chart_no;
	private String birth;
	private String category;
	private String doctor;
	private String other_site; // 새로 추가 됨
	//private String refered_dr;
	//private String final_diagnosis;
	private String clinical_diagnosis;
	private String symptom;
	//private String remark;
	private String idate;
	private String iid;
	
	
	public String getGi_no() {
		return gi_no;
	}
	public void setGi_no(String gi_no) {
		this.gi_no = gi_no;
	}
	
	public String getOther_site() {
		return other_site;
	}
	public void setOther_site(String other_site) {
		this.other_site = other_site;
	}
	
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSnupn() {
		return snupn;
	}
	public void setSnupn(String snupn) {
		this.snupn = snupn;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDoctor() {
		return doctor;
	}
	public void setDoctor(String doctor) {
		this.doctor = doctor;
	}
	/*
	public String getRefered_dr() {
		return refered_dr;
	}
	public void setRefered_dr(String refered_dr) {
		this.refered_dr = refered_dr;
	}*/
	/*
	public String getFinal_diagnosis() {
		return final_diagnosis;
	}
	public void setFinal_diagnosis(String final_diagnosis) {
		this.final_diagnosis = final_diagnosis;
	}
	*/
	public String getClinical_diagnosis() {
		return clinical_diagnosis;
	}
	public void setClinical_diagnosis(String clinical_diagnosis) {
		this.clinical_diagnosis = clinical_diagnosis;
	}
	public String getSymptom() {
		return symptom;
	}
	public void setSymptom(String symptom) {
		this.symptom = symptom;
	}
	/*
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}*/
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	
	



}
