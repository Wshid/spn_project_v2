package spn.dto;

public class SampleUrineDTO {
	private String uri_no;
	private String chart_no;
	private String urine;
	private String urine_col_date;
	private String urine_index;
	private String urine_volume;
	private String urine_storage;
	private String urine_location;
	private String urine_comment;
	private String uipsc;
	private String uipsc_col_date;
	private String uipsc_index;
	private String uipsc_storage;
	private String uipsc_location;
	private String uipsc_comment;
	private String req_date;
	private String req_distribution;
	private String req_order_name;
	private String req_vial;
	private String req_comment;
	private String idate;
	private String iid;
	
	public String getUri_no() {
		return uri_no;
	}
	public void setUri_no(String uri_no) {
		this.uri_no = uri_no;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getUrine() {
		return urine;
	}
	public void setUrine(String urine) {
		this.urine = urine;
	}
	public String getUrine_col_date() {
		return urine_col_date;
	}
	public void setUrine_col_date(String urine_col_date) {
		this.urine_col_date = urine_col_date;
	}
	public String getUrine_index() {
		return urine_index;
	}
	public void setUrine_index(String urine_index) {
		this.urine_index = urine_index;
	}
	public String getUrine_volume() {
		return urine_volume;
	}
	public void setUrine_volume(String urine_volume) {
		this.urine_volume = urine_volume;
	}
	public String getUrine_storage() {
		return urine_storage;
	}
	public void setUrine_storage(String urine_storage) {
		this.urine_storage = urine_storage;
	}
	public String getUrine_location() {
		return urine_location;
	}
	public void setUrine_location(String urine_location) {
		this.urine_location = urine_location;
	}
	public String getUrine_comment() {
		return urine_comment;
	}
	public void setUrine_comment(String urine_comment) {
		this.urine_comment = urine_comment;
	}
	public String getUipsc() {
		return uipsc;
	}
	public void setUipsc(String uipsc) {
		this.uipsc = uipsc;
	}
	public String getUipsc_col_date() {
		return uipsc_col_date;
	}
	public void setUipsc_col_date(String uipsc_col_date) {
		this.uipsc_col_date = uipsc_col_date;
	}
	public String getUipsc_index() {
		return uipsc_index;
	}
	public void setUipsc_index(String uipsc_index) {
		this.uipsc_index = uipsc_index;
	}
	public String getUipsc_storage() {
		return uipsc_storage;
	}
	public void setUipsc_storage(String uipsc_storage) {
		this.uipsc_storage = uipsc_storage;
	}
	public String getUipsc_location() {
		return uipsc_location;
	}
	public void setUipsc_location(String uipsc_location) {
		this.uipsc_location = uipsc_location;
	}
	public String getUipsc_comment() {
		return uipsc_comment;
	}
	public void setUipsc_comment(String uipsc_comment) {
		this.uipsc_comment = uipsc_comment;
	}
	public String getReq_date() {
		return req_date;
	}
	public void setReq_date(String req_date) {
		this.req_date = req_date;
	}
	public String getReq_distribution() {
		return req_distribution;
	}
	public void setReq_distribution(String req_distribution) {
		this.req_distribution = req_distribution;
	}
	public String getReq_order_name() {
		return req_order_name;
	}
	public void setReq_order_name(String req_order_name) {
		this.req_order_name = req_order_name;
	}
	public String getReq_vial() {
		return req_vial;
	}
	public void setReq_vial(String req_vial) {
		this.req_vial = req_vial;
	}
	public String getReq_comment() {
		return req_comment;
	}
	public void setReq_comment(String req_comment) {
		this.req_comment = req_comment;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	
	
}
