package spn.dto;

public class OverviewDTO {
	
	private String chart_no;
	private String dna_genomic;
	private String dna_cellfree;
	private String dna_mitochondria;
	private String tissue_muscle;
	private String tissue_myoblast;
	private String tissue_skin;
	private String tissue_fibroblast;
	private String cell_free_dna;
	private String serum;
	private String csf;
	private String urine_cell;
	private String uipsc;
	private String rna;
	private String informed_consent;
	private String sanger_order;
	private String sanger_ongoing;
	private String sanger_done;
	private String ngs_wgs_order;
	private String ngs_wgs_ongoing;
	private String ngs_wgs_done;
	private String ngs_wes_order;
	private String ngs_wes_ongoing;
	private String ngs_wes_done;
	private String ngs_targeted_order;
	private String ngs_targeted_ongoing;
	private String ngs_targeted_done;
	private String cgh_order;
	private String cgh_ongoing;
	private String cgh_done;
	private String muscle_stain_order;
	private String muscle_stain_ongoing;
	private String muscle_stain_done;
	private String western_order;
	private String western_ongoing;
	private String western_done;
	private String other_order;
	private String other_ongoing;
	private String other_done;
	private String iid;
	private String idate;
	
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getDna_genomic() {
		return dna_genomic;
	}
	public void setDna_genomic(String dna_genomic) {
		this.dna_genomic = dna_genomic;
	}
	public String getDna_cellfree() {
		return dna_cellfree;
	}
	public void setDna_cellfree(String dna_cellfree) {
		this.dna_cellfree = dna_cellfree;
	}
	public String getDna_mitochondria() {
		return dna_mitochondria;
	}
	public void setDna_mitochondria(String dna_mitochondria) {
		this.dna_mitochondria = dna_mitochondria;
	}
	public String getTissue_muscle() {
		return tissue_muscle;
	}
	public void setTissue_muscle(String tissue_muscle) {
		this.tissue_muscle = tissue_muscle;
	}
	public String getTissue_myoblast() {
		return tissue_myoblast;
	}
	public void setTissue_myoblast(String tissue_myoblast) {
		this.tissue_myoblast = tissue_myoblast;
	}
	public String getTissue_skin() {
		return tissue_skin;
	}
	public void setTissue_skin(String tissue_skin) {
		this.tissue_skin = tissue_skin;
	}
	public String getTissue_fibroblast() {
		return tissue_fibroblast;
	}
	public void setTissue_fibroblast(String tissue_fibroblast) {
		this.tissue_fibroblast = tissue_fibroblast;
	}
	public String getCell_free_dna() {
		return cell_free_dna;
	}
	public void setCell_free_dna(String cell_free_dna) {
		this.cell_free_dna = cell_free_dna;
	}
	public String getSerum() {
		return serum;
	}
	public void setSerum(String serum) {
		this.serum = serum;
	}
	public String getCsf() {
		return csf;
	}
	public void setCsf(String csf) {
		this.csf = csf;
	}
	public String getUrine_cell() {
		return urine_cell;
	}
	public void setUrine_cell(String urine_cell) {
		this.urine_cell = urine_cell;
	}
	public String getUipsc() {
		return uipsc;
	}
	public void setUipsc(String uipsc) {
		this.uipsc = uipsc;
	}
	public String getRna() {
		return rna;
	}
	public void setRna(String rna) {
		this.rna = rna;
	}
	public String getInformed_consent() {
		return informed_consent;
	}
	public void setInformed_consent(String informed_consent) {
		this.informed_consent = informed_consent;
	}
	public String getSanger_order() {
		return sanger_order;
	}
	public void setSanger_order(String sanger_order) {
		this.sanger_order = sanger_order;
	}
	public String getSanger_ongoing() {
		return sanger_ongoing;
	}
	public void setSanger_ongoing(String sanger_ongoing) {
		this.sanger_ongoing = sanger_ongoing;
	}
	public String getSanger_done() {
		return sanger_done;
	}
	public void setSanger_done(String sanger_done) {
		this.sanger_done = sanger_done;
	}
	public String getNgs_wgs_order() {
		return ngs_wgs_order;
	}
	public void setNgs_wgs_order(String ngs_wgs_order) {
		this.ngs_wgs_order = ngs_wgs_order;
	}
	public String getNgs_wgs_ongoing() {
		return ngs_wgs_ongoing;
	}
	public void setNgs_wgs_ongoing(String ngs_wgs_ongoing) {
		this.ngs_wgs_ongoing = ngs_wgs_ongoing;
	}
	public String getNgs_wgs_done() {
		return ngs_wgs_done;
	}
	public void setNgs_wgs_done(String ngs_wgs_done) {
		this.ngs_wgs_done = ngs_wgs_done;
	}
	public String getNgs_wes_order() {
		return ngs_wes_order;
	}
	public void setNgs_wes_order(String ngs_wes_order) {
		this.ngs_wes_order = ngs_wes_order;
	}
	public String getNgs_wes_ongoing() {
		return ngs_wes_ongoing;
	}
	public void setNgs_wes_ongoing(String ngs_wes_ongoing) {
		this.ngs_wes_ongoing = ngs_wes_ongoing;
	}
	public String getNgs_wes_done() {
		return ngs_wes_done;
	}
	public void setNgs_wes_done(String ngs_wes_done) {
		this.ngs_wes_done = ngs_wes_done;
	}
	public String getNgs_targeted_order() {
		return ngs_targeted_order;
	}
	public void setNgs_targeted_order(String ngs_targeted_order) {
		this.ngs_targeted_order = ngs_targeted_order;
	}
	public String getNgs_targeted_ongoing() {
		return ngs_targeted_ongoing;
	}
	public void setNgs_targeted_ongoing(String ngs_targeted_ongoing) {
		this.ngs_targeted_ongoing = ngs_targeted_ongoing;
	}
	public String getNgs_targeted_done() {
		return ngs_targeted_done;
	}
	public void setNgs_targeted_done(String ngs_targeted_done) {
		this.ngs_targeted_done = ngs_targeted_done;
	}
	public String getCgh_order() {
		return cgh_order;
	}
	public void setCgh_order(String cgh_order) {
		this.cgh_order = cgh_order;
	}
	public String getCgh_ongoing() {
		return cgh_ongoing;
	}
	public void setCgh_ongoing(String cgh_ongoing) {
		this.cgh_ongoing = cgh_ongoing;
	}
	public String getCgh_done() {
		return cgh_done;
	}
	public void setCgh_done(String cgh_done) {
		this.cgh_done = cgh_done;
	}
	public String getMuscle_stain_order() {
		return muscle_stain_order;
	}
	public void setMuscle_stain_order(String muscle_stain_order) {
		this.muscle_stain_order = muscle_stain_order;
	}
	public String getMuscle_stain_ongoing() {
		return muscle_stain_ongoing;
	}
	public void setMuscle_stain_ongoing(String muscle_stain_ongoing) {
		this.muscle_stain_ongoing = muscle_stain_ongoing;
	}
	public String getMuscle_stain_done() {
		return muscle_stain_done;
	}
	public void setMuscle_stain_done(String muscle_stain_done) {
		this.muscle_stain_done = muscle_stain_done;
	}
	public String getWestern_order() {
		return western_order;
	}
	public void setWestern_order(String western_order) {
		this.western_order = western_order;
	}
	public String getWestern_ongoing() {
		return western_ongoing;
	}
	public void setWestern_ongoing(String western_ongoing) {
		this.western_ongoing = western_ongoing;
	}
	public String getWestern_done() {
		return western_done;
	}
	public void setWestern_done(String western_done) {
		this.western_done = western_done;
	}
	public String getOther_order() {
		return other_order;
	}
	public void setOther_order(String other_order) {
		this.other_order = other_order;
	}
	public String getOther_ongoing() {
		return other_ongoing;
	}
	public void setOther_ongoing(String other_ongoing) {
		this.other_ongoing = other_ongoing;
	}
	public String getOther_done() {
		return other_done;
	}
	public void setOther_done(String other_done) {
		this.other_done = other_done;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	
	
	
}
