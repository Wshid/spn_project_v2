package spn.dto;

public class SampleRnaDTO {
	private String rna_no;
	private String chart_no;
	private String col_date;
	private String col_type;
	private String col_type_other_detail;
	private String col_index;
	private String col_concentration;
	private String col_260_280;
	private String col_260_230;
	private String col_location;
	private String col_comment;
	private String req_date;
	private String req_location;
	private String req_distribution;
	private String req_distribution_other_detail;
	private String req_order_name;
	private String req_vial;
	private String req_comment;
	private String idate;
	private String iid;
	

	public String getRna_no() {
		return rna_no;
	}
	public void setRna_no(String rna_no) {
		this.rna_no = rna_no;
	}
	public String getChart_no() {
		return chart_no;
	}
	public void setChart_no(String chart_no) {
		this.chart_no = chart_no;
	}
	public String getCol_date() {
		return col_date;
	}
	public void setCol_date(String col_date) {
		this.col_date = col_date;
	}
	public String getCol_type() {
		return col_type;
	}
	public void setCol_type(String col_type) {
		this.col_type = col_type;
	}
	public String getCol_type_other_detail() {
		return col_type_other_detail;
	}
	public void setCol_type_other_detail(String col_type_other_detail) {
		this.col_type_other_detail = col_type_other_detail;
	}
	public String getCol_index() {
		return col_index;
	}
	public void setCol_index(String col_index) {
		this.col_index = col_index;
	}
	public String getCol_concentration() {
		return col_concentration;
	}
	public void setCol_concentration(String col_concentration) {
		this.col_concentration = col_concentration;
	}
	public String getCol_260_280() {
		return col_260_280;
	}
	public void setCol_260_280(String col_260_280) {
		this.col_260_280 = col_260_280;
	}
	public String getCol_260_230() {
		return col_260_230;
	}
	public void setCol_260_230(String col_260_230) {
		this.col_260_230 = col_260_230;
	}
	public String getCol_location() {
		return col_location;
	}
	public void setCol_location(String col_location) {
		this.col_location = col_location;
	}
	public String getCol_comment() {
		return col_comment;
	}
	public void setCol_comment(String col_comment) {
		this.col_comment = col_comment;
	}
	public String getReq_date() {
		return req_date;
	}
	public void setReq_date(String req_date) {
		this.req_date = req_date;
	}
	public String getReq_location() {
		return req_location;
	}
	public void setReq_location(String req_location) {
		this.req_location = req_location;
	}
	public String getReq_distribution() {
		return req_distribution;
	}
	public void setReq_distribution(String req_distribution) {
		this.req_distribution = req_distribution;
	}
	public String getReq_distribution_other_detail() {
		return req_distribution_other_detail;
	}
	public void setReq_distribution_other_detail(
			String req_distribution_other_detail) {
		this.req_distribution_other_detail = req_distribution_other_detail;
	}
	public String getReq_order_name() {
		return req_order_name;
	}
	public void setReq_order_name(String req_order_name) {
		this.req_order_name = req_order_name;
	}
	public String getReq_vial() {
		return req_vial;
	}
	public void setReq_vial(String req_vial) {
		this.req_vial = req_vial;
	}
	public String getReq_comment() {
		return req_comment;
	}
	public void setReq_comment(String req_comment) {
		this.req_comment = req_comment;
	}
	public String getIdate() {
		return idate;
	}
	public void setIdate(String idate) {
		this.idate = idate;
	}
	public String getIid() {
		return iid;
	}
	public void setIid(String iid) {
		this.iid = iid;
	}
	

}
