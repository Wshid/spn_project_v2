package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.InfoDAO;
import spn.dto.InfoDTO;

public class RegisterFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward=new ActionForward();
		HttpSession session = request.getSession();
		if(session.getAttribute("id")==null){
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		}
		else{
			forward.setForward(true);
			forward.setNextURL("./SPN/Register.jsp");
			return forward;
		}
	}

}