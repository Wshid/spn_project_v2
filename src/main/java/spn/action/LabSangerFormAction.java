package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.DbImageDAO;
import spn.dao.HistoryDAO;
import spn.dao.InfoDAO;
import spn.dao.LabDAO;
import spn.dto.DbImageDTO;
import spn.dto.HistoryDTO;
import spn.dto.InfoDTO;
import spn.dto.LabSangerDTO;

public class LabSangerFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward=new ActionForward();
		HttpSession session = request.getSession();
		if(session.getAttribute("id")==null){
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			
			return forward;
		}
		else{
			String searchVal="";
			if(request.getParameter("searchVal")!=null) { //검색을 통한 접속이라면,
				searchVal=request.getParameter("searchVal");
				session.setAttribute("searchVal", searchVal);
				//searchVal=(String)session.getAttribute(request.getParameter("searchVal").toString());
			}
			else { // Info를 통한 접속일 
				searchVal = (String) session.getAttribute("searchVal");
			}
			//String searchVal = (String) session.getAttribute("searchVal");
			
			
			
			InfoDAO infoDAO=InfoDAO.getInfoDAO();
			
			List<InfoDTO> infoList = new ArrayList<InfoDTO>();
			infoList = (List) infoDAO.selectInfo(searchVal);
			
			if (infoList.size() != 0) {
				String snupn = infoList.get(0).getSnupn();
				String chart_no= infoList.get(0).getChart_no();
				String name = infoList.get(0).getName();
				String sex= infoList.get(0).getSex();
				String birth = infoList.get(0).getBirth();
				String doctor = infoList.get(0).getDoctor();
				String other_site= infoList.get(0).getOther_site();
				//String refered_dr= infoList.get(0).getRefered_dr();
				String category = infoList.get(0).getCategory();
				//String final_diagnosis = infoList.get(0).getFinal_diagnosis();
				String clinical_diagnosis = infoList.get(0).getClinical_diagnosis();
				String symptom = infoList.get(0).getSymptom();
				//String remark = infoList.get(0).getRemark();
				
				request.setAttribute("snupn", snupn);
				request.setAttribute("chart_no", chart_no);
				request.setAttribute("name", name);
				request.setAttribute("sex", sex);
				request.setAttribute("birth", birth);
				request.setAttribute("doctor", doctor);
				//request.setAttribute("refered_dr", refered_dr);
				request.setAttribute("other_site", other_site);
				request.setAttribute("category", category);
				//request.setAttribute("final_diagnosis", final_diagnosis);
				request.setAttribute("clinical_diagnosis", clinical_diagnosis);
				request.setAttribute("symptom", symptom);
				//request.setAttribute("remark", remark);
				
			}

			
			LabDAO dao=LabDAO.getLabDAO();
			
			List<LabSangerDTO> li = new ArrayList<LabSangerDTO>();
			li = (List) dao.selectSanger(searchVal);
			
			List<String> exp_date = new ArrayList<String>();
			List<String> gene= new ArrayList<String>();
			List<String> position = new ArrayList<String>();
			List<String> snp= new ArrayList<String>();
			List<String> type = new ArrayList<String>();
			List<String> de_novo = new ArrayList<String>();
			List<String> mutation= new ArrayList<String>();
			List<String> family_test_F = new ArrayList<String>();
			List<String> family_test_M = new ArrayList<String>();
			List<String> family_test_S = new ArrayList<String>();
			List<String> result = new ArrayList<String>();
			List<String> reported = new ArrayList<String>();
			List<String> comment = new ArrayList<String>();
			List<String> idate = new ArrayList<String>();
			List<String> iid = new ArrayList<String>();
			List<String> ss_no = new ArrayList<String>();
			
			
			if (li.size() != 0) {
				
				for(LabSangerDTO sd: li) {
					exp_date.add(sd.getExp_date());
					gene.add(sd.getGene());
					position.add(sd.getPosition());
					snp.add(sd.getSnp());
					type.add(sd.getType());
					de_novo.add(sd.getDe_novo());
					mutation.add(sd.getMutation());
					family_test_F.add(sd.getFamily_test_f());
					family_test_M.add(sd.getFamily_test_m());
					family_test_S.add(sd.getFamily_test_s());
					result.add(sd.getResult());
					reported.add(sd.getReported());
					comment.add(sd.getComment());
					idate.add(sd.getIdate());
					iid.add(sd.getIid());
					ss_no.add(sd.getSs_no());
				}
				
				//request.setAttribute("searchSangerNumber", ss_no); // searchSangerNumber�� Session�� �־��ָ鼭, ���� submit/change ��ư ��ǥ�� Ȱ��
				
			}
			

			request.setAttribute("exp_date", exp_date);
			request.setAttribute("gene", gene);
			request.setAttribute("position", position);
			request.setAttribute("snp", snp);
			request.setAttribute("type", type);
			request.setAttribute("de_novo", de_novo);
			request.setAttribute("mutation", mutation);
			request.setAttribute("family_test_F", family_test_F);
			request.setAttribute("family_test_M", family_test_M);
			request.setAttribute("family_test_S", family_test_S);
			request.setAttribute("result", result);
			request.setAttribute("reported", reported);
			request.setAttribute("comment", comment);
			request.setAttribute("idate", idate);
			request.setAttribute("iid", iid);
			request.setAttribute("ss_no", ss_no);
			
			
			/* IMAGE UPLOAD */
			/*
			request.setAttribute("menu", "lab_sanger"); // Menu�� �������ش�, ���� ���� �̸��� ���ӵȴ�.
			request.setAttribute("subtitle", "main"); // lab_sanger�� ��� ��� ������ �����ϹǷ�, ū �ǹ̸� ���� ����*/
			/* ==== */
			
			DbImageDAO dao_image=DbImageDAO.getDbImageDAO();
			List<DbImageDTO> li_image=new ArrayList<DbImageDTO>(); 
			li_image=(List)dao_image.selectDbImage(searchVal, "lab_sanger");
			ArrayList<String> paths=new ArrayList<String>();
			ArrayList<String> names=new ArrayList<String>();
			ArrayList<String> doctors=new ArrayList<String>();
			
			
			//int max_img_idx=0; SQL ���� �������� ���� ���ʿ�
			// fullpath�� �ʿ��ϹǷ�, �� ���븸 ������ �� �ֵ��� �Ѵ�.
			if(li_image.size()!=0){
				for(int i=0;i<li_image.size();i++){
					//int temp_max=Integer.parseInt(li_image.get(i).getImg_idx());
					/*
					if(temp_max>=max_img_idx){
						max_img_idx=temp_max; // ��ġ�� �ʴ�(Identifier)�� �ε����� �����´�.
					}*/
					names.add(li_image.get(i).getImage_name());
					paths.add(li_image.get(i).getFull_path());
					doctors.add(li_image.get(i).getDoctor());
				}
			}
			
			request.setAttribute("db_image", paths); // �ش� chartnum �׸��� page name�� �ش��ϴ� ������ ������ �����Ͽ� ��������.
			request.setAttribute("names", names);
			request.setAttribute("page_name", "lab_sanger");
			//request.setAttribute("next_idx", dao_image.getNextIndex(searchVal, page_name));
			request.setAttribute("next_idx", dao_image.getNextIndex());
			request.setAttribute("doctors", doctors);
			
			
			/* �� �Ǳ� ������ ��ġ ����
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			
			List<HistoryDTO> his_li=new ArrayList<HistoryDTO>();
			his_li=(List)his_dao.selectHistory(searchVal, "lab_sanger");
			
			System.out.println("LABSANGERFORM : "+his_li.size()); // ���ʿ� 0���� ������
			if(his_li.size()!=0){
				request.setAttribute("his_li", his_li);
			}*/
			
			
			/*
			if(his_li.size()!=0){ // size�� �ϳ��϶��� ���� ������ ���� �ƴϱ� ������
				for(int i=0; i<his_li.size();i++){
					String his_idx=his_li.get(i).getHis_idx();
					String his_iid=his_li.get(i).getIid();
					String his_chart_no=his_li.get(i).getChart_no();
					String his_page_num=his_li.get(i).getPage_name();
					String his_idate=his_li.get(i).getIdate();
					
					
				}

				
				request.setAttribute("his_idx", his_idx);
				request.setAttribute("his_iid", his_iid);
				request.setAttribute("his_chart_no", his_chart_no);
				request.setAttribute("his_page_num", his_page_num);
				request.setAttribute("his_idate", his_idate);
			}*/
	
		forward.setForward(true); // forward �������� ����. �ش� request�� �״�� �����Ͽ� �̵���
		forward.setNextURL("./SPN/LabSanger.jsp");
		return forward;
		}
	}

}