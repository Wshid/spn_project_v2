package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.InfoDAO;
import spn.dao.SampleDAO;
import spn.dto.InfoDTO;
import spn.dto.SampleUrineDTO;

public class SampleUrineFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		} 
		else {
			
			String searchVal = (String) session.getAttribute("searchVal");

			InfoDAO infoDAO=InfoDAO.getInfoDAO();
			
			List<InfoDTO> infoList = new ArrayList<InfoDTO>();
			infoList = (List) infoDAO.selectInfo(searchVal);
			
			if (infoList.size() != 0) {
				String snupn = infoList.get(0).getSnupn();
				String chart_no= infoList.get(0).getChart_no();
				String name = infoList.get(0).getName();
				String sex= infoList.get(0).getSex();
				String birth = infoList.get(0).getBirth();
				String doctor = infoList.get(0).getDoctor();
				//String refered_dr= infoList.get(0).getRefered_dr();
				String other_site= infoList.get(0).getOther_site();
				String category = infoList.get(0).getCategory();
				//String final_diagnosis = infoList.get(0).getFinal_diagnosis();
				String clinical_diagnosis = infoList.get(0).getClinical_diagnosis();
				String symptom = infoList.get(0).getSymptom();
				//String remark = infoList.get(0).getRemark();
				
				request.setAttribute("snupn", snupn);
				request.setAttribute("chart_no", chart_no);
				request.setAttribute("name", name);
				request.setAttribute("sex", sex);
				request.setAttribute("birth", birth);
				request.setAttribute("doctor", doctor);
				//request.setAttribute("refered_dr", refered_dr);
				request.setAttribute("other_site", other_site);
				request.setAttribute("category", category);
				//request.setAttribute("final_diagnosis", final_diagnosis);
				request.setAttribute("clinical_diagnosis", clinical_diagnosis);
				request.setAttribute("symptom", symptom);
				//request.setAttribute("remark", remark);
				
			}
			
			SampleDAO dao=SampleDAO.getSampleDAO();
			
			List<SampleUrineDTO> li = new ArrayList<SampleUrineDTO>();
			li = (List)dao.selectUrine(searchVal);
			
			if(li.size()!=0) {
				String uri_no = li.get(0).getUri_no();
				String urine = li.get(0).getUrine();
				String urine_col_date = li.get(0).getUrine_col_date();
				String urine_index = li.get(0).getUrine_index();
				String urine_volume = li.get(0).getUrine_volume();
				String urine_storage = li.get(0).getUrine_storage();
				String urine_location = li.get(0).getUrine_location();
				String urine_comment = li.get(0).getUrine_comment();
				String uipsc = li.get(0).getUipsc();
				String uipsc_col_date = li.get(0).getUipsc_col_date();
				String uipsc_index = li.get(0).getUipsc_index();
				String uipsc_storage = li.get(0).getUipsc_storage();
				String uipsc_location = li.get(0).getUipsc_location();
				String uipsc_comment = li.get(0).getUipsc_comment();
				String req_date = li.get(0).getReq_date();
				String req_distribution = li.get(0).getReq_distribution();
				String req_order_name = li.get(0).getReq_order_name();
				String req_vial = li.get(0).getReq_vial();
				String req_comment = li.get(0).getReq_comment();
				String idate = li.get(0).getIdate();
				String iid = li.get(0).getIid();
				
				request.setAttribute("uri_no", uri_no);
				request.setAttribute("urine", urine);
				request.setAttribute("urine_col_date", urine_col_date);
				request.setAttribute("urine_index", urine_index);
				request.setAttribute("urine_volume", urine_volume);
				request.setAttribute("urine_storage", urine_storage);
				request.setAttribute("urine_location", urine_location);
				request.setAttribute("urine_comment", urine_comment);
				request.setAttribute("uipsc", uipsc);
				request.setAttribute("uipsc_col_date", uipsc_col_date);
				request.setAttribute("uipsc_index", uipsc_index);
				request.setAttribute("uipsc_storage", uipsc_storage);
				request.setAttribute("uipsc_location", uipsc_location);
				request.setAttribute("uipsc_comment", uipsc_comment);
				request.setAttribute("req_date", req_date);
				request.setAttribute("req_distribution", req_distribution);
				request.setAttribute("req_order_name", req_order_name);
				request.setAttribute("req_vial", req_vial);
				request.setAttribute("req_comment", req_comment);
				request.setAttribute("idate", idate);
				request.setAttribute("iid", iid);

				
			}
			

			forward.setForward(true);
			forward.setNextURL("./SPN/SampleUrine.jsp");
			return forward;
		}
	}

}