package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.ElasticDAO;
import spn.dao.HistoryDAO;
import spn.dao.InfoDAO;
import spn.dao.MemberDAO;
import spn.dao.SampleDAO;
import spn.dto.ElasticDTO;
import spn.dto.HistoryDTO;
import spn.dto.InfoDTO;
import spn.dto.SampleRnaDTO;
import spn.dto.SampleTissueDTO;

public class SampleRnaAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String searchVal = (String) session.getAttribute("searchVal");
		String id=(String)session.getAttribute("id");

		InfoDAO infoDAO = InfoDAO.getInfoDAO();

		List<InfoDTO> infoList = new ArrayList<InfoDTO>();
		infoList = (List) infoDAO.selectInfo(searchVal);

		if (infoList.size() != 0) {
			String chart_no = infoList.get(0).getChart_no();
			request.setAttribute("chart_no", chart_no);
		}
		
		String menu="sample_rna";
		String prikey="rna_no";
		String privalue=null;

		String chart_no = request.getParameter("chart_no");
		String col_date = request.getParameter("col_date");
		String col_type = request.getParameter("col_type");
		String col_type_other_detail = request.getParameter("col_type_other_detail");
		String col_index = request.getParameter("col_index");
		String col_concentration = request.getParameter("col_concentration");
		String col_260_280 = request.getParameter("col_260_280");
		String col_260_230 = request.getParameter("col_260_230");
		String col_location = request.getParameter("col_location");
		String col_comment=request.getParameter("col_comment");
		String req_date = request.getParameter("req_date");
		String req_location = request.getParameter("req_location");
		String req_distribution = request.getParameter("req_distribution");
		String req_distribution_other_detail = request.getParameter("req_distribution_other_detail");
		String req_order_name = request.getParameter("req_order_name");
		String req_vial = request.getParameter("req_vial");
		String req_comment=request.getParameter("req_comment");
		String iid = request.getParameter("iid");


		SampleRnaDTO i = new SampleRnaDTO();

		i.setChart_no(chart_no);
		i.setCol_date(col_date);
		i.setCol_type(col_type);
		i.setCol_type_other_detail(col_type_other_detail);
		i.setCol_index(col_index);
		i.setCol_concentration(col_concentration);
		i.setCol_260_280(col_260_280);
		i.setCol_260_230(col_260_230);
		i.setCol_location(col_location);
		i.setCol_comment(col_comment);
		i.setReq_date(req_date);
		i.setReq_location(req_location);
		i.setReq_distribution(req_distribution);
		i.setReq_distribution_other_detail(req_distribution_other_detail);
		i.setReq_order_name(req_order_name);
		i.setReq_vial(req_vial);
		i.setReq_comment(req_comment);
		i.setIid(iid);

		SampleDAO dao = SampleDAO.getSampleDAO();
		int result = dao.insertRna(i);
		ActionForward forward = new ActionForward();

		if (result != 0) {
			// ��� ������
			
			privalue=String.valueOf(Integer.parseInt(dao.lastIncreaseNumber(menu, prikey))); // 방금 삽입한 값을 참조하기 위해, 다시 한번 조회하도록 한다.
			
			/* elastic 작업 */
			HashMap<String, String> map=new HashMap<String, String>();
			
			map.put("chart_no", chart_no);
			map.put("col_date", col_date);
			map.put("col_type", col_type);
			map.put("col_type_other_detail", col_type_other_detail);
			map.put("col_index", col_index);
			map.put("col_concentration", col_concentration);
			map.put("col_260_280", col_260_280);
			map.put("col_260_230", col_260_230);
			map.put("col_location", col_location);
			map.put("col_comment", col_comment);
			map.put("req_date", req_date);
			map.put("req_location", req_location);
			map.put("req_distribution", req_distribution);
			map.put("req_distribution_other_detail", req_distribution_other_detail);
			map.put("req_order_name", req_order_name);
			map.put("req_vial", req_vial);
			map.put("req_comment", req_comment);
			map.put("iid", iid);
			map.put("menu", menu);
			map.put(prikey, privalue);
			
			
			ElasticDTO eldto=new ElasticDTO();
			ElasticDAO eldao=ElasticDAO.getElasticDAO();
			eldto.setMenu(menu);
			//eldto.setUrl("/sample_rna/sample_rna");
			eldao.insertElastic(eldto, map);
			
			/* History�� �߰��ϴ� �κ� */
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			HistoryDTO h=new HistoryDTO();
			
			InfoDAO i_dao=InfoDAO.getInfoDAO();
			List<InfoDTO> li=new ArrayList<InfoDTO>();
			
			li=(List)i_dao.selectInfo(chart_no); // InfoDAO���� �����ø��� ������? snupn, pa_bame, pa_sex
			h.setPa_name(li.get(0).getName());
			h.setPa_sex(li.get(0).getSex());
			h.setSnupn(li.get(0).getSnupn());
			
			MemberDAO m_dao=MemberDAO.getMemberDAO(); // MemberDao���� ������ �̸�
			h.setName(m_dao.findMember(id).getName());

			h.setChart_no(chart_no);			
			h.setIid(iid);
			h.setPage_name("sample_rna");
			his_dao.insertHistory(h);							
			
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=samplernaform");
		} else {
			// ��Ͻ��н�: ���������� ��������
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=samplernaform");

		}

		return forward;
	}

}
