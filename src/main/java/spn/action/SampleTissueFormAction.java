package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.InfoDAO;
import spn.dao.SampleDAO;
import spn.dto.InfoDTO;
import spn.dto.SampleTissueDTO;

public class SampleTissueFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		} 
		else {
			
			String searchVal="";
			if(request.getParameter("searchVal")!=null) { //검색을 통한 접속이라면,
				searchVal=request.getParameter("searchVal");
				session.setAttribute("searchVal", searchVal);
				//searchVal=(String)session.getAttribute(request.getParameter("searchVal").toString());
			}
			else { // Info를 통한 접속일 
				searchVal = (String) session.getAttribute("searchVal");
			}

			InfoDAO infoDAO=InfoDAO.getInfoDAO();
			
			List<InfoDTO> infoList = new ArrayList<InfoDTO>();
			infoList = (List) infoDAO.selectInfo(searchVal);
			
			if (infoList.size() != 0) {
				String snupn = infoList.get(0).getSnupn();
				String chart_no= infoList.get(0).getChart_no();
				String name = infoList.get(0).getName();
				String sex= infoList.get(0).getSex();
				String birth = infoList.get(0).getBirth();
				String doctor = infoList.get(0).getDoctor();
				//String refered_dr= infoList.get(0).getRefered_dr();
				String other_site= infoList.get(0).getOther_site();
				String category = infoList.get(0).getCategory();
				//String final_diagnosis = infoList.get(0).getFinal_diagnosis();
				String clinical_diagnosis = infoList.get(0).getClinical_diagnosis();
				String symptom = infoList.get(0).getSymptom();
				//String remark = infoList.get(0).getRemark();
				
				request.setAttribute("snupn", snupn);
				request.setAttribute("chart_no", chart_no);
				request.setAttribute("name", name);
				request.setAttribute("sex", sex);
				request.setAttribute("birth", birth);
				request.setAttribute("doctor", doctor);
				//request.setAttribute("refered_dr", refered_dr);
				request.setAttribute("other_site", other_site);
				request.setAttribute("category", category);
				//request.setAttribute("final_diagnosis", final_diagnosis);
				request.setAttribute("clinical_diagnosis", clinical_diagnosis);
				request.setAttribute("symptom", symptom);
				//request.setAttribute("remark", remark);
			}
			
			SampleDAO dao=SampleDAO.getSampleDAO();
			
			List<SampleTissueDTO> li = new ArrayList<SampleTissueDTO>();
			li = (List) dao.selectTissue(searchVal);
			
			if(li.size() != 0) {
				String tis_no = li.get(0).getTis_no();
				String bio_date = li.get(0).getBio_date();
				//String bio_type = li.get(0).getBio_type();
				String bio_type_myoblast=li.get(0).getBio_type_myoblast();
				String bio_type_fibroblast=li.get(0).getBio_type_fibroblast();
				String bio_type_u_epithelial=li.get(0).getBio_type_u_epithelial();
				String bio_type_u_ipsc=li.get(0).getBio_type_u_ipsc();
				String bio_type_other=li.get(0).getBio_type_other();
				String bio_type_other_detail = li.get(0).getBio_type_other_detail();
				String bio_index_1 = li.get(0).getBio_index_1();
				//String bio_location_1 = li.get(0).getBio_location_1();
				//String bio_cell = li.get(0).getBio_cell();
				//String bio_index_2 = li.get(0).getBio_index_2();
				//String bio_phase = li.get(0).getBio_phase();
				String bio_storage = li.get(0).getBio_storage();
				//String bio_location_2 = li.get(0).getBio_location_2();
				String bio_comment = li.get(0).getBio_comment();
				String req_date = li.get(0).getReq_date();
				String req_location = li.get(0).getReq_location();
				//String req_distribution = li.get(0).getReq_distribution();
				String req_order_name = li.get(0).getReq_order_name();
				//String req_type = li.get(0).getReq_type();
				//String req_type_other_detail = li.get(0).getReq_type_other_detail();
				String req_vial = li.get(0).getReq_vial();
				String req_comment = li.get(0).getReq_comment();
				String iid = li.get(0).getIid();
				String idate = li.get(0).getIdate();
				
				request.setAttribute("tis_no", tis_no);
				request.setAttribute("bio_date", bio_date);
				//request.setAttribute("bio_type", bio_type);
				request.setAttribute("bio_type_myoblast", bio_type_myoblast);
				request.setAttribute("bio_type_fibroblast", bio_type_fibroblast);
				request.setAttribute("bio_type_u_epithelial", bio_type_u_epithelial);
				request.setAttribute("bio_type_u_ipsc", bio_type_u_ipsc);
				request.setAttribute("bio_type_other", bio_type_other);
				request.setAttribute("bio_type_other_detail", bio_type_other_detail);
				request.setAttribute("bio_index_1", bio_index_1);
				//request.setAttribute("bio_location_1", bio_location_1);
				//request.setAttribute("bio_cell", bio_cell);
				//request.setAttribute("bio_index_2", bio_index_2);
				//request.setAttribute("bio_phase", bio_phase);
				request.setAttribute("bio_storage", bio_storage);
				//request.setAttribute("bio_location_2", bio_location_2);
				request.setAttribute("bio_comment", bio_comment);
				request.setAttribute("req_date", req_date);
				request.setAttribute("req_location", req_location);
				//request.setAttribute("req_distribution", req_distribution);
				request.setAttribute("req_order_name", req_order_name);
				//request.setAttribute("req_type", req_type);
				//request.setAttribute("req_type_other_detail", req_type_other_detail);
				request.setAttribute("req_vial", req_vial);
				request.setAttribute("req_comment", req_comment);
				request.setAttribute("iid", iid);
				request.setAttribute("idate", idate);
			}
			

			forward.setForward(true);
			forward.setNextURL("./SPN/SampleTissue.jsp");
			return forward;
		}
	}

}