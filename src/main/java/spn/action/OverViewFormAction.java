package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.InfoDAO;
import spn.dto.OverviewDTO;

public class OverViewFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform"); // 로그인이 되어있지 않다면, loginform으로 보냄
			return forward;
		} else {
			String searchVal = (String) session.getAttribute("searchVal");
			
			InfoDAO dao=InfoDAO.getInfoDAO();
			
			List<OverviewDTO> li = new ArrayList<OverviewDTO>();
			li = (List) dao.selectOverview(searchVal);
			
			if (li.size() != 0) {
				String dna_genomic=li.get(0).getDna_genomic();
				String dna_cellfree=li.get(0).getDna_cellfree();
				String dna_mitochondria=li.get(0).getDna_mitochondria();
				String tissue_muscle=li.get(0).getTissue_muscle();
				String tissue_myoblast=li.get(0).getTissue_myoblast();
				String tissue_skin=li.get(0).getTissue_skin();
				String tissue_fibroblast=li.get(0).getTissue_fibroblast();
				String cell_free_dna=li.get(0).getCell_free_dna();
				String serum=li.get(0).getSerum();
				String csf=li.get(0).getCsf();
				String urine_cell=li.get(0).getUrine_cell();
				String uipsc=li.get(0).getUipsc();
				String rna=li.get(0).getRna();
				String informed_consent=li.get(0).getInformed_consent();
				String sanger_order=li.get(0).getSanger_order();
				String sanger_ongoing=li.get(0).getSanger_ongoing();
				String sanger_done=li.get(0).getSanger_done();
				String ngs_wgs_order=li.get(0).getNgs_wgs_order();
				String ngs_wgs_ongoing=li.get(0).getNgs_wgs_ongoing();
				String ngs_wgs_done=li.get(0).getNgs_wgs_done();
				String ngs_wes_order=li.get(0).getNgs_wes_order();
				String ngs_wes_ongoing=li.get(0).getNgs_wes_ongoing();
				String ngs_wes_done=li.get(0).getNgs_wes_done();
				String ngs_targeted_order=li.get(0).getNgs_targeted_order();
				String ngs_targeted_ongoing=li.get(0).getNgs_targeted_ongoing();
				String ngs_targeted_done=li.get(0).getNgs_targeted_done();
				String cgh_order=li.get(0).getCgh_order();
				String cgh_ongoing=li.get(0).getCgh_ongoing();
				String cgh_done=li.get(0).getCgh_done();
				String muscle_stain_order=li.get(0).getMuscle_stain_order();
				String muscle_stain_ongoing=li.get(0).getMuscle_stain_ongoing();
				String muscle_stain_done=li.get(0).getMuscle_stain_done();
				String western_order=li.get(0).getWestern_order();
				String western_ongoing=li.get(0).getWestern_ongoing();
				String western_done=li.get(0).getWestern_done();
				String other_order=li.get(0).getOther_order();
				String other_ongoing=li.get(0).getOther_ongoing();
				String other_done=li.get(0).getOther_done();
				String iid=li.get(0).getIid();
				String idate=li.get(0).getIdate();
				
				request.setAttribute("dna_genomic", dna_genomic);
				request.setAttribute("dna_cellfree", dna_cellfree);
				request.setAttribute("dna_mitochondria", dna_mitochondria);
				request.setAttribute("tissue_muscle", tissue_muscle);
				request.setAttribute("tissue_myoblast", tissue_myoblast);
				request.setAttribute("tissue_skin", tissue_skin);
				request.setAttribute("tissue_fibroblast", tissue_fibroblast);
				request.setAttribute("cell_free_dna", cell_free_dna);
				request.setAttribute("serum", serum);
				request.setAttribute("csf", csf);
				request.setAttribute("urine_cell", urine_cell);
				request.setAttribute("uipsc", uipsc);
				request.setAttribute("rna", rna);
				request.setAttribute("informed_consent", informed_consent);
				request.setAttribute("sanger_order", sanger_order);
				request.setAttribute("sanger_ongoing", sanger_ongoing);
				request.setAttribute("sanger_done", sanger_done);
				request.setAttribute("ngs_wgs_order", ngs_wgs_order);
				request.setAttribute("ngs_wgs_ongoing", ngs_wgs_ongoing);
				request.setAttribute("ngs_wgs_done", ngs_wgs_done);
				request.setAttribute("ngs_wes_order", ngs_wes_order);
				request.setAttribute("ngs_wes_ongoing", ngs_wes_ongoing);
				request.setAttribute("ngs_wes_done", ngs_wes_done);
				request.setAttribute("ngs_targeted_order", ngs_targeted_order);
				request.setAttribute("ngs_targeted_ongoing", ngs_targeted_ongoing);
				request.setAttribute("ngs_targeted_done", ngs_targeted_done);
				request.setAttribute("cgh_order", cgh_order);
				request.setAttribute("cgh_ongoing", cgh_ongoing);
				request.setAttribute("cgh_done", cgh_done);
				request.setAttribute("muscle_stain_order", muscle_stain_order);
				request.setAttribute("muscle_stain_ongoing", muscle_stain_ongoing);
				request.setAttribute("muscle_stain_done", muscle_stain_done);
				request.setAttribute("western_order", western_order);
				request.setAttribute("western_ongoing", western_ongoing);
				request.setAttribute("western_done", western_done);
				request.setAttribute("other_order", other_order);
				request.setAttribute("other_ongoing", other_ongoing);
				request.setAttribute("other_done", other_done);
				request.setAttribute("iid", iid);
				request.setAttribute("idate", idate);

			}
			
			
			forward.setForward(true);
			forward.setNextURL("./SPN/OverView.jsp");
			return forward;
		}
	}

}