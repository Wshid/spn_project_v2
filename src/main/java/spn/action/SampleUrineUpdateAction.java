package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.HistoryDAO;
import spn.dao.InfoDAO;
import spn.dao.MemberDAO;
import spn.dao.SampleDAO;
import spn.dto.HistoryDTO;
import spn.dto.InfoDTO;
import spn.dto.SampleUrineDTO;

public class SampleUrineUpdateAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String searchVal = (String) session.getAttribute("searchVal");
		String id=(String) session.getAttribute("id");

		InfoDAO infoDAO = InfoDAO.getInfoDAO();

		List<InfoDTO> infoList = new ArrayList<InfoDTO>();
		infoList = (List) infoDAO.selectInfo(searchVal);

		if (infoList.size() != 0) {
			String chart_no = infoList.get(0).getChart_no();
			request.setAttribute("chart_no", chart_no);
		}

		String uri_no = request.getParameter("uri_no");
		String chart_no = request.getParameter("chart_no");
		String urine = request.getParameter("urine");
		String urine_col_date = request.getParameter("urine_col_date"); 
		String urine_index = request.getParameter("urine_index");
		String urine_volume = request.getParameter("urine_volume");
		String urine_storage = request.getParameter("urine_storage");
		String urine_location = request.getParameter("urine_location");
		String urine_comment = request.getParameter("urine_comment");
		String uipsc = request.getParameter("uipsc");
		String uipsc_col_date = request.getParameter("uipsc_col_date");
		String uipsc_index = request.getParameter("uipsc_index");
		String uipsc_storage = request.getParameter("uipsc_storage");
		String uipsc_location = request.getParameter("uipsc_location");
		String uipsc_comment = request.getParameter("uipsc_comment");
		String req_date = request.getParameter("req_date");
		String req_distribution = request.getParameter("req_distribution");
		String req_order_name = request.getParameter("req_order_name");
		String req_vial = request.getParameter("req_vial");
		String req_comment = request.getParameter("req_comment");
		String iid = request.getParameter("iid");
 
		SampleUrineDTO i = new SampleUrineDTO();

		i.setUri_no(uri_no);
		i.setChart_no(chart_no);
		i.setUrine(urine);
		i.setUrine_col_date(urine_col_date);
		i.setUrine_index(urine_index);
		i.setUrine_volume(urine_volume);
		i.setUrine_storage(urine_storage);
		i.setUrine_location(urine_location);
		i.setUrine_comment(urine_comment);
		i.setUipsc(uipsc);
		i.setUipsc_col_date(uipsc_col_date);
		i.setUipsc_index(uipsc_index);
		i.setUipsc_storage(uipsc_storage);
		i.setUipsc_location(uipsc_location);
		i.setUipsc_comment(uipsc_comment);
		i.setReq_date(req_date);
		i.setReq_distribution(req_distribution);
		i.setReq_order_name(req_order_name);
		i.setReq_vial(req_vial);
		i.setReq_comment(req_comment);
		i.setIid(iid);

		SampleDAO dao = SampleDAO.getSampleDAO();
		int result = dao.updateUrine(i);

		ActionForward forward = new ActionForward();

		if (result != 0) {
			// 등록 성공시
			
			/* History를 추가하는 부분 */
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			HistoryDTO h=new HistoryDTO();
			
			InfoDAO i_dao=InfoDAO.getInfoDAO();
			List<InfoDTO> li=new ArrayList<InfoDTO>();
			
			li=(List)i_dao.selectInfo(chart_no); // InfoDAO에서 가져올만한 정보는? snupn, pa_bame, pa_sex
			h.setPa_name(li.get(0).getName());
			h.setPa_sex(li.get(0).getSex());
			h.setSnupn(li.get(0).getSnupn());
			
			MemberDAO m_dao=MemberDAO.getMemberDAO(); // MemberDao에서 가져올 이름
			h.setName(m_dao.findMember(id).getName());

			h.setChart_no(chart_no);			
			h.setIid(iid);
			h.setPage_name("sample_urine");
			his_dao.insertHistory(h);			
						
			
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=sampleurineform");
		} else {
			// 등록실패시: 에러페이지 만들어야함
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=sampleurineform");

		}

		return forward;
	}

}
