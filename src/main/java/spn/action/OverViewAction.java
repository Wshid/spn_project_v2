package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.InfoDAO;
import spn.dto.InfoDTO;
import spn.dto.OverviewDTO;


public class OverViewAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String searchVal = (String) session.getAttribute("searchVal");

		InfoDAO infoDAO = InfoDAO.getInfoDAO();
		List<InfoDTO> infoList = new ArrayList<InfoDTO>();
		infoList = (List) infoDAO.selectInfo(searchVal);
		
		if (infoList.size() != 0) {
			String chart_no = infoList.get(0).getChart_no();
			request.setAttribute("chart_no", chart_no);
		}

		String chart_no = request.getParameter("chart_no");
		String dna_genomic=request.getParameter("dna_genomic");
		String dna_cellfree=request.getParameter("dna_cellfree");
		String dna_mitochondria=request.getParameter("dna_mitochondria");
		String tissue_muscle=request.getParameter("tissue_muscle");
		String tissue_myoblast=request.getParameter("tissue_myoblast");
		String tissue_skin=request.getParameter("tissue_skin");
		String tissue_fibroblast=request.getParameter("tissue_fibroblast");
		String cell_free_dna=request.getParameter("cell_free_dna");
		String serum=request.getParameter("serum");
		String csf=request.getParameter("csf");
		String urine_cell=request.getParameter("urine_cell");
		String uipsc=request.getParameter("uipsc");
		String rna=request.getParameter("rna");
		String informed_consent=request.getParameter("informed_consent");
		String sanger_order=request.getParameter("sanger_order");
		String sanger_ongoing=request.getParameter("sanger_ongoing");
		String sanger_done=request.getParameter("sanger_done");
		String ngs_wgs_order=request.getParameter("ngs_wgs_order");
		String ngs_wgs_ongoing=request.getParameter("ngs_wgs_ongoing");
		String ngs_wgs_done=request.getParameter("ngs_wgs_done");
		String ngs_wes_order=request.getParameter("ngs_wes_order");
		String ngs_wes_ongoing=request.getParameter("ngs_wes_ongoing");
		String ngs_wes_done=request.getParameter("ngs_wes_done");
		String ngs_targeted_order=request.getParameter("ngs_targeted_order");
		String ngs_targeted_ongoing=request.getParameter("ngs_targeted_ongoing");
		String ngs_targeted_done=request.getParameter("ngs_targeted_done");
		String cgh_order=request.getParameter("cgh_order");
		String cgh_ongoing=request.getParameter("cgh_ongoing");
		String cgh_done=request.getParameter("cgh_done");
		String muscle_stain_order=request.getParameter("muscle_stain_order");
		String muscle_stain_ongoing=request.getParameter("muscle_stain_ongoing");
		String muscle_stain_done=request.getParameter("muscle_stain_done");
		String western_order=request.getParameter("western_order");
		String western_ongoing=request.getParameter("western_ongoing");
		String western_done=request.getParameter("western_done");
		String other_order=request.getParameter("other_order");
		String other_ongoing=request.getParameter("other_ongoing");
		String other_done=request.getParameter("other_done");
		String iid=request.getParameter("iid");

		OverviewDTO i = new OverviewDTO();

		i.setChart_no(chart_no);
		i.setDna_genomic(dna_genomic);
		i.setDna_cellfree(dna_cellfree);
		i.setDna_mitochondria(dna_mitochondria);
		i.setTissue_muscle(tissue_muscle);
		i.setTissue_myoblast(tissue_myoblast);
		i.setTissue_skin(tissue_skin);
		i.setTissue_fibroblast(tissue_fibroblast);
		i.setCell_free_dna(cell_free_dna);
		i.setSerum(serum);
		i.setCsf(csf);
		i.setUrine_cell(urine_cell);
		i.setUipsc(uipsc);
		i.setRna(rna);
		i.setInformed_consent(informed_consent);
		i.setSanger_order(sanger_order);
		i.setSanger_ongoing(sanger_ongoing);
		i.setSanger_done(sanger_done);
		i.setNgs_wgs_order(ngs_wgs_order);
		i.setNgs_wgs_ongoing(ngs_wgs_ongoing);
		i.setNgs_wgs_done(ngs_wgs_done);
		i.setNgs_wes_order(ngs_wes_order);
		i.setNgs_wes_ongoing(ngs_wes_ongoing);
		i.setNgs_wes_done(ngs_wes_done);
		i.setNgs_targeted_order(ngs_targeted_order);
		i.setNgs_targeted_ongoing(ngs_targeted_ongoing);
		i.setNgs_targeted_done(ngs_targeted_done);
		i.setCgh_order(cgh_order);
		i.setCgh_ongoing(cgh_ongoing);
		i.setCgh_done(cgh_done);
		i.setMuscle_stain_order(muscle_stain_order);
		i.setMuscle_stain_ongoing(muscle_stain_ongoing);
		i.setMuscle_stain_done(muscle_stain_done);
		i.setWestern_order(western_order);
		i.setWestern_ongoing(western_ongoing);
		i.setWestern_done(western_done);
		i.setOther_order(other_order);
		i.setOther_ongoing(other_ongoing);
		i.setOther_done(other_done);
		i.setIid(iid);
		
		InfoDAO dao=InfoDAO.getInfoDAO();
		int result=dao.insertOverview(i);
		
		ActionForward forward= new ActionForward();
		
		if(result != 0) {
			//등록 성공시
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=overviewform");
		}else {
			//등록실패시: 에러페이지 만들어야함
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=overviewform");

		}
		
		return forward;
	}

}
