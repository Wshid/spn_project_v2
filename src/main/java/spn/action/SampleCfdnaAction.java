package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.ElasticDAO;
import spn.dao.HistoryDAO;
import spn.dao.InfoDAO;
import spn.dao.MemberDAO;
import spn.dao.SampleDAO;
import spn.dto.ElasticDTO;
import spn.dto.HistoryDTO;
import spn.dto.InfoDTO;
import spn.dto.SampleCfdnaDTO;

public class SampleCfdnaAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String searchVal = (String) session.getAttribute("searchVal");
		String id=(String)session.getAttribute("id");

		InfoDAO infoDAO = InfoDAO.getInfoDAO();

		List<InfoDTO> infoList = new ArrayList<InfoDTO>();
		infoList = (List) infoDAO.selectInfo(searchVal);

		if (infoList.size() != 0) {
			String chart_no = infoList.get(0).getChart_no();
			request.setAttribute("chart_no", chart_no);
		}
		
		String menu="sample_cfdna";
		String prikey="cfd_no";
		String privalue=null;

		String chart_no = request.getParameter("chart_no");
		String col_date = request.getParameter("col_date");
		String col_location = request.getParameter("col_location");
		String col_who = request.getParameter("col_who");
		String col_who_patient_detail = request.getParameter("col_who_patient_detail");
		String col_who_father_detail = request.getParameter("col_who_father_detail");
		String col_who_mother_detail = request.getParameter("col_who_mother_detail");
		String col_who_sibling_detail = request.getParameter("col_who_sibling_detail");
		String col_who_other_detail = request.getParameter("col_who_other_detail");
		String col_index = request.getParameter("col_index");
		String col_wk = request.getParameter("col_wk");
		String col_plasma_volume = request.getParameter("col_plasma_volume");
		String col_cf_no = request.getParameter("col_cf_no");
		String col_concentration_1 = request.getParameter("col_concentration_1");
		String col_concentration_2 = request.getParameter("col_concentration_2");
		String col_260_280_1 =request.getParameter("col_260_280_1");
		String col_260_280_2 =request.getParameter("col_260_280_2");
		String col_260_230_1 =request.getParameter("col_260_230_1");
		String col_260_230_2 =request.getParameter("col_260_230_2");
		String col_bioanalyzer_size_1 =request.getParameter("col_bioanalyzer_size_1");
		String col_bioanalyzer_size_2 =request.getParameter("col_bioanalyzer_size_2");
		String col_bioanalyzer_concentation_1 =request.getParameter("col_bioanalyzer_concentation_1");
		String col_bioanalyzer_concentation_2 =request.getParameter("col_bioanalyzer_concentation_2");
		String col_comment = request.getParameter("col_comment");
		String req_date = request.getParameter("req_date");
		String req_location = request.getParameter("req_location");
		String req_who = request.getParameter("req_who");
		String req_who_other_detail = request.getParameter("req_who_other_detail");
		String req_distribution = request.getParameter("req_distribution");
		String req_order_name = request.getParameter("req_order_name");
		String req_vial = request.getParameter("req_vial");
		String req_comment = request.getParameter("req_comment");
		String iid = request.getParameter("iid");

		SampleCfdnaDTO i = new SampleCfdnaDTO();

		i.setChart_no(chart_no);
		i.setCol_date(col_date);
		i.setCol_location(col_location);
		i.setCol_who(col_who);
		i.setCol_who_patient_detail(col_who_patient_detail);
		i.setCol_who_father_detail(col_who_father_detail);
		i.setCol_who_mother_detail(col_who_mother_detail);
		i.setCol_who_sibling_detail(col_who_sibling_detail);
		i.setCol_who_other_detail(col_who_other_detail);
		i.setCol_index(col_index);
		i.setCol_wk(col_wk);
		i.setCol_plasma_volume(col_plasma_volume);
		i.setCol_cf_no(col_cf_no);
		i.setCol_concentration_1(col_concentration_1);
		i.setCol_concentration_2(col_concentration_2);
		i.setCol_260_280_1(col_260_280_1);
		i.setCol_260_280_2(col_260_280_2);
		i.setCol_260_230_1(col_260_230_1);
		i.setCol_260_230_2(col_260_230_2);
		i.setCol_bioanalyzer_size_1(col_bioanalyzer_size_1);
		i.setCol_bioanalyzer_size_2(col_bioanalyzer_size_2);
		i.setCol_bioanalyzer_concentation_1(col_bioanalyzer_concentation_1);
		i.setCol_bioanalyzer_concentation_2(col_bioanalyzer_concentation_2);
		i.setCol_comment(col_comment);
		i.setReq_date(req_date);
		i.setReq_location(req_location);
		i.setReq_who(req_who);
		i.setReq_who_other_detail(req_who_other_detail);
		i.setReq_distribution(req_distribution);
		i.setReq_order_name(req_order_name);
		i.setReq_vial(req_vial);
		i.setReq_comment(req_comment);
		i.setIid(iid);

		SampleDAO dao = SampleDAO.getSampleDAO();
		int result = dao.insertCfdna(i);
		ActionForward forward = new ActionForward();

		if (result != 0) {
			// ��� ������
			privalue=String.valueOf(Integer.parseInt(dao.lastIncreaseNumber(menu, prikey))); // 방금 삽입한 값을 참조하기 위해, 다시 한번 조회하도록 한다.
			
			/* elastic 작업 */
			HashMap<String, String> map=new HashMap<String, String>();
			
			map.put("chart_no", chart_no);
			map.put("col_date", col_date);
			map.put("col_location", col_location);
			map.put("col_who", col_who);
			map.put("col_who_patient_detail", col_who_patient_detail);
			map.put("col_who_father_detail", col_who_father_detail);
			map.put("col_who_mother_detail", col_who_mother_detail);
			map.put("col_who_sibling_detail", col_who_sibling_detail);
			map.put("col_who_other_detail", col_who_other_detail);
			map.put("col_index", col_index);
			map.put("col_wk", col_wk);
			map.put("col_plasma_volume", col_plasma_volume);
			map.put("col_cf_no", col_cf_no);
			map.put("col_concentration_1", col_concentration_1);
			map.put("col_concentration_2", col_concentration_2);
			map.put("col_260_280_1", col_260_280_1);
			map.put("col_260_280_2", col_260_280_2);
			map.put("col_260_230_1", col_260_230_1);
			map.put("col_260_230_2", col_260_230_2);
			map.put("col_bioanalyzer_size_1", col_bioanalyzer_size_1);
			map.put("col_bioanalyzer_size_2", col_bioanalyzer_size_2);
			map.put("col_bioanalyzer_concentation_1", col_bioanalyzer_concentation_1);
			map.put("col_bioanalyzer_concentation_2", col_bioanalyzer_concentation_2);
			map.put("col_comment", col_comment);
			map.put("req_date", req_date);
			map.put("req_location", req_location);
			map.put("req_who", req_who);
			map.put("req_who_other_detail", req_who_other_detail);
			map.put("req_distribution", req_distribution);
			map.put("req_order_name", req_order_name);
			map.put("req_vial", req_vial);
			map.put("req_comment", req_comment);
			map.put("menu", menu);
			map.put("iid", iid);
			map.put(prikey, privalue);
			
			ElasticDTO eldto=new ElasticDTO();
			ElasticDAO eldao=ElasticDAO.getElasticDAO();
			eldto.setMenu(menu);
			//eldto.setUrl("/sample_cfdna/sample_cfdna");
			eldao.insertElastic(eldto, map);
			
			/* History�� �߰��ϴ� �κ� */
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			HistoryDTO h=new HistoryDTO();
			
			InfoDAO i_dao=InfoDAO.getInfoDAO();
			List<InfoDTO> li=new ArrayList<InfoDTO>();
			
			li=(List)i_dao.selectInfo(chart_no); // InfoDAO���� �����ø��� ������? snupn, pa_bame, pa_sex
			h.setPa_name(li.get(0).getName());
			h.setPa_sex(li.get(0).getSex());
			h.setSnupn(li.get(0).getSnupn());
			
			MemberDAO m_dao=MemberDAO.getMemberDAO(); // MemberDao���� ������ �̸�
			h.setName(m_dao.findMember(id).getName());

			h.setChart_no(chart_no);			
			h.setIid(iid);
			h.setPage_name("sample_cfdna");
			his_dao.insertHistory(h);
						
			
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=samplecfdnaform");
		} else {
			// ��Ͻ��н�: ���������� ��������
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=samplecfdnaform");

		}

		return forward;
	}

}
