package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.ElasticDAO;
import spn.dao.HistoryDAO;
import spn.dao.InfoDAO;
import spn.dao.MemberDAO;
import spn.dao.SampleDAO;
import spn.dto.ElasticDTO;
import spn.dto.HistoryDTO;
import spn.dto.InfoDTO;
import spn.dto.SampleSerumDTO;

public class SampleSerumAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String searchVal = (String) session.getAttribute("searchVal");
		String id=(String)session.getAttribute("id");

		InfoDAO infoDAO = InfoDAO.getInfoDAO();

		List<InfoDTO> infoList = new ArrayList<InfoDTO>();
		infoList = (List) infoDAO.selectInfo(searchVal);

		if (infoList.size() != 0) {
			String chart_no = infoList.get(0).getChart_no();
			request.setAttribute("chart_no", chart_no);
		}
		
		String menu="sample_serum";
		String prikey="sc_no";
		String privalue=null;

		String chart_no = request.getParameter("chart_no");
		//String experiment = request.getParameter("experiment");
		String experiment_serum = request.getParameter("experiment_serum");
		String experiment_csf = request.getParameter("experiment_csf");
		String col_date = request.getParameter("col_date");
		String dis_date = request.getParameter("dis_date");
		String col_index = request.getParameter("col_index");
		String no = request.getParameter("no");
		String storage = request.getParameter("storage");
		String aquaporin_4_ab = request.getParameter("aquaporin_4_ab");
		String autoimmune_encephalitis = request.getParameter("autoimmune_encephalitis");
		String order_other=request.getParameter("order_other");
		String order_other_detail=request.getParameter("order_other_detail");
		String experiment_place = request.getParameter("experiment_place");
		String comment_1 = request.getParameter("comment_1");
		String location = request.getParameter("location");
		String comment_2 =request.getParameter("comment_2");
		String req_date = request.getParameter("req_date");
		String req_location = request.getParameter("req_location");
		String req_order_name = request.getParameter("req_order_name");
		String req_vial = request.getParameter("req_vial");
		String req_comment = request.getParameter("req_comment");
		
		String iid = request.getParameter("iid");

		SampleSerumDTO i = new SampleSerumDTO();

		i.setChart_no(chart_no);
		//i.setExperiment(experiment);
		i.setExperiment_serum(experiment_serum);
		i.setExperiment_csf(experiment_csf);
		i.setCol_date(col_date);
		i.setDis_date(dis_date);
		i.setCol_index(col_index);
		i.setNo(no);
		i.setStorage(storage);
		i.setAquaporin_4_ab(aquaporin_4_ab);
		i.setAutoimmune_encephalitis(autoimmune_encephalitis);
		i.setOrder_other(order_other);
		i.setOrder_other_detail(order_other_detail);
		i.setExperiment_place(experiment_place);
		i.setComment_1(comment_1);
		i.setLocation(location);
		i.setComment_2(comment_2);
		i.setReq_date(req_date);
		i.setReq_location(req_location);
		i.setReq_order_name(req_order_name);
		i.setReq_vial(req_vial);
		i.setReq_comment(req_comment);
		i.setIid(iid);

		SampleDAO dao = SampleDAO.getSampleDAO();
		int result = dao.insertSerum(i);
		ActionForward forward = new ActionForward();

		if (result != 0) {
			// ��� ������
			privalue=String.valueOf(Integer.parseInt(dao.lastIncreaseNumber(menu, prikey))); // 방금 삽입한 값을 참조하기 위해, 다시 한번 조회하도록 한다.
			
			/* elastic 작업 */
			HashMap<String, String> map=new HashMap<String, String>();
			
			map.put("chart_no", chart_no);
			map.put("experiment_serum", experiment_serum);
			map.put("experiment_csf", experiment_csf);
			map.put("col_date", col_date);
			map.put("dis_date", dis_date);
			map.put("col_index", col_index);
			map.put("no", no);
			map.put("storage", storage);
			map.put("aquaporin_4_ab", aquaporin_4_ab);
			map.put("autoimmune_encephalitis", autoimmune_encephalitis);
			map.put("order_other", order_other);
			map.put("order_other_detail", order_other_detail);
			map.put("experiment_place", experiment_place);
			map.put("comment_1", comment_1);
			map.put("location", location);
			map.put("comment_2", comment_2);
			map.put("req_date", req_date);
			map.put("req_location", req_location);
			map.put("req_order_name", req_order_name);
			map.put("req_vial", req_vial);
			map.put("req_comment", req_comment);
			map.put("iid", iid);
			map.put("menu", menu);
			map.put(prikey, privalue);
			
			
			ElasticDTO eldto=new ElasticDTO();
			ElasticDAO eldao=ElasticDAO.getElasticDAO();
			eldto.setMenu(menu);
			//eldto.setUrl("/sample_serum/sample_serum");
			eldao.insertElastic(eldto, map);
			/* History�� �߰��ϴ� �κ� */
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			HistoryDTO h=new HistoryDTO();
			
			InfoDAO i_dao=InfoDAO.getInfoDAO();
			List<InfoDTO> li=new ArrayList<InfoDTO>();
			
			li=(List)i_dao.selectInfo(chart_no); // InfoDAO���� �����ø��� ������? snupn, pa_bame, pa_sex
			h.setPa_name(li.get(0).getName());
			h.setPa_sex(li.get(0).getSex());
			h.setSnupn(li.get(0).getSnupn());
			
			MemberDAO m_dao=MemberDAO.getMemberDAO(); // MemberDao���� ������ �̸�
			h.setName(m_dao.findMember(id).getName());

			h.setChart_no(chart_no);			
			h.setIid(iid);
			h.setPage_name("sample_serum");
			his_dao.insertHistory(h);			
						
			
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=sampleserumform");
		} else {
			// ��Ͻ��н�: ���������� ��������
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=sampleserumform");

		}

		return forward;
	}

}
