package spn.action;

public class ActionForward {
	private String nextURL; //어느 URL로 이동할지 정해주어야해. URL과 페이지 이동방식을 설정하려 한다.
	private boolean isForward;//true면 Forward방식으로 이동하고,false면 Redirect 방식으로 한다. boolean은 isForward로 넘겨주면 된다.
	//getter와 setter메소드는 source 메뉴에 가서 설정하면 자동으로 만들어진다.
	public String getNextURL() {
		return nextURL;
	}
	public void setNextURL(String nextURL) {
		this.nextURL = nextURL;
	}
	public boolean isForward() {
		return isForward;
	}
	public void setForward(boolean isForward) {
		this.isForward = isForward;
	}
	

}
