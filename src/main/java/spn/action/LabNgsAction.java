package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.ElasticDAO;
import spn.dao.HistoryDAO;
import spn.dao.InfoDAO;
import spn.dao.LabDAO;
import spn.dao.MemberDAO;
import spn.dto.ElasticDTO;
import spn.dto.HistoryDTO;
import spn.dto.InfoDTO;
import spn.dto.LabNgsDTO;


public class LabNgsAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String searchVal = (String) session.getAttribute("searchVal");
		String id=(String) session.getAttribute("id");
		String menu="lab_ngs";
		String prikey="ngs_no";
		String privalue=null;
		
		InfoDAO infoDAO = InfoDAO.getInfoDAO();

		List<InfoDTO> infoList = new ArrayList<InfoDTO>();
		infoList = (List) infoDAO.selectInfo(searchVal);
		
		if (infoList.size() != 0) {
			String chart_no = infoList.get(0).getChart_no();
			request.setAttribute("chart_no", chart_no);
		}
		
		String chart_no=request.getParameter("chart_no");
		String experiment=request.getParameter("experiment");
		String exp_date=request.getParameter("exp_date");
		String case_no=request.getParameter("case_no");
		String exp_place=request.getParameter("exp_place");
		String sample_yn_f=request.getParameter("sample_yn_f");
		String sample_yn_m=request.getParameter("sample_yn_m");
		String sample_yn_s=request.getParameter("sample_yn_s");
		String sample_yn_o=request.getParameter("sample_yn_o");
		String sample_yn_o_detail=request.getParameter("sample_yn_o_detail");
		String result=request.getParameter("result");
		String result_pending_detail=request.getParameter("result_pending_detail");
		String comment=request.getParameter("comment");
		String iid=request.getParameter("iid");
		String ngs_no = request.getParameter("ngs_no");
		
		LabNgsDTO i = new LabNgsDTO();

		i.setChart_no(chart_no);
		i.setExperiment(experiment);
		i.setExp_date(exp_date);
		i.setCase_no(case_no);
		i.setExp_place(exp_place);
		i.setSample_yn_f(sample_yn_f);
		i.setSample_yn_m(sample_yn_m);
		i.setSample_yn_s(sample_yn_s);
		i.setSample_yn_o(sample_yn_o);
		i.setSample_yn_o_detail(sample_yn_o_detail);
		i.setResult(result);
		i.setResult_pending_detail(result_pending_detail);
		i.setComment(comment);
		i.setIid(iid);
		i.setNgs_no(ngs_no);
		
		LabDAO dao=LabDAO.getLabDAO();
		int result1=dao.insertNgs(i);
		ActionForward forward= new ActionForward();
		
		System.out.println("result1 : "+result1); 
		
		if(result1 != 0) {
			//��� ������
			
			privalue=String.valueOf(Integer.parseInt(dao.lastIncreaseNumber(menu, prikey))); // 방금 삽입한 값을 참조하기 위해, 다시 한번 조회하도록 한다.
			
			/* elastic 작업 */
			HashMap<String, String> map=new HashMap<String, String>();
			
			map.put("chart_no", chart_no);
			map.put("experiment", experiment);
			map.put("exp_date", exp_date);
			map.put("case_no", case_no);
			map.put("exp_place", exp_place);
			map.put("sample_yn_f", sample_yn_f);
			map.put("sample_yn_m", sample_yn_m);
			map.put("sample_yn_s", sample_yn_s);
			map.put("sample_yn_o", sample_yn_o);
			map.put("sample_yn_o_detail", sample_yn_o_detail);
			map.put("result", result);
			map.put("comment", comment);
			map.put("menu", menu);
			map.put("iid", iid);
			map.put(prikey, privalue);
			
			ElasticDTO eldto=new ElasticDTO();
			ElasticDAO eldao=ElasticDAO.getElasticDAO();
			//eldto.setUrl("/lab_ngs/lab_ngs");
			eldto.setMenu(menu);
			eldao.insertElastic(eldto, map);
			
			/* History�� �߰��ϴ� �κ� */
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			HistoryDTO h=new HistoryDTO();
			
			InfoDAO i_dao=InfoDAO.getInfoDAO();
			List<InfoDTO> li=new ArrayList<InfoDTO>();
			
			li=(List)i_dao.selectInfo(chart_no); // InfoDAO���� �����ø��� ������? snupn, pa_bame, pa_sex
			h.setPa_name(li.get(0).getName());
			h.setPa_sex(li.get(0).getSex());
			h.setSnupn(li.get(0).getSnupn());
			
			MemberDAO m_dao=MemberDAO.getMemberDAO(); // MemberDao���� ������ �̸�
			h.setName(m_dao.findMember(id).getName());

			h.setChart_no(chart_no);			
			h.setIid(iid);
			h.setPage_name("lab_ngs");
			
			his_dao.insertHistory(h);
			
			forward.setForward(false); // redirect ������� �ѱ�
			
			forward.setNextURL("SPN?cmd=labngsform");
		}else {
			//��Ͻ��н�: ���������� ��������
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=labngsform");

		}
		
		return forward;
	}

}
