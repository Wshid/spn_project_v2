package spn.action;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import com.mysql.jdbc.Util;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import spn.dao.DbImageDAO;
import spn.dao.InfoDAO;
import spn.dao.MemberDAO;
import spn.dto.DbImageDTO;
import spn.dto.InfoDTO;

import java.util.*;

public class DbImageDeleteAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		ActionForward forward=new ActionForward();
		HttpSession session=request.getSession();
		
		System.out.println("In DbImageAction");
		
		String rootPath=request.getSession().getServletContext().getRealPath("/");
		
		
		String searchVal=(String)session.getAttribute("searchVal");
		//String page_name=(String)request.getAttribute("menu");
		//String subtitle=(String)request.getAttribute("subtitle");
		String file_name=(String)request.getParameter("delete_file_name");
		String page_name=(String)request.getParameter("delete_page_name");
		
		
		String file_path=rootPath+"db_image\\"+page_name+"\\"+file_name;
		String forwardUrl="";
		
		
		switch(page_name){
		case "lab_sanger":
			forwardUrl="SPN?cmd=labsangerform";
			break;
		case "lab_ngs":
			forwardUrl="SPN?cmd=labngsform";
			break;
		case "lab_cgh":
			forwardUrl="SPN?cmd=labcghform";
			break;
		case "info":
			forwardUrl="SPN?cmd=infoform";
			break;
		case "lab_stain":
			forwardUrl="SPN?cmd=labstainform";
			break;
		default:
			System.out.println("NOT FORWARDED ON DbImageDelete");
			break;
		}		
		//System.out.println(file_path);
		
		
		try{
			File f=new File(file_path);
			System.out.println(file_path);
			if(f.delete()){
				System.out.println("파일 지우기 성공");
			}else{
				System.out.println("파일 지우기 실패");
			}
			System.out.println("Delete File Success!(Local)");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
		try{
			DbImageDTO dto=new DbImageDTO(); // 연산 필요 인자만 setting해서 넘긴다.
			//dto.setChart_no(searchVal);
			//dto.setPage_name(page_name);
			//dto.setSubtitle(subtitle);
			dto.setImage_name(file_name);
			
			DbImageDAO dao=DbImageDAO.getDbImageDAO();
			
			int result=dao.deleteDbImage(dto);
			
			if(result!=0){
				System.out.println("Delete DbImage Success");
				forward.setForward(false);
				forward.setNextURL(forwardUrl);
			}else{
				forward.setForward(false);
				forward.setNextURL(forwardUrl);
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		
		//forward.setForward(false);
		//forward.setNextURL("SPN?cmd=labsangerform");
		return forward;	
		
	}

}
