package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.InfoDAO;
import spn.dao.SampleDAO;
import spn.dto.InfoDTO;
import spn.dto.SampleCfdnaDTO;
import spn.dto.SampleDnaDTO;

public class SampleCfdnaFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		} 
		else {
			
			String searchVal="";
			if(request.getParameter("searchVal")!=null) { //검색을 통한 접속이라면,
				searchVal=request.getParameter("searchVal");
				session.setAttribute("searchVal", searchVal);
				//searchVal=(String)session.getAttribute(request.getParameter("searchVal").toString());
			}
			else { // Info를 통한 접속일 
				searchVal = (String) session.getAttribute("searchVal");
			}

			InfoDAO infoDAO=InfoDAO.getInfoDAO();
			
			List<InfoDTO> infoList = new ArrayList<InfoDTO>();
			infoList = (List) infoDAO.selectInfo(searchVal);
			
			if (infoList.size() != 0) {
				String snupn = infoList.get(0).getSnupn();
				String chart_no= infoList.get(0).getChart_no();
				String name = infoList.get(0).getName();
				String sex= infoList.get(0).getSex();
				String birth = infoList.get(0).getBirth();
				String doctor = infoList.get(0).getDoctor();
				//String refered_dr= infoList.get(0).getRefered_dr();
				String other_site= infoList.get(0).getOther_site();
				String category = infoList.get(0).getCategory();
				//String final_diagnosis = infoList.get(0).getFinal_diagnosis();
				String clinical_diagnosis = infoList.get(0).getClinical_diagnosis();
				String symptom = infoList.get(0).getSymptom();
				//String remark = infoList.get(0).getRemark();
				
				request.setAttribute("snupn", snupn);
				request.setAttribute("chart_no", chart_no);
				request.setAttribute("name", name);
				request.setAttribute("sex", sex);
				request.setAttribute("birth", birth);
				request.setAttribute("doctor", doctor);
				//request.setAttribute("refered_dr", refered_dr);
				request.setAttribute("other_site", other_site);
				request.setAttribute("category", category);
				//request.setAttribute("final_diagnosis", final_diagnosis);
				request.setAttribute("clinical_diagnosis", clinical_diagnosis);
				request.setAttribute("symptom", symptom);
				//request.setAttribute("remark", remark);
				
			}
			
			SampleDAO dao = SampleDAO.getSampleDAO();
			
			List<SampleCfdnaDTO> li = new ArrayList<SampleCfdnaDTO>();
			li = (List) dao.selectCfdna(searchVal);
			
			
			List<String> cfd_no=new ArrayList<String>();
			List<String> col_date=new ArrayList<String>();
			List<String> col_location=new ArrayList<String>();
			List<String> col_who=new ArrayList<String>();
			List<String> col_who_patient_detail=new ArrayList<String>();
			List<String> col_who_father_detail=new ArrayList<String>();
			List<String> col_who_mother_detail=new ArrayList<String>();
			List<String> col_who_sibling_detail=new ArrayList<String>();
			List<String> col_who_other_detail=new ArrayList<String>();
			//List<String> col_type=new ArrayList<String>();
			//List<String> col_type_other_detail=new ArrayList<String>();
			List<String> col_index=new ArrayList<String>();
			List<String> col_wk=new ArrayList<String>();
			List<String> col_plasma_volume=new ArrayList<String>();
			List<String> col_cf_no=new ArrayList<String>();
			List<String> col_concentration_1=new ArrayList<String>();
			List<String> col_concentration_2=new ArrayList<String>();
			List<String> col_260_280_1=new ArrayList<String>();
			List<String> col_260_280_2=new ArrayList<String>();
			List<String> col_260_230_1=new ArrayList<String>();
			List<String> col_260_230_2=new ArrayList<String>();
			List<String> col_bioanalyzer_size_1=new ArrayList<String>();
			List<String> col_bioanalyzer_size_2=new ArrayList<String>();
			List<String> col_bioanalyzer_concentation_1=new ArrayList<String>();
			List<String> col_bioanalyzer_concentation_2=new ArrayList<String>();
			List<String> col_comment=new ArrayList<String>();
			List<String> req_date=new ArrayList<String>();
			List<String> req_location=new ArrayList<String>();
			List<String> req_who=new ArrayList<String>();
			List<String> req_who_other_detail=new ArrayList<String>();
			List<String> req_distribution=new ArrayList<String>();
			List<String> req_order_name=new ArrayList<String>();
			List<String> req_vial=new ArrayList<String>();
			List<String> req_comment=new ArrayList<String>();
			List<String> idate=new ArrayList<String>();
			List<String> iid=new ArrayList<String>();
			
			System.out.println("li size");
			System.out.println(li.size());
			
			if (li.size() != 0) {
				for(SampleCfdnaDTO cf : li) {
					cfd_no.add(cf.getCfd_no());
					col_date.add(cf.getCol_date());
					col_location.add(cf.getCol_location());
					col_who.add(cf.getCol_who());
					col_who_patient_detail.add(cf.getCol_who_patient_detail());
					col_who_father_detail.add(cf.getCol_who_father_detail());
					col_who_mother_detail.add(cf.getCol_who_mother_detail());
					col_who_sibling_detail.add(cf.getCol_who_sibling_detail());
					col_who_other_detail.add(cf.getCol_who_other_detail());
					//col_type.add(cf.getCol_type
					//col_who_other_detail.add(cf.getCol_who_other_detail());
					col_index.add(cf.getCol_index());
					col_wk.add(cf.getCol_wk());
					col_plasma_volume.add(cf.getCol_plasma_volume());
					col_cf_no.add(cf.getCol_cf_no());
					col_concentration_1.add(cf.getCol_concentration_1());
					col_concentration_2.add(cf.getCol_concentration_2());
					col_260_280_1.add(cf.getCol_260_280_1());
					col_260_280_2.add(cf.getCol_260_280_2());
					col_260_230_1.add(cf.getCol_260_230_1());
					col_260_230_2.add(cf.getCol_260_230_2());
					col_bioanalyzer_size_1.add(cf.getCol_bioanalyzer_size_1());
					col_bioanalyzer_size_2.add(cf.getCol_bioanalyzer_size_2());
					col_bioanalyzer_concentation_1.add(cf.getCol_bioanalyzer_concentation_1());
					col_bioanalyzer_concentation_2.add(cf.getCol_bioanalyzer_concentation_2());
					col_comment.add(cf.getCol_comment());
					req_date.add(cf.getReq_date());
					req_location.add(cf.getReq_location());
					req_who.add(cf.getReq_who());
					req_who_other_detail.add(cf.getReq_who_other_detail());
					req_distribution.add(cf.getReq_distribution());
					req_order_name.add(cf.getReq_order_name());
					req_vial.add(cf.getReq_vial());
					req_comment.add(cf.getReq_comment());
					idate.add(cf.getIdate());
					iid.add(cf.getIid());
				}
			}
				
			request.setAttribute("cfd_no", cfd_no);
			
			System.out.println("cfd_no check");
			System.out.println(cfd_no);
			
			request.setAttribute("col_date", col_date);
			request.setAttribute("col_location", col_location);
			request.setAttribute("col_who", col_who);
			request.setAttribute("col_who_patient_detail", col_who_patient_detail);
			request.setAttribute("col_who_father_detail", col_who_father_detail);
			request.setAttribute("col_who_mother_detail", col_who_mother_detail);
			request.setAttribute("col_who_sibling_detail", col_who_sibling_detail);
			request.setAttribute("col_who_other_detail", col_who_other_detail);
			request.setAttribute("col_index", col_index);
			request.setAttribute("col_wk", col_wk);
			request.setAttribute("col_plasma_volume", col_plasma_volume);
			request.setAttribute("col_cf_no", col_cf_no);
			request.setAttribute("col_concentration_1", col_concentration_1);
			request.setAttribute("col_concentration_2", col_concentration_2);
			request.setAttribute("col_260_280_1", col_260_280_1);
			request.setAttribute("col_260_280_2", col_260_280_2);
			request.setAttribute("col_260_230_1", col_260_230_1);
			request.setAttribute("col_260_230_2", col_260_230_2);
			request.setAttribute("col_bioanalyzer_size_1", col_bioanalyzer_size_1);
			request.setAttribute("col_bioanalyzer_size_2", col_bioanalyzer_size_2);
			request.setAttribute("col_bioanalyzer_concentation_1", col_bioanalyzer_concentation_1);
			request.setAttribute("col_bioanalyzer_concentation_2", col_bioanalyzer_concentation_2);
			request.setAttribute("col_comment", col_comment);
			request.setAttribute("req_date", req_date);
			request.setAttribute("req_location", req_location);
			request.setAttribute("req_who", req_who);
			request.setAttribute("req_who_other_detail", req_who_other_detail);
			request.setAttribute("req_distribution", req_distribution);
			request.setAttribute("req_order_name", req_order_name);
			request.setAttribute("req_vial", req_vial);
			request.setAttribute("req_comment", req_comment);
			request.setAttribute("idate", idate);
			request.setAttribute("iid", iid);

			forward.setForward(true);
			forward.setNextURL("./SPN/SampleCfdna.jsp");
			return forward;
		}
	}

}