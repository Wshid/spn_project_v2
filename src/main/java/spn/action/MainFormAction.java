package spn.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class MainFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		session.setAttribute("searchVal", "");
		session.setAttribute("NotFound", 0); // 검색 여부 판단 변수
		//session.removeAttribute("NotFound");
		
		if (session.getAttribute("id") == null) {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		} else {
			forward.setForward(true);
			forward.setNextURL("/SPN/Main.jsp");
			return forward;
		}
	}

}