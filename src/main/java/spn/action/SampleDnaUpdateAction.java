package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.ElasticDAO;
import spn.dao.HistoryDAO;
import spn.dao.InfoDAO;
import spn.dao.LabDAO;
import spn.dao.MemberDAO;
import spn.dao.SampleDAO;
import spn.dto.ElasticDTO;
import spn.dto.HistoryDTO;
import spn.dto.InfoDTO;
import spn.dto.LabNgsDTO;
import spn.dto.SampleDnaDTO;


public class SampleDnaUpdateAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		String searchVal = (String) session.getAttribute("searchVal");
		String id=(String) session.getAttribute("id");

		InfoDAO infoDAO = InfoDAO.getInfoDAO();
		List<InfoDTO> infoList = new ArrayList<InfoDTO>();
		infoList = (List) infoDAO.selectInfo(searchVal);
		
		if (infoList.size() != 0) {
			String chart_no = infoList.get(0).getChart_no();
			request.setAttribute("chart_no", chart_no);
		}
		
		String menu="sample_dna";
		String prikey="dna_no";
		String privalue=null;
		
		
		String dna_no=request.getParameter("dna_no");
		String chart_no=request.getParameter("chart_no");
		String col_date=request.getParameter("col_date");
		String col_who=request.getParameter("col_who");
		String col_who_patient_detail=request.getParameter("col_who_patient_detail");
		String col_who_father_detail=request.getParameter("col_who_father_detail");
		String col_who_mother_detail=request.getParameter("col_who_mother_detail");
		String col_who_sibling_detail=request.getParameter("col_who_sibling_detail");
		String col_who_other_detail=request.getParameter("col_who_other_detail");
		String col_type=request.getParameter("col_type");
		String col_type_other_detail=request.getParameter("col_type_other_detail");
		String col_index=request.getParameter("col_index");
		String col_agreement=request.getParameter("col_agreement");
		String col_performed_by=request.getParameter("col_performed_by");		
		String col_concentration=request.getParameter("col_concentration");
		String col_260_280=request.getParameter("col_260_280");
		String col_260_230=request.getParameter("col_260_230");
		String col_location=request.getParameter("col_location");
		String col_comment=request.getParameter("col_comment");
		String req_date=request.getParameter("req_date");
		String req_location=request.getParameter("req_location");
		String req_who=request.getParameter("req_who");
		String req_who_other_detail=request.getParameter("req_who_other_detail");
		String req_distribution=request.getParameter("req_distribution");
		String req_order_name=request.getParameter("req_order_name");
		String req_type=request.getParameter("req_type");
		String req_type_other_detail=request.getParameter("req_type_other_detail");
		String req_vial=request.getParameter("req_vial");
		String req_comment=request.getParameter("req_comment");
		String iid=request.getParameter("iid");
				
		
		SampleDnaDTO i = new SampleDnaDTO();

		i.setDna_no(dna_no);
		i.setChart_no(chart_no);
		i.setCol_date(col_date);
		i.setCol_who(col_who);
		i.setCol_who_patient_detail(col_who_patient_detail);
		i.setCol_who_father_detail(col_who_father_detail);
		i.setCol_who_mother_detail(col_who_mother_detail);
		i.setCol_who_sibling_detail(col_who_sibling_detail);
		i.setCol_who_other_detail(col_who_other_detail);
		i.setCol_type(col_type);
		i.setCol_type_other_detail(col_type_other_detail);
		i.setCol_index(col_index);
		i.setCol_agreement(col_agreement);
		i.setCol_performed_by(col_performed_by);
		i.setCol_concentration(col_concentration);
		i.setCol_260_280(col_260_280);
		i.setCol_260_230(col_260_230);
		i.setCol_location(col_location);
		i.setCol_comment(col_comment);
		i.setReq_date(req_date);
		i.setReq_location(req_location);
		i.setReq_who(req_who);
		i.setReq_who_other_detail(req_who_other_detail);
		i.setReq_distribution(req_distribution);
		i.setReq_order_name(req_order_name);
		i.setReq_type(req_type);
		i.setReq_type_other_detail(req_type_other_detail);
		i.setReq_vial(req_vial);
		i.setReq_comment(req_comment);
		i.setIid(iid);
		
		SampleDAO dao=SampleDAO.getSampleDAO();
		int result=dao.updateDna(i);
		
		System.out.println("IS INSERT");
		
		ActionForward forward= new ActionForward();
		
		if(result != 0) {
			//��� ������

			privalue=String.valueOf(Integer.parseInt(dao.lastIncreaseNumber(menu, prikey))); // 방금 삽입한 값을 참조하기 위해, 다시 한번 조회하도록 한다.
			
			/* elastic 작업 */
			HashMap<String, String> map=new HashMap<String, String>();
			
			map.put("chart_no", chart_no);
			map.put("col_date", col_date);
			map.put("col_who", col_who);
			map.put("col_who_patient_detail", col_who_patient_detail);
			map.put("col_who_father_detail", col_who_father_detail);
			map.put("col_who_mother_detail", col_who_mother_detail);
			map.put("col_who_sibling_detail", col_who_sibling_detail);
			map.put("col_who_other_detail", col_who_other_detail);
			map.put("col_type", col_type);
			map.put("col_type_other_detail", col_type_other_detail);
			map.put("col_index", col_index);
			map.put("col_agreement", col_agreement);
			
			map.put("col_performed_by", col_performed_by);
			map.put("col_concentration", col_concentration);
			map.put("col_260_280", col_260_280);
			map.put("col_260_230", col_260_230);
			map.put("col_location", col_location);
			map.put("col_comment", col_comment);
			map.put("req_date", req_date);
			map.put("req_location", req_location);
			map.put("req_who", req_who);
			map.put("req_who_other_detail", req_who_other_detail);
			map.put("req_distribution", req_distribution);
			map.put("req_order_name", req_order_name);
			map.put("req_type", req_type);
			map.put("req_type_other_detail", req_type_other_detail);
			map.put("req_vial", req_vial);
			map.put("req_comment", req_comment);
			
			map.put("menu", menu);
			map.put("iid", iid);
			map.put(prikey, privalue);
			
			ElasticDTO eldto=new ElasticDTO();
			ElasticDAO eldao=ElasticDAO.getElasticDAO();
			
			HashMap<String, String> map_search=new HashMap<String, String>();
			map_search.put(prikey, privalue);
			String _id=eldao.getElasticId(map_search);
			
			eldto.setMenu(menu);
			//eldto.setUrl("/sample_dna/sample_dna");
			eldao.updateElastic(eldto, map);
			
			/* History�� �߰��ϴ� �κ� */
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			HistoryDTO h=new HistoryDTO();
			
			InfoDAO i_dao=InfoDAO.getInfoDAO();
			List<InfoDTO> li=new ArrayList<InfoDTO>();
			
			li=(List)i_dao.selectInfo(chart_no); // InfoDAO���� �����ø��� ������? snupn, pa_bame, pa_sex
			h.setPa_name(li.get(0).getName());
			h.setPa_sex(li.get(0).getSex());
			h.setSnupn(li.get(0).getSnupn());
			
			MemberDAO m_dao=MemberDAO.getMemberDAO(); // MemberDao���� ������ �̸�
			h.setName(m_dao.findMember(id).getName());

			h.setChart_no(chart_no);			
			h.setIid(iid);
			h.setPage_name(menu);
			
			his_dao.insertHistory(h);
			
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=samplednaform");
		}else {
			//��Ͻ��н�: ���������� ��������
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=samplednaform");

		}
		
		return forward;
	}

}
