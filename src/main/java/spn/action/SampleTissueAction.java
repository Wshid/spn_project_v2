package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.ElasticDAO;
import spn.dao.HistoryDAO;
import spn.dao.InfoDAO;
import spn.dao.MemberDAO;
import spn.dao.SampleDAO;
import spn.dto.ElasticDTO;
import spn.dto.HistoryDTO;
import spn.dto.InfoDTO;
import spn.dto.SampleTissueDTO;

public class SampleTissueAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		String searchVal = (String) session.getAttribute("searchVal");
		String id=(String) session.getAttribute("id");

		InfoDAO infoDAO = InfoDAO.getInfoDAO();

		List<InfoDTO> infoList = new ArrayList<InfoDTO>();
		infoList = (List) infoDAO.selectInfo(searchVal);

		if (infoList.size() != 0) {
			String chart_no = infoList.get(0).getChart_no();
			request.setAttribute("chart_no", chart_no);
		}
		
		String menu="sample_tissue";
		String prikey="tis_no";
		String privalue=null;

		String chart_no = request.getParameter("chart_no");
		String bio_date = request.getParameter("bio_date");
		//String bio_type = request.getParameter("bio_type");
		String bio_type_myoblast = request.getParameter("bio_type_myoblast");
		String bio_type_fibroblast = request.getParameter("bio_type_fibroblast");
		String bio_type_u_epithelial = request.getParameter("bio_type_epithelial");
		String bio_type_u_ipsc = request.getParameter("bio_type_u_ipsc");
		String bio_type_other = request.getParameter("bio_type_other");
		String bio_type_other_detail = request.getParameter("bio_type_other_detail");
		String bio_index_1 = request.getParameter("bio_index_1");
		//String bio_location_1 = request.getParameter("bio_location_1");
		//String bio_cell = request.getParameter("bio_cell");
		//String bio_index_2 = request.getParameter("bio_index_2");
		//String bio_phase = request.getParameter("bio_phase");
		String bio_storage = request.getParameter("bio_storage");
		//String bio_location_2 = request.getParameter("bio_location_2");
		String bio_comment = request.getParameter("bio_comment");
		String req_date = request.getParameter("req_date");
		String req_location = request.getParameter("req_location");
		//String req_distribution = request.getParameter("req_distribution");
		String req_order_name = request.getParameter("req_order_name");
		//String req_type = request.getParameter("req_type");
		//String req_type_other_detail = request.getParameter("req_type_other_detail");
		String req_vial = request.getParameter("req_vial");
		String req_comment = request.getParameter("req_comment");
		String iid = request.getParameter("iid");

		SampleTissueDTO i = new SampleTissueDTO();

		i.setChart_no(chart_no);
		i.setBio_date(bio_date);
		//i.setBio_type(bio_type);
		i.setBio_type_myoblast(bio_type_myoblast);
		i.setBio_type_fibroblast(bio_type_fibroblast);
		i.setBio_type_u_epithelial(bio_type_u_epithelial);
		i.setBio_type_u_ipsc(bio_type_u_ipsc);
		i.setBio_type_other(bio_type_other);
		i.setBio_type_other_detail(bio_type_other_detail);
		i.setBio_index_1(bio_index_1);
		//i.setBio_location_1(bio_location_1);
		//i.setBio_cell(bio_cell);
		//i.setBio_index_2(bio_index_2);
		//i.setBio_phase(bio_phase);
		i.setBio_storage(bio_storage);
		//i.setBio_location_2(bio_location_2);
		i.setBio_comment(bio_comment);
		i.setReq_date(req_date);
		i.setReq_location(req_location);
		//i.setReq_distribution(req_distribution);
		i.setReq_order_name(req_order_name);
		//i.setReq_type(req_type);
		//i.setReq_type_other_detail(req_type_other_detail);
		i.setReq_vial(req_vial);
		i.setReq_comment(req_comment);
		i.setIid(iid);

		SampleDAO dao = SampleDAO.getSampleDAO();
		int result = dao.insertTissue(i);
		ActionForward forward = new ActionForward();

		if (result != 0) {
			// ��� ������
			privalue=String.valueOf(Integer.parseInt(dao.lastIncreaseNumber(menu, prikey))); // 방금 삽입한 값을 참조하기 위해, 다시 한번 조회하도록 한다.
			
			/* elastic 작업 */
			HashMap<String, String> map=new HashMap<String, String>();
			
			map.put("chart_no", chart_no);
			map.put("bio_date", bio_date);
			map.put("bio_type_myoblast", bio_type_myoblast);
			map.put("bio_type_fibroblast", bio_type_fibroblast);
			map.put("bio_type_u_epithelial", bio_type_u_epithelial);
			map.put("bio_type_u_ipsc", bio_type_u_ipsc);
			map.put("bio_type_other", bio_type_other);
			map.put("bio_type_other_detail", bio_type_other_detail);
			map.put("bio_index_1", bio_index_1);
			map.put("bio_storage", bio_storage);
			map.put("bio_comment", bio_comment);
			map.put("req_date", req_date);
			map.put("req_location", req_location);
			map.put("req_order_name", req_order_name);
			map.put("req_vial", req_vial);
			map.put("req_comment", req_comment);
			map.put("iid", iid);
			map.put("menu", menu);
			map.put(prikey, privalue);
			
			
			ElasticDTO eldto=new ElasticDTO();
			ElasticDAO eldao=ElasticDAO.getElasticDAO();
			eldto.setMenu(menu);
			//eldto.setUrl("/sample_tissue/sample_tissue");
			eldao.insertElastic(eldto, map);
			
			/* History�� �߰��ϴ� �κ� */
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			HistoryDTO h=new HistoryDTO();
			
			InfoDAO i_dao=InfoDAO.getInfoDAO();
			List<InfoDTO> li=new ArrayList<InfoDTO>();
			
			li=(List)i_dao.selectInfo(chart_no); // InfoDAO���� �����ø��� ������? snupn, pa_bame, pa_sex
			h.setPa_name(li.get(0).getName());
			h.setPa_sex(li.get(0).getSex());
			h.setSnupn(li.get(0).getSnupn());
			
			MemberDAO m_dao=MemberDAO.getMemberDAO(); // MemberDao���� ������ �̸�
			h.setName(m_dao.findMember(id).getName());

			h.setChart_no(chart_no);			
			h.setIid(iid);
			h.setPage_name("sample_tissue");
			
			his_dao.insertHistory(h);
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=sampletissueform");
		} else {
			// ��Ͻ��н�: ���������� ��������
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=sampletissueform");

		}

		return forward;
	}

}
