package spn.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.ElasticDAO;
import spn.dto.ElasticDTO;

public class SearchFormAction implements Action{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession session = request.getSession();
		String searchVal="";
		if(request.getParameter("searchVal")!=null) { //검색을 통한 접속이라면,
			searchVal=request.getParameter("searchVal");
			session.setAttribute("searchVal", searchVal);
			//searchVal=(String)session.getAttribute(request.getParameter("searchVal").toString());
		}
		else { // statistic을 통한 접속일 경우  
			searchVal = "";
		}

		String prevPage = request.getParameter("prev_page"); // 단순 String이기 때문에 이렇게 보냄
		ActionForward forward= new ActionForward();
		//String id=(String) session.getAttribute("id");
		

		String searchResult="";
		String searchCount="";
		ElasticDAO dao=ElasticDAO.getElasticDAO();
		ElasticDTO dto=new ElasticDTO();
		String url="";
		
		// 사실상 여기서 분기를 해주어야 하나..
		if(prevPage==null){ // 검색을 통한 접속이라면
			dto.setSearchText(searchVal);
			url="/_search";
			request.setAttribute("searchText", searchVal);

		}else if(prevPage.equals("statistic") || prevPage.equals("main")){  // 이전 페이지가 통계 페이지 였다면
			String menu=request.getParameter("menu");
			url="/"+menu+"/"+menu+"/_search";
			request.setAttribute("searchText", menu.toUpperCase()); // 여기 수정해야 
		}else {
			System.out.println("ERROR SearchFormAction URL");
		}
		
		System.out.println("SearchFormAction URL : "+url);
		dto.setUrl(url);
		dto.setSize("1000");
		
		
		
		
		ElasticDTO ret=dao.searchElastic(dto);
		searchResult=ret.getResponseJsonString();
		
		searchCount=ret.getCount();
		
		request.setAttribute("searchResult", searchResult);
		request.setAttribute("searchCount", searchCount);
		
		
		//forward.setForward(true);
		//forward.setNextURL("./SPN/SampleUrine.jsp");
		//return forward;
		
		if(searchCount=="0") { // 연산결과가 없을경우,
			// moal을 띄울 수 있을까?
			session.setAttribute("NotFound",1);
			forward.setNextURL("./SPN/SearchResult.jsp");
			forward.setForward(true);
		}else {
			forward.setForward(true);
			forward.setNextURL("./SPN/SearchResult.jsp");
			session.setAttribute("NotFound",0);
		}
		
		return forward;
	}

}
