package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.ElasticDAO;
import spn.dao.InfoDAO;
import spn.dto.ElasticDTO;

public class SearchInfoAction implements Action{
	 
	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String searchVal = request.getParameter("searchVal");

		HttpSession session = request.getSession();
		session.setAttribute("searchVal", searchVal);
		ActionForward forward= new ActionForward();
/*
		InfoDAO dao=InfoDAO.getInfoDAO();
		
		List<InfoDTO> li = new ArrayList<InfoDTO>();
		li = (List) dao.selectInfo(searchVal);

		if (li.size() == 0 ) {
			//��ϵ������� ������ϰ��
			forward.setForward(true);
			//forward.setNextURL("SPN?cmd=mainform");
			forward.setNextURL("SPN?cmd=infoform");
			session.setAttribute("searchVal", "");
			session.setAttribute("NotFound", 1); // 1�� ��� ��ã��
			
			return forward;
		}
		//��Ͻ��н�: ���������� ��������
		forward.setForward(true);
		session.setAttribute("NotFound", 0); // 0 �ϰ�� ã��
		forward.setNextURL("SPN?cmd=infoform");
		return forward;
		
*/		
		/* 사용자에게 특정 값만을 불러들어와서 판단한다 */
		
		String searchResult="";
		String searchCount="";
		ElasticDAO dao=ElasticDAO.getElasticDAO();
		ElasticDTO dto=new ElasticDTO();
		dto.setSearchText(searchVal);
		dto.setSize("1000");
		
		
		ElasticDTO ret=dao.searchElastic(dto);
		searchResult=ret.getResponseJsonString();
		searchCount=ret.getCount();
		
		if(searchCount=="0") { // 연산결과가 없을경우,
			// moal을 띄울 수 있을까?
			session.setAttribute("NotFound",1);
		}else {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=searchform"); // searchform이라는 pattern로 이
			session.setAttribute("NotFound",0);
		}
		
		
		return forward;
	}
}
