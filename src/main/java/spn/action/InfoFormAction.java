package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.DbImageDAO;
import spn.dao.InfoDAO;
import spn.dto.DbImageDTO;
import spn.dto.InfoDTO;

public class InfoFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward=new ActionForward();
		HttpSession session = request.getSession();
		System.out.println("In InfoFormAction");
		//System.out.println("SEARCH VAR : "+request.getParameter("searchVal"));
		if(session.getAttribute("id")==null){
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		}
		else{
			String searchVal="";
			if(request.getParameter("searchVal")!=null) { //검색을 통한 접속이라면,
				searchVal=request.getParameter("searchVal");
				session.setAttribute("searchVal", searchVal);
				//searchVal=(String)session.getAttribute(request.getParameter("searchVal").toString());
			}
			else { // Info를 통한 접속일 
				searchVal = (String) session.getAttribute("searchVal");
			}
			System.out.println("CLEAR BEFORE searchVal : "+request.getParameter("clear"));
			if(request.getParameter("clear")!=null) {
				session.removeAttribute("searchVal");
				searchVal="";
			}
			
			System.out.println("SEARCH VAR : "+searchVal);
			InfoDAO dao=InfoDAO.getInfoDAO();
			
			List<InfoDTO> li = new ArrayList<InfoDTO>();
			li = (List) dao.selectInfo(searchVal);
			
			if (li.size() != 0) {
				String gi_no = li.get(0).getGi_no();
				String snupn = li.get(0).getSnupn();
				String chart_no= li.get(0).getChart_no();
				String name = li.get(0).getName();
				String sex= li.get(0).getSex();
				String birth = li.get(0).getBirth();
				String doctor = li.get(0).getDoctor();
				//String refered_dr= li.get(0).getRefered_dr();
				String other_site= li.get(0).getOther_site();
				String category = li.get(0).getCategory();
				//String final_diagnosis = li.get(0).getFinal_diagnosis();
				String clinical_diagnosis = li.get(0).getClinical_diagnosis();
				String symptom = li.get(0).getSymptom();
				//String remark = li.get(0).getRemark();
				String iid = li.get(0).getIid();
				String idate = li.get(0).getIdate();
				
				request.setAttribute("gi_no", gi_no);
				request.setAttribute("snupn", snupn);
				request.setAttribute("chart_no", chart_no);
				request.setAttribute("name", name);
				request.setAttribute("sex", sex);
				request.setAttribute("birth", birth);
				request.setAttribute("doctor", doctor);
				//request.setAttribute("refered_dr", refered_dr);
				request.setAttribute("other_site", other_site);
				request.setAttribute("category", category);
				//request.setAttribute("final_diagnosis", final_diagnosis);
				request.setAttribute("clinical_diagnosis", clinical_diagnosis);
				request.setAttribute("symptom", symptom);
				//request.setAttribute("remark", remark);
				request.setAttribute("iid", iid);
				request.setAttribute("idate", idate);
				
			}
			
			/* �̹��� ���ε� ���� ��, ������ ������ 1�常 Ȱ���� ���� */
			DbImageDAO dao_image=DbImageDAO.getDbImageDAO();
			List<DbImageDTO> li_image=new ArrayList<DbImageDTO>(); 
			li_image=(List)dao_image.selectDbImage(searchVal, "info"); // ���⼭�� �Ѱ��� �������� �ϰ�����
			String path="", file_name="", doctor="";
			
			if(li_image.size()!=0){
				//for(int i=0;i<li_image.size();i++){
					file_name = (li_image.get(0).getImage_name());
					path = (li_image.get(0).getFull_path());
					doctor = (li_image.get(0).getDoctor());
				//}
			}
			
			request.setAttribute("path", path); // �ش� chartnum �׸��� page name�� �ش��ϴ� ������ ������ �����Ͽ� ��������.
			request.setAttribute("file_name", file_name);
			request.setAttribute("page_name", "info");
			request.setAttribute("next_idx", dao_image.getNextIndex());
			request.setAttribute("doctor", doctor);
			
	
			forward.setForward(true);
			forward.setNextURL("./SPN/Info.jsp");
			return forward;
		}
	}

}