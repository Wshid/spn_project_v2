
package spn.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import spn.action.ActionForward;

//인터페이스는 앞에 abstract 안 붙여도 자동으로 붙어! 어차피 추상메소드 밖에 못들어가거든. 
public interface Action {
	
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
    //이걸 상속받아 기능별로 action기능을 하나 하나 따로 만들 것.

}