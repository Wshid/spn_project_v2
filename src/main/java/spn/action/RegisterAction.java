package spn.action;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.MemberDAO;
import spn.dto.MemberDTO;


public class RegisterAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
				
		String id=request.getParameter("id");
		String pw=request.getParameter("pw");
		String name=request.getParameter("name");
		String affiliation=request.getParameter("affiliation");
		String phone=request.getParameter("phone");
		String permission=request.getParameter("permission");
		
		
		MemberDTO m = new MemberDTO();

		m.setId(id);
		m.setPw(pw);
		m.setName(name);
		m.setAffiliation(affiliation);
		m.setPhone(phone);
		m.setPermission(permission);
		
		MemberDAO dao=MemberDAO.getMemberDAO();
		int result=dao.insertMember(m);
		
		ActionForward forward= new ActionForward();
		
		if(result != 0) {
			//등록 성공시 User List로 이동!!!!!!!! 수정 해아 함
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=memberlist");
		}else {
			//등록실패시: 에러페이지 만들어야함
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=memberlist");

		}
		
		return forward;
	}

}
