package spn.action;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
//import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Iterator;
//import java.net.URLConnection;
//import java.net.UnknownHostException;
/*
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
*/
import java.util.Map;

import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.RequestLine;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;

/* ElasticSearch */
/*
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
*/
/* json File */
//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;

public class ElasticTest{
	
	public static void main(String[] args) {
		
		
		String responseBody=requestQueryAllElastic("475", 5);
		
		try {
	
			JSONParser parser = new JSONParser();
			JSONObject jsonObj = (JSONObject) parser.parse( responseBody );
			
			//System.out.println(((JSONObject)jsonObj.get("hits")).get("hits"));
			
			// 구조 - jsonObj.hits.hits[]형태이기 때문에, jsonArray로 중간 변환을 한 후, _source에 있는 실제 내용을 크롤링 한다.
			JSONObject first_hits=(JSONObject)jsonObj.get("hits");
			
			JSONArray jsonArr=(JSONArray)first_hits.get("hits"); 
			
			
			System.out.println("TOTAL : "+first_hits.get("total").toString());
			
			for(int i=0;i<jsonArr.size();i++){
				JSONObject temp=(JSONObject)((JSONObject)jsonArr.get(i)).get("_source");
				
				//Iterator<String> iter=((JSONObject)jsonArr.get(i)).keySet().iterator();
				Iterator<String> iter=temp.keySet().iterator();
				
				while(iter.hasNext()) { // 값이 null이면 못가져오는 상황 발
					String temp_iter=iter.next(); //키 목록 추출이 가능
					System.out.println(temp_iter);
					
					if(!temp.get(temp_iter).toString().isEmpty()) { // null값 처리 
						String temp_value=temp.get(temp_iter).toString();
						//System.out.println(temp_iter);
						System.out.println(temp_iter + " : " + temp_value);
					}
					
				} // 생각해보니 json처리의경우 javascript에 넘기면 됨
				
				
				
				//System.out.println(((JSONObject)jsonArr.get(i)).keySet());
				
				//System.out.println("name : "+temp.get("name")); // 잘 받아옴
			}
			System.out.println(jsonObj.size());
			
			
			
		} catch(Exception e) {
			e.printStackTrace();
		} finally {
			System.out.println(responseBody);
			
		}
		
		/*
		IndexResponse response = client.prepareIndex("main_info", "logs")
		        .setSource(json, XContentType.JSON)
		        .get();
		*/
		
		
		
	}
	
	// 나중에 이용할 수 있을 듯
	public static String makeQueryString(String searchText, int size) {
		String json =
					"{" +
		        			"\"query\":{"+
		        				"\"query_string\":{" +
		        					"\"query\":\"*"+searchText+"*\""+
		        				"}"+
		        			"},"+
		        			"\"size\":\""+size+"\""+
		        		"}";
		return json;
	}
	
	public static String requestQueryAllElastic(String searchText, int size) {
		
		String json=makeQueryString(searchText, size);
		try {
			RestClient restClient = RestClient.builder( // Initializing
			        new HttpHost("localhost", 9200, "http")).build();
			Map<String, String> params = Collections.singletonMap("pretty", "true");
			String jsonString = json;
			HttpEntity entity = new NStringEntity(jsonString, ContentType.APPLICATION_JSON);
			Response response = restClient.performRequest("GET", "main_info/_search", params, entity);
			//RequestLine requestLine = response.getRequestLine(); 
			//HttpHost host = response.getHost(); 
			//int statusCode = response.getStatusLine().getStatusCode(); 
			//Header[] headers = response.getHeaders(); 
			String responseBody = EntityUtils.toString(response.getEntity()); 
			
			System.out.println(responseBody);
			
			restClient.close();
			return responseBody;
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "";
	}

}
