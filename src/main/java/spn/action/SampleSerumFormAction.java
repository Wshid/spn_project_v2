package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.InfoDAO;
import spn.dao.SampleDAO;
import spn.dto.InfoDTO;
import spn.dto.SampleSerumDTO;

public class SampleSerumFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		} 
		else {
			
			String searchVal="";
			if(request.getParameter("searchVal")!=null) { //검색을 통한 접속이라면,
				searchVal=request.getParameter("searchVal");
				session.setAttribute("searchVal", searchVal);
				//searchVal=(String)session.getAttribute(request.getParameter("searchVal").toString());
			}
			else { // Info를 통한 접속일 
				searchVal = (String) session.getAttribute("searchVal");
			}

			InfoDAO infoDAO=InfoDAO.getInfoDAO();
			
			List<InfoDTO> infoList = new ArrayList<InfoDTO>();
			infoList = (List) infoDAO.selectInfo(searchVal);
			
			if (infoList.size() != 0) {
				String snupn = infoList.get(0).getSnupn();
				String chart_no= infoList.get(0).getChart_no();
				String name = infoList.get(0).getName();
				String sex= infoList.get(0).getSex();
				String birth = infoList.get(0).getBirth();
				String doctor = infoList.get(0).getDoctor();
				//String refered_dr= infoList.get(0).getRefered_dr();
				String other_site= infoList.get(0).getOther_site();
				String category = infoList.get(0).getCategory();
				//String final_diagnosis = infoList.get(0).getFinal_diagnosis();
				String clinical_diagnosis = infoList.get(0).getClinical_diagnosis();
				String symptom = infoList.get(0).getSymptom();
				//String remark = infoList.get(0).getRemark();
				
				request.setAttribute("snupn", snupn);
				request.setAttribute("chart_no", chart_no);
				request.setAttribute("name", name);
				request.setAttribute("sex", sex);
				request.setAttribute("birth", birth);
				request.setAttribute("doctor", doctor);
				//request.setAttribute("refered_dr", refered_dr);
				request.setAttribute("other_site", other_site);
				request.setAttribute("category", category);
				//request.setAttribute("final_diagnosis", final_diagnosis);
				request.setAttribute("clinical_diagnosis", clinical_diagnosis);
				request.setAttribute("symptom", symptom);
				//request.setAttribute("remark", remark);
				
			}
			
			SampleDAO dao = SampleDAO.getSampleDAO();
			
			List<SampleSerumDTO> li = new ArrayList<SampleSerumDTO>();
			li = (List) dao.selectSerum(searchVal);
			
			if(li.size() != 0) {
				String sc_no = li.get(0).getSc_no();
				//String experiment = li.get(0).getExperiment();
				String experiment_serum = li.get(0).getExperiment_serum();
				String experiment_csf = li.get(0).getExperiment_csf();
				String col_date = li.get(0).getCol_date();
				String dis_date = li.get(0).getDis_date();
				String col_index = li.get(0).getCol_index();
				String no = li.get(0).getNo();
				String storage = li.get(0).getStorage();
				String aquaporin_4_ab = li.get(0).getAquaporin_4_ab();
				String autoimmune_encephalitis = li.get(0).getAutoimmune_encephalitis();
				String order_other=li.get(0).getOrder_other();
				String order_other_detail=li.get(0).getOrder_other_detail();
				String experiment_place = li.get(0).getExperiment_place();
				String comment_1 = li.get(0).getComment_1();
				String location = li.get(0).getLocation();
				String comment_2 = li.get(0).getComment_2();
				String req_date = li.get(0).getReq_date();
				String req_location = li.get(0).getReq_location();
				String req_order_name = li.get(0).getReq_order_name();
				String req_vial = li.get(0).getReq_vial();
				String req_comment = li.get(0).getReq_comment();
				String idate = li.get(0).getIdate();
				String iid = li.get(0).getIid();
				
				request.setAttribute("sc_no", sc_no);
				//request.setAttribute("experiment", experiment);
				request.setAttribute("experiment_serum", experiment_serum);
				request.setAttribute("experiment_csf", experiment_csf);
				request.setAttribute("col_date", col_date);
				request.setAttribute("dis_date", dis_date);
				request.setAttribute("col_index", col_index);
				request.setAttribute("no", no);
				request.setAttribute("storage", storage);
				request.setAttribute("aquaporin_4_ab", aquaporin_4_ab);
				request.setAttribute("autoimmune_encephalitis", autoimmune_encephalitis);
				request.setAttribute("order_other", order_other);
				request.setAttribute("order_other_detail", order_other_detail);
				request.setAttribute("experiment_place", experiment_place);
				request.setAttribute("comment_1", comment_1);
				request.setAttribute("location", location);
				request.setAttribute("comment_2", comment_2);
				request.setAttribute("req_date", req_date);
				request.setAttribute("req_location", req_location);
				request.setAttribute("req_order_name", req_order_name);
				request.setAttribute("req_vial", req_vial);
				request.setAttribute("req_comment", req_comment);
				request.setAttribute("idate", idate);
				request.setAttribute("iid", iid);
				
			}

			forward.setForward(true);
			forward.setNextURL("./SPN/SampleSerum.jsp");
			return forward;
		}
	}

}