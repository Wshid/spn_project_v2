package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.DbImageDAO;
import spn.dao.InfoDAO;
import spn.dao.LabDAO;
import spn.dto.DbImageDTO;
import spn.dto.InfoDTO;
import spn.dto.LabStainDTO;

public class LabStainFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		} 
		else {
			
			String searchVal="";
			if(request.getParameter("searchVal")!=null) { //검색을 통한 접속이라면,
				searchVal=request.getParameter("searchVal");
				session.setAttribute("searchVal", searchVal);
				//searchVal=(String)session.getAttribute(request.getParameter("searchVal").toString());
			}
			else { // Info를 통한 접속일 
				searchVal = (String) session.getAttribute("searchVal");
			}

			InfoDAO infoDAO=InfoDAO.getInfoDAO();
			
			List<InfoDTO> infoList = new ArrayList<InfoDTO>();
			infoList = (List) infoDAO.selectInfo(searchVal);
			
			if (infoList.size() != 0) {
				String snupn = infoList.get(0).getSnupn();
				String chart_no= infoList.get(0).getChart_no();
				String name = infoList.get(0).getName();
				String sex= infoList.get(0).getSex();
				String birth = infoList.get(0).getBirth();
				String doctor = infoList.get(0).getDoctor();
				//String refered_dr= infoList.get(0).getRefered_dr();
				String other_site= infoList.get(0).getOther_site();
				String category = infoList.get(0).getCategory();
				//String final_diagnosis = infoList.get(0).getFinal_diagnosis();
				String clinical_diagnosis = infoList.get(0).getClinical_diagnosis();
				String symptom = infoList.get(0).getSymptom();
				//String remark = infoList.get(0).getRemark();
				
				request.setAttribute("snupn", snupn);
				request.setAttribute("chart_no", chart_no);
				request.setAttribute("name", name);
				request.setAttribute("sex", sex);
				request.setAttribute("birth", birth);
				request.setAttribute("doctor", doctor);
				//request.setAttribute("refered_dr", refered_dr);
				request.setAttribute("other_site", other_site);
				request.setAttribute("category", category);
				//request.setAttribute("final_diagnosis", final_diagnosis);
				request.setAttribute("clinical_diagnosis", clinical_diagnosis);
				request.setAttribute("symptom", symptom);
				//request.setAttribute("remark", remark);
				
			}
			
			LabDAO dao = LabDAO.getLabDAO();
			
			List<LabStainDTO> li = new ArrayList<LabStainDTO>();
			li = (List) dao.selectStain(searchVal);
			
			if (li.size() != 0) {
				String sms_no = li.get(0).getSms_no();
				String experiment = li.get(0).getExperiment();
				String m_index_no = li.get(0).getM_index_no();
				String discription = li.get(0).getDiscription();
				String pathology_diagnosis = li.get(0).getPathology_diagnosis();
				String result = li.get(0).getResult();
				String comment = li.get(0).getComment();
				String exp_date_1 = li.get(0).getExp_date_1();
				String antibody_1 = li.get(0).getAntibody_1();
				String antibody_2 = li.get(0).getAntibody_2();
				String antibody_3 = li.get(0).getAntibody_3();
				String antibody_4 = li.get(0).getAntibody_4();
				String antibody_5 = li.get(0).getAntibody_5();
				String antibody_6 = li.get(0).getAntibody_6();
				String antibody_7 = li.get(0).getAntibody_7();
				String antibody_8 = li.get(0).getAntibody_8();
				String antibody_9 = li.get(0).getAntibody_9();
				String antibody_10 = li.get(0).getAntibody_10();
				String antibody_11 = li.get(0).getAntibody_11();
				String antibody_12 = li.get(0).getAntibody_12();
				String antibody_13 = li.get(0).getAntibody_13();
				String antibody_14 = li.get(0).getAntibody_14();
				String antibody_15 = li.get(0).getAntibody_15();
				String antibody_16 = li.get(0).getAntibody_16();
				String antibody_17 = li.get(0).getAntibody_17();
				String antibody_18 = li.get(0).getAntibody_18();
				String exp_date_2 = li.get(0).getExp_date_2();
				String antibody_19 = li.get(0).getAntibody_19();
				String antibody_20 = li.get(0).getAntibody_20();
				String antibody_21 = li.get(0).getAntibody_21();
				String antibody_22 = li.get(0).getAntibody_22();
				String antibody_23 = li.get(0).getAntibody_23();
				String antibody_24 = li.get(0).getAntibody_24();
				String antibody_25 = li.get(0).getAntibody_25();
				String idate = li.get(0).getIdate();
				String iid = li.get(0).getIid();
				
				request.setAttribute("sms_no", sms_no);
				request.setAttribute("experiment", experiment);
				request.setAttribute("m_index_no", m_index_no);
				request.setAttribute("discription", discription);
				request.setAttribute("pathology_diagnosis", pathology_diagnosis);
				request.setAttribute("result", result);
				request.setAttribute("comment", comment);
				request.setAttribute("exp_date_1", exp_date_1);
				request.setAttribute("antibody_1", antibody_1);
				request.setAttribute("antibody_2", antibody_2);
				request.setAttribute("antibody_3", antibody_3);
				request.setAttribute("antibody_4", antibody_4);
				request.setAttribute("antibody_5", antibody_5);
				request.setAttribute("antibody_6", antibody_6);
				request.setAttribute("antibody_7", antibody_7);
				request.setAttribute("antibody_8", antibody_8);
				request.setAttribute("antibody_9", antibody_9);
				request.setAttribute("antibody_10", antibody_10);
				request.setAttribute("antibody_11", antibody_11);
				request.setAttribute("antibody_12", antibody_12);
				request.setAttribute("antibody_13", antibody_13);
				request.setAttribute("antibody_14", antibody_14);
				request.setAttribute("antibody_15", antibody_15);
				request.setAttribute("antibody_16", antibody_16);
				request.setAttribute("antibody_17", antibody_17);
				request.setAttribute("antibody_18", antibody_18);
				request.setAttribute("exp_date_2", exp_date_2);
				request.setAttribute("antibody_19", antibody_19);
				request.setAttribute("antibody_20", antibody_20);
				request.setAttribute("antibody_21", antibody_21);
				request.setAttribute("antibody_22", antibody_22);
				request.setAttribute("antibody_23", antibody_23);
				request.setAttribute("antibody_24", antibody_24);
				request.setAttribute("antibody_25", antibody_25);
				request.setAttribute("idate", idate);
				request.setAttribute("iid", iid);
				
			}
			
			/* IMAGE UPLOAD */
			/*
			request.setAttribute("menu", "lab_sanger"); // Menu�� �������ش�, ���� ���� �̸��� ���ӵȴ�.
			request.setAttribute("subtitle", "main"); // lab_sanger�� ��� ��� ������ �����ϹǷ�, ū �ǹ̸� ���� ����*/
			/* ==== */
			
			DbImageDAO dao_image=DbImageDAO.getDbImageDAO();
			List<DbImageDTO> li_image=new ArrayList<DbImageDTO>(); 
			li_image=(List)dao_image.selectDbImage(searchVal, "lab_stain");
			ArrayList<String> paths=new ArrayList<String>();
			ArrayList<String> names=new ArrayList<String>();
			ArrayList<String> doctors=new ArrayList<String>();
			
			
			//int max_img_idx=0; SQL ���� �������� ���� ���ʿ�
			// fullpath�� �ʿ��ϹǷ�, �� ���븸 ������ �� �ֵ��� �Ѵ�.
			if(li_image.size()!=0){
				for(int i=0;i<li_image.size();i++){
					//int temp_max=Integer.parseInt(li_image.get(i).getImg_idx());
					/*
					if(temp_max>=max_img_idx){
						max_img_idx=temp_max; // ��ġ�� �ʴ�(Identifier)�� �ε����� �����´�.
					}*/
					names.add(li_image.get(i).getImage_name());
					paths.add(li_image.get(i).getFull_path());
					doctors.add(li_image.get(i).getDoctor());
				}
			}
			
			request.setAttribute("db_image", paths); // �ش� chartnum �׸��� page name�� �ش��ϴ� ������ ������ �����Ͽ� ��������.
			request.setAttribute("names", names);
			request.setAttribute("page_name", "lab_stain");
			//request.setAttribute("next_idx", dao_image.getNextIndex(searchVal, page_name));
			request.setAttribute("next_idx", dao_image.getNextIndex());
			request.setAttribute("doctors", doctors);
			

			forward.setForward(true);
			forward.setNextURL("./SPN/LabStain.jsp");
			return forward;
		}
	}

}