package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.ElasticDAO;
import spn.dao.HistoryDAO;
import spn.dao.InfoDAO;
import spn.dao.MemberDAO;
import spn.dao.SampleDAO;
import spn.dto.ElasticDTO;
import spn.dto.HistoryDTO;
import spn.dto.InfoDTO;
import spn.dto.SampleCellDTO;

public class SampleCellUpdateAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		System.out.println("IN UPDATE ACTION CELL");
		HttpSession session = request.getSession();
		String searchVal = (String) session.getAttribute("searchVal");
		String id=(String)session.getAttribute("id");
		
		String menu="sample_cell";
		String prikey="ce_no";
		String privalue=null;

		InfoDAO infoDAO = InfoDAO.getInfoDAO();

		List<InfoDTO> infoList = new ArrayList<InfoDTO>();
		infoList = (List) infoDAO.selectInfo(searchVal);

		if (infoList.size() != 0) {
			String chart_no = infoList.get(0).getChart_no();
			request.setAttribute("chart_no", chart_no);
		}
		

		String ce_no = request.getParameter("ce_no");
		String chart_no = request.getParameter("chart_no");
		String col_date = request.getParameter("col_date");
		String cell_type_myoblast = request.getParameter("cell_type_myoblast");
		String cell_type_fibroblast = request.getParameter("cell_type_fibroblast");
		String cell_type_u_epithelial = request.getParameter("cell_type_u_epithelial");
		String cell_type_u_ipsc = request.getParameter("cell_type_u_ipsc");
		String cell_type_other = request.getParameter("cell_type_other");
		String cell_type_other_detail = request.getParameter("cell_type_other_detail");
		String cell_index = request.getParameter("cell_index");
		String cell_phase = request.getParameter("cell_phase");
		String cell_storage = request.getParameter("cell_storage");
		String cell_comment = request.getParameter("cell_comment");
		String req_date = request.getParameter("req_date");
		String req_location = request.getParameter("req_location");
		String req_order_name = request.getParameter("req_order_name");
		String req_vial = request.getParameter("req_vial");
		String req_comment = request.getParameter("req_comment");
		String iid = request.getParameter("iid");


		SampleCellDTO i = new SampleCellDTO();
		
		i.setCe_no(ce_no);
		i.setChart_no(chart_no);
		i.setCol_date(col_date);
		i.setCell_type_myoblast(cell_type_myoblast);
		i.setCell_type_fibroblast(cell_type_fibroblast);
		i.setCell_type_u_epithelial(cell_type_u_epithelial);
		i.setCell_type_u_ipsc(cell_type_u_ipsc);
		i.setCell_type_other(cell_type_other);
		i.setCell_type_other_detail(cell_type_other_detail);
		i.setCell_index(cell_index);
		i.setCell_phase(cell_phase);
		i.setCell_storage(cell_storage);
		i.setCell_comment(cell_comment);
		i.setReq_date(req_date);
		i.setReq_location(req_location);
		i.setReq_order_name(req_order_name);
		i.setReq_vial(req_vial);
		i.setReq_comment(req_comment);
		i.setIid(iid);

		SampleDAO dao = SampleDAO.getSampleDAO();
		int result = dao.updateCell(i);

		ActionForward forward = new ActionForward();

		if (result != 0) {
			// ��� ������
			privalue=String.valueOf(Integer.parseInt(dao.lastIncreaseNumber(menu, prikey))); // 방금 삽입한 값을 참조하기 위해, 다시 한번 조회하도록 한다.
			/* elastic 작업 */
			HashMap<String, String> map=new HashMap<String, String>();
			
			map.put("chart_no", chart_no);
			map.put("col_date", col_date);
			map.put("cell_type_myoblast", cell_type_myoblast);
			map.put("cell_type_fibroblast", cell_type_fibroblast);
			map.put("cell_type_u_epithelial", cell_type_u_epithelial);
			map.put("cell_type_u_ipsc", cell_type_u_ipsc);
			map.put("cell_type_other", cell_type_other);
			map.put("cell_type_other_detail", cell_type_other_detail);
			map.put("cell_index", cell_index);
			map.put("cell_storage", cell_storage);
			map.put("cell_phase", cell_phase);
			map.put("cell_comment", cell_comment);
			map.put("req_date", req_date);
			map.put("req_location", req_location);
			map.put("req_order_name", req_order_name);
			map.put("req_vial", req_vial);
			map.put("req_comment", req_comment);
			map.put("menu", menu);
			map.put("iid", iid);
			map.put(prikey, privalue);
			
			ElasticDTO eldto=new ElasticDTO();
			ElasticDAO eldao=ElasticDAO.getElasticDAO();
			
			HashMap<String, String> map_search=new HashMap<String, String>();
			map_search.put(prikey, privalue);
			String _id=eldao.getElasticId(map_search);
			
			eldto.setMenu(menu);
			eldto.set_id(_id);
			//eldto.setUrl("/sample_cell/sample_cell");
			eldao.updateElastic(eldto, map);
			
			/* History�� �߰��ϴ� �κ� */
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			HistoryDTO h=new HistoryDTO();
			
			InfoDAO i_dao=InfoDAO.getInfoDAO();
			List<InfoDTO> li=new ArrayList<InfoDTO>();
			
			li=(List)i_dao.selectInfo(chart_no); // InfoDAO���� �����ø��� ������? snupn, pa_bame, pa_sex
			h.setPa_name(li.get(0).getName());
			h.setPa_sex(li.get(0).getSex());
			h.setSnupn(li.get(0).getSnupn());
			
			MemberDAO m_dao=MemberDAO.getMemberDAO(); // MemberDao���� ������ �̸�
			h.setName(m_dao.findMember(id).getName());

			h.setChart_no(chart_no);			
			h.setIid(iid);
			h.setPage_name(menu);
			his_dao.insertHistory(h);
			
			
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=samplecellform");
		} else {
			// ��Ͻ��н�: ���������� ��������
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=samplecellform");

		}

		return forward;
	}

}
