package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.DbImageDAO;
import spn.dao.ElasticDAO;
import spn.dao.HistoryDAO;
import spn.dao.InfoDAO;
import spn.dao.LabDAO;
import spn.dao.MemberDAO;
import spn.dto.DbImageDTO;
import spn.dto.ElasticDTO;
import spn.dto.HistoryDTO;
import spn.dto.InfoDTO;
import spn.dto.LabStainDTO;


public class LabStainUpdateAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String searchVal = (String) session.getAttribute("searchVal");
		String id=(String)session.getAttribute("id");
		
		InfoDAO infoDAO = InfoDAO.getInfoDAO();

		List<InfoDTO> infoList = new ArrayList<InfoDTO>();
		infoList = (List) infoDAO.selectInfo(searchVal);
		
		if (infoList.size() != 0) {
			String chart_no = infoList.get(0).getChart_no();
			request.setAttribute("chart_no", chart_no);
		}
		
		String menu="lab_stain";
		String prikey="sms_no";
		String privalue=null;
		
		String sms_no=request.getParameter("sms_no");
		String chart_no=request.getParameter("chart_no");
		String experiment=request.getParameter("experiment");
		String m_index_no=request.getParameter("m_index_no");
		String discription=request.getParameter("discription");
		String pathology_diagnosis=request.getParameter("pathology_diagnosis");
		String result=request.getParameter("result");
		String comment=request.getParameter("comment");
		String exp_date_1=request.getParameter("exp_date_1");
		String antibody_1=request.getParameter("antibody_1"); // antibody_1_1,2,3�� �Ǿ�� �� ��(Dys 1,2,3)
		String antibody_2=request.getParameter("antibody_2");
		String antibody_3=request.getParameter("antibody_3");
		String antibody_4=request.getParameter("antibody_4");
		String antibody_5=request.getParameter("antibody_5");
		String antibody_6=request.getParameter("antibody_6");
		String antibody_7=request.getParameter("antibody_7");
		String antibody_8=request.getParameter("antibody_8");
		String antibody_9=request.getParameter("antibody_9");
		String antibody_10=request.getParameter("antibody_10");
		String antibody_11=request.getParameter("antibody_11");
		String antibody_12=request.getParameter("antibody_12");
		String antibody_13=request.getParameter("antibody_13");
		String antibody_14=request.getParameter("antibody_14");
		String antibody_15=request.getParameter("antibody_15");
		String antibody_16=request.getParameter("antibody_16");
		String antibody_17=request.getParameter("antibody_17");
		String antibody_18=request.getParameter("antibody_18");
		String exp_date_2=request.getParameter("exp_date_2");
		String antibody_19=request.getParameter("antibody_19");
		String antibody_20=request.getParameter("antibody_20");
		String antibody_21=request.getParameter("antibody_21");
		String antibody_22=request.getParameter("antibody_22");
		String antibody_23=request.getParameter("antibody_23");
		String antibody_24=request.getParameter("antibody_24");
		String antibody_25=request.getParameter("antibody_25");
		String iid=request.getParameter("iid");
		
		LabStainDTO i = new LabStainDTO();

		i.setSms_no(sms_no);
		i.setChart_no(chart_no);
		i.setExperiment(experiment);
		i.setM_index_no(m_index_no);
		i.setDiscription(discription);
		i.setPathology_diagnosis(pathology_diagnosis);
		i.setResult(result);
		i.setComment(comment);
		i.setExp_date_1(exp_date_1);
		i.setAntibody_1(antibody_1);
		i.setAntibody_2(antibody_2);
		i.setAntibody_3(antibody_3);
		i.setAntibody_4(antibody_4);
		i.setAntibody_5(antibody_5);
		i.setAntibody_6(antibody_6);
		i.setAntibody_7(antibody_7);
		i.setAntibody_8(antibody_8);
		i.setAntibody_9(antibody_9);
		i.setAntibody_10(antibody_10);
		i.setAntibody_11(antibody_11);
		i.setAntibody_12(antibody_12);
		i.setAntibody_13(antibody_13);
		i.setAntibody_14(antibody_14);
		i.setAntibody_15(antibody_15);
		i.setAntibody_16(antibody_16);
		i.setAntibody_17(antibody_17);
		i.setAntibody_18(antibody_18);
		i.setExp_date_2(exp_date_2);
		i.setAntibody_19(antibody_19);
		i.setAntibody_20(antibody_20);
		i.setAntibody_21(antibody_21);
		i.setAntibody_22(antibody_22);
		i.setAntibody_23(antibody_23);
		i.setAntibody_24(antibody_24);
		i.setAntibody_25(antibody_25);
		i.setIid(iid);

		
		LabDAO dao=LabDAO.getLabDAO();
		int result1=dao.updateStain(i);
		
		ActionForward forward= new ActionForward();
		
		if(result1 != 0) {
			//��� ������
			
			privalue=String.valueOf(Integer.parseInt(dao.lastIncreaseNumber(menu, prikey))); // 방금 삽입한 값을 참조하기 위해, 다시 한번 조회하도록 한다.
			
			/* elastic 작업 */
			HashMap<String, String> map=new HashMap<String, String>();
			
			map.put("chart_no", chart_no);
			map.put("experiment", experiment);
			map.put("m_index_no", m_index_no);
			map.put("discription", discription);
			map.put("pathology_diagnosis", pathology_diagnosis);
			map.put("result", result);
			map.put("comment", comment);
			map.put("exp_date_1", exp_date_1);
			map.put("exp_date_2", exp_date_2);
			map.put("antibody_1", antibody_1);
			map.put("antibody_2", antibody_2);
			map.put("antibody_3", antibody_3);
			map.put("antibody_4", antibody_4);
			map.put("antibody_5", antibody_5);
			map.put("antibody_6", antibody_6);
			map.put("antibody_7", antibody_7);
			map.put("antibody_8", antibody_8);
			map.put("antibody_9", antibody_9);
			map.put("antibody_10", antibody_10);
			map.put("antibody_11", antibody_11);
			map.put("antibody_12", antibody_12);
			map.put("antibody_13", antibody_13);
			map.put("antibody_14", antibody_14);
			map.put("antibody_15", antibody_15);
			map.put("antibody_16", antibody_16);
			map.put("antibody_17", antibody_17);
			map.put("antibody_18", antibody_18);
			map.put("antibody_19", antibody_19);
			map.put("antibody_20", antibody_20);
			map.put("antibody_21", antibody_21);
			map.put("antibody_22", antibody_22);
			map.put("antibody_23", antibody_23);
			map.put("antibody_24", antibody_24);
			map.put("antibody_25", antibody_25);
			map.put("menu", menu);
			map.put("iid", iid);
			map.put(prikey, privalue);
			
			ElasticDTO eldto=new ElasticDTO();
			ElasticDAO eldao=ElasticDAO.getElasticDAO();
			HashMap<String, String> map_search=new HashMap<String, String>();
			map_search.put(prikey, privalue);
			String _id=eldao.getElasticId(map_search);
			
			eldto.setMenu(menu);
			eldto.set_id(_id);
			//eldto.setUrl("/lab_stain/lab_stain");
			eldao.updateElastic(eldto, map);
			
			/* History�� �߰��ϴ� �κ� */
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			HistoryDTO h=new HistoryDTO();
			
			InfoDAO i_dao=InfoDAO.getInfoDAO();
			List<InfoDTO> li=new ArrayList<InfoDTO>();
			
			li=(List)i_dao.selectInfo(chart_no); // InfoDAO���� �����ø��� ������? snupn, pa_bame, pa_sex
			h.setPa_name(li.get(0).getName());
			h.setPa_sex(li.get(0).getSex());
			h.setSnupn(li.get(0).getSnupn());
			
			MemberDAO m_dao=MemberDAO.getMemberDAO(); // MemberDao���� ������ �̸�
			h.setName(m_dao.findMember(id).getName());

			h.setChart_no(chart_no);
			h.setIid(iid);
			h.setPage_name("lab_stain");
			
			his_dao.insertHistory(h);

			
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=labstainform");
		}else {
			//��Ͻ��н�: ���������� ��������
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=labstainform");

		}
		

		
		return forward;
	}

}
