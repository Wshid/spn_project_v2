package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.InfoDAO;
import spn.dao.SampleDAO;
import spn.dto.InfoDTO;
import spn.dto.SampleCellDTO;

public class SampleCellFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		} 
		else {
			
			String searchVal="";
			if(request.getParameter("searchVal")!=null) { //검색을 통한 접속이라면,
				searchVal=request.getParameter("searchVal");
				session.setAttribute("searchVal", searchVal);
				//searchVal=(String)session.getAttribute(request.getParameter("searchVal").toString());
			}
			else { // Info를 통한 접속일 
				searchVal = (String) session.getAttribute("searchVal");
			}

			InfoDAO infoDAO=InfoDAO.getInfoDAO();
			
			List<InfoDTO> infoList = new ArrayList<InfoDTO>();
			infoList = (List) infoDAO.selectInfo(searchVal);
			
			if (infoList.size() != 0) {
				String snupn = infoList.get(0).getSnupn();
				String chart_no= infoList.get(0).getChart_no();
				String name = infoList.get(0).getName();
				String sex= infoList.get(0).getSex();
				String birth = infoList.get(0).getBirth();
				String doctor = infoList.get(0).getDoctor();
				//String refered_dr= infoList.get(0).getRefered_dr();
				String other_site= infoList.get(0).getOther_site();
				String category = infoList.get(0).getCategory();
				//String final_diagnosis = infoList.get(0).getFinal_diagnosis();
				String clinical_diagnosis = infoList.get(0).getClinical_diagnosis();
				String symptom = infoList.get(0).getSymptom();
				//String remark = infoList.get(0).getRemark();
				
				request.setAttribute("snupn", snupn);
				request.setAttribute("chart_no", chart_no);
				request.setAttribute("name", name);
				request.setAttribute("sex", sex);
				request.setAttribute("birth", birth);
				request.setAttribute("doctor", doctor);
				//request.setAttribute("refered_dr", refered_dr);
				request.setAttribute("other_site", other_site);
				request.setAttribute("category", category);
				//request.setAttribute("final_diagnosis", final_diagnosis);
				request.setAttribute("clinical_diagnosis", clinical_diagnosis);
				request.setAttribute("symptom", symptom);
				//request.setAttribute("remark", remark);
			}
			
			SampleDAO dao=SampleDAO.getSampleDAO();
			
			List<SampleCellDTO> li = new ArrayList<SampleCellDTO>();
			li = (List) dao.selectCell(searchVal);
			
			if(li.size() != 0) {
				String ce_no = li.get(0).getCe_no();
				String col_date = li.get(0).getCol_date();
				//String bio_type = li.get(0).getBio_type();
				String cell_type_myoblast=li.get(0).getCell_type_myoblast();
				String cell_type_fibroblast=li.get(0).getCell_type_fibroblast();
				String cell_type_u_epithelial=li.get(0).getCell_type_u_epithelial();
				String cell_type_u_ipsc=li.get(0).getCell_type_u_ipsc();
				String cell_type_other=li.get(0).getCell_type_other();
				String cell_type_other_detail = li.get(0).getCell_type_other_detail();
				String cell_index = li.get(0).getCell_index();
				String cell_phase = li.get(0).getCell_phase();
				String cell_storage = li.get(0).getCell_storage();
				String cell_comment = li.get(0).getCell_comment();
				String req_date = li.get(0).getReq_date();
				String req_location = li.get(0).getReq_location();
				String req_order_name = li.get(0).getReq_order_name();
				String req_vial = li.get(0).getReq_vial();
				String req_comment = li.get(0).getReq_comment();
				String iid = li.get(0).getIid();
				String idate = li.get(0).getIdate();
				
				request.setAttribute("ce_no", ce_no);
				request.setAttribute("col_date", col_date);
				request.setAttribute("cell_type_myoblast", cell_type_myoblast);
				request.setAttribute("cell_type_fibroblast", cell_type_fibroblast);
				request.setAttribute("cell_type_u_epithelial", cell_type_u_epithelial);
				request.setAttribute("cell_type_u_ipsc", cell_type_u_ipsc);
				request.setAttribute("cell_type_other", cell_type_other);
				request.setAttribute("cell_type_other_detail", cell_type_other_detail);
				request.setAttribute("cell_index", cell_index);
				request.setAttribute("cell_phase", cell_phase);
				request.setAttribute("cell_storage", cell_storage);
				request.setAttribute("cell_comment", cell_comment);
				request.setAttribute("req_date", req_date);
				request.setAttribute("req_location", req_location);
				request.setAttribute("req_order_name", req_order_name);
				request.setAttribute("req_vial", req_vial);
				request.setAttribute("req_comment", req_comment);
				request.setAttribute("iid", iid);
				request.setAttribute("idate", idate);
				System.out.println("idate : "+idate);
				
				System.out.println("ce_no : "+ce_no);
			}
			
			
			

			forward.setForward(true);
			forward.setNextURL("./SPN/SampleCell.jsp");
			return forward;
		}
	}

}