package spn.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*import com.mysql.fabric.xmlrpc.base.Member;*/


import spn.dao.MemberDAO;

public class LoginAction implements Action{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		ActionForward forward = new ActionForward();
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		System.out.println("loginAction id : "+id);
		System.out.println("loginAction pw : "+pw);
		
		MemberDAO dao = MemberDAO.getMemberDAO();
		
		//System.out.println("LoginAction id : "+id);
		//System.out.println("LoginAction pw : "+pw);
		
		
		if (dao.isMember(id, pw)==0) {
			
			System.out.println("로그인실패");
			
/*			response.setContentType("text/html; charset=euc-kr");
			response.setContentType("text/html; charset=UTF-8");
			
			PrintWriter out = response.getWriter();
			
			out.println("<script>");

			out.println("alert('message');");


			out.println("</script>");
			
			
			out.println("<script>alert('판매자의 주소정보가 없습니다. 주소를 추가해 주세요');</script>");
			
			out.flush();*/
			
			forward.setNextURL("SPN?cmd=loginform");
			/*
			PrintWriter out = response.getWriter();
			out.println("<script>");
			out.println("alert('아이디나 패스워드를 확인해 주세요.');");
			out.println("</script>");
			
			out.flush();
			*/
			return forward;
		}
		else {
			System.out.println("로그인성공");
			HttpSession session = request.getSession();
			String perm=dao.findMember(id).getPermission();
			
			session.setAttribute("id", id);
			session.setAttribute("pw", pw);
			//session.setAttribute("permission", dao.findMember(id).getPermission());
			System.out.println("Login Action permission : "+perm);
			session.setAttribute("perm", perm);
			//forward.setNextURL("SPN?cmd=mainform");
			forward.setNextURL("SPN?cmd=mainform");
		}
		return forward;
	}

}
