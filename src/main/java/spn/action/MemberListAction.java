package spn.action;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.MemberDAO;
import spn.dto.MemberDTO;



public class MemberListAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("List");
		ActionForward forward=new ActionForward();
		HttpSession session = request.getSession();


		
		MemberDAO dao= MemberDAO.getMemberDAO();
		
		List<MemberDTO> list= dao.selectMember();
		
		request.setAttribute("list", list);
		
		forward.setForward(true);
		
		forward.setNextURL("./SPN/MemberList.jsp");
		return forward;
	}
	}


