package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.ElasticDAO;
import spn.dao.InfoDAO;
import spn.dto.ElasticDTO;
import spn.dto.InfoDTO;


public class InfoAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String snupn=request.getParameter("snupn");
		String chart_no=request.getParameter("chart_no");
		String name=request.getParameter("name");
		String sex=request.getParameter("sex");
		String birth=request.getParameter("birth");
		String doctor=request.getParameter("doctor");
		//String refered_dr=request.getParameter("refered_dr");
		String other_site=request.getParameter("other_site");
		String category=request.getParameter("category");
		//String final_diagnosis=request.getParameter("final_diagnosis");
		String clinical_diagnosis=request.getParameter("clinical_diagnosis");
		String symptom=request.getParameter("symptom");
		//String remark=request.getParameter("remark");
		String iid=request.getParameter("iid");
		
		InfoDAO dao=InfoDAO.getInfoDAO();
		String menu="info";
		String prikey="gi_no";
		String privalue=null;
		/*
		 * ���� ���� �߰�
		 */
		HttpSession session = request.getSession();
		List<InfoDTO> li = new ArrayList<InfoDTO>();
		//System.out.println("Infoaction chart_no"+chart_no);
		li = (List) dao.selectInfo(chart_no);
		
		ActionForward forward= new ActionForward();
		
		System.out.println("li.size : "+li.size());
		
		if(li.size()!=0) { // �̹� �ش� ��Ʈ�� ���� ��,
			forward.setForward(false);
			session.setAttribute("DuplicateChart", 1);
			forward.setNextURL("SPN?cmd=infoform");
			return forward;
			
		}else {
			session.setAttribute("DuplicateChart", 0);
		}
		
		
		InfoDTO i = new InfoDTO();
		i.setSnupn(snupn);
		i.setChart_no(chart_no);
		i.setName(name);
		i.setSex(sex);
		i.setBirth(birth);
		i.setDoctor(doctor);
		//i.setRefered_dr(refered_dr);
		i.setOther_site(other_site);
		i.setCategory(category);
		//i.setFinal_diagnosis(final_diagnosis);
		i.setClinical_diagnosis(clinical_diagnosis);
		i.setSymptom(symptom);
		//i.setRemark(remark);
		i.setIid(iid);
		

		
		dao.selectInfo(i.getChart_no());
		int result=dao.insertInfo(i);
		
		
		
		if(result != 0) {
			//��� ������
			//HttpSession session = request.getSession();
			
			privalue=String.valueOf(Integer.parseInt(dao.lastIncreaseNumber(menu, prikey))); // 방금 삽입한 값을 참조하기 위해, 다시 한번 조회하도록 한다.
			
			/* elastic 작업 */
			HashMap<String, String> map=new HashMap<String, String>();
			
			map.put("snupn", snupn);
			map.put("chart_no", chart_no);
			map.put("name", name);
			map.put("sex", sex);
			map.put("birth", birth);
			map.put("doctor", doctor);
			map.put("other_site", other_site);
			map.put("category", category);
			map.put("clinical_diagnosis", clinical_diagnosis);
			map.put("symptom", symptom);
			map.put("iid", iid);
			map.put("menu", menu);
			map.put(prikey, privalue);
			
			
			ElasticDTO eldto=new ElasticDTO();
			ElasticDAO eldao=ElasticDAO.getElasticDAO();
			//eldto.setUrl("/lab_cgh/lab_cgh");
			eldto.setMenu(menu);
			eldao.insertElastic(eldto, map);
			
			
			session.setAttribute("searchVal", chart_no);
			session.setAttribute("DuplicateChart", 0);
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=infoform");
		}else {
			//��Ͻ��н�: ���������� ��������
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=infoform");

		}
		
		return forward;
	}

}
