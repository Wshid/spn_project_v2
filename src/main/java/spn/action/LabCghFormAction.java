package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.DbImageDAO;
import spn.dao.InfoDAO;
import spn.dao.LabDAO;
import spn.dto.DbImageDTO;
import spn.dto.InfoDTO;
import spn.dto.LabCghDTO;

public class LabCghFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		
		if (session.getAttribute("id") == null) {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		} 
		else {
			
			String searchVal="";
			if(request.getParameter("searchVal")!=null) { //검색을 통한 접속이라면,
				searchVal=request.getParameter("searchVal");
				session.setAttribute("searchVal", searchVal);
				//searchVal=(String)session.getAttribute(request.getParameter("searchVal").toString());
			}
			else { // Info를 통한 접속일 
				searchVal = (String) session.getAttribute("searchVal");
			}
			//String searchVal = (String) session.getAttribute("searchVal");

			InfoDAO infoDAO=InfoDAO.getInfoDAO();
			
			List<InfoDTO> infoList = new ArrayList<InfoDTO>();
			infoList = (List) infoDAO.selectInfo(searchVal);
			
			if (infoList.size() != 0) {
				String snupn = infoList.get(0).getSnupn();
				String chart_no= infoList.get(0).getChart_no();
				String name = infoList.get(0).getName();
				String sex= infoList.get(0).getSex();
				String birth = infoList.get(0).getBirth();
				String doctor = infoList.get(0).getDoctor();
				//String refered_dr= infoList.get(0).getRefered_dr();
				String other_site= infoList.get(0).getOther_site();
				String category = infoList.get(0).getCategory();
				//String final_diagnosis = infoList.get(0).getFinal_diagnosis();
				String clinical_diagnosis = infoList.get(0).getClinical_diagnosis();
				String symptom = infoList.get(0).getSymptom();
				//String remark = infoList.get(0).getRemark();
				
				request.setAttribute("snupn", snupn);
				request.setAttribute("chart_no", chart_no);
				request.setAttribute("name", name);
				request.setAttribute("sex", sex);
				request.setAttribute("birth", birth);
				request.setAttribute("doctor", doctor);
				//request.setAttribute("refered_dr", refered_dr);
				request.setAttribute("category", category);
				request.setAttribute("other_site", other_site);
				//request.setAttribute("final_diagnosis", final_diagnosis);
				request.setAttribute("clinical_diagnosis", clinical_diagnosis);
				request.setAttribute("symptom", symptom);
				//request.setAttribute("remark", remark);
				
			}
			
			LabDAO dao=LabDAO.getLabDAO();
			
			List<LabCghDTO> li = new ArrayList<LabCghDTO>();
			li = (List) dao.selectCgh(searchVal);
			
			if (li.size() != 0) {
				String cgh_no = li.get(0).getCgh_no();
				String exp_date = li.get(0).getExp_date();
				String chip_no = li.get(0).getChip_no();
				String chromosome_study_yn = li.get(0).getChromosome_study_yn();
				String fish_confirmation_yn = li.get(0).getFish_confirmation_yn();
				String qpcr_yn = li.get(0).getQpcr_yn();
				String platform=li.get(0).getPlatform();
				String platform_other_detail=li.get(0).getPlatform_other_detail();
				String parents_test_f = li.get(0).getParents_test_f();
				String parents_test_m = li.get(0).getParents_test_m();
				String cnv_type = li.get(0).getCnv_type();
				String pathogenic_result_location = li.get(0).getPathogenic_result_location();
				String pathogenic_result_size = li.get(0).getPathogenic_result_size();
				String pathogenic_result_url = li.get(0).getPathogenic_result_url();
				String non_pathogenic_result_location = li.get(0).getNon_pathogenic_result_location();
				String non_pathogenic_result_size = li.get(0).getNon_pathogenic_result_size();
				String non_pathogenic_result_url = li.get(0).getNon_pathogenic_result_url();
				String comment = li.get(0).getComment();
				String idate = li.get(0).getIdate();
				String iid = li.get(0).getIid();
				
				request.setAttribute("cgh_no", cgh_no);
				request.setAttribute("exp_date", exp_date);
				request.setAttribute("chip_no", chip_no);
				request.setAttribute("chromosome_study_yn", chromosome_study_yn);
				request.setAttribute("fish_confirmation_yn", fish_confirmation_yn);
				request.setAttribute("qpcr_yn", qpcr_yn);
				request.setAttribute("platform", platform);
				request.setAttribute("platform_other_detail", platform_other_detail);
				request.setAttribute("parents_test_f", parents_test_f);
				request.setAttribute("parents_test_m", parents_test_m);
				request.setAttribute("cnv_type", cnv_type);
				request.setAttribute("pathogenic_result_location", pathogenic_result_location);
				request.setAttribute("pathogenic_result_size", pathogenic_result_size);
				request.setAttribute("pathogenic_result_url", pathogenic_result_url);
				request.setAttribute("non_pathogenic_result_location", non_pathogenic_result_location);
				request.setAttribute("non_pathogenic_result_size", non_pathogenic_result_size);
				request.setAttribute("non_pathogenic_result_url", non_pathogenic_result_url);
				request.setAttribute("comment", comment);
				request.setAttribute("idate", idate);
				request.setAttribute("iid", iid);
				
				System.out.println("LabCghFromACtion comment : "+comment);
				
				
			}
			
			DbImageDAO dao_image=DbImageDAO.getDbImageDAO();
			List<DbImageDTO> li_image=new ArrayList<DbImageDTO>(); 
			li_image=(List)dao_image.selectDbImage(searchVal, "lab_cgh");
			ArrayList<String> paths=new ArrayList<String>();
			ArrayList<String> names=new ArrayList<String>();
			ArrayList<String> doctors=new ArrayList<String>();
			
			
			//int max_img_idx=0; SQL ���� �������� ���� ���ʿ�
			// fullpath�� �ʿ��ϹǷ�, �� ���븸 ������ �� �ֵ��� �Ѵ�.
			if(li_image.size()!=0){
				for(int i=0;i<li_image.size();i++){
					//int temp_max=Integer.parseInt(li_image.get(i).getImg_idx());
					/*
					if(temp_max>=max_img_idx){
						max_img_idx=temp_max; // ��ġ�� �ʴ�(Identifier)�� �ε����� �����´�.
					}*/
					names.add(li_image.get(i).getImage_name());
					paths.add(li_image.get(i).getFull_path());
					doctors.add(li_image.get(i).getDoctor());
				}
			}
			
			request.setAttribute("db_image", paths); // �ش� chartnum �׸��� page name�� �ش��ϴ� ������ ������ �����Ͽ� ��������.
			request.setAttribute("names", names);
			request.setAttribute("page_name", "lab_cgh");
			//request.setAttribute("next_idx", dao_image.getNextIndex(searchVal, page_name));
			request.setAttribute("next_idx", dao_image.getNextIndex());
			request.setAttribute("doctors", doctors);

			forward.setForward(true);
			forward.setNextURL("./SPN/LabCgh.jsp");
			return forward;
		}
	}

}