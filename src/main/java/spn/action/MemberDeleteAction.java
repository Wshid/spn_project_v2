package spn.action;

import java.io.IOException;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.MemberDAO;
import spn.dto.MemberDTO;

public class MemberDeleteAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String mb_no = request.getParameter("mb_no");

		MemberDAO dao = MemberDAO.getMemberDAO();
		int result = dao.deleteMember(mb_no);

		ActionForward forward = new ActionForward();

		if (result != 0) {
			// 등록 성공시
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=memberlist");
		} else {
			// 등록실패시: 에러페이지 만들어야함
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=memberlist");
		}
		return forward;
	}
}
