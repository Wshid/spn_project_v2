package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.InfoDAO;
import spn.dto.InfoDTO;


public class InfoUpdateAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String gi_no=request.getParameter("gi_no");
		String snupn=request.getParameter("snupn");
		String chart_no=request.getParameter("chart_no");
		String name=request.getParameter("name");
		String sex=request.getParameter("sex");
		String birth=request.getParameter("birth");
		String doctor=request.getParameter("doctor");
		//String refered_dr=request.getParameter("refered_dr");
		String other_site=request.getParameter("other_site");
		String category=request.getParameter("category");
		//String final_diagnosis=request.getParameter("final_diagnosis");
		String clinical_diagnosis=request.getParameter("clinical_diagnosis");
		String symptom=request.getParameter("symptom");
		//String remark=request.getParameter("remark");
		String iid=request.getParameter("iid");
		
		// 예외로직 추가
		HttpSession session = request.getSession();
		ActionForward forward= new ActionForward();
		InfoDAO dao=InfoDAO.getInfoDAO();
		List<InfoDTO> li = new ArrayList<InfoDTO>();
		//System.out.println("Infoaction chart_no"+chart_no);
		li = (List) dao.selectInfo(chart_no);
		
		if(li.size()>=1) { // 이미 해당 차트가 있을 때,
			forward.setForward(false);
			session.setAttribute("DuplicateChart", 1);
			forward.setNextURL("SPN?cmd=infoform");
			return forward;
			
		} else {
			session.setAttribute("DuplicateChart", 0);
		}
		
		InfoDTO i = new InfoDTO();

		i.setGi_no(gi_no);
		i.setSnupn(snupn);
		i.setChart_no(chart_no);
		i.setName(name);
		i.setSex(sex);
		i.setBirth(birth);
		i.setDoctor(doctor);
		//i.setRefered_dr(refered_dr);
		i.setOther_site(other_site);
		i.setCategory(category);
		//i.setFinal_diagnosis(final_diagnosis);
		i.setClinical_diagnosis(clinical_diagnosis);
		i.setSymptom(symptom);
		//i.setRemark(remark);
		i.setIid(iid);
		
		//InfoDAO dao=InfoDAO.getInfoDAO();
		int result=dao.updateinfo(i);
		
		
		
		if(result != 0) {
			//등록 성공시
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=infoform");
		}else {
			//등록실패시: 에러페이지 만들어야함
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=infoform");

		}
		
		return forward;
	}

}
