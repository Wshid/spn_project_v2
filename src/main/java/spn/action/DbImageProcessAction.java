package spn.action;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//import com.mysql.jdbc.Util;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import spn.dao.DbImageDAO;
import spn.dao.InfoDAO;
import spn.dao.MemberDAO;
import spn.dto.DbImageDTO;
import spn.dto.InfoDTO;

import java.util.*;

public class DbImageProcessAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		ActionForward forward=new ActionForward();
		HttpSession session=request.getSession();
		
		
		String rootPath=request.getSession().getServletContext().getRealPath("/"); // request.getrealpath()의 경우 함수 중복으로 인한 deprecated
		String id=(String)session.getAttribute("id");
		String searchVal=(String)session.getAttribute("searchVal");
		String menu, subtitle;
		String originFolderPath=rootPath+"db_image/";
		
		//String path=request.getContextPath();
		
		String uploadFileName="", saveFolderPath="", ext="";
		File targetDir=null;
		
		String forwardUrl="";
		
		int size=1024*1024*15;
		
		try{ // 파일이 이미 존재한다는 예외처리도 해주면 좋을 듯 한데.. 아 복잡하네 이거
			MultipartRequest multi = new MultipartRequest(request, originFolderPath, size, "EUC-KR", new DefaultFileRenamePolicy());
			Enumeration files=multi.getFileNames();
			
			System.out.println("DbImage hasmoreElements");
			System.out.println(files.hasMoreElements()); // element가 없다고 뜸.. 왜지.....
			
			menu=multi.getParameter("menu"); //munu 자체는 multi 객체를 통해 받도록 한다.
			
			
			/* -============================ 수정 해야할 곳 */
			switch(menu){
			case "lab_sanger":
				forwardUrl="SPN?cmd=labsangerform";
				break;
			case "lab_ngs":
				forwardUrl="SPN?cmd=labngsform";
				break;
			case "lab_cgh":
				forwardUrl="SPN?cmd=labcghform";
				break;
			case "lab_stain":
				forwardUrl="SPN?cmd=labstainform";
				break;
			case "info":
				forwardUrl="SPN?cmd=infoform";
				break;
			default:
				System.out.println("NOT FORWARDED ON DbImageProcess");
				break;
			}
			
			/* ======================================== */
			
			System.out.println("forwardURL : "+forwardUrl);
			subtitle=multi.getParameter("subtitle");
			
			System.out.println("menu : "+menu);
			System.out.println("subtitle : "+subtitle);
			String newFileName=menu+"_"+searchVal+"_"+subtitle; // ex) lab_sanger_73240123_1
			System.out.println("newFileName : "+newFileName);
			System.out.println(originFolderPath);
			
			while(files.hasMoreElements()){ // 보통은 element로 하나만 받음, 두개 이상하시는분들도 있으...려나..
				System.out.println("Dbimage More Element While");
				String str=(String)files.nextElement();	
				uploadFileName=multi.getFilesystemName(str);
				
				int i=uploadFileName.lastIndexOf("."); // 확장자를 따오는 부분
				ext=uploadFileName.substring(i, uploadFileName.length());
				
				saveFolderPath=originFolderPath+"/"+menu;
				targetDir=new File(saveFolderPath);
				
				if(!targetDir.exists()){
					targetDir.mkdirs();
				}
				
				File oldFile=new File(originFolderPath+"/"+uploadFileName); // 업로드 되었던 파일 이름
				System.out.println("old File : "+oldFile);
				File newFile=new File(saveFolderPath+"/"+newFileName+ext); // 변경할 파일 이름
				
				oldFile.renameTo(newFile);
				oldFile.delete();
				
				// 여기서 SQL 추가 작업을 실시한다
				DbImageDTO dto=new DbImageDTO();
				DbImageDAO dao=DbImageDAO.getDbImageDAO();
				MemberDAO dao_mem=MemberDAO.getMemberDAO(); // doctor name을 가져오는 정보
				
				dto.setImage_name(newFileName+ext);
				dto.setChart_no(searchVal);
				dto.setDoctor(dao_mem.findMember(id).getName());
				dto.setFull_path(saveFolderPath+"/"+newFileName+ext);
				dto.setSubtitle(subtitle);
				dto.setPage_name(menu);
				System.out.println("Before Insert Image");
				int result=dao.insertDbImage(dto);
				System.out.println("After Insert Image");
				
				if(result!=0){
					System.out.println("DbImageProcess insert Success"); // 정상 종료
					forward.setForward(false);
					forward.setNextURL(forwardUrl); // 메뉴에 따라 분기를 해야하지만, 일단은 이런 방식으로 하기로
										
				}else{
					forward.setForward(false);
					forward.setNextURL(forwardUrl);
				}
			
			}
			return forward;
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
		//InfoDAO i_dao=InfoDAO.getInfoDAO();
		//List<InfoDTO> li=new ArrayList<InfoDTO>();
		
		//li=(List)i_dao.selectInfo(searchVal);
		
		
		//System.out.println("ImageProcessAction, path : "+path);
		/* db_image/[menu]/[menu]_[subtitle]_[chartnum] */
		

		// TODO Auto-generated method stub
		return forward;
	}

}
