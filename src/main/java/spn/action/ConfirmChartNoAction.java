package spn.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.fabric.xmlrpc.base.Member;

import spn.dao.InfoDAO;

public class ConfirmChartNoAction implements Action{

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		ActionForward forward = new ActionForward();
		String chart_id = request.getParameter("chart_id");
		
		InfoDAO dao = InfoDAO.getInfoDAO();
		
		try {
			if (dao.confirmChartNo(chart_id)==1) {
				System.out.println("��ϵ� chart no");
				HttpSession session = request.getSession();
				session.setAttribute("chart_id", chart_id);
				forward.setNextURL("SPN?cmd=confirmchartnoform");
			
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return forward;
	}

}
