package spn.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.ElasticDAO;

public class StatisticFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		} else {
			
			String searchVal="";
			if(request.getParameter("searchVal")!=null) { //검색을 통한 접속이라면,
				searchVal=request.getParameter("searchVal");
				session.setAttribute("searchVal", searchVal);
				//searchVal=(String)session.getAttribute(request.getParameter("searchVal").toString());
			}
			else { // Info를 통한 접속일 
				searchVal = (String) session.getAttribute("searchVal");
			}
			
			ElasticDAO dao=ElasticDAO.getElasticDAO();
			String menu_count=dao.getMenuCount();
			request.setAttribute("menu_count", menu_count);
			
			

			forward.setForward(true);
			forward.setNextURL("./SPN/Statistic.jsp");
			return forward;
		}
	}

}