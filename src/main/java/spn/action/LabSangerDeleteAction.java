package spn.action;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import spn.dao.ElasticDAO;
import spn.dao.HistoryDAO;
import spn.dao.InfoDAO;
import spn.dao.LabDAO;
import spn.dao.MemberDAO;
import spn.dto.ElasticDTO;
import spn.dto.HistoryDTO;
import spn.dto.InfoDTO;
import spn.dto.LabSangerDTO;
import spn.dto.MemberDTO;

public class LabSangerDeleteAction implements Action {
	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		String searchVal = (String) session.getAttribute("searchVal");
		String id=(String) session.getAttribute("id");

		InfoDAO infoDAO = InfoDAO.getInfoDAO();

		List<InfoDTO> infoList = new ArrayList<InfoDTO>();
		infoList = (List) infoDAO.selectInfo(searchVal);
		
		if (infoList.size() != 0) {
			String chart_no = infoList.get(0).getChart_no();
			request.setAttribute("chart_no", chart_no);
		}
		
		String menu="lab_sanger";
		
		String chart_no = request.getParameter("chart_no");
		/*
		String exp_date = request.getParameter("exp_date");
		String gene = request.getParameter("gene");
		String position = request.getParameter("position");
		String snp = request.getParameter("snp");
		String type = request.getParameter("type");
		String de_novo = request.getParameter("de_novo");
		String mutation = request.getParameter("mutation");
		String family_test_f = request.getParameter("family_test_f");
		String family_test_m = request.getParameter("family_test_m");
		String family_test_s = request.getParameter("family_test_s");
		String result = request.getParameter("result");
		String reported = request.getParameter("reported");
		String comment = request.getParameter("comment");
		*/
		String iid = request.getParameter("iid");
		String ss_no = request.getParameter("ss_no");
		

		
		
		/* mysql 제거 부분 */
		LabSangerDTO i = new LabSangerDTO();
		/*
		i.setChart_no(chart_no);
		i.setExp_date(exp_date);
		i.setGene(gene);
		i.setPosition(position);
		i.setSnp(snp);
		i.setType(type);
		i.setDe_novo(de_novo);
		
		i.setMutation(mutation);
		i.setFamily_test_f(family_test_f);
		i.setFamily_test_m(family_test_m);
		i.setFamily_test_s(family_test_s);
		i.setResult(result);
		i.setReported(reported);
		i.setComment(comment);
		i.setIid(iid);*/
		
		
		

		LabDAO dao = LabDAO.getLabDAO();
		
		//dao.lastIncreaseNumber(menu, "ss_no");
		System.out.println("LabSangerDeleteAction ss_no : "+ss_no);
		int result1 = dao.deleteSanger(ss_no);
		
		/* elastic 제거 부분 */
		/* elastic 작업 */
		HashMap<String, String> map=new HashMap<String, String>();
		/*
		map.put("chart_no", chart_no);
		map.put("exp_date", exp_date);
		map.put("gene", gene);
		map.put("type", type);
		map.put("de_novo", de_novo);
		map.put("position", position);
		*/
		map.put("ss_no", ss_no);  
		
		
		ElasticDAO eldao=ElasticDAO.getElasticDAO();
		eldao.deleteElastic(map, menu);


		
		ActionForward forward = new ActionForward();

		if (result1 != 0) {
			// ��� ������
			/* History�� �߰��ϴ� �κ� */
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			HistoryDTO h=new HistoryDTO();
			
			InfoDAO i_dao=InfoDAO.getInfoDAO();
			List<InfoDTO> li=new ArrayList<InfoDTO>();
			
			li=(List)i_dao.selectInfo(chart_no); // InfoDAO���� �����ø��� ������? snupn, pa_bame, pa_sex
			h.setPa_name(li.get(0).getName());
			h.setPa_sex(li.get(0).getSex());
			h.setSnupn(li.get(0).getSnupn());
			
			MemberDAO m_dao=MemberDAO.getMemberDAO(); // MemberDao���� ������ �̸�
			h.setName(m_dao.findMember(id).getName());

			h.setChart_no(chart_no);			
			h.setIid(iid);
			h.setPage_name(menu);
			
			his_dao.insertHistory(h);
			
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=labsangerform");
		} else {
			// ��Ͻ��н�: ���������� ��������
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=labsangerform");

		}
		
		/* IMAGE UPLOAD */
		/*
		int maxSize=1024*1024*10; // 10M ���Ϸ� ��´�.
		//String path=request.getContextPath();
		String rootPath=request.getSession().getServletContext().getRealPath("/"); // request.getrealpath()�� ��� �Լ� �ߺ����� ���� deprecated
		String savePath=rootPath+"db_image";
		String uploadFileName="";
		//String newFileName=menu+"_"+searchVal+"_"+subtitle; // ex) lab_sanger_73240123_1
		String ext=""; // Filename Extension
		
		try{
			MultipartRequest multi = new MultipartRequest(request, savePath, maxSize, "EUC-KR", new DefaultFileRenamePolicy()); 
			
			
			String menu=multi.getParameter("menu");
			String subtitle=request.getParameter("subtitle");
			String newFileName=menu+"_"+searchVal+"_"+subtitle; // ex) lab_sanger_73240123_1
			
			System.out.println("menu : "+menu);
			System.out.println("subtitle : "+subtitle);
			System.out.println("exp_date : "+exp_date);
			
			Enumeration files=multi.getFileNames();
			while(files.hasMoreElements()){
				String str=(String)files.nextElement();	
				uploadFileName=multi.getFilesystemName(str);
				
				File oldFile=new File(savePath+"/"+uploadFileName); // ���ε� �Ǿ��� ���� �̸�
				File newFile=new File(savePath+"/"+newFileName+ext); // ������ ���� �̸�
				
				oldFile.renameTo(newFile);
			
			}
		}catch(Exception e){
			e.printStackTrace();
			
		}*/
		
		
		
		return forward;
	}

}
