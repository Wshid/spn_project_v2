package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.HistoryDAO;
import spn.dto.HistoryDTO;

public class HistoryFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward=new ActionForward();
		HttpSession session = request.getSession();
		System.out.println(request.getAttribute("his_li"));
		System.out.println("HIS");
		if(session.getAttribute("id")==null){
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		}
		else{
			
			String searchVal = (String) session.getAttribute("searchVal");
			String page_name=request.getParameter("page_name");
			
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			
			List<HistoryDTO> his_li=new ArrayList<HistoryDTO>();
			System.out.println("page_name : "+page_name);
			his_li=(List)his_dao.selectHistory(searchVal, page_name);
			
			System.out.println("LABSANGERFORM : "+his_li.size()); // 애초에 0값이 나오네
			if(his_li.size()!=0){
				request.setAttribute("his_li", his_li);
			}
	
		forward.setForward(true);
		forward.setNextURL("./SPN/History.jsp");
		return forward;
		}
	}

}