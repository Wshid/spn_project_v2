package spn.action;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.interceptors.SessionAssociationInterceptor;

import spn.dao.ElasticDAO;
import spn.dao.HistoryDAO;
import spn.dao.InfoDAO;
import spn.dao.LabDAO;
import spn.dao.MemberDAO;
import spn.dto.LabCghDTO;
import spn.dto.ElasticDTO;
import spn.dto.HistoryDTO;
import spn.dto.InfoDTO;


public class LabCghAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		String searchVal = (String) session.getAttribute("searchVal");
		String id=(String) session.getAttribute("id");

		InfoDAO infoDAO = InfoDAO.getInfoDAO();
		List<InfoDTO> infoList = new ArrayList<InfoDTO>();
		infoList = (List) infoDAO.selectInfo(searchVal);
		
		if (infoList.size() != 0) {
			String chart_no = infoList.get(0).getChart_no();
			request.setAttribute("chart_no", chart_no);
		}
		
		String menu="lab_cgh";
		String prikey="cgh_no";
		String privalue=null;
		
		String chart_no=request.getParameter("chart_no");
		String exp_date=request.getParameter("exp_date");
		String chip_no=request.getParameter("chip_no");
		String chromosome_study_yn=request.getParameter("chromosome_study_yn");
		String fish_confirmation_yn=request.getParameter("fish_confirmation_yn");
		String qpcr_yn=request.getParameter("qpcr_yn");
		String platform=request.getParameter("platform");
		String platform_other_detail=request.getParameter("platform_other_detail");
		String parents_test_f=request.getParameter("parents_test_f");
		String parents_test_m=request.getParameter("parents_test_m");
		String cnv_type=request.getParameter("cnv_type");
		String pathogenic_result_location=request.getParameter("pathogenic_result_location");
		String pathogenic_result_size=request.getParameter("pathogenic_result_size");
		String pathogenic_result_url=request.getParameter("pathogenic_result_url");
		String non_pathogenic_result_location=request.getParameter("non_pathogenic_result_location");
		String non_pathogenic_result_size=request.getParameter("non_pathogenic_result_size");
		String non_pathogenic_result_url=request.getParameter("non_pathogenic_result_url");
		String comment=request.getParameter("comment");
		String iid=request.getParameter("iid");
		
		LabCghDTO i = new LabCghDTO();

		i.setChart_no(chart_no);
		i.setExp_date(exp_date);
		i.setChip_no(chip_no);
		i.setChromosome_study_yn(chromosome_study_yn);
		i.setFish_confirmation_yn(fish_confirmation_yn);
		i.setQpcr_yn(qpcr_yn);
		i.setPlatform(platform);
		i.setPlatform_other_detail(platform_other_detail);
		i.setParents_test_f(parents_test_f);
		i.setParents_test_m(parents_test_m);
		i.setCnv_type(cnv_type);
		i.setPathogenic_result_location(pathogenic_result_location);
		i.setPathogenic_result_size(pathogenic_result_size);
		i.setPathogenic_result_url(pathogenic_result_url);
		i.setPathogenic_result_location(non_pathogenic_result_location);
		i.setPathogenic_result_size(non_pathogenic_result_size);
		i.setNon_pathogenic_result_location(non_pathogenic_result_location);
		i.setNon_pathogenic_result_size(non_pathogenic_result_size);
		i.setNon_pathogenic_result_url(non_pathogenic_result_url);
		i.setComment(comment);
		i.setIid(iid);
		
		LabDAO dao=LabDAO.getLabDAO();
		int result=dao.insertCgh(i);		
		ActionForward forward= new ActionForward();
		
		if(result != 0) {
			//��� ������
			privalue=String.valueOf(Integer.parseInt(dao.lastIncreaseNumber(menu, prikey))); // 방금 삽입한 값을 참조하기 위해, 다시 한번 조회하도록 한다.
			
			/* elastic 작업 */
			HashMap<String, String> map=new HashMap<String, String>();
			
			map.put("chart_no", chart_no);
			map.put("exp_date", exp_date);
			map.put("chip_no", chip_no);
			map.put("chromosome_study_yn", chromosome_study_yn);
			map.put("qpcr_yn", qpcr_yn);
			map.put("platform", platform);
			map.put("platform_other_detail", platform_other_detail);
			map.put("parents_test_f", parents_test_f);
			map.put("parents_test_m", parents_test_m);
			map.put("cnv_type", cnv_type);
			map.put("pathogenic_result_url", pathogenic_result_url);
			map.put("pathogenic_result_size", pathogenic_result_size);
			map.put("pathogenic_result_location", pathogenic_result_location);
			map.put("non_pathogenic_result_url", non_pathogenic_result_url);
			map.put("non_pathogenic_result_size", non_pathogenic_result_size);
			map.put("non_pathogenic_result_location", non_pathogenic_result_location);
			map.put("comment", comment);
			map.put("menu", menu);
			map.put("iid", iid);
			map.put(prikey, privalue);
			
			ElasticDTO eldto=new ElasticDTO();
			ElasticDAO eldao=ElasticDAO.getElasticDAO();
			//eldto.setUrl("/lab_cgh/lab_cgh");
			eldto.setMenu(menu);
			eldao.insertElastic(eldto, map);
			
			/* History�� �߰��ϴ� �κ� */
			HistoryDAO his_dao=HistoryDAO.getHistoryDAO();
			HistoryDTO h=new HistoryDTO();
			
			InfoDAO i_dao=InfoDAO.getInfoDAO();
			List<InfoDTO> li=new ArrayList<InfoDTO>();
			
			li=(List)i_dao.selectInfo(chart_no); // InfoDAO���� �����ø��� ������? snupn, pa_bame, pa_sex
			h.setPa_name(li.get(0).getName());
			h.setPa_sex(li.get(0).getSex());
			h.setSnupn(li.get(0).getSnupn());
			
			MemberDAO m_dao=MemberDAO.getMemberDAO(); // MemberDao���� ������ �̸�
			h.setName(m_dao.findMember(id).getName());

			h.setChart_no(chart_no);			
			h.setIid(iid);
			h.setPage_name("lab_cgh");
			
			his_dao.insertHistory(h);
			
			
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=labcghform");
		}else {
			//��Ͻ��н�: ���������� ��������
			forward.setForward(false);
			forward.setNextURL("SPN?cmd=labcghform");

		}
		
		return forward;
	}

}
