package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.DbImageDAO;
import spn.dao.InfoDAO;
import spn.dao.LabDAO;
import spn.dto.DbImageDTO;
import spn.dto.InfoDTO;
import spn.dto.LabNgsDTO;

public class LabNgsFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		} else {
			String searchVal="";
			if(request.getParameter("searchVal")!=null) { //검색을 통한 접속이라면,
				searchVal=request.getParameter("searchVal");
				session.setAttribute("searchVal", searchVal);
				//searchVal=(String)session.getAttribute(request.getParameter("searchVal").toString());
			}
			else { // Info를 통한 접속일 
				searchVal = (String) session.getAttribute("searchVal");
			}

			InfoDAO infoDAO = InfoDAO.getInfoDAO();

			List<InfoDTO> infoList = new ArrayList<InfoDTO>();
			infoList = (List) infoDAO.selectInfo(searchVal);

			if (infoList.size() != 0) {
				String snupn = infoList.get(0).getSnupn();
				String chart_no = infoList.get(0).getChart_no();
				String name = infoList.get(0).getName();
				String sex = infoList.get(0).getSex();
				String birth = infoList.get(0).getBirth();
				String doctor = infoList.get(0).getDoctor();
				//String refered_dr = infoList.get(0).getRefered_dr();
				String other_site = infoList.get(0).getOther_site();
				String category = infoList.get(0).getCategory();
				//String final_diagnosis = infoList.get(0).getFinal_diagnosis();
				String clinical_diagnosis = infoList.get(0)
						.getClinical_diagnosis();
				String symptom = infoList.get(0).getSymptom();
				//String remark = infoList.get(0).getRemark();

				request.setAttribute("snupn", snupn);
				request.setAttribute("chart_no", chart_no);
				request.setAttribute("name", name);
				request.setAttribute("sex", sex);
				request.setAttribute("birth", birth);
				request.setAttribute("doctor", doctor);
				//request.setAttribute("refered_dr", refered_dr);
				request.setAttribute("other_site", other_site);
				request.setAttribute("category", category);
				//request.setAttribute("final_diagnosis", final_diagnosis);
				request.setAttribute("clinical_diagnosis", clinical_diagnosis);
				request.setAttribute("symptom", symptom);
				//request.setAttribute("remark", remark);
			}

			LabDAO dao = LabDAO.getLabDAO();

			List<LabNgsDTO> li = new ArrayList<LabNgsDTO>();
			li = (List) dao.selectNgs(searchVal);

			if (li.size() != 0) {
				String experiment = li.get(0).getExperiment();
				String exp_date = li.get(0).getExp_date();
				String case_no = li.get(0).getCase_no();
				String exp_place = li.get(0).getExp_place();
				String sample_yn_f = li.get(0).getSample_yn_f();
				String sample_yn_m = li.get(0).getSample_yn_m();
				String sample_yn_s = li.get(0).getSample_yn_s();
				String sample_yn_o = li.get(0).getSample_yn_o();
				String sample_yn_o_detail = li.get(0).getSample_yn_o_detail();
				String result = li.get(0).getResult();
				String result_pending_detail = li.get(0).getResult_pending_detail();
				String comment = li.get(0).getComment();
				String idate = li.get(0).getIdate();
				String ngs_no = li.get(0).getNgs_no();
				String iid = li.get(0).getIid();
			
				
				request.setAttribute("experiment", experiment);
				request.setAttribute("exp_date", exp_date);
				request.setAttribute("case_no", case_no);
				request.setAttribute("exp_place", exp_place);
				request.setAttribute("sample_yn_f", sample_yn_f);
				request.setAttribute("sample_yn_m", sample_yn_m);
				request.setAttribute("sample_yn_s", sample_yn_s);
				request.setAttribute("sample_yn_o", sample_yn_o);
				request.setAttribute("sample_yn_o_detail", sample_yn_o_detail);
				request.setAttribute("result", result);
				request.setAttribute("result_pending_detail", result_pending_detail);
				request.setAttribute("comment", comment);
				request.setAttribute("idate", idate);
				request.setAttribute("ngs_no", ngs_no);
				request.setAttribute("iid", iid);

			}
			
			/*
			DbImageDAO dao_image=DbImageDAO.getDbImageDAO();
			List<DbImageDTO> li_image=new ArrayList<DbImageDTO>(); 
			li_image=(List)dao_image.selectDbImage(searchVal, "lab_ngs");
			ArrayList<String> paths=new ArrayList<String>();
			ArrayList<String> names=new ArrayList<String>();
			ArrayList<String> doctors=new ArrayList<String>();
			//int max_img_idx=0;
			// fullpath�� �ʿ��ϹǷ�, �� ���븸 ������ �� �ֵ��� �Ѵ�.
			if(li_image.size()!=0){
				for(int i=0;i<li_image.size();i++){
					
					int temp_max=Integer.parseInt(li_image.get(i).getImg_idx());
					if(temp_max>=max_img_idx){
						max_img_idx=temp_max; // ��ġ�� �ʴ�(Identifier)�� �ε����� �����´�.
					}
					names.add(li_image.get(i).getImage_name());
					paths.add(li_image.get(i).getFull_path());
					doctors.add(li_image.get(i).getDoctor());
				}
			}
			
			request.setAttribute("db_image", paths); // �ش� chartnum �׸��� page name�� �ش��ϴ� ������ ������ �����Ͽ� ��������.
			request.setAttribute("names", names);
			request.setAttribute("page_name", "lab_ngs");
			request.setAttribute("next_idx", dao_image.getNextIndex());
			request.setAttribute("doctors", doctors);
*/
			forward.setForward(true);
			forward.setNextURL("./SPN/LabNgs.jsp");
			return forward;
		}
	}

}