package spn.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import spn.dao.InfoDAO;
import spn.dao.SampleDAO;
import spn.dto.InfoDTO;
import spn.dto.SampleDnaDTO;

public class SampleDnaFormAction implements Action {

	@Override
	public ActionForward execute(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ActionForward forward = new ActionForward();
		HttpSession session = request.getSession();
		if (session.getAttribute("id") == null) {
			forward.setForward(true);
			forward.setNextURL("SPN?cmd=loginform");
			return forward;
		} 
		else {
			
			String searchVal="";
			if(request.getParameter("searchVal")!=null) { //검색을 통한 접속이라면,
				searchVal=request.getParameter("searchVal");
				session.setAttribute("searchVal", searchVal);
				//searchVal=(String)session.getAttribute(request.getParameter("searchVal").toString());
			}
			else { // Info를 통한 접속일 
				searchVal = (String) session.getAttribute("searchVal");
			}

			InfoDAO infoDAO=InfoDAO.getInfoDAO();
			
			List<InfoDTO> infoList = new ArrayList<InfoDTO>();
			infoList = (List) infoDAO.selectInfo(searchVal);
			
			if (infoList.size() != 0) {
				String snupn = infoList.get(0).getSnupn();
				String chart_no= infoList.get(0).getChart_no();
				String name = infoList.get(0).getName();
				String sex= infoList.get(0).getSex();
				String birth = infoList.get(0).getBirth();
				String doctor = infoList.get(0).getDoctor();
				String other_site= infoList.get(0).getOther_site();
				//String refered_dr= infoList.get(0).getRefered_dr();
				String category = infoList.get(0).getCategory();
				//String final_diagnosis = infoList.get(0).getFinal_diagnosis();
				String clinical_diagnosis = infoList.get(0).getClinical_diagnosis();
				String symptom = infoList.get(0).getSymptom();
				//String remark = infoList.get(0).getRemark();
				
				request.setAttribute("snupn", snupn);
				request.setAttribute("chart_no", chart_no);
				request.setAttribute("name", name);
				request.setAttribute("sex", sex);
				request.setAttribute("birth", birth);
				request.setAttribute("doctor", doctor);
				//request.setAttribute("refered_dr", refered_dr);
				request.setAttribute("other_site", other_site);
				request.setAttribute("category", category);
				//request.setAttribute("final_diagnosis", final_diagnosis);
				request.setAttribute("clinical_diagnosis", clinical_diagnosis);
				request.setAttribute("symptom", symptom);
				//request.setAttribute("remark", remark);
				
			}
			
			SampleDAO dao = SampleDAO.getSampleDAO();
			
			List<SampleDnaDTO> li = new ArrayList<SampleDnaDTO>();
			li = (List) dao.selectDna(searchVal);
			
			List<String> dna_no=new ArrayList<String>();
			List<String> col_date=new ArrayList<String>();
			List<String> col_who=new ArrayList<String>();
			List<String> col_who_patient_detail=new ArrayList<String>();
			List<String> col_who_father_detail=new ArrayList<String>();
			List<String> col_who_mother_detail=new ArrayList<String>();
			List<String> col_who_sibling_detail=new ArrayList<String>();
			List<String> col_who_other_detail=new ArrayList<String>();
			List<String> col_type=new ArrayList<String>();
			List<String> col_type_other_detail=new ArrayList<String>();
			List<String> col_index=new ArrayList<String>();
			List<String> col_agreement=new ArrayList<String>();
			List<String> col_performed_by=new ArrayList<String>();
			List<String> col_concentration=new ArrayList<String>();
			List<String> col_260_280=new ArrayList<String>();
			List<String> col_260_230=new ArrayList<String>();
			List<String> col_location=new ArrayList<String>();
			List<String> col_comment=new ArrayList<String>();
			List<String> req_date=new ArrayList<String>();
			List<String> req_location=new ArrayList<String>();
			List<String> req_who=new ArrayList<String>();
			List<String> req_who_other_detail=new ArrayList<String>();
			List<String> req_distribution=new ArrayList<String>();
			List<String> req_order_name=new ArrayList<String>();
			List<String> req_type=new ArrayList<String>();
			List<String> req_type_other_detail=new ArrayList<String>();
			List<String> req_vial=new ArrayList<String>();
			List<String> req_comment=new ArrayList<String>();
			List<String> idate=new ArrayList<String>();
			List<String> iid=new ArrayList<String>();
			
			
			
			/*
			 * Tab ���·� ���� �Ǳ� ������, �޴� ���´� ��� ����Ʈ�� ��ȯ�ȴ�
			 * 
			 */
			if (li.size() != 0) {
				for(SampleDnaDTO sd : li) {
					dna_no.add(sd.getDna_no()); // �ش� �÷��� list�� ��� ���� �� ������?
					col_date.add(sd.getCol_date());
					col_who.add(sd.getCol_who());
					col_who_patient_detail.add(sd.getCol_who_patient_detail());
					System.out.println("PATIENT DETAIL : "+sd.getCol_who_patient_detail());
					col_who_father_detail.add(sd.getCol_who_father_detail());
					col_who_mother_detail.add(sd.getCol_who_mother_detail());
					col_who_sibling_detail.add(sd.getCol_who_sibling_detail());
					col_who_other_detail.add(sd.getCol_who_other_detail());
					col_type.add(sd.getCol_type());
					col_type_other_detail.add(sd.getCol_type_other_detail());
					col_index.add(sd.getCol_index());
					col_agreement.add(sd.getCol_agreement());
					col_performed_by.add(sd.getCol_performed_by());
					col_concentration.add(sd.getCol_concentration());
					col_260_280.add(sd.getCol_260_280());
					col_260_230.add(sd.getCol_260_230());
					col_location.add(sd.getCol_location());
					col_comment.add(sd.getCol_comment());
					req_date.add(sd.getReq_date());
					req_location.add(sd.getReq_location());
					req_who.add(sd.getReq_who());
					req_who_other_detail.add(sd.getReq_who_other_detail());
					req_distribution.add(sd.getReq_distribution());
					req_order_name.add(sd.getReq_order_name());
					req_type.add(sd.getReq_type());
					req_type_other_detail.add(sd.getReq_type_other_detail());
					req_vial.add(sd.getReq_vial());
					req_comment.add(sd.getReq_comment());
					idate.add(sd.getIdate());
					iid.add(sd.getIid());		
					//System.out.println("RUNRUN SAMPLEDNAFORM");
				}
			}
				/*
				String dna_no = li.get(0).getDna_no(); // �ش� �÷��� list�� ��� ���� �� ������?
				
				String col_date = li.get(0).getCol_date();
				String col_who = li.get(0).getCol_who();
				String col_who_other_detail = li.get(0).getCol_who_other_detail();
				String col_type = li.get(0).getCol_type();
				String col_type_other_detail = li.get(0).getCol_type_other_detail();
				String col_index = li.get(0).getCol_index();
				String col_concentration = li.get(0).getCol_concentration();
				String col_260_280 = li.get(0).getCol_260_280();
				String col_260_230 = li.get(0).getCol_260_230();
				String col_location = li.get(0).getCol_location();
				String col_comment = li.get(0).getCol_comment();
				String req_date = li.get(0).getReq_date();
				String req_who = li.get(0).getReq_who();
				String req_who_other_detail = li.get(0).getReq_who_other_detail();
				String req_distribution = li.get(0).getReq_distribution();
				String req_order_name = li.get(0).getReq_order_name();
				String req_type = li.get(0).getReq_type();
				String req_type_other_detail = li.get(0).getReq_type_other_detail();
				String req_vial = li.get(0).getReq_vial();
				String req_comment = li.get(0).getReq_comment();
				String idate = li.get(0).getIdate();
				String iid = li.get(0).getIid();
				*/
				request.setAttribute("dna_no", dna_no);
				request.setAttribute("col_date", col_date);
				request.setAttribute("col_who", col_who);
				request.setAttribute("col_who_patient_detail", col_who_patient_detail);
				request.setAttribute("col_who_father_detail", col_who_father_detail);
				request.setAttribute("col_who_mother_detail", col_who_mother_detail);
				request.setAttribute("col_who_sibling_detail", col_who_sibling_detail);
				request.setAttribute("col_who_other_detail", col_who_other_detail);
				request.setAttribute("col_type", col_type);
				request.setAttribute("col_type_other_detail", col_type_other_detail);
				request.setAttribute("col_index", col_index);
				request.setAttribute("col_agreement", col_agreement);
				request.setAttribute("col_performed_by", col_performed_by);
				request.setAttribute("col_concentration", col_concentration);
				request.setAttribute("col_260_280", col_260_280);
				request.setAttribute("col_260_230", col_260_230);
				request.setAttribute("col_location", col_location);
				request.setAttribute("col_comment", col_comment);
				request.setAttribute("req_date", req_date);
				request.setAttribute("req_location", req_location);
				request.setAttribute("req_who", req_who);
				request.setAttribute("req_who_other_detail", req_who_other_detail);
				request.setAttribute("req_distribution", req_distribution);
				request.setAttribute("req_order_name", req_order_name);
				request.setAttribute("req_type", req_type);
				request.setAttribute("req_type_other_detail", req_type_other_detail);
				request.setAttribute("req_vial", req_vial);
				request.setAttribute("req_comment", req_comment);
				request.setAttribute("idate", idate);
				request.setAttribute("iid", iid);
				
				/*
				List<String> test= new ArrayList<String>();
				test.add("1st String");
				test.add("2nd String");
				test.add("3rd String");
				request.setAttribute("test", test);*/
			
			

			forward.setForward(true);
			forward.setNextURL("./SPN/SampleDna.jsp");
			return forward;
		}
	}

}