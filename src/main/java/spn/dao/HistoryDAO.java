package spn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import spn.dto.HistoryDTO;
import spn.util.DBManager;

public class HistoryDAO {
	static HistoryDAO dao=new HistoryDAO();
	
	private HistoryDAO(){
		System.out.println("HistoryDAO");
	}
	
	public static HistoryDAO getHistoryDAO(){
		return dao;
	}
	
	public int insertHistory(HistoryDTO i){
		//String sql="insert into spn.history(iid, name, chart_no, page_name, idate)"+
		//		" values(?,?,?,now())";
		String sql="insert into spn.history(iid, name, chart_no, snupn, pa_name, pa_sex, page_name, idate)"+
				" values(?,?,?,?,?,?,?,now())";
		Connection conn=DBManager.getConnection();
		PreparedStatement pstmt= null;
		int result=0;
		System.out.println(result);
		
		try{
			pstmt=conn.prepareStatement(sql);
			
			pstmt.setString(1, i.getIid());
			pstmt.setString(2, i.getName());
			pstmt.setString(3, i.getChart_no());
			pstmt.setString(4, i.getSnupn());
			pstmt.setString(5, i.getPa_name());
			pstmt.setString(6, i.getPa_sex());
			pstmt.setString(7, i.getPage_name());
			
			result=pstmt.executeUpdate();
			
		} catch(SQLException e){
			e.printStackTrace();
		} finally{
			DBManager.close(conn, pstmt);
		}
		return result;
	}
	
	public List<HistoryDTO> selectHistory(String searchVal, String page_name){
		String sql="SELECT * FROM spn.history where chart_no='" + searchVal + "' AND page_name='"+page_name+"'";
		
		Connection con=DBManager.getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		List<HistoryDTO> list=new ArrayList<HistoryDTO>();
		
		try{
			pstmt=con.prepareStatement(sql);
			rs=pstmt.executeQuery();
			
			while(rs.next()){
				HistoryDTO i=new HistoryDTO();
				i.setHis_idx(rs.getString("his_idx"));
				i.setIid(rs.getString("iid"));
				i.setName(rs.getString("name"));
				i.setChart_no(rs.getString("chart_no"));
				i.setSnupn(rs.getString("snupn"));
				i.setPa_name(rs.getString("pa_name"));
				i.setPa_sex(rs.getString("pa_sex"));
				i.setIdate(rs.getString("idate"));
				
				list.add(i);
			}
		} catch(SQLException e){
			e.printStackTrace();
		} finally{
			DBManager.close(con,  pstmt);
		}
		return list;
	}
}
