package spn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import spn.dto.LabCghDTO;
import spn.dto.LabNgsDTO;
import spn.dto.LabSangerDTO;
import spn.dto.LabStainDTO;
import spn.util.DBManager;

public class LabDAO {

	static LabDAO dao = new LabDAO();

	private LabDAO() {
		System.out.println("LabDAO");
	}

	public static LabDAO getLabDAO() {
		return dao;
	}
	
	public String lastIncreaseNumber(String table_name, String prikey) {
		String sql = "SELECT "+prikey+" FROM spn."+table_name+" ORDER BY "+prikey+" desc LIMIT 1";
		// pstmt 사용이 제대로 되지 않으므로, 일반 문자열로 변환 
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<LabSangerDTO> list = new ArrayList<LabSangerDTO>();
		
		String ret="";

		try {
			pstmt = con.prepareStatement(sql);
			//pstmt.setString(1, prikey);
			//pstmt.setString(2, prikey);
			rs = pstmt.executeQuery();

			
			while (rs.next()) {
				ret=rs.getString(prikey);
				break;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return ret;
	}

	/* Lab Test 1 - Sanger Sequencing */
	public int insertSanger(LabSangerDTO i) {

		String sql = "insert into spn.lab_sanger(chart_no, exp_date, gene, position, snp, type, de_novo, mutation, "
				+ "family_test_F, family_test_M, family_test_S,result, reported, iid, comment, idate) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getExp_date());
			pstmt.setString(3, i.getGene());
			pstmt.setString(4, i.getPosition());
			pstmt.setString(5, i.getSnp());
			pstmt.setString(6, i.getType());
			pstmt.setString(7, i.getDe_novo());
			pstmt.setString(8, i.getMutation());
			pstmt.setString(9, i.getFamily_test_f());
			pstmt.setString(10, i.getFamily_test_m());
			pstmt.setString(11, i.getFamily_test_s());
			pstmt.setString(12, i.getResult());
			pstmt.setString(13, i.getReported());
			pstmt.setString(14, i.getIid());
			pstmt.setString(15, i.getComment());

			result = pstmt.executeUpdate();

			System.out.println("Lab Sanger Sequencing insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}

	public List<LabSangerDTO> selectSanger(String searchVal) {

		String sql = "select * from spn.lab_sanger where chart_no ='"
				+ searchVal + "'order by ss_no"; // desc 제거 

		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<LabSangerDTO> list = new ArrayList<LabSangerDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				LabSangerDTO i = new LabSangerDTO();
				i.setExp_date(rs.getString("exp_date"));
				i.setGene(rs.getString("gene"));
				i.setPosition(rs.getString("position"));
				i.setSnp(rs.getString("snp"));
				i.setType(rs.getString("type"));
				i.setDe_novo(rs.getString("de_novo"));
				i.setMutation(rs.getString("mutation"));
				i.setFamily_test_f(rs.getString("family_test_f"));
				i.setFamily_test_m(rs.getString("family_test_m"));
				i.setFamily_test_s(rs.getString("family_test_s"));
				i.setResult(rs.getString("result"));
				i.setReported(rs.getString("reported"));
				i.setComment(rs.getString("comment"));
				i.setIdate(rs.getString("idate"));
				i.setIid(rs.getString("iid"));
				i.setSs_no(rs.getString("ss_no"));

				list.add(i);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}

	public int updateSanger(LabSangerDTO i) {

		String ss_no = i.getSs_no();

		String sql = "update spn.lab_sanger set chart_no=?, exp_date=?, gene=?, position=?, snp=?, type=?,"
				+ "de_novo=?, mutation=?, family_test_f=?, family_test_m=?, family_test_s=?, "
				+ "result=?, reported=?, iid=?, comment=?, idate=now() where ss_no='" + ss_no + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getExp_date());
			pstmt.setString(3, i.getGene());
			pstmt.setString(4, i.getPosition());
			pstmt.setString(5, i.getSnp());
			pstmt.setString(6, i.getType());
			pstmt.setString(7, i.getDe_novo());
			pstmt.setString(8, i.getMutation());
			pstmt.setString(9, i.getFamily_test_f());
			pstmt.setString(10, i.getFamily_test_m());
			pstmt.setString(11, i.getFamily_test_s());
			pstmt.setString(12, i.getResult());
			pstmt.setString(13, i.getReported());
			pstmt.setString(14, i.getIid());
			pstmt.setString(15, i.getComment());

			result = pstmt.executeUpdate();
			
			System.out.println(result);
			System.out.println("Lab Sanger Sequencing Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	
	public int deleteSanger(String ss_no) {


		String sql = "DELETE FROM lab_sanger WHERE ss_no='"+ss_no + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		

		try {

			pstmt = con.prepareStatement(sql);

			result = pstmt.executeUpdate();
			
			System.out.println(result);
			System.out.println("Lab Sanger Sequencing Delete success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	
	

	/* Lab Test 2 - Next Generation Sequencing */
	public int insertNgs(LabNgsDTO i) {

		String sql = "insert into spn.lab_ngs(chart_no,experiment,exp_date,case_no,exp_place,"
				+ "sample_yn_f,sample_yn_m,sample_yn_s, sample_yn_o, sample_yn_o_detail, result, result_pending_detail,comment,iid,idate) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		//System.out.println(result);

		try {
			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getExperiment());
			pstmt.setString(3, i.getExp_date());
			pstmt.setString(4, i.getCase_no());
			pstmt.setString(5, i.getExp_place());
			pstmt.setString(6, i.getSample_yn_f());
			pstmt.setString(7, i.getSample_yn_m());
			pstmt.setString(8, i.getSample_yn_s());
			pstmt.setString(9, i.getSample_yn_o());
			pstmt.setString(10, i.getSample_yn_o_detail());
			pstmt.setString(11, i.getResult());
			pstmt.setString(12, i.getResult_pending_detail());
			pstmt.setString(13, i.getComment());
			pstmt.setString(14, i.getIid());
			
			System.out.println(i.getChart_no());
			System.out.println(i.getExperiment());
			System.out.println(i.getExp_date());
			System.out.println(i.getCase_no());
			System.out.println(i.getExp_place());
			System.out.println(i.getSample_yn_f());
			System.out.println(i.getSample_yn_m());
			System.out.println(i.getSample_yn_s());
			System.out.println(i.getSample_yn_o());
			System.out.println(i.getSample_yn_o_detail());
			System.out.println(i.getResult());
			System.out.println(i.getResult_pending_detail());
			//System.out.println(i.getCommnet);

			result = pstmt.executeUpdate();

			System.out.println("Lab Next Generation Sequencing insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;

	}

	public List<LabNgsDTO> selectNgs(String searchVal) {

		String sql = "select * from spn.lab_ngs where chart_no ='" + searchVal
				+ "'order by ngs_no desc";

		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<LabNgsDTO> list = new ArrayList<LabNgsDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				LabNgsDTO i = new LabNgsDTO();
				
				i.setExperiment(rs.getString("experiment"));
				i.setExp_date(rs.getString("exp_date"));
				i.setCase_no(rs.getString("case_no"));
				i.setExp_place(rs.getString("exp_place"));
				i.setSample_yn_f(rs.getString("sample_yn_f"));
				i.setSample_yn_m(rs.getString("sample_yn_m"));
				i.setSample_yn_s(rs.getString("sample_yn_s"));
				i.setSample_yn_o(rs.getString("sample_yn_o"));
				i.setSample_yn_o_detail(rs.getString("sample_yn_o_detail"));
				i.setResult(rs.getString("Result"));
				i.setResult_pending_detail(rs.getString("Result_pending_detail"));
				i.setComment(rs.getString("comment"));
				i.setIdate(rs.getString("idate"));
				i.setNgs_no(rs.getString("ngs_no"));
				i.setIid(rs.getString("iid"));
				i.setIdate(rs.getString("idate"));

				list.add(i);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}

	public int updateNgs(LabNgsDTO i) {

		String ngs_no = i.getNgs_no();

		String sql = "update spn.lab_ngs set chart_no=?,experiment=?,exp_date=?,case_no=?,"
				+ "exp_place=?,sample_yn_f=?,sample_yn_m=?,sample_yn_s=?, sample_yn_o=?, sample_yn_o_detail=?,"
				+ "result=?, result_pending_detail=?, comment=?,iid=?,idate=now()"
				+ "where ngs_no='"+ngs_no+"'";

		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getExperiment());
			pstmt.setString(3, i.getExp_date());
			pstmt.setString(4, i.getCase_no());
			pstmt.setString(5, i.getExp_place());
			pstmt.setString(6, i.getSample_yn_f());
			pstmt.setString(7, i.getSample_yn_m());
			pstmt.setString(8, i.getSample_yn_s());
			pstmt.setString(9, i.getSample_yn_o());
			pstmt.setString(10, i.getSample_yn_o_detail());
			pstmt.setString(11, i.getResult());
			pstmt.setString(12, i.getResult_pending_detail());
			pstmt.setString(13, i.getComment());
			pstmt.setString(14, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Lab NGS Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}

	/* Lab Test 3 - CGH array */
	public int insertCgh(LabCghDTO i) {

		String sql = "insert into spn.lab_cgh(chart_no,exp_date,chip_no,chromosome_study_yn,fish_confirmation_yn,"
				+ "qpcr_yn, platform, platform_other_detail, parents_test_f, parents_test_m,cnv_type,pathogenic_result_location,pathogenic_result_size, pathogenic_result_url,"
				+ "non_pathogenic_result_location,non_pathogenic_result_size, non_pathogenic_result_url,comment,iid,idate)"
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getExp_date());
			pstmt.setString(3, i.getChip_no());
			pstmt.setString(4, i.getChromosome_study_yn());
			pstmt.setString(5, i.getFish_confirmation_yn());
			pstmt.setString(6, i.getQpcr_yn());
			pstmt.setString(7, i.getPlatform());
			pstmt.setString(8, i.getPlatform_other_detail());
			pstmt.setString(9, i.getParents_test_f());
			pstmt.setString(10, i.getParents_test_m());
			pstmt.setString(11, i.getCnv_type());
			pstmt.setString(12, i.getPathogenic_result_location());
			pstmt.setString(13, i.getPathogenic_result_size());
			pstmt.setString(14, i.getPathogenic_result_url());
			pstmt.setString(15, i.getNon_pathogenic_result_location());
			pstmt.setString(16, i.getNon_pathogenic_result_size());
			pstmt.setString(17, i.getNon_pathogenic_result_url());
			pstmt.setString(18, i.getComment());
			pstmt.setString(19, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("CGH Array insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;

	}

	public List<LabCghDTO> selectCgh(String searchVal) {

		String sql = "select * from spn.lab_cgh where chart_no ='" + searchVal
				+ "'order by cgh_no desc";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<LabCghDTO> list = new ArrayList<LabCghDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				LabCghDTO i = new LabCghDTO();
				i.setCgh_no(rs.getString("cgh_no"));
				i.setExp_date(rs.getString("exp_date"));
				i.setChip_no(rs.getString("chip_no"));
				i.setChromosome_study_yn(rs.getString("chromosome_study_yn"));
				i.setFish_confirmation_yn(rs.getString("fish_confirmation_yn"));
				i.setQpcr_yn(rs.getString("qpcr_yn"));
				i.setPlatform(rs.getString("platform"));
				i.setPlatform_other_detail(rs.getString("platform_other_detail"));
				i.setParents_test_f(rs.getString("parents_test_f"));
				i.setParents_test_m(rs.getString("parents_test_m"));
				i.setCnv_type(rs.getString("cnv_type"));
				i.setPathogenic_result_location(rs
						.getString("pathogenic_result_location"));
				i.setPathogenic_result_size(rs
						.getString("pathogenic_result_size"));
				i.setPathogenic_result_url(rs
						.getString("pathogenic_result_url"));
				i.setNon_pathogenic_result_location(rs
						.getString("non_pathogenic_result_location"));
				i.setNon_pathogenic_result_size(rs
						.getString("non_pathogenic_result_size"));
				i.setNon_pathogenic_result_url(rs
						.getString("non_pathogenic_result_url"));
				i.setComment(rs.getString("comment"));
				i.setIdate(rs.getString("idate"));
				i.setIid(rs.getString("iid"));

				list.add(i);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}
	
	public int updateCgh(LabCghDTO i) {

		String cgh_no = i.getCgh_no();

		String sql = "update spn.lab_cgh set chart_no=?,exp_date=?,chip_no=?,chromosome_study_yn=?,fish_confirmation_yn=?,"
				+ "qpcr_yn=?, platform=?, platform_other_detail=?, parents_test_f=?, parents_test_m=?, cnv_type=?, pathogenic_result_location=?,"
				+ "pathogenic_result_size=?, pathogenic_result_url=?, non_pathogenic_result_location=?,non_pathogenic_result_size=?,"
				+ "non_pathogenic_result_url=?,comment=?,iid=?,idate=now() where cgh_no='"+cgh_no+"'";

		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);
		System.out.println(sql);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getExp_date());
			pstmt.setString(3, i.getChip_no());
			pstmt.setString(4, i.getChromosome_study_yn());
			pstmt.setString(5, i.getFish_confirmation_yn());
			pstmt.setString(6, i.getQpcr_yn());
			pstmt.setString(7, i.getPlatform());
			pstmt.setString(8, i.getPlatform_other_detail());
			pstmt.setString(9, i.getParents_test_f());
			pstmt.setString(10, i.getParents_test_m());
			pstmt.setString(11, i.getCnv_type());
			pstmt.setString(12, i.getPathogenic_result_location());
			pstmt.setString(13, i.getPathogenic_result_size());
			pstmt.setString(14, i.getPathogenic_result_url());
			pstmt.setString(15, i.getNon_pathogenic_result_location());
			pstmt.setString(16, i.getNon_pathogenic_result_size());
			pstmt.setString(17, i.getNon_pathogenic_result_url());
			pstmt.setString(18, i.getComment());
			pstmt.setString(19, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Lab CGH Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	

	/* Lab Test 4 - Stain Muscle & Skin */
	public int insertStain(LabStainDTO i) {

		String sql = "insert into spn.lab_stain(chart_no,experiment,m_index_no,discription,pathology_diagnosis,result,comment,"
				+ "exp_date_1,antibody_1,antibody_2,antibody_3,antibody_4,antibody_5,antibody_6,antibody_7,antibody_8,antibody_9,"
				+ "antibody_10,antibody_11,antibody_12,antibody_13,antibody_14,antibody_15,antibody_16,antibody_17,antibody_18,"
				+ "exp_date_2,antibody_19,antibody_20,antibody_21,antibody_22,antibody_23,antibody_24,antibody_25,iid,idate) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getExperiment());
			pstmt.setString(3, i.getM_index_no());
			pstmt.setString(4, i.getDiscription());
			pstmt.setString(5, i.getPathology_diagnosis());
			pstmt.setString(6, i.getResult());
			pstmt.setString(7, i.getComment());
			pstmt.setString(8, i.getExp_date_1());
			pstmt.setString(9, i.getAntibody_1());
			pstmt.setString(10, i.getAntibody_2());
			pstmt.setString(11, i.getAntibody_3());
			pstmt.setString(12, i.getAntibody_4());
			pstmt.setString(13, i.getAntibody_5());
			pstmt.setString(14, i.getAntibody_6());
			pstmt.setString(15, i.getAntibody_7());
			pstmt.setString(16, i.getAntibody_8());
			pstmt.setString(17, i.getAntibody_9());
			pstmt.setString(18, i.getAntibody_10());
			pstmt.setString(19, i.getAntibody_11());
			pstmt.setString(20, i.getAntibody_12());
			pstmt.setString(21, i.getAntibody_13());
			pstmt.setString(22, i.getAntibody_14());
			pstmt.setString(23, i.getAntibody_15());
			pstmt.setString(24, i.getAntibody_16());
			pstmt.setString(25, i.getAntibody_17());
			pstmt.setString(26, i.getAntibody_18());
			pstmt.setString(27, i.getExp_date_2());
			pstmt.setString(28, i.getAntibody_19());
			pstmt.setString(29, i.getAntibody_20());
			pstmt.setString(30, i.getAntibody_21());
			pstmt.setString(31, i.getAntibody_22());
			pstmt.setString(32, i.getAntibody_23());
			pstmt.setString(33, i.getAntibody_24());
			pstmt.setString(34, i.getAntibody_25());
			pstmt.setString(35, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Stain Muscle & Skin insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;

	}

	public List<LabStainDTO> selectStain(String searchVal) {

		String sql = "select * from spn.lab_stain where chart_no ='"
				+ searchVal + "'order by sms_no desc";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<LabStainDTO> list = new ArrayList<LabStainDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				LabStainDTO i = new LabStainDTO();

				i.setSms_no(rs.getString("sms_no"));
				i.setExperiment(rs.getString("experiment"));
				i.setM_index_no(rs.getString("m_index_no"));
				i.setDiscription(rs.getString("discription"));
				i.setPathology_diagnosis(rs.getString("pathology_diagnosis"));
				i.setResult(rs.getString("result"));
				i.setComment(rs.getString("comment"));
				i.setExp_date_1(rs.getString("exp_date_1"));
				i.setAntibody_1(rs.getString("antibody_1"));
				i.setAntibody_2(rs.getString("antibody_2"));
				i.setAntibody_3(rs.getString("antibody_3"));
				i.setAntibody_4(rs.getString("antibody_4"));
				i.setAntibody_5(rs.getString("antibody_5"));
				i.setAntibody_6(rs.getString("antibody_6"));
				i.setAntibody_7(rs.getString("antibody_7"));
				i.setAntibody_8(rs.getString("antibody_8"));
				i.setAntibody_9(rs.getString("antibody_9"));
				i.setAntibody_10(rs.getString("antibody_10"));
				i.setAntibody_11(rs.getString("antibody_11"));
				i.setAntibody_12(rs.getString("antibody_12"));
				i.setAntibody_13(rs.getString("antibody_13"));
				i.setAntibody_14(rs.getString("antibody_14"));
				i.setAntibody_15(rs.getString("antibody_15"));
				i.setAntibody_16(rs.getString("antibody_16"));
				i.setAntibody_17(rs.getString("antibody_17"));
				i.setAntibody_18(rs.getString("antibody_18"));
				i.setExp_date_2(rs.getString("exp_date_2"));
				i.setAntibody_19(rs.getString("antibody_19"));
				i.setAntibody_20(rs.getString("antibody_20"));
				i.setAntibody_21(rs.getString("antibody_21"));
				i.setAntibody_22(rs.getString("antibody_22"));
				i.setAntibody_23(rs.getString("antibody_23"));
				i.setAntibody_24(rs.getString("antibody_24"));
				i.setAntibody_25(rs.getString("antibody_25"));
				i.setIdate(rs.getString("idate"));
				i.setIid(rs.getString("iid"));

				list.add(i);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}
	
	public int updateStain(LabStainDTO i) {

		String sms_no = i.getSms_no();

		String sql = "update spn.lab_stain set chart_no=?,experiment=?,m_index_no=?,discription=?,pathology_diagnosis=?,result=?,comment=?,"
				+ "exp_date_1=?,antibody_1=?,antibody_2=?,antibody_3=?,antibody_4=?,antibody_5=?,antibody_6=?,antibody_7=?,antibody_8=?,antibody_9=?,"
				+ "antibody_10=?,antibody_11=?,antibody_12=?,antibody_13=?,antibody_14=?,antibody_15=?,antibody_16=?,antibody_17=?,antibody_18=?,"
				+ "exp_date_2=?,antibody_19=?,antibody_20=?,antibody_21=?,antibody_22=?,antibody_23=?,antibody_24=?,antibody_25=?,iid=?,idate=now()"
				+ "where sms_no='"+sms_no+"'";

		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getExperiment());
			pstmt.setString(3, i.getM_index_no());
			pstmt.setString(4, i.getDiscription());
			pstmt.setString(5, i.getPathology_diagnosis());
			pstmt.setString(6, i.getResult());
			pstmt.setString(7, i.getComment());
			pstmt.setString(8, i.getExp_date_1());
			pstmt.setString(9, i.getAntibody_1());
			pstmt.setString(10, i.getAntibody_2());
			pstmt.setString(11, i.getAntibody_3());
			pstmt.setString(12, i.getAntibody_4());
			pstmt.setString(13, i.getAntibody_5());
			pstmt.setString(14, i.getAntibody_6());
			pstmt.setString(15, i.getAntibody_7());
			pstmt.setString(16, i.getAntibody_8());
			pstmt.setString(17, i.getAntibody_9());
			pstmt.setString(18, i.getAntibody_10());
			pstmt.setString(19, i.getAntibody_11());
			pstmt.setString(20, i.getAntibody_12());
			pstmt.setString(21, i.getAntibody_13());
			pstmt.setString(22, i.getAntibody_14());
			pstmt.setString(23, i.getAntibody_15());
			pstmt.setString(24, i.getAntibody_16());
			pstmt.setString(25, i.getAntibody_17());
			pstmt.setString(26, i.getAntibody_18());
			pstmt.setString(27, i.getExp_date_2());
			pstmt.setString(28, i.getAntibody_19());
			pstmt.setString(29, i.getAntibody_20());
			pstmt.setString(30, i.getAntibody_21());
			pstmt.setString(31, i.getAntibody_22());
			pstmt.setString(32, i.getAntibody_23());
			pstmt.setString(33, i.getAntibody_24());
			pstmt.setString(34, i.getAntibody_25());
			pstmt.setString(35, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Lab Stain Muscle & Skin Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}

}
