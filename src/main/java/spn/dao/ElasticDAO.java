package spn.dao;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import spn.dto.ElasticDTO;

public class ElasticDAO {
	static ElasticDAO dao=new ElasticDAO();
	static String SERVER_IP="localhost";
	static int SERVER_PORT=9200;
	
	private ElasticDAO() {
	
		System.out.println("ElasticDAO");
	}
	
	public static ElasticDAO getElasticDAO() {
		return dao;
	}
	
	public static Response constructClient(String IP, int PORT, String METHOD, String jsonString, String requestURL) {
		Response response=null;
		try {
			RestClient restClient = RestClient.builder( // Initializing
			        new HttpHost(IP, PORT, "http")).build();
			Map<String, String> params=null;
			
			//if(METHOD=="GET") {
				params= Collections.singletonMap("pretty", "true");
			//}else {
				//params = Collections.emptyMap();
			//}
			//String jsonString = json;
			HttpEntity entity = new NStringEntity(jsonString, ContentType.APPLICATION_JSON);
			response = restClient.performRequest(METHOD, requestURL, params, entity);
			restClient.close();
			
			return response;
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}
	public static String makeQueryString(String searchText, int size) {
		String json =
					"{" +
		        			"\"query\":{"+
		        				"\"query_string\":{" +
		        					"\"query\":\"*"+searchText+"*\""+
		        				"}"+
		        			"},"+
		        			"\"size\":\""+size+"\""+
		        		"}";
		return json;
	}
	
	
	public static String makeQueryString(HashMap<String, String> kv, int size) { // 다중 필드 검색 
		
		String json =
				"{" +
	        			"\"query\":{"+
	        				"\"bool\":{" +
	        					"\"must\":[";
		
		Set<Entry<String, String>> set = kv.entrySet();
		Iterator<Entry<String, String>> itr = set.iterator();
		while (itr.hasNext()) {
			Entry<String, String> e = (Entry<String, String>)itr.next();
			String next_key=e.getKey(), next_value=e.getValue();
			
			if (next_value == null || next_value.equals("")) continue; // null 값일때는 검색 문자열에서 제외 
			else json+="{\"match\":{\""+e.getKey()+"\" : \""+e.getValue()+"\"} },";
		}
		json=json.substring(0, json.length()-1); // 만들어진 문자열의 마지막 ,를 제거하도록 한다.
		
		json+=			"]"+
	        			"}"+
	        		"},"+
	        		"\"size\":\""+size+"\""+
	        	"}";
		
		System.out.println(json);
		
		return json;
	}
	
	public static String getMenuCount() { // menu : count 인 json 형태의 문자열 리턴
		String url="";
		String menu_count=""; // 각 메뉴별 카운트를 임시 저장할 변수 
		String menus[]= {"info", 
				"lab_sanger", "lab_ngs", "lab_cgh", "lab_stain", 
				"sample_dna", "sample_tissue", "sample_cell", "sample_cfdna", "sample_serum", "sample_rna"
				};
		JSONObject obj = new JSONObject();
		
		String json="{}"; // ### null이나 공백이 아닌 {}의 json형태는 지켜주어야 한다.  
		System.out.println(json);
		
		for(String menu : menus) {
			url="/"+menu+"/"+menu+"/_search";
		
			try {
				Response response=constructClient(SERVER_IP, SERVER_PORT, "GET", json, url);
				String responseBody = EntityUtils.toString(response.getEntity()); 
				
				JSONParser parser = new JSONParser();
				JSONObject jsonObj = (JSONObject) parser.parse( responseBody );
				JSONObject first_hits=(JSONObject)jsonObj.get("hits");
				
				//JSONArray jsonArr=(JSONArray)first_hits.get("hits"); 
				
				menu_count=first_hits.get("total").toString();
				obj.put(menu, menu_count);
				
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		return obj.toJSONString();
	}
	

	
	// GET METHOD, ElasticDTO를 받아 리턴한다.
	public static ElasticDTO searchElastic(ElasticDTO i) {
		
		String searchText=i.getSearchText(); // 검색할 문자열
		String url=i.getUrl(); // 검색할 index를 포함한 url주소
		int size=Integer.parseInt(i.getSize()); // 검색 내용 최대 개수 제한
		String json="";
		
		if(searchText==null || searchText.equals("")) { // 검색할 문자열이 없다면, json 문자열을 makeQueryString으로 만들지 않는다.
			json="{}";
		}else {
			json=makeQueryString(searchText, size);
		}
		
		ElasticDTO ret=new ElasticDTO();
		
		System.out.println("searchText : "+searchText);
		System.out.println("ElasticDAO=====");
		System.out.println(url);
		System.out.println(json);
		
		try {
			Response response=constructClient(SERVER_IP, SERVER_PORT, "GET", json, url);
			String responseBody = EntityUtils.toString(response.getEntity()); 
			
			//System.out.println("RESPONSE BODY===");
			//System.out.println(responseBody);
			
			JSONParser parser = new JSONParser();
			JSONObject jsonObj = (JSONObject) parser.parse( responseBody );
			JSONObject first_hits=(JSONObject)jsonObj.get("hits");
			
			//JSONArray jsonArr=(JSONArray)first_hits.get("hits"); 
			
			String count=first_hits.get("total").toString();
			
			
			System.out.println(responseBody);
			//JSONObject second_hits=(JSONObject)first_hits.get("hits");
			
			//JSONObject source=(JSONObject)first_hits.get("_source");
			
			//System.out.println("ID : "+source.get("id").toString());
			
			
			ret.setResponseJsonString(responseBody);
			ret.setCount(count);
			System.out.println("Count : "+count);
			
			return ret;
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		
		return ret;
	}
	
	// 특정 query를 날려, id값을 가져올 수 있도록 한다.
	public static String getElasticId(HashMap<String, String> kv) {
		
		System.out.println("ElasticDAO/getElasticId()");
		String url="/_search";
		int size=10;
		String id="";
		
		String json=makeQueryString(kv, size);
		
		
		try {
			Response response=constructClient(SERVER_IP, SERVER_PORT, "GET", json, url);
			String responseBody = EntityUtils.toString(response.getEntity()); 
			
			JSONParser parser = new JSONParser();
			JSONObject jsonObj = (JSONObject) parser.parse( responseBody );
			JSONObject first_hits=(JSONObject)jsonObj.get("hits");
			
			int count=Integer.parseInt(first_hits.get("total").toString());
			
			
			System.out.println("Count : "+count);
			
			if(count==1) { // count==1 일때만 작업 
				System.out.println("COUNT is 1 : ElasticDAO getElasticID");
				JSONArray second_hits=(JSONArray) first_hits.get("hits");
				id= ((JSONObject)second_hits.get(0)).get("_id").toString();
				return id;
				
			}else if(count==0) {
				System.out.println("GETELASTIC ID, COUNT = 0");
			}else {
				System.out.println("ERROR, NOT ABLE COUNT GETELASTICID()");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		return id;
	}
	
	
	
	
	// POST - insertElastic
	public int insertElastic(ElasticDTO i, HashMap<String, String> kv) { // 특정 key,value 값을 받도록 한다.
		String url="/"+i.getMenu()+"/"+i.getMenu();
		Set<Entry<String, String>> set = kv.entrySet();
		Iterator<Entry<String, String>> itr = set.iterator();
		JSONObject jsonobj=new JSONObject();
		while (itr.hasNext()) {
			Entry<String, String> e = (Entry<String, String>)itr.next();
			jsonobj.put(e.getKey(), e.getValue());
		}
	  
		System.out.println(jsonobj.toJSONString());

		
		try {
			Response response=constructClient(SERVER_IP, SERVER_PORT, "POST", jsonobj.toJSONString(), url);
			String responseBody = EntityUtils.toString(response.getEntity()); 
		}catch(Exception e) {
			e.printStackTrace();
		}

		return 0; // return 값의 의미 부여 
	}
	
	// PUT - updateElastic
	public int updateElastic(ElasticDTO i, HashMap<String, String> kv) { // 특정 key,value 값을 받도록 한다.
		//String url=i.getUrl(); // menu가 포함된 값을 가져온다.
		String url="/"+i.getMenu()+"/"+i.getMenu()+"/"+i.get_id();
		System.out.println("updateElastic url : "+url);
		Set<Entry<String, String>> set = kv.entrySet();
		Iterator<Entry<String, String>> itr = set.iterator();
		JSONObject jsonobj=new JSONObject();
		while (itr.hasNext()) {
			Entry<String, String> e = (Entry<String, String>)itr.next();
			jsonobj.put(e.getKey(), e.getValue());
		}
	  
		System.out.println(jsonobj.toJSONString());

		
		try {
			Response response=constructClient(SERVER_IP, SERVER_PORT, "PUT", jsonobj.toJSONString(), url);
			String responseBody = EntityUtils.toString(response.getEntity()); 
		}catch(Exception e) {
			e.printStackTrace();
		}

		return 0; // return 값의 의미 부여 
	}
	
	public int deleteElastic(HashMap<String, String> kv, String menu) {
		String json="";
		String url="";
		
		try {
			String _id=getElasticId(kv);
			
			System.out.println("_id : "+_id);
			url="/"+menu+"/"+menu+"/"+_id;
			
			Response response=constructClient(SERVER_IP, SERVER_PORT, "DELETE", json, url);
			String responseBody = EntityUtils.toString(response.getEntity()); 
			
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		
		return 0;
	}
	
	
	// DELETE
}
