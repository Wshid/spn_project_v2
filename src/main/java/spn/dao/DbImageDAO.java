package spn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import spn.dto.DbImageDTO;
import spn.util.DBManager;

public class DbImageDAO {
	static DbImageDAO dao=new DbImageDAO();
	
	private DbImageDAO(){
		System.out.println("DbImageDAO");
	}
	
	public static DbImageDAO getDbImageDAO(){
		return dao;
	}
	
	public int insertDbImage(DbImageDTO i){ // 새로 생성할때
		String sql="insert into spn.db_image(chart_no, image_name, subtitle, full_path, doctor, page_name, cdate, idate)"+
				" values(?,?,?,?,?,?,now(),now())";
		Connection conn=DBManager.getConnection();
		PreparedStatement pstmt=null;
		int result=0;
		
		try{
			pstmt=conn.prepareStatement(sql);
			
			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getImage_name());
			pstmt.setString(3, i.getSubtitle());
			pstmt.setString(4, i.getFull_path());
			pstmt.setString(5, i.getDoctor());
			pstmt.setString(6, i.getPage_name());
			
			result=pstmt.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		} finally{
			DBManager.close(conn, pstmt);
		}
		
		return result;
		
	}
	
	public int updateDbImage(DbImageDTO i){
		String img_idx=i.getImg_idx();
		
		String sql="update spn.db_image set chart_no=?, image_name=? subtitle=?, full_path=?, doctor=?, page_name=?, idate=now() "+
				"where img_idx='"+img_idx+"'";
		
		Connection conn=DBManager.getConnection();
		PreparedStatement pstmt=null;
		int result=0;
		
		try{
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getImage_name());
			pstmt.setString(3, i.getSubtitle());
			pstmt.setString(4, i.getFull_path());
			pstmt.setString(5, i.getDoctor());
			pstmt.setString(6, i.getPage_name());
			
			result=pstmt.executeUpdate();
			
			System.out.println("updateDbImage result : "+result);
			System.out.println("DbImage Update success");
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			DBManager.close(conn, pstmt);
		}
		
		return result;
	}
	
	public int deleteDbImage(DbImageDTO i){
		// identify하게 남는 정보는 file_name과 subtitle 정도 일듯
		//String img_idx=i.getImg_idx();
		//String sql="DELETE FROM spn.db_image WHERE img_idx=?"+img_idx;
		String file_name=i.getImage_name(); //fileName으로 제거하기
		String sql="DELETE FROM spn.db_image WHERE image_name=?";
		Connection conn=DBManager.getConnection();
		PreparedStatement pstmt=null;
		int result=0;
		
		try{
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, file_name);
			result=pstmt.executeUpdate();
			
			System.out.println("DbImage delete success");
		} catch(SQLException e){
			e.printStackTrace();
		} finally{
			DBManager.close(conn, pstmt);
		}
		return result;
	}
	
	public int getNextIndex(){
		String sql="SELECT `auto_increment` FROM INFORMATION_SCHEMA.TABLES WHERE table_name='db_image'";
		Connection conn=DBManager.getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		int result=0;
		try{
			pstmt=conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()) {
				result=rs.getInt("auto_increment");
			}
			System.out.println("DbImage Next Index success");
		} catch(SQLException e){
			e.printStackTrace();
		} finally{
			DBManager.close(conn,  pstmt);
		}
		
		return result;
	}
	
	public List<DbImageDTO> selectDbImage(String searchVal, String page_name){
		String sql="select * from spn.db_image WHERE chart_no='"+searchVal+"' and page_name='"+page_name+"' order by img_idx desc";
		Connection conn=DBManager.getConnection();
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		List<DbImageDTO> list=new ArrayList<DbImageDTO>();
		
		try{
			pstmt=conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
			
			while(rs.next()){
				DbImageDTO i=new DbImageDTO();
				
				i.setImg_idx(rs.getString("img_idx"));
				i.setChart_no(rs.getString("chart_no"));
				i.setImage_name(rs.getString("image_name"));
				i.setSubtitle(rs.getString("subtitle"));
				i.setFull_path(rs.getString("full_path"));
				i.setCdate(rs.getString("cdate"));
				i.setIdate(rs.getString("idate"));
				i.setDoctor(rs.getString("doctor"));
				i.setPage_name(rs.getString("page_name"));
				
				list.add(i);
			}
		} catch (SQLException e){
			e.printStackTrace();
		} finally{
			DBManager.close(conn, pstmt);
		}
		return list;
	}
}
