package spn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.mysql.jdbc.Statement;

import java.util.ArrayList;

import spn.dto.MemberDTO;
import spn.util.DBManager;

public class MemberDAO {

	static MemberDAO dao = new MemberDAO();

	private MemberDAO() {
		System.out.println("MemberDAO");
	}

	public static MemberDAO getMemberDAO() {
		return dao;
	}

	/*
	 * public int isMember(String id, String pw) { String sql = "select "+ pw
	 * +" from spn.member where id=?"; Connection con =
	 * DBManager.getConnection(); PreparedStatement pstmt = null; ResultSet rs =
	 * null;
	 * 
	 * try { pstmt = con.prepareStatement(sql); pstmt.setString(1, id); rs =
	 * pstmt.executeQuery();
	 * 
	 * if (rs.next()) {
	 * 
	 * //id ���� String dbPW=rs.getString("pw"); if(pw.equals(dbPW)){ return 1;
	 * }else { <script type="text/javascript"> }
	 * 
	 * 
	 * 
	 * }
	 * 
	 * } catch (SQLException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } finally { DBManager.close(con, pstmt); }
	 * 
	 * return 0;
	 * 
	 * }
	 */

	public int isMember(String id, String pw) {
		//String sql = "select * from spn.member where id='" + id
		String sql = "select * from spn.member where id='" + id
				+ "' and pw='" + pw + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			if (rs.next()) {

				return 1;

			}

		} catch (SQLException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return 0;

	}

	public int insertMember(MemberDTO m) {

		String sql = "insert into spn.member(id,pw,name,affiliation,phone,permission,regdate)"
				+ " values(?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {
			System.out.println(m.getId());
			System.out.println(m.getPw());
			System.out.println(m.getName());
			System.out.println(m.getAffiliation());
			System.out.println(m.getPhone());
			System.out.println(m.getPermission());

			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, m.getId());
			pstmt.setString(2, m.getPw());
			pstmt.setString(3, m.getName());
			pstmt.setString(4, m.getAffiliation());
			pstmt.setString(5, m.getPhone());
			pstmt.setString(6, m.getPermission());

			result = pstmt.executeUpdate();

			System.out.println("Memeber insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return 0;
	}
	
	public MemberDTO findMember(String id){
		Connection conn=DBManager.getConnection();
		
		PreparedStatement pstmt=null;
		ResultSet rs=null;
		String sql="select * from spn.member where id=?";
		MemberDTO m=new MemberDTO();
		
		try{
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, id);
			
			rs=pstmt.executeQuery();
			
			while(rs.next()){
				m.setMb_no(rs.getString("mb_no"));
				m.setId(id);
				//m.setPw(rs.getString("pw"));
				m.setName(rs.getString("name"));
				m.setAffiliation(rs.getString("affiliation"));
				m.setPermission(rs.getString("permission"));
				m.setPhone(rs.getString("phone"));
				break;
			}
		} catch(SQLException e){
			e.printStackTrace();
		} finally{
			DBManager.close(conn, pstmt, rs);
		}
		return m;
	}

	public List<MemberDTO> selectMember() {

		Connection conn = DBManager.getConnection();

		List<MemberDTO> list = new ArrayList<>();

		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = "select * from spn.member order by mb_no desc";

		try {
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				MemberDTO m = new MemberDTO();
				m.setMb_no(rs.getString("mb_no"));
				m.setId(rs.getString("id"));
				m.setPw(rs.getString("pw"));
				m.setName(rs.getString("name"));
				m.setAffiliation(rs.getString("affiliation"));
				m.setPhone(rs.getString("phone"));
				m.setPermission(rs.getString("permission"));
				m.setRegdate(rs.getString("regdate"));
				list.add(m);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(conn, pstmt, rs);
		}

		return list;

	}

	public int deleteMember(String mb_no) {
		String sql = "delete from spn.member where mb_no='" + mb_no + "'";

		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;

		try {
			pstmt = con.prepareStatement(sql);
			result = pstmt.executeUpdate();
			System.out.println("Member delete success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return 0;

	}

}
