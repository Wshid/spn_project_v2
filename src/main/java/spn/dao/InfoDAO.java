package spn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import spn.dto.InfoDTO;
import spn.dto.LabSangerDTO;
import spn.dto.OverviewDTO;
import spn.dto.SampleRnaDTO;
import spn.util.DBManager;

public class InfoDAO {

	static InfoDAO dao = new InfoDAO();

	private InfoDAO() {
		System.out.println("InfoDAO");
	}

	public static InfoDAO getInfoDAO() {
		return dao;
	}
	
	public String lastIncreaseNumber(String table_name, String prikey) {
		String sql = "SELECT "+prikey+" FROM spn."+table_name+" ORDER BY "+prikey+" desc LIMIT 1";
		// pstmt 사용이 제대로 되지 않으므로, 일반 문자열로 변환 
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<LabSangerDTO> list = new ArrayList<LabSangerDTO>();
		
		String ret="";

		try {
			pstmt = con.prepareStatement(sql);
			//pstmt.setString(1, prikey);
			//pstmt.setString(2, prikey);
			rs = pstmt.executeQuery();

			
			while (rs.next()) {
				ret=rs.getString(prikey);
				break;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return ret;
	}

	public int insertInfo(InfoDTO i) {

		//String sql = "insert into spn.info(snupn,chart_no,name,sex,birth,doctor,refered_dr,category,"
		//		+ "clinical_diagnosis,symptom,remark,iid,idate) values(?,?,?,?,?,?,?,?,?,?,?,?,?,now())";

		String sql = "insert into spn.info(snupn,chart_no,name,sex,birth,doctor,other_site,category,"
				+ "clinical_diagnosis,symptom,iid,idate) values(?,?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {
			/*
			 * System.out.println(i.getSnupn());
			 */

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getSnupn());
			pstmt.setString(2, i.getChart_no());
			pstmt.setString(3, i.getName());
			pstmt.setString(4, i.getSex());
			pstmt.setString(5, i.getBirth());
			pstmt.setString(6, i.getDoctor());
			//pstmt.setString(7, i.getRefered_dr());
			pstmt.setString(7, i.getOther_site());
			pstmt.setString(8, i.getCategory());
			//pstmt.setString(9, i.getFinal_diagnosis());
			pstmt.setString(9, i.getClinical_diagnosis());
			pstmt.setString(10, i.getSymptom());
			//pstmt.setString(12, i.getRemark());
			pstmt.setString(11, i.getIid()); // iid�� ���� �ǹ��ϴ°ɱ�?

			result = pstmt.executeUpdate();

			System.out.println("General info insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}

	public List<InfoDTO> selectInfo(String searchVal) {

		String sql = "select * from spn.info where chart_no ='" + searchVal
				+ "' ";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<InfoDTO> list = new ArrayList<InfoDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				InfoDTO i = new InfoDTO();

				i.setGi_no(rs.getString("gi_no"));
				i.setSnupn(rs.getString("snupn"));
				i.setChart_no(rs.getString("chart_no"));
				i.setName(rs.getString("name"));
				i.setSex(rs.getString("sex"));
				i.setBirth(rs.getString("birth"));
				i.setDoctor(rs.getString("doctor"));
				//i.setRefered_dr(rs.getString("refered_dr"));
				i.setOther_site(rs.getString("other_site"));
				i.setCategory(rs.getString("category"));
				//i.setFinal_diagnosis(rs.getString("clinical_diagnosis"));
				i.setClinical_diagnosis(rs.getString("clinical_diagnosis"));
				i.setSymptom(rs.getString("symptom"));
				//i.setRemark(rs.getString("remark"));
				i.setIid(rs.getString("iid"));
				i.setIdate(rs.getString("idate"));
				list.add(i);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}
	
	public int updateinfo(InfoDTO i) {

		String gi_no = i.getGi_no();

		//String sql = "update spn.info set snupn=?,chart_no=?,name=?,sex=?,birth=?,doctor=?,refered_dr=?,category=?,"
		//		+ "final_diagnosis=?,clinical_diagnosis=?,symptom=?,remark=?,iid=?,idate=now()"
		//		+ " where gi_no='" + gi_no + "'";
		String sql = "update spn.info set snupn=?,chart_no=?,name=?,sex=?,birth=?,doctor=?,other_site=?,category=?,"
				+ "clinical_diagnosis=?,symptom=?,iid=?,idate=now()"
				+ " where gi_no='" + gi_no + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getSnupn());
			pstmt.setString(2, i.getChart_no());
			pstmt.setString(3, i.getName());
			pstmt.setString(4, i.getSex());
			pstmt.setString(5, i.getBirth());
			pstmt.setString(6, i.getDoctor());
			//pstmt.setString(7, i.getRefered_dr());
			pstmt.setString(7, i.getOther_site());
			pstmt.setString(8, i.getCategory());
			//pstmt.setString(9, i.getFinal_diagnosis());
			pstmt.setString(9, i.getClinical_diagnosis());
			pstmt.setString(10, i.getSymptom());
			//pstmt.setString(12, i.getRemark());
			pstmt.setString(11, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("General Info Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	

	
	

	/* Chart_No �ߺ� Ȯ�� �޼��� */
	public int confirmChartNo(String chart_no) throws Exception {
		String sql = "select chart_id from spn.info where chart_no='"
				+ chart_no + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			if (rs.next()) {
				return 1;
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return 0;
	}
	
	
	
	
	
	public int insertOverview(OverviewDTO i) {

		String sql = "insert into spn.overview "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {
			/*
			 * System.out.println(i.getSnupn());
			 */

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getDna_genomic());
			pstmt.setString(3, i.getDna_cellfree());
			pstmt.setString(4, i.getDna_mitochondria());
			pstmt.setString(5, i.getTissue_muscle());
			pstmt.setString(6, i.getTissue_myoblast());
			pstmt.setString(7, i.getTissue_skin());
			pstmt.setString(8, i.getTissue_fibroblast());
			pstmt.setString(9, i.getCell_free_dna());
			pstmt.setString(10, i.getSerum());
			pstmt.setString(11, i.getCsf());
			pstmt.setString(12, i.getUrine_cell());
			pstmt.setString(13, i.getUipsc());
			pstmt.setString(14, i.getRna());
			pstmt.setString(15, i.getInformed_consent());
			pstmt.setString(16, i.getSanger_order());
			pstmt.setString(17, i.getSanger_ongoing());
			pstmt.setString(18, i.getSanger_done());
			pstmt.setString(19, i.getNgs_wgs_order());
			pstmt.setString(20, i.getNgs_wgs_ongoing());
			pstmt.setString(21, i.getNgs_wgs_done());
			pstmt.setString(22, i.getNgs_wes_order());
			pstmt.setString(23, i.getNgs_wes_ongoing());
			pstmt.setString(24, i.getNgs_wes_done());
			pstmt.setString(25, i.getNgs_targeted_order());
			pstmt.setString(26, i.getNgs_targeted_ongoing());
			pstmt.setString(27, i.getNgs_targeted_done());
			pstmt.setString(28, i.getCgh_order());
			pstmt.setString(29, i.getCgh_ongoing());
			pstmt.setString(30, i.getCgh_done());
			pstmt.setString(31, i.getMuscle_stain_order());
			pstmt.setString(32, i.getMuscle_stain_ongoing());
			pstmt.setString(33, i.getMuscle_stain_done());
			pstmt.setString(34, i.getWestern_order());
			pstmt.setString(35, i.getWestern_ongoing());
			pstmt.setString(36, i.getWestern_done());
			pstmt.setString(37, i.getOther_order());
			pstmt.setString(38, i.getOther_ongoing());
			pstmt.setString(39, i.getOther_done());
			pstmt.setString(40, i.getIid());
			
			result = pstmt.executeUpdate();

			System.out.println("OverView insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}

	public List<OverviewDTO> selectOverview(String searchVal) {

		String sql = "select * from spn.overview where chart_no ='" + searchVal
				+ "' ";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<OverviewDTO> list = new ArrayList<OverviewDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				OverviewDTO i = new OverviewDTO();

				i.setDna_genomic(rs.getString("dna_genomic"));
				i.setDna_cellfree(rs.getString("dna_cellfree"));
				i.setDna_mitochondria(rs.getString("dna_mitochondria"));
				i.setTissue_muscle(rs.getString("tissue_muscle"));
				i.setTissue_myoblast(rs.getString("tissue_myoblast"));
				i.setTissue_skin(rs.getString("tissue_skin"));
				i.setTissue_fibroblast(rs.getString("tissue_fibroblast"));
				i.setCell_free_dna(rs.getString("cell_free_dna"));
				i.setSerum(rs.getString("serum"));
				i.setCsf(rs.getString("csf"));
				i.setUrine_cell(rs.getString("urine_cell"));
				i.setUipsc(rs.getString("uipsc"));
				i.setRna(rs.getString("rna"));
				i.setInformed_consent(rs.getString("informed_consent"));
				i.setSanger_order(rs.getString("sanger_order"));
				i.setSanger_ongoing(rs.getString("sanger_ongoing"));
				i.setSanger_done(rs.getString("sanger_done"));
				i.setNgs_wgs_order(rs.getString("ngs_wgs_order"));
				i.setNgs_wgs_ongoing(rs.getString("ngs_wgs_ongoing"));
				i.setNgs_wgs_done(rs.getString("ngs_wgs_done"));
				i.setNgs_wes_order(rs.getString("ngs_wes_order"));
				i.setNgs_wes_ongoing(rs.getString("ngs_wes_ongoing"));
				i.setNgs_wes_done(rs.getString("ngs_wes_done"));
				i.setNgs_targeted_order(rs.getString("ngs_targeted_order"));
				i.setNgs_targeted_ongoing(rs.getString("ngs_targeted_ongoing"));
				i.setNgs_targeted_done(rs.getString("ngs_targeted_done"));
				i.setCgh_order(rs.getString("cgh_order"));
				i.setCgh_ongoing(rs.getString("cgh_ongoing"));
				i.setCgh_done(rs.getString("cgh_done"));
				i.setMuscle_stain_order(rs.getString("muscle_stain_order"));
				i.setMuscle_stain_ongoing(rs.getString("muscle_stain_ongoing"));
				i.setMuscle_stain_done(rs.getString("muscle_stain_done"));
				i.setWestern_order(rs.getString("western_order"));
				i.setWestern_ongoing(rs.getString("western_ongoing"));
				i.setWestern_done(rs.getString("western_done"));
				i.setOther_order(rs.getString("other_order"));
				i.setOther_ongoing(rs.getString("other_ongoing"));
				i.setOther_done(rs.getString("other_done"));
				i.setIid(rs.getString("iid"));
				i.setIdate(rs.getString("idate"));
		
				list.add(i);
			
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}
	
	public int updateOverview(OverviewDTO i) {

		String chart_no = i.getChart_no();

		String sql = "update spn.overview set chart_no=?, dna_genomic=?, dna_cellfree=?, dna_mitochondria=?, tissue_muscle=?, "
				+ "tissue_myoblast=?, tissue_skin=?, tissue_fibroblast=?, cell_free_dna=?, serum=?, csf=?, "
				+ "urine_cell=?, uipsc=?, rna=?, informed_consent=?, sanger_order=?, sanger_ongoing=?, sanger_done=?,"
				+ "ngs_wgs_order=?, ngs_wgs_ongoing=?, ngs_wgs_done=?, ngs_wes_order=?, ngs_wes_ongoing=?, ngs_wes_done=?,"
				+ "ngs_targeted_order=?, ngs_targeted_ongoing=?, ngs_targeted_done=?, cgh_order=?, cgh_ongoing=?, "
				+ "cgh_done=?, muscle_stain_order=?, muscle_stain_ongoing=?, muscle_stain_done=?, western_order=?, "
				+ "western_ongoing=?, western_done=?, other_order=?, other_ongoing=?, other_done=?, iid=?, idate=now()"
				+ " where chart_no='" + chart_no + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getDna_genomic());
			pstmt.setString(3, i.getDna_cellfree());
			pstmt.setString(4, i.getDna_mitochondria());
			pstmt.setString(5, i.getTissue_muscle());
			pstmt.setString(6, i.getTissue_myoblast());
			pstmt.setString(7, i.getTissue_skin());
			pstmt.setString(8, i.getTissue_fibroblast());
			pstmt.setString(9, i.getCell_free_dna());
			pstmt.setString(10, i.getSerum());
			pstmt.setString(11, i.getCsf());
			pstmt.setString(12, i.getUrine_cell());
			pstmt.setString(13, i.getUipsc());
			pstmt.setString(14, i.getRna());
			pstmt.setString(15, i.getInformed_consent());
			pstmt.setString(16, i.getSanger_order());
			pstmt.setString(17, i.getSanger_ongoing());
			pstmt.setString(18, i.getSanger_done());
			pstmt.setString(19, i.getNgs_wgs_order());
			pstmt.setString(20, i.getNgs_wgs_ongoing());
			pstmt.setString(21, i.getNgs_wgs_done());
			pstmt.setString(22, i.getNgs_wes_order());
			pstmt.setString(23, i.getNgs_wes_ongoing());
			pstmt.setString(24, i.getNgs_wes_done());
			pstmt.setString(25, i.getNgs_targeted_order());
			pstmt.setString(26, i.getNgs_targeted_ongoing());
			pstmt.setString(27, i.getNgs_targeted_done());
			pstmt.setString(28, i.getCgh_order());
			pstmt.setString(29, i.getCgh_ongoing());
			pstmt.setString(30, i.getCgh_done());
			pstmt.setString(31, i.getMuscle_stain_order());
			pstmt.setString(32, i.getMuscle_stain_ongoing());
			pstmt.setString(33, i.getMuscle_stain_done());
			pstmt.setString(34, i.getWestern_order());
			pstmt.setString(35, i.getWestern_ongoing());
			pstmt.setString(36, i.getWestern_done());
			pstmt.setString(37, i.getOther_order());
			pstmt.setString(38, i.getOther_ongoing());
			pstmt.setString(39, i.getOther_done());
			pstmt.setString(40, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Overview Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	

}

