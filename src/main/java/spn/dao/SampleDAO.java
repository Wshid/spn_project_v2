package spn.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import spn.dto.LabSangerDTO;
import spn.dto.SampleCellDTO;
import spn.dto.SampleCfdnaDTO;
import spn.dto.SampleDnaDTO;
import spn.dto.SampleRnaDTO;
import spn.dto.SampleSerumDTO;
import spn.dto.SampleTissueDTO;
import spn.dto.SampleUrineDTO;
import spn.util.DBManager;

public class SampleDAO {

	static SampleDAO dao = new SampleDAO();

	private SampleDAO() {
		System.out.println("SampleDAO");
	}

	public static SampleDAO getSampleDAO() {
		return dao;
	}
	
	public String lastIncreaseNumber(String table_name, String prikey) {
		String sql = "SELECT "+prikey+" FROM spn."+table_name+" ORDER BY "+prikey+" desc LIMIT 1";
		// pstmt 사용이 제대로 되지 않으므로, 일반 문자열로 변환 
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<LabSangerDTO> list = new ArrayList<LabSangerDTO>();
		
		String ret="";

		try {
			pstmt = con.prepareStatement(sql);
			//pstmt.setString(1, prikey);
			//pstmt.setString(2, prikey);
			rs = pstmt.executeQuery();

			
			while (rs.next()) {
				ret=rs.getString(prikey);
				break;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return ret;
	}

	/* Sample Bank 1 - DNA */
	public int insertDna(SampleDnaDTO i) {

		String sql = "insert into spn.sample_dna(chart_no,col_date,col_who,col_who_patient_detail,col_who_father_detail,"
				+ "col_who_mother_detail,col_who_sibling_detail,col_who_other_detail,col_type,"
				+ "col_type_other_detail,col_index,col_agreement,col_performed_by,col_concentration,col_260_280,col_260_230,col_location,"
				+ "col_comment,req_date,req_location,req_who,req_who_other_detail,req_distribution,req_order_name,"
				+ "req_type,req_type_other_detail,req_vial,req_comment,iid,idate) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getCol_date());
			pstmt.setString(3, i.getCol_who());
			pstmt.setString(4, i.getCol_who_patient_detail());
			pstmt.setString(5, i.getCol_who_father_detail());
			pstmt.setString(6, i.getCol_who_mother_detail());
			pstmt.setString(7, i.getCol_who_sibling_detail());
			pstmt.setString(8, i.getCol_who_other_detail());
			pstmt.setString(9, i.getCol_type());
			pstmt.setString(10, i.getCol_type_other_detail());
			pstmt.setString(11, i.getCol_index());
			pstmt.setString(12,  i.getCol_agreement());
			pstmt.setString(13, i.getCol_performed_by());
			pstmt.setString(14, i.getCol_concentration());
			pstmt.setString(15, i.getCol_260_280());
			pstmt.setString(16, i.getCol_260_230());
			pstmt.setString(17, i.getCol_location());
			pstmt.setString(18, i.getCol_comment());
			pstmt.setString(19, i.getReq_date());
			pstmt.setString(20, i.getReq_location());
			pstmt.setString(21, i.getReq_who());
			pstmt.setString(22, i.getReq_who_other_detail());
			pstmt.setString(23, i.getReq_distribution());
			pstmt.setString(24, i.getReq_order_name());
			pstmt.setString(25, i.getReq_type());
			pstmt.setString(26, i.getReq_type_other_detail());
			pstmt.setString(27, i.getReq_vial());
			pstmt.setString(28, i.getReq_comment());
			pstmt.setString(29, i.getIid());

			//System.out.println("SAMPLE DAO PATIENT DETAIL");
			//System.out.println(i.getCol_who_patient_detail());
			
			result = pstmt.executeUpdate();
			

			System.out.println("Sample DAO insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}

	public List<SampleDnaDTO> selectDna(String searchVal) {

		String sql = "select * from spn.sample_dna where chart_no ='"
				+ searchVal + "'order by dna_no";// desc";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<SampleDnaDTO> list = new ArrayList<SampleDnaDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				SampleDnaDTO i = new SampleDnaDTO();

				i.setDna_no(rs.getString("dna_no"));
				i.setCol_date(rs.getString("col_date"));
				i.setCol_who(rs.getString("col_who"));
				i.setCol_who_patient_detail(rs.getString("col_who_patient_detail"));
				i.setCol_who_father_detail(rs.getString("col_who_father_detail"));
				i.setCol_who_mother_detail(rs.getString("col_who_mother_detail"));
				i.setCol_who_sibling_detail(rs.getString("col_who_sibling_detail"));
				i.setCol_who_other_detail(rs.getString("col_who_other_detail"));
				i.setCol_type(rs.getString("col_type"));
				i.setCol_type_other_detail(rs.getString("col_type_other_detail"));
				i.setCol_index(rs.getString("col_index"));
				i.setCol_agreement(rs.getString("col_agreement"));
				i.setCol_performed_by(rs.getString("col_performed_by"));
				i.setCol_concentration(rs.getString("col_concentration"));
				i.setCol_260_280(rs.getString("col_260_280"));
				i.setCol_260_230(rs.getString("col_260_230"));
				i.setCol_location(rs.getString("col_location"));
				i.setCol_comment(rs.getString("col_comment"));
				i.setReq_date(rs.getString("req_date"));
				i.setReq_location(rs.getString("req_location"));
				i.setReq_who(rs.getString("req_who"));
				i.setReq_who_other_detail(rs.getString("req_who_other_detail"));
				i.setReq_distribution(rs.getString("req_distribution"));
				i.setReq_order_name(rs.getString("req_order_name"));
				i.setReq_type(rs.getString("req_type"));
				i.setReq_type_other_detail(rs.getString("req_type_other_detail"));
				i.setReq_vial(rs.getString("req_vial"));
				i.setReq_comment(rs.getString("req_comment"));
				i.setIdate(rs.getString("idate"));
				i.setIid(rs.getString("iid"));

				list.add(i);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}
	
	public int updateDna(SampleDnaDTO i) {

		String dna_no = i.getDna_no();

		String sql = "update spn.sample_dna set chart_no=?,col_date=?,col_who=?,col_who_patient_detail=?,col_who_father_detail=?,"
				+ "col_who_mother_detail=?,col_who_sibling_detail=?,col_who_other_detail=?,col_type=?,"
				+ "col_type_other_detail=?,col_index=?,col_agreement=?, col_performed_by=?, col_concentration=?,col_260_280=?,col_260_230=?,col_location=?,"
				+ "col_comment=?,req_date=?,req_location=?,req_who=?,req_who_other_detail=?,req_distribution=?,req_order_name=?,"
				+ "req_type=?,req_type_other_detail=?,req_vial=?,req_comment=?,iid=?,idate=now() where dna_no='" + dna_no + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getCol_date());
			pstmt.setString(3, i.getCol_who());
			pstmt.setString(4, i.getCol_who_patient_detail());
			pstmt.setString(5, i.getCol_who_father_detail());
			pstmt.setString(6, i.getCol_who_mother_detail());
			pstmt.setString(7, i.getCol_who_sibling_detail());
			pstmt.setString(8, i.getCol_who_other_detail());
			pstmt.setString(9, i.getCol_type());
			pstmt.setString(10, i.getCol_type_other_detail());
			pstmt.setString(11, i.getCol_index());
			pstmt.setString(12, i.getCol_agreement());
			pstmt.setString(13, i.getCol_performed_by());
			pstmt.setString(14, i.getCol_concentration());
			pstmt.setString(15, i.getCol_260_280());
			pstmt.setString(16, i.getCol_260_230());
			pstmt.setString(17, i.getCol_location());
			pstmt.setString(18, i.getCol_comment());
			pstmt.setString(19, i.getReq_date());
			pstmt.setString(20, i.getReq_location());
			pstmt.setString(21, i.getReq_who());
			pstmt.setString(22, i.getReq_who_other_detail());
			pstmt.setString(23, i.getReq_distribution());
			pstmt.setString(24, i.getReq_order_name());
			pstmt.setString(25, i.getReq_type());
			pstmt.setString(26, i.getReq_type_other_detail());
			pstmt.setString(27, i.getReq_vial());
			pstmt.setString(28, i.getReq_comment());
			pstmt.setString(29, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Sample DNA Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	
	

	/* Sample Bank 2 - Tissue */
	public int insertTissue(SampleTissueDTO i) {
/*
		String sql = "insert into spn.sample_tissue(chart_no,bio_date,bio_type,bio_type_other_detail,"
				+ "bio_index_1,bio_location_1,bio_cell,bio_index_2,bio_phase,bio_storage,bio_location_2,"
				+ "bio_comment,req_date,req_location,req_distribution,req_order_name,req_type,req_type_other_detail,"
				+ "req_vial,req_comment,iid,idate) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";*/
		String sql = "insert into spn.sample_tissue(chart_no,bio_date,bio_type_myoblast, bio_type_fibroblast,"
				+ "bio_type_u_epithelial, bio_type_u_ipsc, bio_type_other, bio_type_other_detail,"
				+ "bio_index_1,bio_storage, bio_comment,req_date,req_location,req_order_name,"
				+ "req_vial,req_comment,iid,idate) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getBio_date());
			//pstmt.setString(3, i.getBio_type());
			pstmt.setString(3, i.getBio_type_myoblast());
			pstmt.setString(4, i.getBio_type_fibroblast());
			pstmt.setString(5, i.getBio_type_u_epithelial());
			pstmt.setString(6, i.getBio_type_u_ipsc());
			pstmt.setString(7, i.getBio_type_other());
			pstmt.setString(8, i.getBio_type_other_detail());
			pstmt.setString(9, i.getBio_index_1());
			//pstmt.setString(6, i.getBio_location_1());
			//pstmt.setString(7, i.getBio_cell());
			//pstmt.setString(8, i.getBio_index_2());
			//pstmt.setString(9, i.getBio_phase());
			pstmt.setString(10, i.getBio_storage());
			//pstmt.setString(11, i.getBio_location_2());
			pstmt.setString(11, i.getBio_comment());
			pstmt.setString(12, i.getReq_date());
			pstmt.setString(13, i.getReq_location());
			//pstmt.setString(15, i.getReq_distribution());
			pstmt.setString(14, i.getReq_order_name());
			//pstmt.setString(17, i.getReq_type());
			//pstmt.setString(18, i.getReq_type_other_detail());
			pstmt.setString(15, i.getReq_vial());
			pstmt.setString(16, i.getReq_comment());
			pstmt.setString(17, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Sample Tissue insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}

	public List<SampleTissueDTO> selectTissue(String searchVal) {

		String sql = "select * from spn.sample_tissue where chart_no ='"
				+ searchVal + "'order by tis_no desc";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<SampleTissueDTO> list = new ArrayList<SampleTissueDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				SampleTissueDTO i = new SampleTissueDTO();

				i.setTis_no(rs.getString("tis_no"));
				i.setBio_date(rs.getString("bio_date"));
				//i.setBio_type(rs.getString("bio_type"));
				i.setBio_type_myoblast(rs.getString("bio_type_myoblast"));
				i.setBio_type_fibroblast(rs.getString("bio_type_fibroblast"));
				i.setBio_type_u_epithelial(rs.getString("bio_type_u_epithelial"));
				i.setBio_type_u_ipsc(rs.getString("bio_type_u_ipsc"));
				i.setBio_type_other(rs.getString("bio_type_other"));
				i.setBio_type_other_detail(rs.getString("bio_type_other_detail"));
				i.setBio_index_1(rs.getString("bio_index_1"));
				//i.setBio_location_1(rs.getString("bio_location_1"));
				//i.setBio_cell(rs.getString("bio_cell"));
				//i.setBio_index_2(rs.getString("bio_index_2"));
				//i.setBio_phase(rs.getString("bio_phase"));
				i.setBio_storage(rs.getString("bio_storage"));
				//i.setBio_location_2(rs.getString("bio_location_2"));
				i.setBio_comment(rs.getString("bio_comment"));
				i.setReq_date(rs.getString("req_date"));
				i.setReq_location(rs.getString("req_location"));
				//i.setReq_distribution(rs.getString("req_distribution"));
				i.setReq_order_name(rs.getString("req_order_name"));
				//i.setReq_type(rs.getString("req_type"));
				//i.setReq_type_other_detail(rs.getString("req_type_other_detail"));
				i.setReq_vial(rs.getString("req_vial"));
				i.setReq_comment(rs.getString("req_comment"));
				i.setIdate(rs.getString("idate"));
				i.setIid(rs.getString("iid"));

				list.add(i);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}
	
	public int updateTissue(SampleTissueDTO i) {

		String tis_no = i.getTis_no();
		
		System.out.println("update tis_no : "+tis_no);
		String sql = "update spn.sample_tissue set chart_no=?,bio_date=?,bio_type_myoblast=?,"
				+ "bio_type_fibroblast=?, bio_type_u_epithelial=?, bio_type_u_ipsc=?, bio_type_other=?,"
				+ "bio_type_other_detail=?, bio_index_1=?,bio_storage=?, bio_comment=?,req_date=?,"
				+ "req_location=?,req_order_name=?,"
				+ "req_vial=?,req_comment=?,iid=?,idate=now() where tis_no='" + tis_no + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getBio_date());
			//pstmt.setString(3, i.getBio_type());
			System.out.println("MYOBLAST : "+i.getBio_type_myoblast());
			System.out.println("FIBROBLAST : "+i.getBio_type_fibroblast());
			pstmt.setString(3, i.getBio_type_myoblast());
			pstmt.setString(4, i.getBio_type_fibroblast());
			pstmt.setString(5, i.getBio_type_u_epithelial());
			pstmt.setString(6, i.getBio_type_u_ipsc());
			pstmt.setString(7, i.getBio_type_other());
			pstmt.setString(8, i.getBio_type_other_detail());
			pstmt.setString(9, i.getBio_index_1());
			//pstmt.setString(6, i.getBio_location_1());
			//pstmt.setString(7, i.getBio_cell());
			//pstmt.setString(8, i.getBio_index_2());
			//pstmt.setString(9, i.getBio_phase());
			pstmt.setString(10, i.getBio_storage());
			//pstmt.setString(11, i.getBio_location_2());
			pstmt.setString(11, i.getBio_comment());
			pstmt.setString(12, i.getReq_date());
			pstmt.setString(13, i.getReq_location());
			//pstmt.setString(15, i.getReq_distribution());
			pstmt.setString(14, i.getReq_order_name());
			//pstmt.setString(17, i.getReq_type());
			//pstmt.setString(18, i.getReq_type_other_detail());
			pstmt.setString(15, i.getReq_vial());
			pstmt.setString(16, i.getReq_comment());
			pstmt.setString(17, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Sample Tissue Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	
	
	

	/* Sample Bank 3 - Cell Free DNA */
	public int insertCfdna(SampleCfdnaDTO i) {

		String sql = "insert into spn.sample_cfdna(chart_no,col_date,col_location,col_who,col_who_patient_detail, col_who_father_detail,"
				+ "col_who_mother_detail, col_who_sibling_detail, col_who_other_detail,col_index,col_wk,"
				+ "col_plasma_volume,col_cf_no,col_concentration_1,col_concentration_2,col_260_280_1,col_260_280_2,"
				+ "col_260_230_1,col_260_230_2,col_bioanalyzer_size_1,col_bioanalyzer_size_2,col_bioanalyzer_concentation_1,"
				+ "col_bioanalyzer_concentation_2,col_comment,req_date,req_location,req_who,req_who_other_detail,req_distribution,"
				+ "req_order_name,req_vial,req_comment,iid,idate) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getCol_date());
			pstmt.setString(3, i.getCol_location());
			pstmt.setString(4, i.getCol_who());
			pstmt.setString(5, i.getCol_who_patient_detail());
			pstmt.setString(6, i.getCol_who_father_detail());
			pstmt.setString(7, i.getCol_who_mother_detail());
			pstmt.setString(8, i.getCol_who_sibling_detail());
			pstmt.setString(9, i.getCol_who_other_detail());
			pstmt.setString(10, i.getCol_index());
			pstmt.setString(11, i.getCol_wk());
			pstmt.setString(12, i.getCol_plasma_volume());
			pstmt.setString(13, i.getCol_cf_no());
			pstmt.setString(14, i.getCol_concentration_1());
			pstmt.setString(15, i.getCol_concentration_2());
			pstmt.setString(16, i.getCol_260_280_1());
			pstmt.setString(17, i.getCol_260_280_2());
			pstmt.setString(18, i.getCol_260_230_1());
			pstmt.setString(19, i.getCol_260_230_2());
			pstmt.setString(20, i.getCol_bioanalyzer_size_1());
			pstmt.setString(21, i.getCol_bioanalyzer_size_2());
			pstmt.setString(22, i.getCol_bioanalyzer_concentation_1());
			pstmt.setString(23, i.getCol_bioanalyzer_concentation_2());
			pstmt.setString(24, i.getCol_comment());
			pstmt.setString(25, i.getReq_date());
			pstmt.setString(26, i.getReq_location());
			pstmt.setString(27, i.getReq_who());
			pstmt.setString(28, i.getReq_who_other_detail());
			pstmt.setString(29, i.getReq_distribution());
			pstmt.setString(30, i.getReq_order_name());
			pstmt.setString(31, i.getReq_vial());
			pstmt.setString(32, i.getReq_comment());
			pstmt.setString(33, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Sample Cell Free DNA insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}

	public List<SampleCfdnaDTO> selectCfdna(String searchVal) {

		String sql = "select * from spn.sample_cfdna where chart_no ='"
				+ searchVal + "' order by cfd_no"; // desc ����
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<SampleCfdnaDTO> list = new ArrayList<SampleCfdnaDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				SampleCfdnaDTO i = new SampleCfdnaDTO();

				i.setCfd_no(rs.getString("cfd_no"));
				i.setCol_date(rs.getString("col_date"));
				i.setCol_location(rs.getString("col_location"));
				i.setCol_who(rs.getString("col_who"));
				i.setCol_who_patient_detail(rs.getString("col_who_patient_detail"));
				i.setCol_who_father_detail(rs.getString("col_who_father_detail"));
				i.setCol_who_mother_detail(rs.getString("col_who_mother_detail"));
				i.setCol_who_sibling_detail(rs.getString("col_who_sibling_detail"));
				i.setCol_who_other_detail(rs.getString("col_who_other_detail"));
				i.setCol_index(rs.getString("col_index"));
				i.setCol_wk(rs.getString("col_wk"));
				i.setCol_plasma_volume(rs.getString("col_plasma_volume"));
				i.setCol_cf_no(rs.getString("col_cf_no"));
				i.setCol_concentration_1(rs.getString("col_concentration_1"));
				i.setCol_concentration_2(rs.getString("col_concentration_2"));
				i.setCol_260_280_1(rs.getString("col_260_280_1"));
				i.setCol_260_280_2(rs.getString("col_260_280_2"));
				i.setCol_260_230_1(rs.getString("col_260_230_1"));
				i.setCol_260_230_2(rs.getString("col_260_230_2"));
				i.setCol_bioanalyzer_size_1(rs
						.getString("col_bioanalyzer_size_1"));
				i.setCol_bioanalyzer_size_2(rs
						.getString("col_bioanalyzer_size_2"));
				i.setCol_bioanalyzer_concentation_1(rs
						.getString("col_bioanalyzer_concentation_1"));
				i.setCol_bioanalyzer_concentation_2(rs
						.getString("col_bioanalyzer_concentation_2"));
				i.setCol_comment(rs.getString("col_comment"));
				i.setReq_date(rs.getString("req_date"));
				i.setReq_location(rs.getString("req_location"));
				i.setReq_who(rs.getString("req_who"));
				i.setReq_who_other_detail(rs.getString("req_who_other_detail"));
				i.setReq_distribution(rs.getString("req_distribution"));
				i.setReq_order_name(rs.getString("req_order_name"));
				i.setReq_vial(rs.getString("req_vial"));
				i.setReq_comment(rs.getString("req_comment"));
				i.setIdate(rs.getString("idate"));
				i.setIid(rs.getString("iid"));

				list.add(i);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}
	
	public int updateCfdna(SampleCfdnaDTO i) {

		String cfd_no = i.getCfd_no();

		String sql = "update spn.sample_cfdna set chart_no=?,col_date=?,col_location=?,col_who=?,col_who_patient_detail=?, col_who_father_detail=?,"
				+ "col_who_mother_detail=?, col_who_sibling_detail=?, col_who_other_detail=?,col_index=?,col_wk=?,"
				+ "col_plasma_volume=?,col_cf_no=?,col_concentration_1=?,col_concentration_2=?,col_260_280_1=?,col_260_280_2=?,"
				+ "col_260_230_1=?,col_260_230_2=?,col_bioanalyzer_size_1=?,col_bioanalyzer_size_2=?,col_bioanalyzer_concentation_1=?,"
				+ "col_bioanalyzer_concentation_2=?,col_comment=?,req_date=?,req_location=?,req_who=?,req_who_other_detail=?,req_distribution=?,"
				+ "req_order_name=?,req_vial=?,req_comment=?,iid=?,idate=now() where cfd_no='" + cfd_no + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getCol_date());
			pstmt.setString(3, i.getCol_location());
			pstmt.setString(4, i.getCol_who());
			pstmt.setString(5, i.getCol_who_patient_detail());
			pstmt.setString(6, i.getCol_who_father_detail());
			pstmt.setString(7, i.getCol_who_mother_detail());
			pstmt.setString(8, i.getCol_who_sibling_detail());
			pstmt.setString(9, i.getCol_who_other_detail());
			pstmt.setString(10, i.getCol_index());
			pstmt.setString(11, i.getCol_wk());
			pstmt.setString(12, i.getCol_plasma_volume());
			pstmt.setString(13, i.getCol_cf_no());
			pstmt.setString(14, i.getCol_concentration_1());
			pstmt.setString(15, i.getCol_concentration_2());
			pstmt.setString(16, i.getCol_260_280_1());
			pstmt.setString(17, i.getCol_260_280_2());
			pstmt.setString(18, i.getCol_260_230_1());
			pstmt.setString(19, i.getCol_260_230_2());
			pstmt.setString(20, i.getCol_bioanalyzer_size_1());
			pstmt.setString(21, i.getCol_bioanalyzer_size_2());
			pstmt.setString(22, i.getCol_bioanalyzer_concentation_1());
			pstmt.setString(23, i.getCol_bioanalyzer_concentation_2());
			pstmt.setString(24, i.getCol_comment());
			pstmt.setString(25, i.getReq_date());
			pstmt.setString(26, i.getReq_location());
			pstmt.setString(27, i.getReq_who());
			pstmt.setString(28, i.getReq_who_other_detail());
			pstmt.setString(29, i.getReq_distribution());
			pstmt.setString(30, i.getReq_order_name());
			pstmt.setString(31, i.getReq_vial());
			pstmt.setString(32, i.getReq_comment());
			pstmt.setString(33, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Sample Cell Free DNA Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	

	/* Sample Bank 4 - Serum & CSF */
	public int insertSerum(SampleSerumDTO i) {

		String sql = "insert into spn.sample_serum(chart_no,experiment_serum,experiment_csf,col_date,dis_date,col_index,no,storage,"
				+ "aquaporin_4_ab,autoimmune_encephalitis,order_other,order_other_detail,experiment_place,comment_1,location,comment_2,"
				+ "req_date,req_location,req_order_name,req_vial,req_comment,iid,idate) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getExperiment_serum());
			pstmt.setString(3, i.getExperiment_csf());
			pstmt.setString(4, i.getCol_date());
			pstmt.setString(5, i.getDis_date());
			pstmt.setString(6, i.getCol_index());
			pstmt.setString(7, i.getNo());
			pstmt.setString(8, i.getStorage());
			pstmt.setString(9, i.getAquaporin_4_ab());
			pstmt.setString(10, i.getAutoimmune_encephalitis());
			pstmt.setString(11, i.getOrder_other());
			pstmt.setString(12, i.getOrder_other_detail());
			pstmt.setString(13, i.getExperiment_place());
			pstmt.setString(14, i.getComment_1());
			pstmt.setString(15, i.getLocation());
			pstmt.setString(16, i.getComment_2());
			pstmt.setString(17, i.getReq_date());
			pstmt.setString(18, i.getReq_location());
			pstmt.setString(19, i.getReq_location());
			pstmt.setString(20, i.getReq_vial());
			pstmt.setString(21, i.getReq_comment());
			pstmt.setString(22, i.getIid());
			
			result = pstmt.executeUpdate();

			System.out.println("Sample Serum & CSF insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}

	public List<SampleSerumDTO> selectSerum(String searchVal) {

		String sql = "select * from spn.sample_serum where chart_no ='"
				+ searchVal + "' order by sc_no desc";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<SampleSerumDTO> list = new ArrayList<SampleSerumDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				SampleSerumDTO i = new SampleSerumDTO();

				i.setSc_no(rs.getString("sc_no"));
				i.setExperiment_serum(rs.getString("experiment_serum"));
				i.setExperiment_csf(rs.getString("experiment_csf"));
				i.setCol_date(rs.getString("col_date"));
				i.setDis_date(rs.getString("dis_date"));
				i.setDis_date(rs.getString("dis_date"));
				i.setCol_index(rs.getString("col_index"));
				i.setNo(rs.getString("no"));
				i.setStorage(rs.getString("storage"));
				i.setAquaporin_4_ab(rs.getString("aquaporin_4_ab"));
				i.setAutoimmune_encephalitis(rs
						.getString("autoimmune_encephalitis"));
				i.setOrder_other(rs.getString("order_other"));
				i.setOrder_other_detail(rs.getString("order_other_detail"));
				i.setExperiment_place(rs.getString("experiment_place"));
				i.setComment_1(rs.getString("comment_1"));
				i.setLocation(rs.getString("location"));
				i.setComment_2(rs.getString("comment_2"));
				i.setReq_date(rs.getString("req_date"));
				i.setReq_location(rs.getString("req_location"));
				i.setReq_order_name(rs.getString("req_order_name"));
				i.setReq_vial(rs.getString("req_vial"));
				i.setReq_comment(rs.getString("req_comment"));
				i.setIdate(rs.getString("idate"));
				i.setIid(rs.getString("iid"));

				list.add(i);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}
	
	public int updateSerum(SampleSerumDTO i) {

		String sc_no = i.getSc_no();

		String sql = "update spn.sample_serum set chart_no=?,experiment_serum=?,experiment_csf=?,col_date=?,dis_date=?,col_index=?,no=?,storage=?,"
				+ "aquaporin_4_ab=?,autoimmune_encephalitis=?,order_other=?,order_other_detail=?,experiment_place=?,comment_1=?,location=?,comment_2=?,"
				+ "req_date=?,req_location=?,req_order_name=?,req_vial=?,req_comment=?,iid=?,idate=now()"
				+ " where sc_no='" + sc_no + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getExperiment_serum());
			pstmt.setString(3, i.getExperiment_csf());
			pstmt.setString(4, i.getCol_date());
			pstmt.setString(5, i.getDis_date());
			pstmt.setString(6, i.getCol_index());
			pstmt.setString(7, i.getNo());
			pstmt.setString(8, i.getStorage());
			pstmt.setString(9, i.getAquaporin_4_ab());
			pstmt.setString(10, i.getAutoimmune_encephalitis());
			pstmt.setString(11, i.getOrder_other());
			pstmt.setString(12, i.getOrder_other_detail());
			pstmt.setString(13, i.getExperiment_place());
			pstmt.setString(14, i.getComment_1());
			pstmt.setString(15, i.getLocation());
			pstmt.setString(16, i.getComment_2());
			pstmt.setString(17, i.getReq_date());
			pstmt.setString(18, i.getReq_location());
			pstmt.setString(19, i.getReq_location());
			pstmt.setString(20, i.getReq_vial());
			pstmt.setString(21, i.getReq_comment());
			pstmt.setString(22, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Sample Serum & CSF Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	
	

	/* Sample Bank 5 - Urine */
	public int insertUrine(SampleUrineDTO i) {

		String sql = "insert into spn.sample_urine(chart_no,urine,urine_col_date,urine_index,urine_volume,urine_storage,"
				+ "urine_location,urine_comment,uipsc,uipsc_col_date,uipsc_index,uipsc_storage,uipsc_location,uipsc_comment,"
				+ "req_date,req_distribution,req_order_name,req_vial,req_comment,iid,idate) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getUrine());
			pstmt.setString(3, i.getUrine_col_date());
			pstmt.setString(4, i.getUrine_index());
			pstmt.setString(5, i.getUrine_volume());
			pstmt.setString(6, i.getUrine_storage());
			pstmt.setString(7, i.getUrine_location());
			pstmt.setString(8, i.getUrine_comment());
			pstmt.setString(9, i.getUipsc());
			pstmt.setString(10, i.getUipsc_col_date());
			pstmt.setString(11, i.getUipsc_index());
			pstmt.setString(12, i.getUipsc_storage());
			pstmt.setString(13, i.getUipsc_location());
			pstmt.setString(14, i.getUipsc_comment());
			pstmt.setString(15, i.getReq_date());
			pstmt.setString(16, i.getReq_distribution());
			pstmt.setString(17, i.getReq_order_name());
			pstmt.setString(18, i.getReq_vial());
			pstmt.setString(19, i.getReq_comment());
			pstmt.setString(20, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Sample Urine insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}

	public List<SampleUrineDTO> selectUrine(String searchVal) {

		String sql = "select * from spn.sample_urine where chart_no ='"
				+ searchVal + "' order by uri_no desc";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<SampleUrineDTO> list = new ArrayList<SampleUrineDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				SampleUrineDTO i = new SampleUrineDTO();

				i.setUri_no(rs.getString("uri_no"));
				i.setUrine(rs.getString("urine"));
				i.setUrine_col_date(rs.getString("urine_col_date"));
				i.setUrine_index(rs.getString("urine_index"));
				i.setUrine_volume(rs.getString("urine_volume"));
				i.setUrine_storage(rs.getString("urine_storage"));
				i.setUrine_location(rs.getString("urine_location"));
				i.setUrine_comment(rs.getString("urine_comment"));
				i.setUipsc(rs.getString("uipsc"));
				i.setUipsc_col_date(rs.getString("uipsc_col_date"));
				i.setUipsc_index(rs.getString("uipsc_index"));
				i.setUipsc_storage(rs.getString("uipsc_storage"));
				i.setUipsc_location(rs.getString("uipsc_location"));
				i.setUipsc_comment(rs.getString("uipsc_comment"));
				i.setReq_date(rs.getString("req_date"));
				i.setReq_distribution(rs.getString("req_distribution"));
				i.setReq_order_name(rs.getString("req_order_name"));
				i.setReq_vial(rs.getString("req_vial"));
				i.setReq_comment(rs.getString("req_comment"));
				i.setIdate(rs.getString("idate"));
				i.setIid(rs.getString("iid"));

				list.add(i);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}
	
	public int updateUrine(SampleUrineDTO i) {

		String uri_no = i.getUri_no();

		String sql = "update spn.sample_urine set chart_no=?,urine=?,urine_col_date=?,urine_index=?,urine_volume=?,urine_storage=?,"
				+ "urine_location=?,urine_comment=?,uipsc=?,uipsc_col_date=?,uipsc_index=?,uipsc_storage=?,uipsc_location=?,uipsc_comment=?,"
				+ "req_date=?,req_distribution=?,req_order_name=?,req_vial=?,req_comment=?,iid=?,idate=now()"
				+ " where uri_no='" + uri_no + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getUrine());
			pstmt.setString(3, i.getUrine_col_date());
			pstmt.setString(4, i.getUrine_index());
			pstmt.setString(5, i.getUrine_volume());
			pstmt.setString(6, i.getUrine_storage());
			pstmt.setString(7, i.getUrine_location());
			pstmt.setString(8, i.getUrine_comment());
			pstmt.setString(9, i.getUipsc());
			pstmt.setString(10, i.getUipsc_col_date());
			pstmt.setString(11, i.getUipsc_index());
			pstmt.setString(12, i.getUipsc_storage());
			pstmt.setString(13, i.getUipsc_location());
			pstmt.setString(14, i.getUipsc_comment());
			pstmt.setString(15, i.getReq_date());
			pstmt.setString(16, i.getReq_distribution());
			pstmt.setString(17, i.getReq_order_name());
			pstmt.setString(18, i.getReq_vial());
			pstmt.setString(19, i.getReq_comment());
			pstmt.setString(20, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Sample Urine Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	

	/* Sample Bank 6 - RNA */
	public int insertRna(SampleRnaDTO i) {

		String sql = "insert into spn.sample_rna(chart_no,col_date,col_type,col_type_other_detail,col_index,"
				+ "col_concentration,col_260_280,col_260_230,col_location,col_comment,req_date,req_location,req_distribution,"
				+ "req_distribution_other_detail,req_order_name,req_vial,req_comment,iid,idate) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getCol_date());
			pstmt.setString(3, i.getCol_type());
			pstmt.setString(4, i.getCol_type_other_detail());
			pstmt.setString(5, i.getCol_index());
			pstmt.setString(6, i.getCol_concentration());
			pstmt.setString(7, i.getCol_260_280());
			pstmt.setString(8, i.getCol_260_230());
			pstmt.setString(9, i.getCol_location());
			pstmt.setString(10, i.getCol_comment());
			pstmt.setString(11, i.getReq_date());
			pstmt.setString(12, i.getReq_location());
			pstmt.setString(13, i.getReq_distribution());
			pstmt.setString(14, i.getReq_distribution_other_detail());
			pstmt.setString(15, i.getReq_order_name());
			pstmt.setString(16, i.getReq_vial());
			pstmt.setString(17, i.getReq_comment());
			pstmt.setString(18, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Sample RNA insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	
	public List<SampleRnaDTO> selectRna(String searchVal) {

		String sql = "select * from spn.sample_rna where chart_no ='"
				+ searchVal + "' order by rna_no desc";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<SampleRnaDTO> list = new ArrayList<SampleRnaDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				SampleRnaDTO i = new SampleRnaDTO();

				i.setRna_no(rs.getString("rna_no"));
				i.setCol_date(rs.getString("col_date"));
				i.setCol_type(rs.getString("col_type"));
				i.setCol_type_other_detail(rs.getString("col_type_other_detail"));
				i.setCol_index(rs.getString("col_index"));
				i.setCol_concentration(rs.getString("col_concentration"));
				i.setCol_260_280(rs.getString("col_260_280"));
				i.setCol_260_230(rs.getString("col_260_230"));
				i.setCol_location(rs.getString("col_location"));
				i.setCol_comment(rs.getString("col_comment"));
				i.setReq_date(rs.getString("req_date"));
				i.setReq_location(rs.getString("req_location"));
				i.setReq_distribution(rs.getString("req_distribution"));
				i.setReq_distribution_other_detail(rs.getString("req_distribution_other_detail"));
				i.setReq_order_name(rs.getString("req_order_name"));
				i.setReq_vial(rs.getString("req_vial"));
				i.setReq_comment(rs.getString("req_comment"));
				i.setIdate(rs.getString("idate"));
				i.setIid(rs.getString("iid"));

				list.add(i);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}
	
	public int updateRna(SampleRnaDTO i) {

		String rna_no = i.getRna_no();

		String sql = "update spn.sample_rna set chart_no=?,col_date=?,col_type=?,col_type_other_detail=?,col_index=?,"
				+ "col_concentration=?,col_260_280=?,col_260_230=?,col_location=?,col_comment=?,req_date=?,req_location=?,req_distribution=?,"
				+ "req_distribution_other_detail=?,req_order_name=?,req_vial=?,req_comment=?,iid=?,idate=now()"
				+ " where rna_no='" + rna_no + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getCol_date());
			pstmt.setString(3, i.getCol_type());
			pstmt.setString(4, i.getCol_type_other_detail());
			pstmt.setString(5, i.getCol_index());
			pstmt.setString(6, i.getCol_concentration());
			pstmt.setString(7, i.getCol_260_280());
			pstmt.setString(8, i.getCol_260_230());
			pstmt.setString(9, i.getCol_location());
			pstmt.setString(10, i.getCol_comment());
			pstmt.setString(11, i.getReq_date());
			pstmt.setString(12, i.getReq_location());
			pstmt.setString(13, i.getReq_distribution());
			pstmt.setString(14, i.getReq_distribution_other_detail());
			pstmt.setString(15, i.getReq_order_name());
			pstmt.setString(16, i.getReq_vial());
			pstmt.setString(17, i.getReq_comment());
			pstmt.setString(18, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Sample RNA Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	
	/* Sample Bank 7 - Cell */
	public int insertCell(SampleCellDTO i) {

		String sql = "insert into spn.sample_cell(chart_no,col_date,cell_type_myoblast,cell_type_fibroblast,"
				+ "cell_type_u_epithelial,cell_type_u_ipsc,cell_type_other,cell_type_other_detail,cell_index,"
				+ "cell_storage,cell_phase,cell_comment,req_date,req_location,req_order_name,req_vial,req_comment,iid,idate) "
				+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now())";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getCol_date());
			pstmt.setString(3, i.getCell_type_myoblast());
			pstmt.setString(4, i.getCell_type_fibroblast());
			pstmt.setString(5, i.getCell_type_u_epithelial());
			pstmt.setString(6, i.getCell_type_u_ipsc());
			pstmt.setString(7, i.getCell_type_other());
			pstmt.setString(8, i.getCell_type_other_detail());
			pstmt.setString(9, i.getCell_index());
			pstmt.setString(10, i.getCell_storage());
			pstmt.setString(11, i.getCell_phase());
			pstmt.setString(12, i.getCell_comment());
			pstmt.setString(13, i.getReq_date());
			pstmt.setString(14, i.getReq_location());
			pstmt.setString(15, i.getReq_order_name());
			pstmt.setString(16, i.getReq_vial());
			pstmt.setString(17, i.getReq_comment());
			pstmt.setString(18, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Sample Cell insert success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}

	public List<SampleCellDTO> selectCell(String searchVal) {

		String sql = "select * from spn.sample_cell where chart_no ='"
				+ searchVal + "'order by ce_no desc";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<SampleCellDTO> list = new ArrayList<SampleCellDTO>();

		try {
			pstmt = con.prepareStatement(sql);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				SampleCellDTO i = new SampleCellDTO();

				i.setCe_no(rs.getString("ce_no"));
				i.setCol_date(rs.getString("col_date"));
				i.setCell_type_myoblast(rs.getString("cell_type_myoblast"));
				i.setCell_type_fibroblast(rs.getString("cell_type_fibroblast"));
				i.setCell_type_u_epithelial(rs.getString("cell_type_u_epithelial"));
				i.setCell_type_u_ipsc(rs.getString("cell_type_u_ipsc"));
				i.setCell_type_other(rs.getString("cell_type_other"));
				i.setCell_type_other_detail(rs.getString("cell_type_other_detail"));
				i.setCell_index(rs.getString("cell_index"));
				i.setCell_storage(rs.getString("cell_storage"));
				i.setCell_phase(rs.getString("cell_phase"));
				i.setCell_comment(rs.getString("cell_comment"));
				i.setReq_date(rs.getString("req_date"));
				i.setReq_location(rs.getString("req_location"));
				i.setReq_order_name(rs.getString("req_order_name"));
				i.setReq_vial(rs.getString("req_vial"));
				i.setReq_comment(rs.getString("req_comment"));
				i.setIid(rs.getString("iid"));
				i.setIdate(rs.getString("idate"));
				
				list.add(i);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}
		return list;
	}
	
	public int updateCell(SampleCellDTO i) {

		String ce_no = i.getCe_no();
		
		System.out.println("update ce_no : "+ce_no);
		String sql = "update spn.sample_cell set chart_no=?,col_date=?,cell_type_myoblast=?,"
				+ "cell_type_fibroblast=?, cell_type_u_epithelial=?, cell_type_u_ipsc=?, cell_type_other=?,"
				+ "cell_type_other_detail=?, cell_index=?,cell_storage=?,cell_phase=?,"
				+ "cell_comment=?, req_date=?, req_location=?, req_order_name=?, req_vial=?, req_comment=?, iid=?,"
				+ "idate=now() where ce_no='" + ce_no + "'";
		Connection con = DBManager.getConnection();
		PreparedStatement pstmt = null;
		int result = 0;
		System.out.println(result);

		try {

			pstmt = con.prepareStatement(sql);

			pstmt.setString(1, i.getChart_no());
			pstmt.setString(2, i.getCol_date());
			pstmt.setString(3, i.getCell_type_myoblast());
			pstmt.setString(4, i.getCell_type_fibroblast());
			pstmt.setString(5, i.getCell_type_u_epithelial());
			pstmt.setString(6, i.getCell_type_u_ipsc());
			pstmt.setString(7, i.getCell_type_other());
			pstmt.setString(8, i.getCell_type_other_detail());
			pstmt.setString(9, i.getCell_index());
			pstmt.setString(10, i.getCell_storage());
			pstmt.setString(11, i.getCell_phase());
			pstmt.setString(12, i.getCell_comment());
			pstmt.setString(13, i.getReq_date());
			pstmt.setString(14, i.getReq_location());
			pstmt.setString(15, i.getReq_order_name());
			pstmt.setString(16, i.getReq_vial());
			pstmt.setString(17, i.getReq_comment());
			pstmt.setString(18, i.getIid());

			result = pstmt.executeUpdate();

			System.out.println("Sample Cell Update success");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			DBManager.close(con, pstmt);
		}

		return result;
	}
	

}
